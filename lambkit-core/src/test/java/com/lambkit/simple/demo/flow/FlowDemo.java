package com.lambkit.simple.demo.flow;

import com.lambkit.core.*;
import com.lambkit.core.flow.FlowContext;
import com.lambkit.core.flow.FlowInstance;
import com.lambkit.core.flow.FlowInstanceObject;
import com.lambkit.core.flow.FlowWorkCenter;
import com.lambkit.core.flow.FlowInstanceJsonBuilder;
import com.lambkit.core.flow.FlowJsonBuilder;

/**
 * @author yangyong(孤竹行)
 */
public class FlowDemo {
    public static void main(String[] args) throws InterruptedException {
        LambkitApp app = Lambkit.start(DefaultLambkitApp.class, FlowDemo.class, args);
        if(app == null) {
            throw new RuntimeException("app start error");
        }
        ////////////////////////////////////////////////////////
        FlowWorkCenter flowWorkCenter = new FlowWorkCenter(app.context(), 100);
        FlowJsonBuilder flowJsonBuilder = new FlowJsonBuilder("flow/test.json");

        FlowInstanceJsonBuilder instanceJsonBuilder = new FlowInstanceJsonBuilder("flow/test_instance1.json", flowJsonBuilder.getFlow());
        FlowInstanceObject instanceObject = instanceJsonBuilder.getInstance();
        FlowContext flowContext = new FlowContext(instanceObject);
        FlowInstance flowInstance = new FlowInstance(app.context(), flowContext);
        try {
            flowWorkCenter.startFlow(flowInstance);
        } catch (LifecycleException e) {
            throw new RuntimeException(e);
        }
        while (true) {
            if(flowContext.getCurrentState().equals(LifecycleState.STOPPED)) {
                break;
            }
            Thread.sleep(100);
        }
        ////////////////////////////////////////////////////////
        Lambkit.stop();
        System.exit(0);
    }
}
