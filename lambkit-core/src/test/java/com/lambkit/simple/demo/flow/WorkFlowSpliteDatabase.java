package com.lambkit.simple.demo.flow;

import com.lambkit.core.flow.FlowInstanceObject;
import com.lambkit.core.flow.FlowObject;
import com.lambkit.core.flow.workflow.IWorkFlowDatabase;

/**
 * @author yangyong(孤竹行)
 */
public class WorkFlowSpliteDatabase implements IWorkFlowDatabase {
    @Override
    public FlowObject getFlow() {
        return null;
    }

    @Override
    public void saveFlow() {

    }

    @Override
    public FlowInstanceObject getInstance() {
        return null;
    }

    @Override
    public void saveInstance() {

    }
}
