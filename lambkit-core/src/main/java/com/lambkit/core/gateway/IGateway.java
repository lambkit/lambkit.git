package com.lambkit.core.gateway;

import com.lambkit.core.http.IHttpContext;

/**
 * @author yangyong(孤竹行)
 */
public interface IGateway {
    void execute(String targetUri, IHttpContext httpContext);
}
