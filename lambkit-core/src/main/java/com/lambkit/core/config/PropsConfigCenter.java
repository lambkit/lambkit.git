package com.lambkit.core.config;

import cn.hutool.core.util.StrUtil;
import cn.hutool.setting.dialect.Props;
import com.lambkit.core.Lambkit;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class PropsConfigCenter implements ConfigCenter {

    private Props props;
    private String name = "props";

    public PropsConfigCenter(String name) {
        this.name = name;
        if(StrUtil.isBlank(this.name)) {
            this.name = "lambkit";
        }
        props = readProp(name + ".txt");
    }

    private Props readProp(String propName) {
        Props theProp = null;
        String rootPath = Lambkit.context().resourceService().getRootPath();
        //优先查找config下有没有properties
        String fileName = rootPath + File.separator + "config" + File.separator + propName;
        File file = new File(fileName);
        if(file.exists()) {
            theProp = new Props(file);
        } else {
            //优先查找root下有没有properties
            fileName = rootPath + File.separator + propName;
            if(file.exists()) {
                theProp = new Props(file);
            } else {
                theProp = new Props(propName);
            }
        }
        if(theProp==null) {
            theProp = new Props();
        }
        return theProp;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getValue(String key) {
        return props.getStr(key);
    }

    @Override
    public boolean containsKey(String key) {
        return props.containsKey(key);
    }

    @Override
    public List<String> getKeys(String prefix) {
        List<String> keyList = new ArrayList<String>();
        for (Map.Entry<Object, Object> entry : props.entrySet()) {
            String key = entry.getKey().toString();
            if (key.startsWith(prefix) && entry.getValue() != null) {
                keyList.add(key);
            }
        }
        return keyList;
    }

    @Override
    public boolean refresh() {
        props.autoLoad(true);
        return true;
    }

    @Override
    public void setValue(String name, String key, String value) {
        if(this.name.equals(name)) {
            props.setProperty(key, value);
        }
    }

    @Override
    public void removeValue(String key) {
        props.remove(key);
    }
}
