package com.lambkit.core;

import com.lambkit.core.pipeline.DefaultPipeLineFactory;
import com.lambkit.core.service.impl.DefaultBeanFactory;
import com.lambkit.core.service.impl.DefaultClassOperateFactory;
import com.lambkit.core.service.impl.DefaultResourceFactory;

public class DefaultLambkitApp extends LambkitApp {
    public DefaultLambkitApp(String name) {
        super(name);
        context().setBeanFactory(new DefaultBeanFactory());
        context().setClassOperateFactory(new DefaultClassOperateFactory());
        context().setPipeLineFactory(new DefaultPipeLineFactory());
        context().setResourceFactory(new DefaultResourceFactory());
    }
}
