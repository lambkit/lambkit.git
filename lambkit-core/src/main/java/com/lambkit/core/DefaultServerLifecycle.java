package com.lambkit.core;

import com.lambkit.core.pipeline.PipeLineContext;

/**
 * @author yangyong(孤竹行)
 */
public class DefaultServerLifecycle implements LambkitServerLifecycle {

    private LifecycleState state = LifecycleState.NEW;
    @Override
    public void init() {
        PipeLineContext pipeLineContext = new PipeLineContext();
        pipeLineContext.setAttr("args", Lambkit.context().getAttr("args"));
        Lambkit.context().getInitPipeLine().invoke(pipeLineContext);
        state = LifecycleState.INITIALIZED;
    }

    @Override
    public LifecycleState getCurrentState() {
        return state;
    }

    @Override
    public void start() {
        Lambkit.context().getStartPipeLine().invoke();
        state = LifecycleState.STARTED;
    }

    @Override
    public void stop() {
        Lambkit.context().getStopPipeLine().invoke();
        state = LifecycleState.STOPPED;
    }
}
