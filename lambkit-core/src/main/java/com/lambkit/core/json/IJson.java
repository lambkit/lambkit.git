package com.lambkit.core.json;

/**
 * @author yangyong(孤竹行)
 */
public interface IJson {

    public String toJson(Object object);

    public <T> T parse(String jsonString, Class<T> type);

}
