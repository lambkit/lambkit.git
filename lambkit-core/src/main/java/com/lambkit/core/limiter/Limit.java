package com.lambkit.core.limiter;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义限流注解
 * <p>说明:</p>
 * <li></li>
 *
 * @author yangyong
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Limit {
	/**
     * 一定时间内最多访问次数，默认50,每秒创建令牌个数，默认:10
     */
    int rate() default 50;

    /**
     * 给定的时间范围 单位(秒) 默认1,获取令牌等待超时时间 默认:500
     */
    int period() default 1;
    
    /**
     * 限流的类型
     */
    LimiterType limitType() default LimiterType.TOKEN_BUCKET;

    /**
     * 降级策略:默认快速失败
     */
    LimitFallbackStrategy fallbackStrategy() default LimitFallbackStrategy.FAIL_FAST;

    /**
     * 当降级策略为:回退 时回退处理接口实现名称
     */
    String fallbackImpl() default "";
}

