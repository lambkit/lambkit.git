package com.lambkit.core.pipeline;

public abstract class Valve extends BaseValve<Valve, PipeLineContext> {
    private Valve next;

    public void next(PipeLineContext ctx, boolean isHandled)  {
        if(!isHandled && next != null) {
            next.invoke(ctx, isHandled);
        }
    }

    @Override
    public void setNext(Valve valve) {
        next = valve;
    }

    @Override
    public Valve getNext() {
        return next;
    }
}
