package com.lambkit.core.pipeline;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author yangyong(孤竹行)
 */
public class PipeLineServiceImpl implements PipeLineService {

    private final Map<String, IPipeLine> piplines = new ConcurrentHashMap<>(5);

    @Override
    public IPipeLine getPipeLine(String route) {
        return piplines.get(route);
    }

    @Override
    public void putPipeline(String route, IPipeLine pipeLine) {
        piplines.put(route, pipeLine);
    }
}
