package com.lambkit.core.pipeline;

public abstract class BaseValve<T extends IValve, C extends IPipLineContext> implements IValve<T, C> {

    private T next;

    public void next(C ctx, boolean isHandled)  {
        if(!isHandled && next != null) {
            next.invoke(ctx, isHandled);
        }
    }

    @Override
    public void setNext(T valve) {
        next = valve;
    }

    @Override
    public T getNext() {
        return next;
    }
}
