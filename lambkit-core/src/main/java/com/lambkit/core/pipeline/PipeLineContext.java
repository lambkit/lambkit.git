package com.lambkit.core.pipeline;

import cn.hutool.core.util.IdUtil;
import com.lambkit.core.Attr;
import com.lambkit.core.LambkitResult;
import com.lambkit.core.LifecycleState;

import java.util.Map;

public class PipeLineContext implements IPipLineContext {
    private Attr attr = new Attr();

    private String id;

    private LambkitResult result;

    private LifecycleState currentState;

    private long startTime = 0; // 开始时间

    private long stopTime = 0; // 时间

    //////////////////////////////////////////////////////////////

    public PipeLineContext() {
        this.setId(IdUtil.simpleUUID());
        this.setCurrentState(LifecycleState.NEW);
        this.setResult(new LambkitResult());
        this.setStartTime(System.currentTimeMillis());
    }

    public boolean isReady() {
        return true;
    }

    public void percent(int percent, String msg) {
        getResult().percent(percent).message(msg);
        if(percent == 100) {
            getResult().code(1).setSuccess(true);
        }
    }

    public LambkitResult resultSuccess(Object data) {
        result = LambkitResult.success().data(data);
        return result;
    }

    public LambkitResult resultFail(String msg) {
        result = LambkitResult.fail().message(msg);
        return result;
    }

    public LambkitResult resultError(int errorCode, String msg) {
        result = LambkitResult.error(errorCode).message(msg);
        return result;
    }
    //////////////////////////////////////////////////////////////
    public LifecycleState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(LifecycleState currentState) {
        this.currentState = currentState;
    }

    public String getId() {
        return id;
    }

    public void setId(String instanceId) {
        this.id = instanceId;
    }

    public LambkitResult getResult() {
        return result;
    }

    public void setResult(LambkitResult result) {
        this.result = result;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getStopTime() {
        return stopTime;
    }

    public void setStopTime(long stopTime) {
        this.stopTime = stopTime;
    }

    @Override
    public Attr attr() {
        return attr;
    }

    @Override
    public <T> T getAttr(String name) {
        return attr != null ? attr.get(name) : null;
    }

    @Override
    public <T> T getAttr(String name, T defaultValue) {
        return attr != null ? attr.get(name, defaultValue) : null;
    }

    @Override
    public String getAttrToStr(String name) {
        return attr != null ? attr.getStr(name) : null;
    }

    @Override
    public Integer getAttrToInt(String name) {
        return attr != null ? attr.getInt(name) : null;
    }

    @Override
    public void setAttr(String name, Object value) {
        if(attr != null) {
            attr.set(name, value);
        }
    }

    @Override
    public void removeAttr(String name) {
        if(attr != null) {
            attr.remove(name);
        }
    }

    @Override
    public void setAttrs(Map<String, Object> attrMap) {
        if(attr != null) {
            attr.putAll(attrMap);
        }
    }

}
