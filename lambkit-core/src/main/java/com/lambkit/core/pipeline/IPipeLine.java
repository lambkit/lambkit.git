package com.lambkit.core.pipeline;

/**
 * 管道
 */
public interface IPipeLine<T extends IValve, C extends IPipLineContext> {
    void invoke(C context);

    /**
     * 设置后面阀门
     * @param valve
     */
    void addValve(T valve);

    T firstValve();
}
