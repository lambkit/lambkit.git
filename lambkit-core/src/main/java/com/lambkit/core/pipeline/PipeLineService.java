package com.lambkit.core.pipeline;

/**
 * 管道接口
 * @author yangyong(孤竹行)
 */
public interface PipeLineService {
    /**
     * 获取已注册的一个管道Pipline
     * @param route
     * @return
     */
    IPipeLine getPipeLine(String route);

    void putPipeline(String route, IPipeLine pipeLine);
}
