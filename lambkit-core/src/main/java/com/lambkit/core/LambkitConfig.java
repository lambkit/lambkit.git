/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.core;


import com.lambkit.core.Lambkit;
import com.lambkit.core.config.annotation.PropConfig;

@PropConfig(prefix = "lambkit")
public class LambkitConfig {

	private String name = "lambkit";
    private String version = Lambkit.version();
    private String mode = "dev";
    private String encryptKey = "xYb&KpO%4DP9tV6v";
    private boolean commondEnable = false;
    private String commondSecret;
    private boolean modelCacheEnable = false;
    //扫描的包地址
    private String packages;
	private String contextPath = "";
	/**
	 * 资源放置的方式，classpath会打包到jar中，webapp是指定路径的文件夹
	 */
	private String resourcePathType = "classpath";
	/**
	 * 静态资源文件夹名称
	 */
	private String resourcePath = "static";
	/**
	 * 模板资源文件夹名称
	 */
	private String templatePath = "templates";

	private String filter;// = "shiro,org.apache.shiro.web.servlet.ShiroFilter,/*";
    private String listener;

	private boolean lambkitUndertow = true;

	public boolean isDevMode() {
        return "dev".equals(mode);
    }

    @Override
    public String toString() {
        return "LambkitConfig {" +
                "version='" + version + '\'' +
                ", mode='" + mode + '\'' +
                '}';
    }
    
    public String getVersion() {
        return version;
    }
    
    public void setVersion(String version) {
		this.version = version;
	}

    public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPackages() {
		return packages;
	}

	public void setPackages(String packages) {
		this.packages = packages;
	}

	public String getEncryptKey() {
		return encryptKey;
	}

	public void setEncryptKey(String encryptKey) {
		this.encryptKey = encryptKey;
	}

	public boolean isModelCacheEnable() {
		return modelCacheEnable;
	}

	public void setModelCacheEnable(boolean modelCacheEnable) {
		this.modelCacheEnable = modelCacheEnable;
	}

	public boolean isCommondEnable() {
		return commondEnable;
	}

	public void setCommondEnable(boolean commondEnable) {
		this.commondEnable = commondEnable;
	}

	public String getCommondSecret() {
		return commondSecret;
	}

	public void setCommondSecret(String commondSecret) {
		this.commondSecret = commondSecret;
	}

	public String getContextPath() {
		return contextPath;
	}

	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public String getListener() {
		return listener;
	}

	public void setListener(String listener) {
		this.listener = listener;
	}

	public boolean isLambkitUndertow() {
		return lambkitUndertow;
	}

	public void setLambkitUndertow(boolean lambkitUndertow) {
		this.lambkitUndertow = lambkitUndertow;
	}

	public String getResourcePathType() {
		return resourcePathType;
	}

	public void setResourcePathType(String resourcePathType) {
		this.resourcePathType = resourcePathType;
	}

	public String getTemplatePath() {
		return templatePath;
	}

	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}
}
