package com.lambkit.core.service.impl;

import com.lambkit.core.service.ResourceFactory;
import com.lambkit.core.service.ResourceService;

/**
 * @author yangyong(孤竹行)
 */
public class DefaultResourceFactory implements ResourceFactory {

    private ResourceService resourceService = new ResourceServiceImpl();
    @Override
    public ResourceService getResourceService() {
        return resourceService;
    }
}
