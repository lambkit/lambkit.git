package com.lambkit.core.service;

import java.util.List;
import java.util.Map;

public interface BeanFactory {

    /**
     * get or create bean
     * @param clazz
     * @return
     * @param <T>
     * @throws Exception
     */
    <T> T get(Class<T> clazz);

    <T> void set(Class<T> clazz, T bean);

    <T> void set(Class<T> interfaceClass, Class<? extends T>... implementClass);

    <T> T getBean(Class<T> clazz) throws Exception;

    <T> Map<String, T> getBeansMapOfType(Class<T> clazz) throws Exception;

    <T> List<T> getBeansOfType(Class<T> type) throws Exception;

}
