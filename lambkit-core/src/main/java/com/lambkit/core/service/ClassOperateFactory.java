package com.lambkit.core.service;

public interface ClassOperateFactory {

    ClassOperateService getClassOperateService();
}
