package com.lambkit.core.service;

/**
 * @author yangyong(孤竹行)
 */
public interface ScanClassProcess {
    void process(Class<?> clazz);
}
