package com.lambkit.core.service;

import java.util.List;

/**
 * resource在不同打包的情况下获取的方法
 * @author yangyong(孤竹行)
 */
public interface ResourceService {

    String getRootClassPath(String resourcePathType);
    String getRootClassPath();

    /**
     * 按照resourcePathType的配置，获取web资源路径
     * @return
     */
    String getWebResourcePath(String resourcePath, String resourcePathType);
    String getWebResourceClassPath(String resourcePath);
    String getWebResourceWebRootPath(String resourcePath, String resourcePathType);
    /**
     * resourcePath=file:开头直接返回,否则返回rootPath+resourcePath
     * @return
     */
    String getWebResourceFilePath(String resourcePath);
    List<String> getWebResourcePathList(String resourcePath, String resourcePathType);

    String getWebTemplatePath(String templatePath, String resourcePathType);

    /**
     * 共享文件目录，fatjar为单独的文件夹，正常是webapp，用于上传和下载文件
     * @return
     */
    String getWebSharePath(String resourcePath, String resourcePathType);

    /**
     * webroot的目录，fatjar为classpath，正常是webapp
     * @return
     */
    String getWebRootPath(String resourcePathType);
    String getWebRootPath();

    /**
     * 所运行的文件夹地址
     * @return
     */
    String getRootPath();

    /**
     * 查找配置文件，/root/config, /root, /classpath
     * @param fileName
     * @return
     */
    String getResourceFile(String fileName);

    /**
     * 扫描文件夹下的文件和目录
     * @param folderPath
     */
    void scanFolder(String folderPath, String resourcePathType, ScanFileProcess fileProcess);
    void scanFolder(String folderPath, ScanFileProcess fileProcess);
}
