package com.lambkit.core.registry;

/**
 * @author yangyong(孤竹行)
 */
public interface RegisterClient extends ServiceRegistry, ServiceDiscovery{
}
