package com.lambkit.core.registry;

import java.io.Serializable;

/**
 * @author yangyong(孤竹行)
 */
public class ServiceInstanceListVo implements Serializable {
    private String namespaceId;//	String	否	命名空间Id，默认为public
    private String groupName;//	String	否	分组名，默认为DEFAULT_GROUP
    private String  serviceName;//	String	是	服务名
    private String  ip;//	String	是	IP地址
    private int  port;//	int	是	端口号
    private String  clusterName;//	String	否	集群名称，默认为DEFAULT

    private boolean healthyOnly;//	boolean	否	是否只获取健康实例，默认为false
    private String app;//	String	否	应用名，默认为空

    public String getNamespaceId() {
        return namespaceId;
    }

    public void setNamespaceId(String namespaceId) {
        this.namespaceId = namespaceId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public boolean isHealthyOnly() {
        return healthyOnly;
    }

    public void setHealthyOnly(boolean healthyOnly) {
        this.healthyOnly = healthyOnly;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }
}
