package com.lambkit.core.registry;

public interface ServiceRegistry {
	/**
     * 注册服务
     */
    void registry(ServiceInstance serviceInstance);
}
