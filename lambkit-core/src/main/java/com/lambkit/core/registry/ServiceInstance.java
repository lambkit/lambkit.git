package com.lambkit.core.registry;

import java.io.Serializable;

/**
 * @author yangyong(孤竹行)
 */
public class ServiceInstance implements Serializable {
    private String namespaceId;//	String	否	命名空间Id，默认为public
    private String groupName;//	String	否	分组名，默认为DEFAULT_GROUP
    private String  serviceName;//	String	是	服务名
    private String  ip;//	String	是	IP地址
    private int  port;//	int	是	端口号
    private String  clusterName;//	String	否	集群名称，默认为DEFAULT
    private boolean  healthy;//	boolean	否	是否只查找健康实例，默认为true
    private int  weight;//	boolean	否	实例权重，默认为1.0
    private boolean  enabled;//	boolean	否	是否可用，默认为true
    private String  metadata;//	JSON格式String	否	实例元数据
    private boolean  ephemeral;//	boolean	否	是否为临时实例

    private String instanceId;

    private int  instanceHeartBeatTimeOut;//	int	实例心跳超时时间

    private int  ipDeleteTimeout;//	int	实例删除超时时间

    private int  instanceHeartBeatInterval;//	int	实例心跳间隔

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getNamespaceId() {
        return namespaceId;
    }

    public void setNamespaceId(String namespaceId) {
        this.namespaceId = namespaceId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public boolean isHealthy() {
        return healthy;
    }

    public void setHealthy(boolean healthy) {
        this.healthy = healthy;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public boolean isEphemeral() {
        return ephemeral;
    }

    public void setEphemeral(boolean ephemeral) {
        this.ephemeral = ephemeral;
    }

    public int getInstanceHeartBeatTimeOut() {
        return instanceHeartBeatTimeOut;
    }

    public void setInstanceHeartBeatTimeOut(int instanceHeartBeatTimeOut) {
        this.instanceHeartBeatTimeOut = instanceHeartBeatTimeOut;
    }

    public int getIpDeleteTimeout() {
        return ipDeleteTimeout;
    }

    public void setIpDeleteTimeout(int ipDeleteTimeout) {
        this.ipDeleteTimeout = ipDeleteTimeout;
    }

    public int getInstanceHeartBeatInterval() {
        return instanceHeartBeatInterval;
    }

    public void setInstanceHeartBeatInterval(int instanceHeartBeatInterval) {
        this.instanceHeartBeatInterval = instanceHeartBeatInterval;
    }
}
