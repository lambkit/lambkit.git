package com.lambkit.core;

/**
 * 生命周期
 * @author yangyong(孤竹行)
 */
public interface Lifecycle {

    /**
     * 获取当前状态
     * @return
     */
    LifecycleState getCurrentState();

    default void init() throws LifecycleException {
    }

    void start() throws LifecycleException;

    default void postStart() throws LifecycleException {
    }

    default void preStop() throws LifecycleException {
    }

    default void stop() throws LifecycleException {
    }
}
