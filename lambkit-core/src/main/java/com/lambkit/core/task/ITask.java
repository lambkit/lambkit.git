package com.lambkit.core.task;

import com.lambkit.core.pipeline.IPipeLine;
import com.lambkit.core.pipeline.IValve;
import com.lambkit.core.pipeline.IPipLineContext;

/**
 * 任务
 * @author yangyong(孤竹行)
 */
public interface ITask<T extends IPipeLine, V extends IValve, C extends IPipLineContext> {

    /**
     * 任务名称/标识
     * @return
     */
    String getId();
    void setId(String name);

    ITask addValve(V valve);

    ITask addExecutor(String action, Class<? extends ITaskExecutor> clazz);

    V firstValue();

    V endValue();
}
