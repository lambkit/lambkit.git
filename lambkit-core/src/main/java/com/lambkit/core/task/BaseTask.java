package com.lambkit.core.task;

import com.lambkit.core.Route;
import com.lambkit.core.pipeline.IPipeLine;
import com.lambkit.core.pipeline.IValve;
import com.lambkit.core.pipeline.IPipLineContext;

/**
 * @author yangyong(孤竹行)
 */
public abstract class BaseTask<T extends IPipeLine<V, C>, V extends IValve, C extends IPipLineContext> implements ITask<T, V, C> {

    private String id;

    private T pipLine;

    /**
     * executor route
     */
    private Route route;

    //////////////////////////////////////////////////////
    @Override
    public String getId() {
        return id;
    }

    public ITask addValve(V valve) {
        if(getPipLine()!=null) {
            getPipLine().addValve(valve);
        }
        return this;
    }

    @Override
    public ITask addExecutor(String action, Class<? extends ITaskExecutor> clazz) {
        if(route!=null) {
            route.add(action, clazz);
        }
        return null;
    }

    public V firstValue() {
        if(getPipLine()==null) {
            return null;
        }
        return getPipLine().firstValve();
    }

    public V endValue() {
        if(getPipLine()==null) {
            return null;
        }
        V valve = getPipLine().firstValve();
        V result = valve;
        while (valve != null) {
            result = valve;
            valve = (V) valve.getNext();
        }
        return result;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public T getPipLine() {
        return pipLine;
    }

    public void setPipLine(T pipLine) {
        this.pipLine = pipLine;
    }
    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }
}
