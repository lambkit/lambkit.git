package com.lambkit.core.task.http;

import com.lambkit.core.task.BaseTask;

public class HttpTask extends BaseTask<HttpTaskPipLine, HttpTaskValve, HttpTaskContext> {

    public HttpTask() {
        this.setPipLine(new HttpTaskPipLine());
    }

    public static HttpTask by(HttpTaskValve valve) {
        HttpTask task = new HttpTask();
        task.addValve(valve);
        return task;
    }

    public HttpTask addValve(HttpTaskValve valve) {
        super.addValve(valve);
        return this;
    }
}
