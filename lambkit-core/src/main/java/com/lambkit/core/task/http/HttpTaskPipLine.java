package com.lambkit.core.task.http;

import com.lambkit.core.pipeline.BasePipeLine;

/**
 * @author yangyong(孤竹行)
 */
public class HttpTaskPipLine extends BasePipeLine<HttpTaskValve, HttpTaskContext> {
    @Override
    public void invoke(HttpTaskContext valveContext) {
        HttpTaskValve f = firstValve();
        if(f != null) {
            f.invoke(valveContext, true);
        }
    }
}
