package com.lambkit.core.task.http;

import com.lambkit.core.http.IHttpContext;
import com.lambkit.core.task.TaskContext;

/**
 * @author yangyong(孤竹行)
 */
public class HttpTaskContext extends TaskContext<HttpTask> {
    private IHttpContext httpContext;

    public boolean isReady() {
        boolean flag = super.isReady();
        if(!flag) {
            return false;
        }
        if(httpContext == null) {
            return false;
        }
        return true;
    }

    public IHttpContext getHttpContext() {
        return httpContext;
    }

    public void setHttpContext(IHttpContext httpContext) {
        this.httpContext = httpContext;
    }
}
