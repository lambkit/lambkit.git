package com.lambkit.core.task;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author yangyong(孤竹行)
 */
public class WorkQueue implements IWorkQueue {

    /**
     * LinkedBlockingQueue是一个基于链表的阻塞队列，它实现了BlockingQueue接口。
     * 一个可选有界的阻塞队列，基于链表实现。默认大小为 Integer.MAX_VALUE，适用于任务生产和消费速率不同的场景。
     */
    BlockingQueue<ITaskInstance> queue = new LinkedBlockingQueue<>();

    /**
     * put(E element)：将指定元素插入队列，如果队列已满，则阻塞当前线程，直到有空间可用。
     * add(E element)：将指定元素插入队列，如果队列已满，则抛出异常。
     * offer(E element)：将指定元素插入队列，如果队列已满，则返回 false。 下面将分别对这三个方法进行介绍。
     * @param task
     * @return
     */
    public boolean push(ITaskInstance task) {
        try {
            queue.put(task);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return true;
    }

    /**
     * take()：如果队列为空，当前线程将被阻塞，直到队列中有可用元素。
     * @return
     */
    public ITaskInstance next() {
        ITaskInstance task = null;
        try {
            task = queue.take();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
//        try {
//            //从BlockingQueue取出一个队首的对象，
//            // 如果在指定时间内，队列一旦有数据可取，则立即返回队列中的数据。
//            // 否则时间超时还没有数据可取，返回失败。
//            task = queue.poll(10, TimeUnit.MILLISECONDS);
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }
        return task;
    }

    public boolean remove(ITaskInstance task) {
        return queue.remove(task);
    }

    @Override
    public int size() {
        return queue.size();
    }
}
