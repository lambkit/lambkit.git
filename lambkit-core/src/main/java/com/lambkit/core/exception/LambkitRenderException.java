package com.lambkit.core.exception;

public class LambkitRenderException extends RuntimeException {

    private static final long serialVersionUID = -6448434551667513804L;

    private Integer errorCode;

    public LambkitRenderException() {
        super();
    }

    public LambkitRenderException(String message) {
        super(message);
    }

    public LambkitRenderException(int errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public LambkitRenderException(Throwable cause) {
        super(cause);
    }

    public LambkitRenderException(String message, Throwable cause) {
        super(message, cause);
    }
}
