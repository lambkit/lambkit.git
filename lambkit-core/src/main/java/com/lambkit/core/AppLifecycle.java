package com.lambkit.core;

/**
 * @author yangyong(孤竹行)
 */
public interface AppLifecycle extends Lifecycle {
    void setContext(AppContext context);
}
