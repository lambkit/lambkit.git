package com.lambkit.core.plugin;

import com.lambkit.core.AppContext;
import com.lambkit.core.Lifecycle;

/**
 * 插件
 * @author yangyong(孤竹行)
 */
public interface IPlugin extends Lifecycle {
    void setContext(AppContext context);
}
