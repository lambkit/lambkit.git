package com.lambkit.core.flow;

/**
 * @author yangyong(孤竹行)
 */
public class FlowConsts {

    public static final String FLOW_VERSION = "1.0";
    /**
     * 初始状态
     */
    public static final String FLOW_STATUS_INIT = "init";
    /**
     * 运行状态
     */
    public static final String FLOW_STATUS_START = "start";
    /**
     * 暂停状态
     */
    public static final String FLOW_STATUS_PAUSE = "pause";
    /**
     * 停止状态
     */
    public static final String FLOW_STATUS_STOP = "stop";

}
