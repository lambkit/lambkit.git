package com.lambkit.core.flow.workflow;

import com.lambkit.core.flow.NodeType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 流程节点基类
 */
public abstract class WorkFlowNode {
    private String nodeId;
    private String nodeName;
    private NodeType type;
    private List<WorkFlowTransition> transitions = new ArrayList<>();

    public WorkFlowNode(String id, String name, NodeType type) {
        this.nodeId = id;
        this.nodeName = name;
        this.type = type;
    }

    // 添加流转路径（支持条件表达式）
    public void addTransition(WorkFlowNode target, String condition) {
        //transitions.add(new WorkFlowTransition(target, condition));
    }

    // 获取符合条件的下一个节点
    public WorkFlowNode getNextNode(Map<String, Object> variables) {
        if (transitions.isEmpty()) return null;
        for (WorkFlowTransition t : transitions) {
            if (t.evaluateCondition(variables)) return t.getTarget();
        }
        return transitions.get(0).getTarget(); // 默认第一条路径
    }
}
