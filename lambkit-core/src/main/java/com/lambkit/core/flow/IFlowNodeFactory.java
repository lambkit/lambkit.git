package com.lambkit.core.flow;

/**
 * 流程存储接口
 * @author yangyong(孤竹行)
 */
public interface IFlowNodeFactory {
    IFlowNode get(FlowNodeObject nodeObject, FlowInstance flowInstance);
}