package com.lambkit.core.flow;

import com.lambkit.core.Lifecycle;

/**
 * 流程工作中心
 * @author yangyong(孤竹行)
 */
public interface IFlowWorkCenter extends Lifecycle {
}
