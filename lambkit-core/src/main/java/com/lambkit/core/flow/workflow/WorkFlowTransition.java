package com.lambkit.core.flow.workflow;

import java.util.Map;

public abstract class WorkFlowTransition {
    private WorkFlowNode target;
    private String condition;

    public WorkFlowTransition(WorkFlowNode target, String condition) {
        this.target = target;
        this.condition = condition;
    }

    /**
     * 条件判断
     * @param vars
     * @return
     */
    public abstract boolean evaluateCondition(Map<String,Object> vars);

    public WorkFlowNode getTarget() {
        return target;
    }
}
