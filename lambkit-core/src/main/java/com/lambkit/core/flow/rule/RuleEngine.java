package com.lambkit.core.flow.rule;

/**
 * 规则引擎核心接口
 */
public interface RuleEngine {
    /**
     * 加载规则（支持XML/JSON/YAML）
     * @param ruleContent 规则内容
     * @param format 规则格式
     */
    void loadRule(String ruleContent, RuleFormat format) throws RuleException;

    /**
     * 执行流程链
     * @param chainId 流程ID
     * @param context 执行上下文
     */
    void execute(String chainId, RuleContext context) throws RuleException;

    /**
     * 动态更新规则（热部署）
     * @param newRuleContent 新规则内容
     */
    void reloadRule(String newRuleContent) throws RuleException;
}
