/**
 * 流程引擎
 * 规则引擎 + 工作流 + ***
 * 节点 FlowNode
 * 任务 FlowTask
 * 规则 FlowRule
 * 属性 FlowAttr
 * 引擎 FlowEngine
 * 监控 FlowMonitor
 *
 * @author yangyong(孤竹行)
 */
package com.lambkit.core.flow;
