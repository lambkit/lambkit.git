package com.lambkit.core.flow;

import java.util.List;

/**
 * 后向选择规则
 * @author yangyong(孤竹行)
 */
public interface IFlowNodeNextRule extends IFlowNodeRule {
    List<IFlowNode> decision(IFlowNode flowNode, IFlowContext flowContext);
}
