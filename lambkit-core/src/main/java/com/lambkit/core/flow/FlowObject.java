package com.lambkit.core.flow;

import cn.hutool.core.util.StrUtil;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class FlowObject implements Serializable {
    private static final long serialVersionUID = 1L;
    private String code; // 流程id

    private String title; // 流程名称

    private String type; // 流程类型

    private String name; // 流程名称(内部类型名称)

    private String version; // 流程版本

    private String description; // 流程描述

    private String status = "init"; // 流程状态, init/start/stop

    private String startNode; // 流程节点

    private List<FlowNodeObject> nodes;

    public FlowNodeObject getNodeByCode(String flowNodeId) {
        if(StrUtil.isBlank(flowNodeId)) {
            return null;
        }
        if(nodes!=null) {
            for (FlowNodeObject node : nodes) {
                if (flowNodeId.equals(node.getCode())) {
                    return node;
                }
            }
        }
        return null;
    }

    public void addNode(String flowNodePreId, FlowNodeObject node) {
        FlowNodeObject nodePre = getNodeByCode(flowNodePreId);
        if(nodePre != null) {
            nodePre.getNext().add(node.getCode());
        }
    }

    /////////////////////////////////////////////////
    //get and set

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStartNode() {
        return startNode;
    }

    public void setStartNode(String startNode) {
        this.startNode = startNode;
    }

    public List<FlowNodeObject> getNodes() {
        return nodes;
    }

    public void setNodes(List<FlowNodeObject> nodes) {
        this.nodes = nodes;
    }
}
