package com.lambkit.core.flow;

import com.lambkit.core.flow.*;
import com.lambkit.core.flow.node.EndFlowNode;
import com.lambkit.core.flow.node.NormalFlowNode;
import com.lambkit.core.flow.node.StartFlowNode;

/**
 * @author yangyong(孤竹行)
 */
public class FlowNodeFactory implements IFlowNodeFactory {

    @Override
    public IFlowNode get(FlowNodeObject nodeObject, FlowInstance flowInstance) {
        if("start".equalsIgnoreCase(nodeObject.getName())) {
            return new StartFlowNode(nodeObject, flowInstance);
        } else if("end".equalsIgnoreCase(nodeObject.getName())) {
            return new EndFlowNode(nodeObject, flowInstance);
        } else {
            return new NormalFlowNode(nodeObject, flowInstance);
        }
    }
}
