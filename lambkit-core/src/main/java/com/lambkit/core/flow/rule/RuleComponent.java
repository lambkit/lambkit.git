package com.lambkit.core.flow.rule;

/**
 * 流程节点组件接口
 */
public interface RuleComponent {
    /**
     * 执行组件逻辑
     * @param context 上下文对象
     */
    void process(RuleContext context) throws RuleComponentException;

    /**
     * 是否跳过该组件（动态决策）
     */
    default boolean isSkip(RuleContext context) { return false; }
}
