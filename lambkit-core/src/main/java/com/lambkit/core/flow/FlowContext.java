package com.lambkit.core.flow;

import com.lambkit.core.BaseContext;

/**
 * 流程上下文
 * @author yangyong(孤竹行)
 */
public class FlowContext extends BaseContext implements IFlowContext {
    private FlowInstanceObject instanceObject;

    public FlowContext(FlowInstanceObject instanceObject) {
        this.instanceObject = instanceObject;
        setId(instanceObject.getCode());
    }

    public boolean isReady() {
        if(instanceObject==null) {
            return false;
        }
        return true;
    }

    public FlowInstanceObject getInstanceObject() {
        return instanceObject;
    }

    public void setInstanceObject(FlowInstanceObject instanceObject) {
        this.instanceObject = instanceObject;
    }
}
