/**
 * 处理流
 * 处理流，是对一系列服务执行的过程。提交一个数据和参数集，经过一系列的服务执行过程，输出一个结果。
 * @author yangyong(孤竹行)
 */
package com.lambkit.core.flow.process;