package com.lambkit.core.flow.workflow;

import com.lambkit.core.flow.IFlowBuilder;
import com.lambkit.core.flow.IFlowInstanceBuilder;

/**
 * 工作流数据库接口
 * @author yangyong(孤竹行)
 */
public interface IWorkFlowDatabase extends IFlowBuilder, IFlowInstanceBuilder {

}
