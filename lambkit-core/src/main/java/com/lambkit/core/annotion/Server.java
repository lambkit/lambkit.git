package com.lambkit.core.annotion;

import com.lambkit.core.LambkitApp;
import com.lambkit.core.LambkitServer;

import java.lang.annotation.*;

/**
 * server注解
 * @author yangyong(孤竹行)
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Server {
    Class<? extends LambkitServer> value();
}
