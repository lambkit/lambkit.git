package com.lambkit.core.annotion;

import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ScanPackage {
    String[] value() default {};
}
