package com.lambkit.core.annotion;

import com.lambkit.core.LambkitApp;

import java.lang.annotation.*;

/**
 * app注解
 * @author yangyong(孤竹行)
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface App {
    Class<? extends LambkitApp>[] value();
}
