package com.lambkit.core.annotion;

import com.lambkit.core.config.AppConfig;

import java.lang.annotation.*;

/**
 * bean注解
 * @author yangyong(孤竹行)
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Config {
    Class<? extends AppConfig> value();
}
