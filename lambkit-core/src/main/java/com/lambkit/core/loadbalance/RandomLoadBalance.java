package com.lambkit.core.loadbalance;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 随机（Random）
 * 利用随机数从所有服务器中随机选取一台，可以用服务器数组下标获取。
 * @author yangyong
 *
 */
public class RandomLoadBalance {

	public Server selectServer(List<Server> serverList) {
		Random selector = new Random();
		int next = selector.nextInt(serverList.size());
		return serverList.get(next);
	}

	public static void main(String[] args) {
		List<Server> serverList = new ArrayList<>();
		serverList.add(new Server(1, "服务器1"));
		serverList.add(new Server(2, "服务器2"));
		serverList.add(new Server(3, "服务器3"));
		RandomLoadBalance lb = new RandomLoadBalance();
		for (int i = 0; i < 10; i++) {
			Server selectedServer = lb.selectServer(serverList);
			System.out.format("第%d次请求，选择服务器%s\n", i + 1, selectedServer.toString());
		}
	}
}
