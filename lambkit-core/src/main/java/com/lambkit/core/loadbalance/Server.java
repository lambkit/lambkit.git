package com.lambkit.core.loadbalance;

public class Server {
	private int serverId;

	private String name;

	private int weight;

	public Server(int serverId, String name) {
		this.serverId = serverId;
		this.name = name;
	}

	public Server(int serverId, String name, int weight) {
		this.serverId = serverId;
		this.name = name;
		this.weight = weight;
	}

	public int getServerId() {
		return serverId;
	}

	public void setServerId(int serverId) {
		this.serverId = serverId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

}
