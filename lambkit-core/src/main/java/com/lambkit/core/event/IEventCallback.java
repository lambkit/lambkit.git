package com.lambkit.core.event;

public interface IEventCallback {
	public void execute(IEvent event);
	public void error(IEvent event);
}
