package com.lambkit.core.event;

/**
 * @author yangyong(孤竹行)
 */
public interface IEventBus {
    void register(Class<? extends IEventListener> listener);

    void unregister(Class<? extends IEventListener> listener);

    void post(IEvent event);
}
