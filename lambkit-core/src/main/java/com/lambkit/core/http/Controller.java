package com.lambkit.core.http;

import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class Controller extends BaseHttpContext implements IController {

    private IHttpContext context;

    @Override
    public void setContext(IHttpContext context) {
        this.context = context;
    }

    @Override
    public IHttpContext getContext() {
        return context;
    }

    @Override
    public void doSetCookie(String name, String value, int maxAgeInSeconds, String path, String domain, Boolean isHttpOnly) {
        context.doSetCookie(name, value, maxAgeInSeconds, path, domain, isHttpOnly);
    }

    @Override
    public IRequest getRequest() {
        return context.getRequest();
    }

    @Override
    public void setHttpServletRequest(IRequest request) {
        context.setHttpServletRequest(request);
    }

    @Override
    public void setHttpServletResponse(IResponse response) {
        context.setHttpServletResponse(response);
    }

    @Override
    public IResponse getResponse() {
        return context.getResponse();
    }

    @Override
    public ISession getSession() {
        return context.getSession();
    }

    @Override
    public ISession getSession(boolean create) {
        return context.getSession(create);
    }

    @Override
    public void setTarget(String pathNew) {
        context.setTarget(pathNew);
    }

    @Override
    public ICookie createCookie(String name, String value) {
        return context.createCookie(name, value);
    }

    @Override
    public void setCookie(String name, String value, int maxAgeInSeconds, String path, String domain, Boolean isHttpOnly, String sameSite) {
        context.setCookie(name, value, maxAgeInSeconds, path, domain, isHttpOnly, sameSite);
    }

    @Override
    public IUploadFile getFile(String name, String uploadPath, long maxPostSize) {
        return context.getFile(name, uploadPath, maxPostSize);
    }

    @Override
    public List<IUploadFile> getFiles(String uploadPath) {
        return context.getFiles(uploadPath);
    }
}
