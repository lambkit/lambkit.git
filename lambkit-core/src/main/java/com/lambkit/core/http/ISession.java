package com.lambkit.core.http;

/**
 * @author yangyong(孤竹行)
 */
public interface ISession {
    Object getAttribute(String key);

    void setAttribute(String key, Object value);

    void removeAttribute(String key);

    void invalidate();
}
