package com.lambkit.core.http;

import javax.servlet.ServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Collection;

/**
 * @author yangyong(孤竹行)
 */
public interface IResponse {

    Object getSource();

    int getHttpStatus();

    void setHttpStatus(int status, String msg);

    void setHeader(String name, String value);

    void addHeader(String name, String value);

    String getHeader(String name);

    void setDateHeader(String expires, int i);

    Collection<String> getHeaders(String name);

    Collection<String> getHeaderNames();

    void setContentLength(int contentLength);

    int getContentLength();

    void setContentType(String contentType);

    String getContentType();

    void write(byte[] data) throws IOException;

    void close();

    void addCookie(ICookie cookie);

    OutputStream getOutputStream() throws IOException;

    default void redirect(String url) throws IOException {
        redirect(url, 302);
    }

    void redirect(String url, int code) throws IOException ;


}
