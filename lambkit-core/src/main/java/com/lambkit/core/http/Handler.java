package com.lambkit.core.http;

import com.lambkit.core.processor.IPostProcessor;

/**
 * @author yangyong(孤竹行)
 */
public interface Handler {

    default public boolean handleBefore(IHttpContext context, HttpAction action) {
        return true;
    }

    default public void handleAfter(IHttpContext context, HttpAction action) {
    }

    default int getPriority() {
        return 0;
    }
}
