package com.lambkit.core.http;

/**
 * @author yangyong(孤竹行)
 */
public interface IController extends IHttpContext {
    public void setContext(IHttpContext context);
    public IHttpContext getContext();
}
