package com.lambkit.core.http;

import com.lambkit.core.pipeline.BasePipeLine;
import com.lambkit.core.processor.PostProcessorContainer;

/**
 * @author yangyong(孤竹行)
 */
public class HttpPipLine extends BasePipeLine<HttpValve, HttpPipLineContext> {

    private PostProcessorContainer<HttpPipLineContext> postProcessorContainer = new PostProcessorContainer<HttpPipLineContext>();


    public void handler(HttpPipLineContext handlerContext) {
        postProcessorContainer.handleAfter(handlerContext);
        invoke(handlerContext);
        postProcessorContainer.handleAfter(handlerContext);
    }

    @Override
    public void invoke(HttpPipLineContext valveContext) {
        HttpValve f = firstValve();
        if(f != null) {
            f.invoke(valveContext, true);
        }
    }
}

