package com.lambkit.core.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public interface IRequest {
    Object getSource();

    Enumeration<String> getAttributeNames();

    <T> T getAttribute(String name);

    Map<String, Object> getAttributeMap();

    void setAttribute(String name, Object value);

    void removeAttribute(String name);

    String getRawData();

    String getParameter(String name);

    Map<String, String[]> getParameterMap();

    Enumeration<String> getParameterNames();

    String[] getParameterValues(String name);

    String getHeader(String name);

    ISession getSession(boolean create);

    ICookie[] getCookies();

    String getMethod();

    String getRequestURI();

    String getRequestURL();

    void forward(String path, IResponse response);


    String getRemoteAddr();

    String getScheme();

    String getServerName();

    int getServerPort();

    BufferedReader getReader() throws IOException;

    String getContentType();

    String getQueryString();
}
