package com.lambkit.db.hutool;

import cn.hutool.db.handler.RsHandler;
import com.lambkit.db.ResultSetBuilder;
import com.lambkit.db.RowData;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author yangyong(孤竹行)
 */
public class RowDataHandler implements RsHandler<RowData> {

    private boolean withMetaInfo = true;

    public static RowDataHandler create() {
        return new RowDataHandler();
    }

    public static RowDataHandler create(boolean withMetaInfo) {
        RowDataHandler handler = new RowDataHandler();
        handler.setWithMetaInfo(withMetaInfo);
        return handler;
    }

    @Override
    public RowData handle(ResultSet rs) throws SQLException {
        return ResultSetBuilder.of().build(rs, withMetaInfo, RowData.class);
    }

    public void setWithMetaInfo(boolean withMetaInfo) {
        this.withMetaInfo = withMetaInfo;
    }
}
