package com.lambkit.db.hutool;

import cn.hutool.db.handler.RsHandler;
import com.lambkit.db.ResultSetBuilder;
import com.lambkit.db.RowData;
import com.lambkit.db.PageData;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author yangyong(孤竹行)
 */
public class RowDataPageHandler implements RsHandler<PageData> {

    private PageData pageData;
    private boolean withMetaInfo = true;

    public RowDataPageHandler(PageData pageData) {
        this.pageData = pageData;
    }

    public static RowDataPageHandler create(PageData pageData) {
        return new RowDataPageHandler(pageData);
    }

    public static RowDataPageHandler create(PageData pageData, boolean withMetaInfo) {
        RowDataPageHandler handler = new RowDataPageHandler(pageData);
        handler.setWithMetaInfo(withMetaInfo);
        return handler;
    }
    @Override
    public PageData handle(ResultSet rs) throws SQLException {
        return ResultSetBuilder.of().buildPage(rs, withMetaInfo, pageData, RowData.class);
    }

    public void setWithMetaInfo(boolean withMetaInfo) {
        this.withMetaInfo = withMetaInfo;
    }
}
