package com.lambkit.db.hutool;

import cn.hutool.core.convert.Convert;
import cn.hutool.db.handler.RsHandler;
import com.lambkit.db.IRowData;
import com.lambkit.db.ResultSetBuilder;
import com.lambkit.db.RowData;
import com.lambkit.db.RowModel;

import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

/**
 * @author yangyong(孤竹行)
 */
public class ModelHandler<ROW extends RowModel<ROW>> implements RsHandler<ROW> {

    private boolean withMetaInfo = true;

    private Class<ROW> rowClass;

    public ModelHandler(Class<ROW> rowClass, boolean withMetaInfo) {
        this.rowClass = rowClass;
        this.withMetaInfo = withMetaInfo;
    }

    @Override
    public ROW handle(ResultSet rs) throws SQLException {
        return ResultSetBuilder.of().build(rs, withMetaInfo, rowClass);
    }

    public void setWithMetaInfo(boolean withMetaInfo) {
        this.withMetaInfo = withMetaInfo;
    }
}
