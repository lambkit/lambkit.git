package com.lambkit.db;

import java.util.List;

/**
 * 分页接口
 * @author yangyong(孤竹行)
 */
public interface IPageData<T> {
    List<T> getList();

    void setList(List<T> list);

    int getPage();

    void setPage(int pageNumber);

    int getPageSize();

    void setPageSize(int pageSize);

    int getTotalPage();

    void setTotalPage(int totalPage);

    int getTotal();

    void setTotal(int totalRow);

    boolean isFirst();

    boolean isLast();
}
