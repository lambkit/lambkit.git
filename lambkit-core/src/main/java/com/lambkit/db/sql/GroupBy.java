package com.lambkit.db.sql;

import com.alibaba.fastjson.JSONObject;
import com.lambkit.db.meta.TableMeta;
import com.lambkit.db.mgr.MgrTable;
import com.lambkit.util.SqlKit;

public class GroupBy {

	private ColumnsGroup having;
	private String name;

	/**
	 * 警告信息
	 */
	private String warns = null;
	
	public GroupBy() {
		// TODO Auto-generated constructor stub
	}

	public GroupBy(String name, ColumnsGroup having) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.having = having;
	}
	
	public GroupBy(JSONObject jsonObject, MgrTable tbc) {
		// TODO Auto-generated constructor stub
		if(jsonObject.containsKey("by")) {
			setName(jsonObject.getString("by"));
		}
		if(jsonObject.containsKey("having")) {
			having = new ColumnsGroup();
			having.filter(jsonObject.getJSONObject("having"), tbc.getMeta());
		}
	}
	
	public GroupBy(JSONObject jsonObject, TableMeta meta) {
		// TODO Auto-generated constructor stub
		if(jsonObject.containsKey("by")) {
			setName(jsonObject.getString("by"));
		}
		if(jsonObject.containsKey("having")) {
			having = new ColumnsGroup();
			having.filter(jsonObject.getJSONObject("having"), meta);
		}
	}

	public ColumnsGroup getHaving() {
		return having;
	}
	
	public String getName() {
		return name;
	}
	
	public void setHaving(ColumnsGroup having) {
		this.having = having;
	}
	
	public void setName(String name) {
		String sls = SqlKit.transactSQLInjection(name);
		if(SqlKit.validateSQL(sls)) {
			this.name = sls;
		} else {
			warns = "GroupBy name is not validate sql.";
		}
	}

	public String getWarns() {
		return warns;
	}

	public void setWarns(String warns) {
		this.warns = warns;
	}
}
