package com.lambkit.db;

/**
 * @author yangyong(孤竹行)
 */
public interface IDaoService<M extends RowModel<M>> {
    public IDbOpt<M, PageData<M>> dao();
}
