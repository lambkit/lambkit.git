package com.lambkit.db.mgr;

import cn.hutool.core.util.StrUtil;
import com.lambkit.db.meta.ColumnMeta;
import com.lambkit.db.meta.TableMeta;

public class ChartBuilder {

    public Chart build(MgrTable tbc, String cls, String serias, String yuns, String seriasPrefix) {
        Chart chart = new Chart();
        chart.setClassify(cls);
        chart.setOperation(yuns);
        setClassifyName(chart, tbc, cls);
        setSerias(chart, tbc, serias, seriasPrefix);
        return chart;
    }

    public Chart build(TableMeta meta, String cls, String serias, String yuns, String seriasPrefix, String title) {
        Chart chart = new Chart();
        chart.setClassify(cls);
        chart.setOperation(yuns);
        setSerias(chart, meta, serias, seriasPrefix, title);
        return chart;
    }

    private void addSerias(Chart chart, MgrTable tbc, String serias_name) {
        if(tbc==null) {
            return;
        }
        IField fldserias = tbc.getField(serias_name);
        serias_name = fldserias!=null ? fldserias.getTitle() : serias_name;
        chart.addSerias(serias_name);
    }

//	public void addSerias(String tbname, String serias_name) {
//		if(StrUtil.isBlank(tbname)) return;
//		IFieldDao fdao = getFieldDao();
//		if(fdao==null) return;
//		IField fldserias = fdao.findFirstByTbName(tbname, MgrConstants.NONE, fldname, serias_name);
//		serias_name = fldserias!=null ? fldserias.getTitle() : serias_name;
//		addSerias(serias_name);
//	}

    private void setClassifyName(Chart chart, MgrTable tbc, String classifyName) {
//		if(StrUtil.isBlank(tbname)) return;
//		IFieldDao fdao = getFieldDao();
//		if(fdao==null) return;
//		IField fldm = fdao.findFirstByTbName(tbname, MgrConstants.NONE, fldname, classifyName);
//		classifyName = fldm==null ? classifyName : fldm.getTitle();
//		setClassifyName(classifyName);
        if(tbc==null) {
            return;
        }
        IField fldm = tbc.getField(classifyName);
        classifyName = fldm==null ? classifyName : fldm.getTitle();
        chart.setClassifyName(classifyName);
    }

    /**
     * 设置系列
     * @param tbc
     * @param serias
     * @param prefix
     * @return
     */
    private Chart setSerias(Chart chart, MgrTable tbc, String serias, String prefix) {
        if(!StrUtil.isNotBlank(serias)) {
            return chart;
        }
        String yuns = chart.getOperation();
        if(serias.contains(",")) {
            String[] sers = serias.split(",");
            serias = "";
            int tji = 0;
            if(yuns.contains(",")) {
                String[] yunss = yuns.split(",");
                for (int i = 0; i < sers.length; i++) {
                    String ss=  sers[i];
                    String yun = StrUtil.isNotBlank(yunss[i]) ? yunss[i] : yunss[0];
                    if(StrUtil.isNotBlank(ss)) {
                        String ssSQL = getOneSerias(tbc, ss, yun, prefix);
                        if(StrUtil.isNotBlank(ss)) {
                            serias += "," + ssSQL + tji;
                            addSerias(chart, tbc, ss);
                            tji++;
                        }
                    }
                }
            } else {
                for (int i = 0; i < sers.length; i++) {
                    String ss=  sers[i];
                    if(StrUtil.isNotBlank(ss)) {
                        String ssSQL = getOneSerias(tbc, ss, yuns, prefix);
                        if(StrUtil.isNotBlank(ss)) {
                            serias += "," + ssSQL + tji;
                            addSerias(chart, tbc, ss);//tbc.getName()
                            tji++;
                        }
                    }
                }
            }
            chart.setSeriasSQL(serias.substring(1));
        } else {
            chart.setSeriasSQL(getOneSerias(tbc, serias, yuns, prefix)+"0");
            addSerias(chart, tbc, serias);
        }
        return chart;
    }

    private Chart setSerias(Chart chart, TableMeta meta, String serias, String prefix, String title) {
        if(!StrUtil.isNotBlank(serias)) {
            return chart;
        }
        String yuns = chart.getOperation();
        if(serias.contains(",")) {
            String[] sers = serias.split(",");
            serias = "";
            int tji = 0;
            if(yuns.contains(",")) {
                String[] yunss = yuns.split(",");
                for (int i = 0; i < sers.length; i++) {
                    String ss=  sers[i];
                    String yun = StrUtil.isNotBlank(yunss[i]) ? yunss[i] : yunss[0];
                    if(StrUtil.isNotBlank(ss)) {
                        String ssSQL = getOneSerias(meta, ss, yun, prefix);
                        if(StrUtil.isNotBlank(ss)) {
                            serias += "," + ssSQL + tji;
                            chart.addSerias(title);
                            tji++;
                        }
                    }
                }
            } else {
                for (int i = 0; i < sers.length; i++) {
                    String ss=  sers[i];
                    if(StrUtil.isNotBlank(ss)) {
                        String ssSQL = getOneSerias(meta, ss, yuns, prefix);
                        if(StrUtil.isNotBlank(ss)) {
                            serias += "," + ssSQL + tji;
                            chart.addSerias(title);
                            tji++;
                        }
                    }
                }
            }
            chart.setSeriasSQL(serias.substring(1));
        } else {
            chart.setSeriasSQL(getOneSerias(meta, serias, yuns, prefix)+"0");
            chart.addSerias(title);
        }
        return chart;
    }

    /**
     * 获取单个序列的SQL
     * @param serias
     * @param yuns
     * @param prefix
     * @return
     */
    private String getOneSerias(MgrTable tbc, String serias, String yuns, String prefix) {
        serias = serias.trim();
        String serias_type = "";
        IField fld = null;
        if(tbc!=null && tbc.getModel()!=null && tbc.getModel().getId()!=null) {
            fld = tbc.getField(serias);
            if(fld != null) {
                serias_type = fld.getDatatype();
                if(serias_type=="") {
                    return serias = yuns + "(" + prefix + serias + ") as tjs";
                }
            }
        }
        if(fld!=null && fld.getIskey().equals("Y")) {
            yuns = "COUNT";
        } else if(serias_type.endsWith("Integer") ||
                serias_type.endsWith("Long") ||
                serias_type.endsWith("Float") ||
                serias_type.endsWith("Double") ||
                serias_type.startsWith("int") ||
                serias_type.startsWith("long") ||
                serias_type.startsWith("float") ||
                serias_type.startsWith("double") ||
                serias_type.startsWith("number") ||
                serias_type.startsWith("numeric")) {
            if(yuns.equalsIgnoreCase("COUNT")) {
                yuns = "SUM";
            }
        } else {
            yuns = "COUNT";
        }
        return serias = yuns + "(" + prefix + serias + ") as tjs";
    }

    private String getOneSerias(TableMeta meta, String serias, String yuns, String prefix) {
        serias = serias.trim();
        ColumnMeta column = meta.getColumn(serias);
        String serias_type = column.getJavaType();
        if(column.isPrimaryKey()) {
            yuns = "COUNT";
        } else if(serias_type.endsWith("Integer") ||
                serias_type.endsWith("Long") ||
                serias_type.endsWith("Float") ||
                serias_type.endsWith("Double") ||
                serias_type.startsWith("int") ||
                serias_type.startsWith("long") ||
                serias_type.startsWith("float") ||
                serias_type.startsWith("double") ||
                serias_type.startsWith("number") ||
                serias_type.startsWith("numeric")) {
            if(yuns.equalsIgnoreCase("COUNT")) {
                yuns = "SUM";
            }
        } else {
            yuns = "COUNT";
        }
        return serias = yuns + "(" + prefix + serias + ") as tjs";
    }
}
