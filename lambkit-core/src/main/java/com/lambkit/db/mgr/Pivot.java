/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.db.mgr;

import java.io.Serializable;

public class Pivot implements Serializable {
	
	private String prefix;
	private String rowSql;
	private String columnSql;
	private String measureSql;
	private String dataSql;
	private String whereSql;
	private Object[] sqlParas;
	
	private PivotTable table;
	
	/**
	 * 等价
	 * select sum(case when year='2008' and water_system='长江' then pH else null end) as pH from wq_river_section;
	 * select sum(pH) as pH from wq_river_section where year='2008' and water_system='长江';
	 */
	public Pivot() {
		rowSql = "";
		measureSql = "";
		table = new PivotTable();
	}

	public static Pivot by(MgrTable tbc, String row, String column, String measure, String prefix) {
		Pivot pivot = new PivotBuilder().build(tbc, row, column, measure, prefix);
		return pivot;
	}

	public void setSql(MgrTable tbc, String whereSql, Object[] sqlParas) {
		new PivotBuilder().setSql(this, tbc, whereSql, sqlParas);
	}
	
	public PivotTable getTable() {
		return table;
	}

	public void setTable(PivotTable table) {
		this.table = table;
	}

	public String getMeasureSql() {
		return measureSql;
	}
	public void setMeasureSql(String measureSql) {
		this.measureSql = measureSql;
	}

	public String getDataSql() {
		return dataSql;
	}

	public void setDataSql(String dataSql) {
		this.dataSql = dataSql;
	}
	public Object[] getSqlParas() {
		return sqlParas;
	}

	public void setSqlParas(Object[] sqlParas) {
		this.sqlParas = sqlParas;
	}

	public String getRowSql() {
		return rowSql;
	}

	public void setRowSql(String rowSql) {
		this.rowSql = rowSql;
	}

	public String getColumnSql() {
		return columnSql;
	}

	public void setColumnSql(String columnSql) {
		this.columnSql = columnSql;
	}
	
	public String getWhereSql() {
		return whereSql;
	}

	public void setWhereSql(String whereSql) {
		this.whereSql = whereSql;
	}
	
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

}
