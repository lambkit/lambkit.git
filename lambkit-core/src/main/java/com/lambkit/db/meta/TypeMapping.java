package com.lambkit.db.meta;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class TypeMapping {
    protected Map<String, String> map = new HashMap<String, String>(32) {
        {
            this.put("java.util.Date", "java.util.Date");
            this.put("java.sql.Date", "java.util.Date");
            this.put("java.sql.Time", "java.sql.Time");
            this.put("java.sql.Timestamp", "java.util.Date");
            this.put("[B", "byte[]");
            this.put("java.lang.String", "java.lang.String");
            this.put("java.lang.Integer", "java.lang.Integer");
            this.put("java.lang.Long", "java.lang.Long");
            this.put("java.lang.Double", "java.lang.Double");
            this.put("java.lang.Float", "java.lang.Float");
            this.put("java.lang.Boolean", "java.lang.Boolean");
            this.put("java.math.BigDecimal", "java.math.BigDecimal");
            this.put("java.math.BigInteger", "java.math.BigInteger");
            this.put("java.lang.Short", "java.lang.Short");
            this.put("java.lang.Byte", "java.lang.Byte");
            this.put("java.time.LocalDateTime", "java.util.Date");
            this.put("java.time.LocalDate", "java.util.Date");
            this.put("java.time.LocalTime", "java.sql.Time");
        }
    };

    public TypeMapping() {
    }

    public void addMapping(Class<?> from, Class<?> to) {
        this.map.put(from.getName(), to.getName());
    }

    public void addMapping(String from, String to) {
        this.map.put(from, to);
    }

    public void removeMapping(Class<?> from) {
        this.map.remove(from.getName());
    }

    public void removeMapping(String from) {
        this.map.remove(from);
    }

    public String getType(String typeString) {
        return (String)this.map.get(typeString);
    }
}
