package com.lambkit.db;

import com.lambkit.core.config.annotation.PropConfig;

/**
 * @author yangyong(孤竹行)
 */
@PropConfig(prefix="lambkit.db")
public class DbConfig {
    private String name;
    private String type = "mysql";
    private String url;
    private String user;
    private String password;
    private String passwordClassName = "com.lambkit.db.datasource.DataSourcePasswordCracker";
    private String driverClassName = "com.mysql.jdbc.Driver";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordClassName() {
        return passwordClassName;
    }

    public void setPasswordClassName(String passwordClassName) {
        this.passwordClassName = passwordClassName;
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }
}
