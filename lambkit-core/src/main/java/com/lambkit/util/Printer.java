package com.lambkit.util;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Lambkit;

import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class Printer {

    private static Map<String, Boolean> printMap = MapUtil.newConcurrentHashMap();

    public static void print(Object object, String subject, String format, Object... arguments) {
        print(object.getClass(), subject, format, arguments);
    }

    public static void print(Class<?> clazz, String subject, String format, Object... arguments) {
        String msg = StrUtil.format(format, arguments);
        Boolean flag = printMap.get(subject);
        if(flag == null) {
            flag = true;
            String printSubjects = Lambkit.config("lambkit.printer", "none");
            if("all".equals(printSubjects)) {
                flag = false;
            }
            printSubjects = ","+printSubjects.trim()+",";
            if (printSubjects.contains(","+subject+",")) {
                flag = false;
            }
            printMap.put(subject, flag);
        }
        if(flag) {
            System.out.println("[" + clazz.getName() + "]: " + msg);
        }
    }
}
