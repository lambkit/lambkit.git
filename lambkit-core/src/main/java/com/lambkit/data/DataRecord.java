package com.lambkit.data;

import com.lambkit.core.Attr;

/**
 * @author yangyong(孤竹行)
 */
public class DataRecord extends Attr {
    private DataType dataType;

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }
}
