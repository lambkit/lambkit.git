package com.lambkit.plugin.http;

import com.lambkit.core.Lambkit;
import com.lambkit.plugin.smarthttp.SmartHttpApp;

/**
 * @author yangyong(孤竹行)。
 */
public class SmartHttpApplication {
    public static void main(String[] args) {
        Lambkit.start(SmartHttpApp.class, SmartHttpApplication.class, args);
    }
}
