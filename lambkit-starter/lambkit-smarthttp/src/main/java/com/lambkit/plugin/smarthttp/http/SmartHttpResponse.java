package com.lambkit.plugin.smarthttp.http;

import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Lambkit;
import com.lambkit.core.http.ICookie;
import com.lambkit.core.http.IResponse;
import org.smartboot.http.server.HttpResponse;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;

/**
 * @author yangyong(孤竹行)
 */
public class SmartHttpResponse implements IResponse {

    private HttpResponse response;

    public SmartHttpResponse(HttpResponse response) {
        this.response = response;
    }

    @Override
    public Object getSource() {
        return response;
    }

    @Override
    public void addCookie(ICookie cookie) {
        if(cookie instanceof SmartHttpCookie) {
            response.addCookie(((SmartHttpCookie) cookie).getCookie());
        }
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        return response.getOutputStream();
    }

    @Override
    public void redirect(String url, int code) {
        url = getRedirectPath(url);
        this.setHeader("Location", url);
        this.setHttpStatus(code, "Redirecting to " + url);
    }

    public static String getRedirectPath(String location) {
        String ctx = Lambkit.configCenter().getValue("lambkit.web.ctx", "" );
        if (StrUtil.isBlank(ctx)) {
            return location;
        } else if (location.startsWith("/")) {
            return location.startsWith(ctx) ? location : ctx + location.substring(1);
        } else {
            return location;
        }
    }

    @Override
    public void setContentType(String contentType) {
        response.setContentType(contentType);
    }

    @Override
    public String getContentType() {
        return response.getContentType();
    }

    @Override
    public void write(byte[] data) throws IOException {
        response.write(data);
    }

    @Override
    public void close() {
        response.close();
    }

    @Override
    public int getHttpStatus() {
        return response.getHttpStatus();
    }

    @Override
    public void setHttpStatus(int status, String msg) {
        response.setHttpStatus(status, msg);
    }

    @Override
    public void setHeader(String name, String value) {
        response.setHeader(name, value);
    }

    @Override
    public void addHeader(String name, String value) {
        response.addHeader(name, value);
    }

    @Override
    public String getHeader(String name) {
        return response.getHeader(name);
    }

    @Override
    public void setDateHeader(String expires, int i) {
        response.setHeader(expires, String.valueOf(i));
    }

    @Override
    public Collection<String> getHeaders(String name) {
        return response.getHeaders(name);
    }

    @Override
    public Collection<String> getHeaderNames() {
        return response.getHeaderNames();
    }

    @Override
    public void setContentLength(int contentLength) {
        response.setContentLength(contentLength);
    }

    @Override
    public int getContentLength() {
        return response.getContentLength();
    }

}
