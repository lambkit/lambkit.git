package com.lambkit.plugin.smarthttp.http;

import com.lambkit.core.http.ISession;
import com.lambkit.core.Attr;

/**
 * @author yangyong(孤竹行)
 */
public class SmartHttpSession implements ISession {

    private Attr attributes = new Attr();

    @Override
    public Object getAttribute(String key) {
        return attributes.getObj(key);
    }

    @Override
    public void setAttribute(String key, Object value) {
        attributes.set(key, value);
    }

    @Override
    public void removeAttribute(String key) {
        attributes.remove(key);
    }

    @Override
    public void invalidate() {
        attributes.clear();
    }
}
