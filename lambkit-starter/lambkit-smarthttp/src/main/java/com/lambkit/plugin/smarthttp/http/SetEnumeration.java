package com.lambkit.plugin.smarthttp.http;

/**
 * @author yangyong(孤竹行)
 */
import java.util.Enumeration;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Set;

public class SetEnumeration implements Enumeration<String> {
    private final Iterator<String> iterator;

    public SetEnumeration(Set<String> array) {
        if (array == null) {
            throw new IllegalArgumentException("Array cannot be null");
        }

        this.iterator = array.iterator();
    }

    @Override
    public boolean hasMoreElements() {
        return iterator.hasNext();
    }

    @Override
    public String nextElement() {
        if (iterator.hasNext()) {
            return iterator.next();
        } else {
            throw new NoSuchElementException("No more elements in the enumeration");
        }
    }
}
