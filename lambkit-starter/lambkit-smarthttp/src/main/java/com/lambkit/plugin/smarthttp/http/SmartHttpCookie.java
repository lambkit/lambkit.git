package com.lambkit.plugin.smarthttp.http;

import com.lambkit.core.http.ICookie;
import org.smartboot.http.common.Cookie;

/**
 * @author yangyong(孤竹行)
 */
public class SmartHttpCookie implements ICookie {

    private Cookie cookie;

    private String sameSite;
    //public final static String LAX = "Lax";
    public final static String NONE = "None";
    //public final static String STRICT = "Strict";
    //public final static String SET_COOKIE = "Set-Cookie";

    public SmartHttpCookie(String name, String value) {
        this.cookie = new Cookie(name, value);
    }

    public SmartHttpCookie(Cookie cookie) {
        this.cookie = cookie;
    }

    public Cookie getCookie() {
        return cookie;
    }

    @Override
    public String getValue() {
        return cookie.getValue();
    }

    @Override
    public String getName() {
        return cookie.getName();
    }

    @Override
    public void setMaxAge(int maxAgeInSeconds) {
        cookie.setMaxAge(maxAgeInSeconds);
    }

    @Override
    public void setPath(String path) {
        cookie.setPath(path);
    }

    @Override
    public void setDomain(String domain) {
        cookie.setDomain(domain);
    }

    @Override
    public void setHttpOnly(Boolean isHttpOnly) {
        cookie.setHttpOnly(isHttpOnly);
    }

    public String getSameSite() {
        return sameSite;
    }

    public void setSameSite(String sameSite) {
        this.sameSite = sameSite;
        if (NONE.equals(sameSite)) {
            cookie.setSecure(true);
        }
    }
}
