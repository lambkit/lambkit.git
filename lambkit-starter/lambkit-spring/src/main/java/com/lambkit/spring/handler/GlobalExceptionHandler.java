package com.lambkit.spring.handler;

import cn.hutool.core.date.DateUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * 统一异常处理
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    private static final Log log = LogFactory.get();

    private static final String ERROR_MESSAGE = "系统内部错误，请联系管理员！";

    /**
     * 统一异常处理
     * @param request
     * @param response
     * @param exception
     */
    @ExceptionHandler
    public ModelAndView exceptionHandler(HttpServletRequest request, HttpServletResponse response, Exception exception) {
        request.setAttribute("ex", exception);
        ModelAndView modelAndView = null;
        request.setAttribute("status", 500);
        String time = DateUtil.formatDateTime(new Date());
        System.out.println("Lambkit error handler ----------" + time + "-------------------------------");
        boolean ajax = false;
        if (null != request.getHeader("X-Requested-With") && "XMLHttpRequest".equalsIgnoreCase(request.getHeader("X-Requested-With"))) {
            ajax = true;
            modelAndView = new ModelAndView(new MappingJackson2JsonView());
            modelAndView.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            modelAndView.addObject("code", 500);
            modelAndView.addObject("error", exception.getClass().getName());
            modelAndView.addObject("method", request.getMethod());
            modelAndView.addObject("path", request.getRequestURI());
            modelAndView.addObject("timestamp", DateUtil.now());
        } else {
            modelAndView = new ModelAndView("error.html", HttpStatus.INTERNAL_SERVER_ERROR);
        }
//        if (shiroExceptionHandler.errorHandler(request, response, exception, modelAndView)) {
//            // shiro异常处理
//            System.out.println("exception type  ：shiro");
//            System.out.println("exception class ：" + exception.getClass().getName());
//            request.setAttribute("status", 401);
//            if(ajax) {
//                modelAndView.addObject("type", "shiro");
//            }
//            modelAndView.setStatus(HttpStatus.UNAUTHORIZED);
//            return modelAndView;
//        } else {
//            String title = request.getMethod() + " " + request.getRequestURI() + " 统一异常处理：";
//            System.out.println(title + exception.getClass().getName());
//            log.error(title, exception);
//            if(ajax) {
//                request.setAttribute("requestHeader", "ajax");
//                System.out.println("request header  ：ajax");
//            } else {
//                request.setAttribute("requestHeader", "html");
//                System.out.println("request header  ：html");
//            }
//        }

        String title = request.getMethod() + " " + request.getRequestURI() + " 统一异常处理：";
        System.out.println(title + exception.getClass().getName());
        log.error(title, exception);
        if(ajax) {
            request.setAttribute("requestHeader", "ajax");
            System.out.println("request header  ：ajax");
        } else {
            request.setAttribute("requestHeader", "html");
            System.out.println("request header  ：html");
        }
        System.out.println("----------------------------------------------------------------------------------");
        return modelAndView;
    }
}
