package com.lambkit.spring.service;

import com.lambkit.core.service.ResourceFactory;
import com.lambkit.core.service.ResourceService;

/**
 * @author yangyong(孤竹行)
 */
public class SpringResourceFactory implements ResourceFactory {

    private ResourceService resourceService = new SpringResourceService();
    @Override
    public ResourceService getResourceService() {
        return resourceService;
    }
}
