/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.util;

import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Lists;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.lambkit.dao.model.LambkitModel;
import com.lambkit.dao.record.LambkitRecord;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ModelUtils {

	public static Record recordRename(Record record) {
		String[] keys = record.getColumnNames();
		for (String key : keys) {
			String colName = StrUtil.toCamelCase(key);
			if(!colName.equals(key)) {
				record.set(colName, record.get(key));
				record.remove(key);
			}
		}
		return record;
	}

	public static List<Record> recordRename(List<Record> records) {
		for (Record record : records) {
			record = recordRename(record);
		}
		return records;
	}

	public static List<LambkitRecord> modelToPojo(List<LambkitModel<?>> models) {
		if(models==null) {
			return null;
		}
		List<LambkitRecord> result = Lists.newArrayList();
		for(int i=0; i<models.size(); i++) {
			LambkitModel<?> model = models.get(i);
			if(model!=null) {
				result.add(model.toPojo());
			}
		}
		return result;
	}
	
	public static Page<LambkitRecord> modelToPojo(Page<LambkitModel<?>> modelPage) {
		if(modelPage==null) {
			return null;
		}
		List<LambkitRecord> modelList = modelToPojo(modelPage.getList());
		return new Page<LambkitRecord>(modelList, modelPage.getPageNumber(), modelPage.getPageSize(), modelPage.getTotalPage(), modelPage.getTotalRow());
	}
			
	/**
	 * 将Model类转换为Map modelToMap
	 */
	public static Map<String, Object> modelToMap(Model<?> model) {
		/*
		Map<String, Object> map = new HashMap<String, Object>();
		String[] names = model.getAttrNames();
		for (String str : names) {
			map.put(str, model.get(str));
		}
		*/
		Map<String, Object> map = new HashMap<String, Object>(); 
		for(Entry<String, Object> en : model._getAttrsEntrySet())
		{ 
			map.put(en.getKey(), en.getValue()); 
		} 
		return map;
	}

	/**
	 * 将Record转换成Map recordToMap
	 */
	public static Map<String, Object> recordToMap(Record record) {
		/*
		Map<String, Object> map = new HashMap<String, Object>();
		if (record != null) {
			String[] columns = record.getColumnNames();
			for (String col : columns) {
				map.put(col, record.get(col));
			}
		}
		return map;
		*/
		return record.getColumns();
	}
}
