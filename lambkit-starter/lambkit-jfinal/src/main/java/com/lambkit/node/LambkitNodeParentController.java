package com.lambkit.node;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.jfinal.aop.Clear;
import com.lambkit.core.LambkitResult;
import com.lambkit.web.LambkitController;
import com.lambkit.node.c2p.LambkitNodeC2p;
import com.lambkit.node.parent.LambkitNodeP2c;
import com.lambkit.node.parent.LambkitNodeP2s;
import com.lambkit.node.s2p.LambkitNodeS2p;

@Clear
public class LambkitNodeParentController extends LambkitController {
	/**
	 * 子服务节点进行心跳检测
	 */
	public void c2p() {
		if(isPOST()) {
			// 子服务访问父节点
			// 接收节点发送过来的数据
			String data = getRawData();
			if (StrUtil.isBlank(data)) {
				renderJson(LambkitResult.fail().message("参数不正确"));
				return;
			}
			LambkitNodeC2p childNode = JSON.parseObject(data, LambkitNodeC2p.class);
			if (childNode == null) {
				renderJson(LambkitResult.fail().message("参数不正确"));
				return;
			}
			LambkitNodeService service = LambkitNodeManager.me().getService();
			if(service!=null) {
				String time = getHeader("lkntime");
				String sign = getHeader("lknsign");
				boolean flag = service.validationNodeAlive(childNode, time, sign);
				if(!flag) {
					renderJson(LambkitResult.fail().message("校验失败"));
					return;
				}
			}
			LambkitNodeManager.me().addChildNode(childNode);
			if(service!=null) {
				service.childPostAction(childNode);
			}
			// 返回自己节点的信息
			LambkitNode node = LambkitNodeManager.me().getNode();
			if (node != null) {
				LambkitNodeP2c p2c = new LambkitNodeP2c(node);
				renderJson(LambkitResult.success().message("成功").data(p2c));
			} else {
				renderJson(LambkitResult.fail().message("服务器不可用"));
			}
		} else {
			//父节点来访问子节点
			LambkitNode node = LambkitNodeManager.me().getNode();
			if (node != null) {
				LambkitNodeP2c nodeP2c = new LambkitNodeP2c(node);
				renderJson(LambkitResult.success().message("成功").data(nodeP2c));
			} else {
				renderJson(LambkitResult.fail().message("服务器不可用"));
			}
		}
	}

	/**
	 * 兄弟服务节点进行握手监测
	 */
	public void s2p() {
		// 接收节点发送过来的数据
		String data = getRawData();
		if (StrUtil.isBlank(data)) {
			renderJson(LambkitResult.fail().message("fail"));
			return;
		}
		LambkitNodeS2p siblingNode = JSON.parseObject(data, LambkitNodeS2p.class);
		if (siblingNode == null) {
			renderJson(LambkitResult.fail().message("fail"));
			return;
		}
		LambkitNodeService service = LambkitNodeManager.me().getService();
		if(service!=null) {
			String time = getHeader("lkntime");
			String sign = getHeader("lknsign");
			boolean flag = service.validationNodeAlive(siblingNode, time, sign);
			if(!flag) {
				renderJson(LambkitResult.fail().message("校验失败"));
				return;
			}
		}
		LambkitNodeManager.me().setSiblingNodeS2p(siblingNode);
		// 返回自己节点的信息
		LambkitNode node = LambkitNodeManager.me().getNode();
		if (node != null) {
			LambkitNodeBuilder nodeBuilder = new LambkitNodeBuilder();
			LambkitNodeC2p c2p = nodeBuilder.buildC2pNode();
			LambkitNodeP2s p2s = new LambkitNodeP2s(c2p);
			renderJson(LambkitResult.success().message("success").data(p2s));
		} else {
			renderJson(LambkitResult.fail().message("fail"));
		}
	}
}
