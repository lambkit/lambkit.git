package com.lambkit;

import com.lambkit.cache.JFCacheFactory;
import com.lambkit.core.Lambkit;
import com.lambkit.core.LambkitApp;
import com.lambkit.core.LifecycleException;
import com.lambkit.core.pipeline.DefaultPipeLineFactory;
import com.lambkit.core.task.http.HttpWorkCenter;
import com.lambkit.plugin.http.HttpPlugin;
import com.lambkit.service.JFBeanFactory;
import com.lambkit.service.JFResourceFactory;

public class JFinalApp extends LambkitApp {
    public JFinalApp(String name) {
        super(name);
        context().setBeanFactory(new JFBeanFactory());
        context().setPipeLineFactory(new DefaultPipeLineFactory());
        context().setResourceFactory(new JFResourceFactory());
        JFLambkit.setApp(this);
    }

    @Override
    public void start() throws LifecycleException {
        //初始化工作中心
        HttpWorkCenter workCenter = new HttpWorkCenter();
        context().addWorkCenter(workCenter.runOn(this));
        context().addPlugin(new HttpPlugin());
        super.start();
    }

    @Override
    public void postStart() throws LifecycleException {
        super.postStart();
    }

    @Override
    public void stop() throws LifecycleException {
        super.stop();
    }
}
