package com.lambkit.plugin.redis;

import cn.hutool.core.util.StrUtil;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.redis.RedisPlugin;
import com.lambkit.core.Lambkit;
import com.lambkit.core.LifecycleException;
import com.lambkit.core.plugin.Plugin;
import com.lambkit.util.Printer;

public class JFRedisPlugin extends Plugin {

    private RedisPlugin redisPlugin;

    public JFRedisPlugin() {
        RedisConfig config = Lambkit.config(RedisConfig.class);
        String password = StrUtil.isNotBlank(config.getPassword()) ? config.getPassword() : null;
        redisPlugin = new RedisPlugin(config.getDatabase(), config.getAddress(), config.getPort(), config.getTimeout(), password, config.getIndex());
    }

    public JFRedisPlugin(String cacheName, String host) {
        redisPlugin = new RedisPlugin(cacheName, host);
    }

    public JFRedisPlugin(String cacheName, String host, int port) {
        redisPlugin = new RedisPlugin(cacheName, host);
    }

    public JFRedisPlugin(String cacheName, String host, int port, int timeout) {
        redisPlugin = new RedisPlugin(cacheName, host, port, timeout);
    }

    public JFRedisPlugin(String cacheName, String host, int port, int timeout, String password) {
        redisPlugin = new RedisPlugin(cacheName, host, port, timeout, password);
    }

    public JFRedisPlugin(String cacheName, String host, int port, int timeout, String password, int database) {
        redisPlugin = new RedisPlugin(cacheName, host, port, timeout, password, database);
    }

    public JFRedisPlugin(String cacheName, String host, int port, int timeout, String password, int database, String clientName) {
        redisPlugin = new RedisPlugin(cacheName, host, port, timeout, password, database, clientName);
    }

    public JFRedisPlugin(String cacheName, String host, int port, String password) {
        this(cacheName, host, port, 2000, password);
    }

    public JFRedisPlugin(String cacheName, String host, String password) {
        this(cacheName, host, 6379, 2000, password);
    }


    @Override
    public void start() throws LifecycleException {
        redisPlugin.start();
    }

    @Override
    public void stop() throws LifecycleException {
        redisPlugin.stop();
    }
}
