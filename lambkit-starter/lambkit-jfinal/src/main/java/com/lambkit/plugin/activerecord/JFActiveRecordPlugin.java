package com.lambkit.plugin.activerecord;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.Config;
import com.jfinal.plugin.activerecord.IDataSourceProvider;
import com.jfinal.plugin.activerecord.Model;
import com.lambkit.core.LifecycleException;
import com.lambkit.core.plugin.Plugin;

import javax.sql.DataSource;

public class JFActiveRecordPlugin extends Plugin {

    private ActiveRecordPlugin arp;
    public JFActiveRecordPlugin(ActiveRecordPlugin arp) {
        this.arp = arp;
    }

    public JFActiveRecordPlugin(DataSource dataSource) {
        this.arp = new ActiveRecordPlugin(dataSource);
    }

    public JFActiveRecordPlugin(String configName, DataSource dataSource) {
        this.arp = new ActiveRecordPlugin(configName, dataSource);
    }

    public JFActiveRecordPlugin(IDataSourceProvider dataSourceProvider) {
        this.arp = new ActiveRecordPlugin(dataSourceProvider);
    }

    public JFActiveRecordPlugin(String configName, IDataSourceProvider dataSourceProvider) {
        this.arp = new ActiveRecordPlugin(configName, dataSourceProvider);
    }

    public ActiveRecordPlugin getActiveRecordPlugin() {
        return this.arp;
    }

    public Config getConfig() {
        return this.arp.getConfig();
    }

    public boolean addMapping(String tableName, String primaryKey, Class<? extends Model<?>> modelClass) {
        if(arp!=null) {
            arp.addMapping(tableName, primaryKey, modelClass);
            //String configName = arp.getConfig().getName();
            //LambkitManager.me().addMapping(new TableMappingBean(configName, tableName, primaryKey, modelClass.getName()));
            //LambkitTableMapping tableWrapper = TableMappingManager.me().addMapping(configName, tableName, primaryKey, modelClass);
            //DataSourceManager.me().addTable(configName, tableWrapper);
            return true;
        }
        return false;
    }

    public boolean addMapping(String tableName, Class<? extends Model<?>> modelClass) {
        if(arp!=null) {
            arp.addMapping(tableName, modelClass);
            //String configName = arp.getConfig().getName();
            //LambkitManager.me().addMapping(new TableMappingBean(configName, tableName, "", modelClass.getName()));
            //LambkitTableMapping tableWrapper = TableMappingManager.me().addMapping(configName, tableName, "", modelClass);
            //DataSourceManager.me().addTable(configName, tableWrapper);
            return true;
        }
        return false;
    }

    public JFActiveRecordPlugin addSqlTemplate(String sqlTemplate) {
        if(arp!=null) {
            arp.addSqlTemplate(sqlTemplate);
        }
        return this;
    }

    public JFActiveRecordPlugin addSqlTemplate(com.jfinal.template.source.ISource sqlTemplate) {
        if(arp!=null) {
            arp.addSqlTemplate(sqlTemplate);
        }
        return this;
    }

    public JFActiveRecordPlugin setBaseSqlTemplatePath(String baseSqlTemplatePath) {
        if(arp!=null) {
            arp.setBaseSqlTemplatePath(baseSqlTemplatePath);
        }
        return this;
    }

    public void setShowSql(boolean showSql) {
        if(arp!=null) {
            arp.setShowSql(showSql);
        }
    }

    @Override
    public void start() throws LifecycleException {
        if(arp!=null) {
            arp.start();
        }
    }

    @Override
    public void stop() throws LifecycleException {
        super.stop();
        if(arp!=null) {
            arp.stop();
        }
    }
}
