package com.lambkit.undertow;

import com.jfinal.server.undertow.UndertowConfig;
import com.jfinal.server.undertow.WebBuilder;
import io.undertow.servlet.api.DeploymentInfo;

public interface UndertowCallback {

	public void config(UndertowConfig undertowConfig);
	
	public void configFilter(DeploymentInfo deploymentInfo);
	
	public void configBuilder(WebBuilder builder);
}
