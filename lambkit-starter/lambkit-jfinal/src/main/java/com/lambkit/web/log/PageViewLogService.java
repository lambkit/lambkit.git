package com.lambkit.web.log;

import com.lambkit.web.LambkitController;

/**
 * PV访问量日志
 */
public interface PageViewLogService {
    void log(LambkitController controller);
}
