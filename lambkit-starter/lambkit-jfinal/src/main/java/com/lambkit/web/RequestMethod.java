package com.lambkit.web;

public enum RequestMethod {
	GET("get"),
	POST("post"),
	PUT("put"),
	DELETE("delete"),
	ALL("all");

	public String name;

	RequestMethod(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
