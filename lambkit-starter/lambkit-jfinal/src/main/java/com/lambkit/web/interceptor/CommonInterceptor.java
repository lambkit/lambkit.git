/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.web.interceptor;

import cn.hutool.core.util.StrUtil;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

public class CommonInterceptor implements Interceptor {

	@Override
	public void intercept(Invocation inv) {
		
		// 参数CTL_PATH设置
		Controller controller = inv.getController();
		String ctl = inv.getControllerPath();
		// ctl = ctl.endsWith("/") ? ctl : ctl + "/";
		controller.setAttr("ckey", ctl);
		controller.setAttr("akey", inv.getActionKey());
		
		//web配置
//		String webName = controller.getRequest().getHeader("web-name");
//		WebConfig config = StrUtil.isBlank(webName) ? WebManager.me().getDefaultWebConfig() : WebManager.me().getWebConfig(webName);
//		controller.setAttr("web", config);
//		if(StrUtil.isNotBlank(config.getCtx())) {
//			controller.setAttr("ctx", config.getCtx());
//		}
		
		/// 非浏览器请求返回失败
		String userAgent = controller.getRequest().getHeader("user-agent");
		if (!StrUtil.isNotBlank(userAgent)) {
			controller.renderError(404);
		} else {
			inv.invoke();
		}
	}

}
