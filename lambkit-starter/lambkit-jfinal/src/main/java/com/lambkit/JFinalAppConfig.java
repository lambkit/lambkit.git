package com.lambkit;

import com.lambkit.core.Lambkit;
import com.lambkit.core.LifecycleException;
import com.lambkit.core.config.AppConfig;
import com.lambkit.plugin.ehcache.EhcacheConfig;
import com.lambkit.plugin.ehcache.EhcachePlugin;
import com.lambkit.plugin.redis.JFRedisPlugin;
import com.lambkit.plugin.redis.RedisConfig;
import com.lambkit.util.Printer;

/**
 * @author yangyong(孤竹行)
 */
public class JFinalAppConfig extends AppConfig {
    @Override
    public void init() throws LifecycleException {
		Printer.print(this, "starter", "JFinalAppConfig start...");
		EhcacheConfig ehc = Lambkit.config(EhcacheConfig.class);
		if(ehc.isEnable()) {
            getAppContext().addPlugin(new EhcachePlugin());
		}

		RedisConfig rdc = Lambkit.config(RedisConfig.class);
		if(rdc.isEnable()) {
			//分布式session，分布式缓存
			getAppContext().addPlugin(new JFRedisPlugin());
		}
    }
}
