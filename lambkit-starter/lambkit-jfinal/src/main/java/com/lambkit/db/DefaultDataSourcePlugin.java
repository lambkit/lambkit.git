package com.lambkit.db;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

import javax.sql.DataSource;

/**
 * @author yangyong
 */
public class DefaultDataSourcePlugin extends DataSourcePlugin {

	private long lastExecuteCount = 0;
	private long lastTime = 0;

	public DefaultDataSourcePlugin(ActiveRecordPlugin arp) {
		super(arp);
	}

	@Override
	public boolean start() {
		return startArp();
	}

	@Override
	public boolean stop() {
		return stopArp();
	}

	@Override
	public int getDataSourceActiveState() {
		if(getArp()==null) {
			return 0;
		}
		return 1;
	}

	@Override
	public DataSource getDataSource() {
		
		return getArp().getConfig().getDataSource();
	}
	
}
