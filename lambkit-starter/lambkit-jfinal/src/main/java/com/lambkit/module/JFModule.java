package com.lambkit.module;

import com.jfinal.config.*;
import com.jfinal.template.Engine;
import com.lambkit.db.LambkitDataSource;
import com.lambkit.module.IModule;

/**
 * @author yangyong(孤竹行)
 */
public abstract class JFModule extends LambkitModule {

    @Override
    public void init() {}

    @Override
    public void start() {
        onStart();
    }

    @Override
    public void stop() {
        onStop();
    }

    public void configConstant(Constants constants) {}

    public void configRoute(Routes routes) {}

    public void configEngine(Engine engine) {}

    public void configPlugin(Plugins plugins) {}

    public void configMapping(LambkitDataSource lambkitDataSource) {}

    public void configMapping(String name, LambkitDataSource lambkitDataSource) {}

    public void configInterceptor(Interceptors interceptors) {}

    public void configHandler(Handlers handlers) {}

    public void onStart() {}

    public void onStop() {}
}
