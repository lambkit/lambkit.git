package com.lambkit.module;

import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.Plugins;
import com.jfinal.ext.handler.ContextPathHandler;
import com.lambkit.LambkitConsts;
import com.lambkit.node.LambkitNodeManager;
import com.lambkit.web.handler.LambkitHandler;
import com.lambkit.web.interceptor.CommonInterceptor;

/**
 * @author yangyong(孤竹行)
 */
public class DefaultModule extends JFModule {



    public void configInterceptor(Interceptors me) {
        me.add(new CommonInterceptor());
    }

    public void configHandler(Handlers me) {
        me.add(new LambkitHandler());
        //me.add(new ApiMontiorHandler());
        me.add(new ContextPathHandler(LambkitConsts.ATTR_CONTEXT_PATH));
    }

    @Override
    public void onStart() {
        super.onStart();
        LambkitNodeManager.me().initNode();
    }
}
