package com.lambkit.service;

import com.jfinal.aop.Aop;
import com.lambkit.core.service.AbstractBeanFactory;

/**
 * @author yangyong(孤竹行)
 */
public class JFBeanFactory extends AbstractBeanFactory {
    @Override
    public <T> T newInstance(Class<T> clazz) {
        return Aop.get(clazz);
    }
}
