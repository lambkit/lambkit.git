package com.lambkit.cache;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Lambkit;
import com.lambkit.core.cache.ICacheFactory;
import com.lambkit.plugin.redis.RedisCacheImpl;
import com.lambkit.plugin.redis.RedisHashCacheImpl;
import com.lambkit.plugin.ehcache.EhcacheCacheImpl;

import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class JFCacheFactory implements ICacheFactory {

    public static final String TYPE_EHCACHE = "ehcache";
    public static final String TYPE_REDIS = "redis";
    public static final String TYPE_REDIS_HASH = "redisHash";
    public static final String TYPE_EHREDIS = "ehredis";
    public static final String TYPE_NONE_CACHE = "noneCache";

    private String defaultCacheType = null;
    private Map<String, JFCache> caches = MapUtil.newConcurrentHashMap();

    @Override
    public JFCache getCache() {
        if (defaultCacheType == null) {
            CacheConfig config = Lambkit.config(CacheConfig.class);
            defaultCacheType = config.getType();
            JFCache cache = buildCache(config.getType());
            caches.put(defaultCacheType, cache);
        }
        return caches.get(defaultCacheType);
    }

    @Override
    public JFCache getCache(String type) {
        if (StrUtil.isBlank(type)) {
            return getCache();
        }
        String[] cacheKeysStr = type.split(",");
        for (String cacheKey : cacheKeysStr) {
            String isEnable = "false";
            switch (cacheKey) {
                case JFCacheFactory.TYPE_EHCACHE:
                    isEnable = Lambkit.configCenter().getValue("lambkit.ehcache.enable", "false");
                    if ("true".equals(isEnable)) {
                        return new EhcacheCacheImpl();
                    }
                case JFCacheFactory.TYPE_REDIS:
                    isEnable = Lambkit.configCenter().getValue("lambkit.redis.enable", "false");
                    if ("true".equals(isEnable)) {
                        return new RedisCacheImpl();
                    }
                case JFCacheFactory.TYPE_REDIS_HASH:
                    isEnable = Lambkit.configCenter().getValue("lambkit.redis.enable", "false");
                    if ("true".equals(isEnable)) {
                        return new RedisHashCacheImpl();
                    }
            }
        }
        return getCache();
    }

    public Map<String, JFCache> getCaches() {
        return caches;
    }

//    public EhcacheCacheImpl getEhCache() {
//        JFCache cache = caches.get(JFCacheFactory.TYPE_EHCACHE);
//        if (cache == null) {
//            cache = new EhcacheCacheImpl();
//            caches.put(JFCacheFactory.TYPE_EHCACHE, cache);
//        }
//        return (EhcacheCacheImpl) cache;
//    }
//
//    public RedisCacheImpl getRedis() {
//        JFCache cache = caches.get(JFCacheFactory.TYPE_REDIS);
//        if (cache == null) {
//            cache = new RedisCacheImpl();
//            caches.put(JFCacheFactory.TYPE_REDIS, cache);
//        }
//        return (RedisCacheImpl) cache;
//    }
//
//    public RedisCacheImpl getRedis(String name) {
//        String key = JFCacheFactory.TYPE_REDIS + ":" + name;
//        JFCache cache = caches.get(key);
//        if (cache == null) {
//            cache = new RedisCacheImpl(name);
//            caches.put(key, cache);
//        }
//        return (RedisCacheImpl) cache;
//    }
//
//    public RedisHashCacheImpl getRedisHash() {
//        JFCache cache = caches.get(JFCacheFactory.TYPE_REDIS_HASH);
//        if (cache == null) {
//            cache = new RedisHashCacheImpl();
//            caches.put(JFCacheFactory.TYPE_REDIS_HASH, cache);
//        }
//        return (RedisHashCacheImpl) cache;
//    }
//
//    public RedisHashCacheImpl getRedisHash(String name) {
//        String key = JFCacheFactory.TYPE_REDIS_HASH + ":" + name;
//        JFCache cache = caches.get(key);
//        if (cache == null) {
//            cache = new RedisHashCacheImpl(name);
//            caches.put(key, cache);
//        }
//        return (RedisHashCacheImpl) cache;
//    }

    public JFCache buildCache(String type) {
        switch (type) {
            case JFCacheFactory.TYPE_EHCACHE:
                return new EhcacheCacheImpl();
            case JFCacheFactory.TYPE_REDIS:
                return new RedisCacheImpl();
            case JFCacheFactory.TYPE_REDIS_HASH:
                return new RedisHashCacheImpl();
            default:
                return new NoneCacheImpl();
        }
    }
}