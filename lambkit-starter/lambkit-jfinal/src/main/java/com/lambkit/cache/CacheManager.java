package com.lambkit.cache;

import com.lambkit.plugin.redis.RedisCacheImpl;
import com.lambkit.plugin.redis.RedisHashCacheImpl;
import com.lambkit.plugin.ehcache.EhcacheCacheImpl;

import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class CacheManager {
    private static final CacheManager me = new CacheManager();

    private CacheManager() {
    }

    private JFCacheFactory cacheFactory = new JFCacheFactory();

    public static CacheManager me() {
        return me;
    }

    public JFCache getCache() {
        return cacheFactory.getCache();
    }

    public JFCache getCache(String cacheKey) {
        return cacheFactory.getCache(cacheKey);
    }

    public Map<String, JFCache> getCaches() {
        return cacheFactory.getCaches();
    }

//    public EhcacheCacheImpl getEhCache() {
//        return cacheFactory.getEhCache();
//    }
//
//    public RedisCacheImpl getRedis() {
//        return cacheFactory.getRedis();
//    }
//
//    public RedisCacheImpl getRedis(String name) {
//        return cacheFactory.getRedis(name);
//    }
//
//    public RedisHashCacheImpl getRedisHash() {
//        return cacheFactory.getRedisHash();
//    }
//
//    public RedisHashCacheImpl getRedisHash(String name) {
//        return cacheFactory.getRedisHash(name);
//    }

    public JFCache buildCache(String type) {
        return cacheFactory.buildCache(type);
    }
}
