package com.lambkit.plugin.satoken;

import cn.dev33.satoken.dao.SaTokenDao;
import cn.dev33.satoken.util.SaFoxUtil;
import cn.hutool.core.util.StrUtil;
import com.lambkit.LambkitConsts;
import com.lambkit.core.Lambkit;
import com.lambkit.core.cache.ICache;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author yangyong(孤竹行)
 */
public class SaTokenDaoLambkit implements SaTokenDao {

    private ICache cache;
    private String cacheName;

    public SaTokenDaoLambkit(String cacheName, String cacheType) {
        if(StrUtil.isNotBlank(cacheType)) {
            cache = Lambkit.getCache(cacheType);
        } else {
            cache = Lambkit.getCache();
        }
        this.cacheName = cacheName;
    }
    @Override
    public String get(String s) {
        return cache.get(cacheName, s);
    }

    @Override
    public void set(String s, String s1, long l) {
        cache.put(cacheName, s, s1, (int) l);
    }

    @Override
    public void update(String s, String s1) {
        cache.put(cacheName, s, s1);
    }

    @Override
    public void delete(String s) {
        cache.remove(cacheName, s);
    }

    @Override
    public long getTimeout(String s) {
        return cache.getExpire(cacheName, s);
    }

    @Override
    public void updateTimeout(String s, long l) {
        cache.expire(cacheName, s, (int) l);
    }

    @Override
    public Object getObject(String s) {
        return cache.get(cacheName, s);
    }

    @Override
    public void setObject(String s, Object o, long l) {
        cache.put(cacheName, s, o, (int) l);
    }

    @Override
    public void updateObject(String s, Object o) {
        cache.put(cacheName, s, o);
    }

    @Override
    public void deleteObject(String s) {
        cache.remove(cacheName, s);
    }

    @Override
    public long getObjectTimeout(String s) {
        return cache.getExpire(cacheName, s);
    }

    @Override
    public void updateObjectTimeout(String s, long l) {
        cache.expire(cacheName, s, (int) l);
    }

    @Override
    public List<String> searchData(String prefix, String keyword, int start, int size, boolean sortType) {
        List<String> list = cache.getKeys(cacheName, prefix + "*" + keyword + "*");
        return SaFoxUtil.searchList(list, start, size, sortType);
    }
}
