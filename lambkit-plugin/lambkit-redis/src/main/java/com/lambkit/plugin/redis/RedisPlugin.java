package com.lambkit.plugin.redis;

import cn.hutool.db.nosql.redis.RedisDS;
import cn.hutool.setting.Setting;
import com.lambkit.core.Lambkit;
import com.lambkit.core.LifecycleException;
import com.lambkit.core.plugin.Plugin;

/**
 * @author yangyong(孤竹行)
 */
public class RedisPlugin extends Plugin {

    private RedisDS redisDs;

    @Override
    public void start() throws LifecycleException {
        Setting setting = new Setting("config/redis.txt");
        redisDs = RedisDS.create(setting, null);
        Lambkit.context().setAttr("redisDs", redisDs);
    }

    @Override
    public void stop() throws LifecycleException {
        super.stop();
        if(redisDs != null) {
            redisDs.close();
        }
    }
}
