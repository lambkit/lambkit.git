package com.lambkit.plugin.ehcache;

import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Lambkit;
import com.lambkit.core.LifecycleException;
import com.lambkit.core.plugin.Plugin;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.ConfigurationFactory;
import net.sf.ehcache.config.DiskStoreConfiguration;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

/**
 * @author yangyong(孤竹行)
 */
public class EhcachePlugin extends Plugin {
    private static CacheManager cacheManager;
    private String configurationFileName;
    private URL configurationFileURL;
    private InputStream inputStream;
    private Configuration configuration;

    public EhcachePlugin() {
        EhcacheConfig ehc = Lambkit.config(EhcacheConfig.class);
        if(StrUtil.isNotBlank(ehc.getPath())) {
            String ehcacheDiskStorePath = ehc.getPath();
            if("webrootpath".equalsIgnoreCase(ehc.getPath())){
                ehcacheDiskStorePath = Lambkit.context().resourceService().getWebRootPath(Lambkit.app().context().getResourcePathType());
            } else if("classpath".equalsIgnoreCase(ehc.getPath())) {
                ehcacheDiskStorePath = Lambkit.context().resourceService().getRootClassPath(Lambkit.app().context().getResourcePathType());
            }
            File pathFile = new File(ehcacheDiskStorePath, ".ehcache");
            System.out.println(pathFile);
            Configuration cfg = ConfigurationFactory.parseConfiguration();
            cfg.addDiskStore(new DiskStoreConfiguration().path(pathFile.getAbsolutePath()));
            this.configuration = cfg;
        }
    }

    public EhcachePlugin(CacheManager cacheManager) {
        EhcachePlugin.cacheManager = cacheManager;
    }

    public EhcachePlugin(String configurationFileName) {
        this.configurationFileName = configurationFileName;
    }

    public EhcachePlugin(URL configurationFileURL) {
        this.configurationFileURL = configurationFileURL;
    }

    public EhcachePlugin(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public EhcachePlugin(Configuration configuration) {
        this.configuration = configuration;
    }

    private void createCacheManager() {
        if (cacheManager == null) {
            if (this.configurationFileName != null) {
                cacheManager = CacheManager.create(this.configurationFileName);
            } else if (this.configurationFileURL != null) {
                cacheManager = CacheManager.create(this.configurationFileURL);
            } else if (this.inputStream != null) {
                cacheManager = CacheManager.create(this.inputStream);
            } else if (this.configuration != null) {
                cacheManager = CacheManager.create(this.configuration);
            } else {
                cacheManager = CacheManager.create();
            }
        }
    }

    @Override
    public void start() throws LifecycleException {
        this.createCacheManager();
        Lambkit.context().setAttr("ehcache", cacheManager);
    }

    @Override
    public void stop() throws LifecycleException {
        super.stop();
        cacheManager.shutdown();
        cacheManager = null;
    }
}
