package com.lambkit.plugin.qlexpress;

import com.lambkit.core.Lambkit;
import com.lambkit.core.script.IScriptEngine;

import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class QLExpressScriptEngine implements IScriptEngine {
    @Override
    public Object invoke(String scriptText, Map<String, Object> context) throws Exception {
        QLExpressManager qlExpressManager = Lambkit.get(QLExpressManager.class);
        return qlExpressManager.execute(scriptText, context);
    }

    @Override
    public Object invokeMethod(String scriptText, String functionName, Map<String, Object> context) throws Exception {
        QLExpressManager qlExpressManager = Lambkit.get(QLExpressManager.class);
        return qlExpressManager.execute(scriptText, context);
    }
}
