package com.lambkit.dao;

import com.jfinal.plugin.activerecord.IBean;
import com.jfinal.plugin.activerecord.IRow;

public interface ILambkitRow<M> extends IRow<M>, IBean {
}
