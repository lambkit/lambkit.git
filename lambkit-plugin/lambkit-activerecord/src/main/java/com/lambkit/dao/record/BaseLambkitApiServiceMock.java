package com.lambkit.dao.record;

import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.function.Function;

/**
 *  降级实现LambkitApiService抽象类 
 * @author yangyong
 *
 * @param <M>
 */
public abstract class BaseLambkitApiServiceMock<M extends LambkitRecord> implements LambkitApiService<M> {

	@Override
	public M findById(Object idValue) {
		return null;
	}

	@Override
	public M findByPrimaryKey(Object id) {
		return null;
	}

	@Override
	public M findFirst(Example example) {
		
		return null;
	}

	@Override
	public M findFirst(Columns columns) {
		
		return null;
	}

	@Override
	public M findFirst(Columns columns, String orderby) {
		
		return null;
	}

	@Override
	public List<M> find(Example example, Integer count) {
		
		return null;
	}

	@Override
	public List<M> find(Columns columns, Integer count) {
		
		return null;
	}

	@Override
	public List<M> find(Columns columns, String orderby, Integer count) {
		
		return null;
	}

	@Override
	public List<M> find(Example example) {
		
		return null;
	}

	@Override
	public List<M> find(Columns columns) {
		
		return null;
	}

	@Override
	public List<M> find(Columns columns, String orderby) {
		
		return null;
	}

	@Override
	public List<M> findAll() {
		
		return null;
	}

	@Override
	public Page<M> paginate(Integer pageNumber, Integer pageSize, Example example) {
		
		return null;
	}

	@Override
	public Page<M> paginate(Integer pageNumber, Integer pageSize, Columns columns) {
		
		return null;
	}

	@Override
	public Page<M> paginate(Integer pageNumber, Integer pageSize, Columns columns, String orderby) {
		
		return null;
	}

	@Override
	public Page<M> paginate(Example example, Integer offset, Integer limit) {
		
		return null;
	}

	@Override
	public Page<M> paginate(Columns columns, Integer offset, Integer limit) {
		
		return null;
	}

	@Override
	public Page<M> paginate(Columns columns, String orderby, Integer offset, Integer limit) {
		
		return null;
	}

	@Override
	public Long count(Example example) {
		
		return null;
	}

	@Override
	public Long count(Columns columns) {
		
		return null;
	}

	@Override
	public boolean save(M record) {
		
		return false;
	}

	@Override
	public boolean delete(M record) {
		
		return false;
	}

	@Override
	public boolean deleteById(Object idValue) {
		
		return false;
	}

	@Override
	public boolean deleteByIds(Object ...ids) {
		
		return false;
	}

	@Override
	public int delete(Example example) {
		
		return 0;
	}

	@Override
	public int delete(Columns columns) {
		
		return 0;
	}

	@Override
	public int delete(SqlPara sqlPara) {
		return 0;
	}

	@Override
	public boolean update(M record) {
		
		return false;
	}

	@Override
	public int update(M record, Example example) {
		
		return 0;
	}

	@Override
	public int update(M record, Columns columns) {
		
		return 0;
	}

	@Override
	public M findFirst(SqlPara sqlPara) {
		return null;
	}

	@Override
	public List<M> find(SqlPara sqlPara) {
		return null;
	}

	@Override
	public Page<M> paginate(int pageNumber, int pageSize, SqlPara sqlPara) {
		return null;
	}

	@Override
	public int update(SqlPara sqlPara) {
		return 0;
	}

	@Override
	public int update(String sql, Object... paras) {
		return 0;
	}

	@Override
	public int update(String sql) {
		return 0;
	}

	@Override
	public List<M> find(String sql, Object... paras) {
		return null;
	}

	@Override
	public List<M> find(String sql) {
		return null;
	}

	@Override
	public M findFirst(String sql, Object... paras) {
		return null;
	}

	@Override
	public M findFirst(String sql) {
		return null;
	}

	@Override
	public M findByIds(Object... idValues) {
		return null;
	}

	@Override
	public int delete(String sql, Object... paras) {
		return 0;
	}

	@Override
	public int delete(String sql) {
		return 0;
	}

	@Override
	public Page<M> paginate(int pageNumber, int pageSize, String select, String sqlExceptSelect, Object... paras) {
		return null;
	}

	@Override
	public Page<M> paginate(int pageNumber, int pageSize, String select, String sqlExceptSelect) {
		return null;
	}

	@Override
	public boolean tx(IAtom atom) {
		return false;
	}

	@Override
	public boolean tx(int transactionLevel, IAtom atom) {
		return false;
	}

	@Override
	public Future<Boolean> txInNewThread(IAtom atom) {
		return null;
	}

	@Override
	public Future<Boolean> txInNewThread(int transactionLevel, IAtom atom) {
		return null;
	}

	@Override
	public List<M> findByCache(String cacheName, Object key, String sql, Object... paras) {
		return null;
	}

	@Override
	public List<M> findByCache(String cacheName, Object key, String sql) {
		return null;
	}

	@Override
	public M findFirstByCache(String cacheName, Object key, String sql, Object... paras) {
		return null;
	}

	@Override
	public M findFirstByCache(String cacheName, Object key, String sql) {
		return null;
	}

	@Override
	public Page<M> paginateByCache(String cacheName, Object key, int pageNumber, int pageSize, String select, String sqlExceptSelect, Object... paras) {
		return null;
	}

	@Override
	public Page<M> paginateByCache(String cacheName, Object key, int pageNumber, int pageSize, String select, String sqlExceptSelect) {
		return null;
	}

	@Override
	public int[] batch(String sql, Object[][] paras, int batchSize) {
		return new int[0];
	}

	@Override
	public int[] batch(String sql, String columns, List modelOrRecordList, int batchSize) {
		return new int[0];
	}

	@Override
	public int[] batch(List<String> sqlList, int batchSize) {
		return new int[0];
	}

	@Override
	public int[] batchSave(List<? extends M> recordList, int batchSize) {
		return new int[0];
	}

	@Override
	public int[] batchUpdate(List<? extends M> recordList, int batchSize) {
		return new int[0];
	}

	@Override
	public String getSql(String key) {
		return null;
	}

	@Override
	public SqlPara getSqlPara(String key, M record) {
		return null;
	}

	@Override
	public SqlPara getSqlPara(String key, Model model) {
		return null;
	}

	@Override
	public SqlPara getSqlPara(String key, Map data) {
		return null;
	}

	@Override
	public SqlPara getSqlPara(String key, Object... paras) {
		return null;
	}

	@Override
	public SqlPara getSqlParaByString(String content, Map data) {
		return null;
	}

	@Override
	public SqlPara getSqlParaByString(String content, Object... paras) {
		return null;
	}

	@Override
	public void each(Function<M, Boolean> func, String sql, Object... paras) {

	}
}
