package com.lambkit.module;

import com.lambkit.core.plugin.Plugins;
import com.lambkit.db.LambkitDataSource;

/**
 * @author yangyong(孤竹行)
 */
public interface IModule {
    default void init() {}

    default void start() {}

    default void stop() {}

    default void configMapping(LambkitDataSource lambkitDataSource) {}

    default void configMapping(String name, LambkitDataSource lambkitDataSource) {}

    //这里使用activerecord.jar包的Engine和jfinal.jar包的Engine会产生冲突
    //default void configEngine(Engine me) {}

    default void configPlugin(Plugins me) {}
}
