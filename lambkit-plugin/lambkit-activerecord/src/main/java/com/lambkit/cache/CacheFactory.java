package com.lambkit.cache;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Lambkit;
import com.lambkit.core.cache.ICacheFactory;
import com.lambkit.plugin.ehcache.EhcacheCacheImpl;
import com.lambkit.plugin.redis.RedisCacheImpl;
import com.lambkit.plugin.redis.RedisHashCacheImpl;

import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class CacheFactory implements ICacheFactory {

    public static final String TYPE_EHCACHE = "ehcache";
    public static final String TYPE_REDIS = "redis";
    public static final String TYPE_REDIS_HASH = "redisHash";
    public static final String TYPE_EHREDIS = "ehredis";
    public static final String TYPE_NONE_CACHE = "noneCache";

    private String defaultCacheType = null;
    private Map<String, LambkitCache> caches = MapUtil.newConcurrentHashMap();

    @Override
    public LambkitCache getCache() {
        if (defaultCacheType == null) {
            CacheConfig config = Lambkit.config(CacheConfig.class);
            defaultCacheType = config.getType();
            LambkitCache cache = buildCache(config.getType());
            caches.put(defaultCacheType, cache);
        }
        return caches.get(defaultCacheType);
    }

    @Override
    public LambkitCache getCache(String type) {
        if (StrUtil.isBlank(type)) {
            return getCache();
        }
        String[] cacheKeysStr = type.split(",");
        for (String cacheKey : cacheKeysStr) {
            String isEnable = "false";
            switch (cacheKey) {
                case TYPE_EHCACHE:
                    isEnable = Lambkit.configCenter().getValue("lambkit.ehcache.enable", "false");
                    if ("true".equals(isEnable)) {
                        return getEhCache();
                    }
                case TYPE_REDIS:
                    isEnable = Lambkit.configCenter().getValue("lambkit.redis.enable", "false");
                    if ("true".equals(isEnable)) {
                        return getRedis();
                    }
                case TYPE_REDIS_HASH:
                    isEnable = Lambkit.configCenter().getValue("lambkit.redis.enable", "false");
                    if ("true".equals(isEnable)) {
                        return getRedisHash();
                    }
            }
        }
        return getCache();
    }

    public Map<String, LambkitCache> getCaches() {
        return caches;
    }

    public EhcacheCacheImpl getEhCache() {
        LambkitCache cache = caches.get(TYPE_EHCACHE);
        if (cache == null) {
            cache = new EhcacheCacheImpl();
            caches.put(TYPE_EHCACHE, cache);
        }
        return (EhcacheCacheImpl) cache;
    }

    public RedisCacheImpl getRedis() {
        LambkitCache cache = caches.get(TYPE_REDIS);
        if (cache == null) {
            cache = new RedisCacheImpl();
            caches.put(TYPE_REDIS, cache);
        }
        return (RedisCacheImpl) cache;
    }

    public RedisHashCacheImpl getRedisHash() {
        LambkitCache cache = caches.get(TYPE_REDIS_HASH);
        if (cache == null) {
            cache = new RedisHashCacheImpl();
            caches.put(TYPE_REDIS_HASH, cache);
        }
        return (RedisHashCacheImpl) cache;
    }

    public LambkitCache buildCache(String type) {
        switch (type) {
            case TYPE_EHCACHE:
                return new EhcacheCacheImpl();
            case TYPE_REDIS:
                return new RedisCacheImpl();
            case TYPE_REDIS_HASH:
                return new RedisHashCacheImpl();
            default:
                return new NoneCacheImpl();
        }
    }
}
