package com.lambkit.plugin.redis.devops;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Attr;
import com.lambkit.core.cache.ICache;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.io.StringReader;
import java.util.*;

public class MonitorCache {
	
    public Map<String, Object> getRedisInfo(Jedis jedis) {
        Map<String, Object> result = new HashMap<>(3);
        if(jedis==null) {
            return result;
        }
        try {
            // 获取redis缓存完整信息
            String infoStr = jedis.info();
            Properties info = new Properties();
            info.load(new StringReader(infoStr));

            // 获取redis缓存命令统计信息
            String statsStr = jedis.info("commandstats");
            Properties commandStats = new Properties();
            commandStats.load(new StringReader(statsStr));
            List<Map<String, String>> pieList = new ArrayList<>();
            commandStats.stringPropertyNames().forEach(key -> {
                Map<String, String> data = new HashMap<>(2);
                String property = commandStats.getProperty(key);
                data.put("name", StrUtil.removePrefix(key, "cmdstat_"));
                data.put("value", StrUtil.subBetween(property, "calls=", ",usec"));
                pieList.add(data);
            });

            // 获取redis缓存中可用键Key的总数
            Long dbSize = jedis.dbSize();
            result.put("info", info);
            result.put("dbSize", dbSize);
            result.put("commandStats", pieList);
            result.put("commandStatsProp", commandStats);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public List<Attr> getCacheNames(Map<String, ICache> caches) {
        List<Attr> result = CollUtil.newArrayList();
        for (String cacheKey : caches.keySet()) {
            Attr data = Attr.by("type", caches.get(cacheKey).getCacheType());
            data.set("key", cacheKey);
            data.set("names", caches.get(cacheKey).getCacheNames());
            result.add(data);
        }
        return result;
    }

  
    public List getCachekeys(ICache cache, String cacheName) {
        if(cache == null) {
        	return null;
        }
        if(StrUtil.isBlank(cacheName)) {
        	return null;
        }
        cacheName = cacheName.trim();
        return cache!=null ? cache.getKeys(cacheName) : null;
    }

    public <T> T getCacheValue(ICache cache, String cacheName, String key) {
        if(cache == null) {
        	return null;
        }
        if(StrUtil.isBlank(cacheName)) {
        	return null;
        }
        cacheName = cacheName.trim();
        if(StrUtil.isBlank(key)) {
        	return null;
        }
        key = key.trim();
        return cache!=null ? cache.get(cacheName, key) : null;
    }

    public boolean clear(ICache cache, String cacheName, String key) {
        if(cache == null) {
        	return false;
        }
        if(StrUtil.isBlank(cacheName)) {
        	return false;
        }
        cacheName = cacheName.trim();
        if(StrUtil.isBlank(key)) {
            cache.removeAll(cacheName);
        } else {
            key = key.trim();
            cache.remove(cacheName, key);
        }
        return true;
    }

}
