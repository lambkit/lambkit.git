package com.lambkit.plugin.activerecord;

import cn.hutool.log.StaticLog;
import com.jfinal.plugin.activerecord.*;
import com.lambkit.db.*;
import com.lambkit.db.dialect.IDialect;
import com.lambkit.db.hutool.HutoolDb;
import com.lambkit.db.hutool.HutoolDbOpt;
import com.lambkit.db.hutool.RowDataListHandler;
import com.lambkit.db.sql.Example;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

/**
 * @author yangyong(孤竹行)
 */
public class JFinalDbService<T extends IRowData<T>, P extends IPageData<T>> extends JFinalDbOpt<T, P> {
    private Class<T> rowClass;
    private Class<P> pageClass;

    public JFinalDbService(JFinalDb jFinalDb, Class<T> rowClass, Class<P> pageClass)  {
        super(jFinalDb.getDb(), jFinalDb.getDialect());
        this.rowClass = rowClass;
        this.pageClass = pageClass;
    }

    @Override
    public P paginate(Integer pageNumber, Integer pageSize, Sql sql) {
        String[] sqls = PageSqlKit.parsePageSql(sql.getSql());
        String select = sqls[0];
        String sqlExceptSelect = sqls[1];
        return doPaginate(pageNumber, pageSize, (Boolean) null, select, sqlExceptSelect, sql.getPara());
    }

    @Override
    public List<T> find(String sql, Object... paras) {
        Connection conn = null;
        try {
            conn = getConnection();
            return find(getConfig(), conn, sql, paras);
        } catch (Exception e) {
            throw new ActiveRecordException(e);
        } finally {
            closeConnection(conn);
        }
    }

    @Override
    public T findFirst(String sql, Object... paras) {
        List<T> result = this.find(sql, paras);
        return result.size() > 0 ? result.get(0) : null;
//        Connection conn = null;
//        try {
//            conn = getConnection();
//            return findFirst(getConfig(), conn, sql, paras);
//        } catch (Exception e) {
//            throw new ActiveRecordException(e);
//        } finally {
//            closeConnection(conn);
//        }
    }

    protected List<T> find(Config config, Connection conn, String sql, Object... paras) throws SQLException, InstantiationException, IllegalAccessException {
        try (PreparedStatement pst = conn.prepareStatement(sql)) {
            config.getDialect().fillStatement(pst, paras);
            ResultSet rs = pst.executeQuery();
            List<T> result = ResultSetBuilder.of().buildList(rs, true, (Class<T>) rowClass);
            close(rs);
            return result;
        }
    }

    protected T findFirst(Config config, Connection conn, String sql, Object... paras) throws SQLException, InstantiationException, IllegalAccessException {
        try (PreparedStatement pst = conn.prepareStatement(sql)) {
            config.getDialect().fillStatement(pst, paras);
            ResultSet rs = pst.executeQuery();
            T result = ResultSetBuilder.of().build(rs, true, (Class<T>) rowClass);
            close(rs);
            return result;
        }
    }


    protected P doPaginate(int pageNumber, int pageSize, Boolean isGroupBySql, String select, String sqlExceptSelect, Object... paras) {
        Connection conn = null;
        try {
            conn = getConfig().getConnection();
            String totalRowSql = getConfig().getDialect().forPaginateTotalRow(select, sqlExceptSelect, null);
            StringBuilder findSql = new StringBuilder();
            findSql.append(select).append(' ').append(sqlExceptSelect);
            return doPaginateByFullSql(getConfig(), conn, pageNumber, pageSize, isGroupBySql, totalRowSql, findSql, paras);
        } catch (Exception e) {
            throw new ActiveRecordException(e);
        } finally {
            getConfig().close(conn);
        }
    }

    protected P doPaginateByFullSql(Config config, Connection conn, int pageNumber, int pageSize, Boolean isGroupBySql, String totalRowSql, StringBuilder findSql, Object... paras) throws SQLException, InstantiationException, IllegalAccessException {
        if (pageNumber < 1 || pageSize < 1) {
            throw new ActiveRecordException("pageNumber and pageSize must more than 0");
        }
        if (config.getDialect().isTakeOverDbPaginate()) {
            //return getConfig().getDialect().takeOverDbPaginate(conn, pageNumber, pageSize, isGroupBySql, totalRowSql, findSql, paras);
            throw new RuntimeException("You should implements this method in " + config.getDialect().getClass().getName());
        }

        List result = query(config, conn, totalRowSql, paras);
        int size = result.size();
        if (isGroupBySql == null) {
            isGroupBySql = size > 1;
        }

        long totalRow;
        if (isGroupBySql) {
            totalRow = size;
        } else {
            totalRow = (size > 0) ? ((Number)result.get(0)).longValue() : 0;
        }
        if (totalRow == 0) {
            return (P) new PageData<T>(new ArrayList<T>(0), pageNumber, pageSize, 0, 0);
        }

        int totalPage = (int) (totalRow / pageSize);
        if (totalRow % pageSize != 0) {
            totalPage++;
        }

        if (pageNumber > totalPage) {
            return (P) new PageData<T>(new ArrayList<T>(0), pageNumber, pageSize, totalPage, (int)totalRow);
        }

        // --------
        String sql = config.getDialect().forPaginate(pageNumber, pageSize, findSql);
        List<T> list = find(config, conn, sql, paras);
        return (P) new PageData<T>(list, pageNumber, pageSize, totalPage, (int)totalRow);
    }

    protected <T> List<T> query(Config config, Connection conn, String sql, Object... paras) throws SQLException {
        List result = new ArrayList();
        try (PreparedStatement pst = conn.prepareStatement(sql)) {
            config.getDialect().fillStatement(pst, paras);
            ResultSet rs = pst.executeQuery();
            int colAmount = rs.getMetaData().getColumnCount();
            if (colAmount > 1) {
                while (rs.next()) {
                    Object[] temp = new Object[colAmount];
                    for (int i=0; i<colAmount; i++) {
                        temp[i] = rs.getObject(i + 1);
                    }
                    result.add(temp);
                }
            }
            else if(colAmount == 1) {
                while (rs.next()) {
                    result.add(rs.getObject(1));
                }
            }
            close(rs);
            return result;
        }
    }

    public Connection getConnection() throws SQLException {
        return getDb().getConfig().getConnection();
    }

    public void closeConnection(Connection connection) {
        getDb().getConfig().close(connection);
    }

    void close(ResultSet rs, Statement st) throws SQLException {
        if (rs != null) {rs.close();}
        if (st != null) {st.close();}
    }

    void close(ResultSet rs) throws SQLException {
        if (rs != null) {rs.close();}
    }

    void close(Statement st) throws SQLException {
        if (st != null) {st.close();}
    }
}
