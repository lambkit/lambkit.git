package com.lambkit.plugin.redis.mq;

import cn.hutool.core.map.MapUtil;
import com.lambkit.core.mq.Mq;
import com.lambkit.core.mq.Receiver;
import com.lambkit.core.mq.Sender;

import java.util.concurrent.ConcurrentMap;

/**
 * @author yangyong(孤竹行)
 */
public class RedisStreamMq implements Mq {



    private ConcurrentMap<String, RedisStreamSender> senders = MapUtil.newConcurrentHashMap();

    private ConcurrentMap<String, RedisStreamReceiver> receivers = MapUtil.newConcurrentHashMap();

    @Override
    public void addSender(Sender sender) {
        if(sender instanceof RedisStreamSender) {
            senders.put(sender.getName(), (RedisStreamSender) sender);
        }
    }

    @Override
    public void addReceiver(Receiver receiver) {
        if(receiver instanceof RedisStreamReceiver) {
            receivers.put(receiver.getName(), (RedisStreamReceiver) receiver);
        }
    }

    @Override
    public Sender getSender(String name) {
        return senders.get(name);
    }

    @Override
    public Receiver getReceiver(String name) {
        return receivers.get(name);
    }

    @Override
    public void removeSender(String name) {
        senders.remove(name);
    }

    @Override
    public void removeAllSender() {
        senders.clear();
    }

    @Override
    public void removeReceiver(String name) {
        receivers.remove(name);
    }

    @Override
    public void removeAllReceiver() {
        receivers.clear();
    }
}
