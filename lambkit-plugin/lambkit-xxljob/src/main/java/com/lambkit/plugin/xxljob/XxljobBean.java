package com.lambkit.plugin.xxljob;

import java.lang.annotation.*;

/**
 * @author yangyong(孤竹行)
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface XxljobBean {
}
