package com.lambkit.plugin.xxljob;

import com.lambkit.core.config.annotation.PropConfig;

/**
 * @author yangyong(孤竹行)
 */
@PropConfig(prefix = "lambkit.xxljob")
public class XxljobConfig {

    private String adminAddresses;

    private String accessToken;
    //executor appname
    private String appname = "xxl-job-executor-sample";
    //executor registry-address: default use address to registry , otherwise use ip:port if address is null
    private String address;
    private String ip;
    private int port=9998;
    private String logpath="/data/applogs/xxl-job/jobhandler";
    //executor log-retention-days
    private int logretentiondays=30;

    private String scanPackage;

    public String getAdminAddresses() {
        return adminAddresses;
    }

    public void setAdminAddresses(String adminAddresses) {
        this.adminAddresses = adminAddresses;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAppname() {
        return appname;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getLogpath() {
        return logpath;
    }

    public void setLogpath(String logpath) {
        this.logpath = logpath;
    }

    public int getLogretentiondays() {
        return logretentiondays;
    }

    public void setLogretentiondays(int logretentiondays) {
        this.logretentiondays = logretentiondays;
    }

    public String getScanPackage() {
        return scanPackage;
    }

    public void setScanPackage(String scanPackage) {
        this.scanPackage = scanPackage;
    }
}
