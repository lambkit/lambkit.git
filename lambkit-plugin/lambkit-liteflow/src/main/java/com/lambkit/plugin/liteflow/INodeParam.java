package com.lambkit.plugin.liteflow;

import java.util.List;

public interface INodeParam {
    List<INodeParamItem> getItems();
}
