package com.lambkit.plugin.liteflow;

import com.lambkit.core.Lambkit;
import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.flow.FlowBus;

/**
 * @author yangyong(孤竹行)
 */
public class LiteFlowKit {

    private static FlowExecutor defaultFlowExecutor;

    public static FlowExecutor flowExecutor() {
        //return defaultFlowExecutor;
        return Lambkit.get(FlowExecutor.class);
    }

    public static void setFlowExecutor(FlowExecutor flowExecutor) {
        defaultFlowExecutor = flowExecutor;
    }

    /**
     * 主动调用代码全量刷新
     * 1.这样刷新是全量刷新，不过各位同学不用担心其性能，经测试，LiteFlow框架一秒可以刷新1000条规则左右，这都是一些cpu级别的操作，如果你规则没有上大几千，几w条，那么推荐这种方式。
     * 2.如果你的应用是多节点部署的，必须在每个节点上都要刷新，因为规则是存储在jvm内存里的。这就意味着，如果你把刷新规则做成一个rpc接口（诸如dubbo接口之类的），那么rpc接口只会调用到其中一个节点，也就是说，只会有一个节点的规则会刷新。
     * 正确的做法是：利用mq发一个消息，让各个节点去监听到，进行刷新。
     */
    public static void reloadRule() {
        flowExecutor().reloadRule();
    }

    /**
     * 单独刷新某一个规则
     * 既然是指定刷新，那么必须你要获取到改动的EL内容，然后再利用动态代码构建重新build下就可以了，这种方式会自动替换缓存中已有的规则。这种方式不用在build之前销毁流程。
     * 如果是多服务节点部署的情况下，还是要遵循每个节点要都刷新，上面已经说明具体建议的方式。这里不再赘述。
     * @param chain
     * @param elContent
     */
    public static void reloadChain(String chain, String elContent) {
        FlowBus.reloadChain(chain, elContent);
    }

    /**
     * 单独刷新某一个脚本
     * @param nodeId
     * @param scriptContent
     */
    public static void reloadScript(String nodeId, String scriptContent) {
        FlowBus.reloadScript(nodeId, scriptContent);
    }

    /**
     * 销毁一个Chain
     * @param chain
     */
    public static void removeChain(String chain) {
        FlowBus.removeChain(chain);
    }

    public static INodeParam getNodeParam(String subject, String chain) {
        return null;
    }
}
