###create bean,this is a class
#include("/template/java_copyright.include")
package #(basepackage).row;

import com.lambkit.db.RowModel;

#include("/template/java_author.include")
public class #(classname) extends RowModel<#(classname)> {
	public #(classname)() {
		setTableName("#(tableName)");
		setPrimaryKey("#(primaryKey)");
	}
#for(column : columns)
	public #(column.javaType) get#(column.upperName)() {
		return this.get("#(column.name)");
	}
	public void set#(column.upperName)(#(column.javaType) #(column.attrName)) {
		this.set("#(column.name)", #(column.attrName));
	}
#end
}
