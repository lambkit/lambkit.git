##create bean,this is a class
#parse("/template/java_copyright.include")
package $!{basepackage};

import com.lambkit.core.config.annotation.PropConfig;

#parse("/template/java_author.include")
@PropConfig(prefix="lambkit.${propName}")
public class ${moduleName}Config {

	private String serverType = "server";
	private String version = "1.0";
	private String dbPoolName;

	public String getServerType() {
		return serverType;
	}

	public void setServerType(String serverType) {
		this.serverType = serverType;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getDbPoolName() {
		return dbPoolName;
	}

	public void setDbPoolName(String dbPoolName) {
		this.dbPoolName = dbPoolName;
	}
	
}
