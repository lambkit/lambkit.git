package com.lambkit.module;

import com.lambkit.core.AppContext;
import com.lambkit.core.AppLifecycle;
import com.lambkit.core.plugin.Plugins;

/**
 * @author yangyong(孤竹行)
 */
public interface IModule extends AppLifecycle {
    AppContext getAppContext();
}
