package com.lambkit.module;

import com.lambkit.core.AppContext;
import com.lambkit.core.LifecycleException;
import com.lambkit.core.LifecycleState;
import com.lambkit.core.plugin.Plugins;

public abstract class LambkitModule implements IModule {
    private AppContext appContext;
    private LifecycleState currentState = LifecycleState.NEW;

    @Override
    public void setContext(AppContext context) {
        appContext = context;
    }

    @Override
    public AppContext getAppContext() {
        return appContext;
    }

    @Override
    public LifecycleState getCurrentState() {
        return currentState;
    }

    @Override
    public void init() throws LifecycleException {
    }

    @Override
    public void start() throws LifecycleException {

    }

    @Override
    public void stop() throws LifecycleException {

    }
}
