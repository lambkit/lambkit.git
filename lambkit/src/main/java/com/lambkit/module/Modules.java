package com.lambkit.module;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class Modules {

    private List<IModule> modules = new ArrayList<IModule>();

    /**
     * 加入模块
     * @param module
     */
    public void addModule(IModule module) {
        if(module != null) {
            modules.add(module);
        }
    }

    public List<IModule> getModules() {
        return modules;
    }

    public void setModules(List<IModule> modules) {
        this.modules = modules;
    }
}
