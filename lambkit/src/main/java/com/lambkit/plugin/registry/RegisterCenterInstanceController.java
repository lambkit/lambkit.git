package com.lambkit.plugin.registry;

import com.lambkit.core.Lambkit;
import com.lambkit.core.LambkitResult;
import com.lambkit.core.http.Controller;
import com.lambkit.core.registry.ServiceInstance;
import com.lambkit.core.registry.ServiceInstanceList;
import com.lambkit.core.registry.ServiceInstanceListVo;
import com.lambkit.core.registry.ServiceInstanceVo;

/**
 * @author yangyong(孤竹行)
 */
public class RegisterCenterInstanceController extends Controller {

    public void index() {
        if(isPOST()) {
            ServiceInstance serviceInstance = getBean(ServiceInstance.class, null);
            LambkitRegisterCenter registerCenter = Lambkit.get(LambkitRegisterCenter.class);
            boolean flag = registerCenter.createInstance(serviceInstance);
            renderJson(LambkitResult.success().data(flag));

        } else if(isPut()) {
            ServiceInstance serviceInstance = getBean(ServiceInstance.class, null);
            LambkitRegisterCenter registerCenter = Lambkit.get(LambkitRegisterCenter.class);
            boolean flag = registerCenter.updateInstance(serviceInstance);
            renderJson(LambkitResult.success().data(flag));
        } else if (isDelete()) {
            ServiceInstance serviceInstance = getBean(ServiceInstance.class, null);
            LambkitRegisterCenter registerCenter = Lambkit.get(LambkitRegisterCenter.class);
            boolean flag = registerCenter.removeInstance(serviceInstance);
            renderJson(LambkitResult.success().data(flag));
        } else if (isGET()) {
            ServiceInstanceVo serviceInstanceVo = getBean(ServiceInstanceVo.class, null);
            LambkitRegisterCenter registerCenter = Lambkit.get(LambkitRegisterCenter.class);
            ServiceInstance serviceInstance = registerCenter.getInstance(serviceInstanceVo);
            renderJson(LambkitResult.success().data(serviceInstance));
        }
    }

    public void list() {
        if (isGET()) {
            ServiceInstanceListVo serviceInstanceListVo = getBean(ServiceInstanceListVo.class, null);
            LambkitRegisterCenter registerCenter = Lambkit.get(LambkitRegisterCenter.class);
            ServiceInstanceList serviceInstanceList = registerCenter.getInstanceList(serviceInstanceListVo);
            renderJson(LambkitResult.success().data(serviceInstanceList));
        }
    }
}
