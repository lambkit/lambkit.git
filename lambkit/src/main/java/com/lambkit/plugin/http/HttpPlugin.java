package com.lambkit.plugin.http;

import com.lambkit.core.LifecycleException;
import com.lambkit.core.LifecycleState;
import com.lambkit.core.plugin.Plugin;
import com.lambkit.core.task.http.HttpWorkCenter;

/**
 * @author yangyong(孤竹行)
 */
public class HttpPlugin extends Plugin {
    @Override
    public void start() throws LifecycleException {
        setCurrentState(LifecycleState.STARTING);
        HttpWorkCenter workCenter = getAppContext().getWorkCenter(HttpWorkCenter.class);
        if(workCenter != null) {
            workCenter.addHandler(new FirstHttpHandler());
        }
        setCurrentState(LifecycleState.STARTED);
    }
}
