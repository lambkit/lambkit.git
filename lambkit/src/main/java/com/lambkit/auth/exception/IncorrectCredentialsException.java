package com.lambkit.auth.exception;

/**
 * @author yangyong(孤竹行)
 */
public class IncorrectCredentialsException extends AuthException{
    public IncorrectCredentialsException() {
        super();
    }

    public IncorrectCredentialsException(String message) {
        super(message);
    }

    public IncorrectCredentialsException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectCredentialsException(Throwable cause) {
        super(cause);
    }
}
