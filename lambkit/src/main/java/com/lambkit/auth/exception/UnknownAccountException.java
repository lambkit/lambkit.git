package com.lambkit.auth.exception;

/**
 * @author yangyong(孤竹行)
 */
public class UnknownAccountException extends AuthException{
    public UnknownAccountException() {
        super();
    }

    public UnknownAccountException(String message) {
        super(message);
    }

    public UnknownAccountException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownAccountException(Throwable cause) {
        super(cause);
    }
}
