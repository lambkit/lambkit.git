/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.auth;

import com.lambkit.LambkitConsts;
import com.lambkit.core.config.annotation.PropConfig;

@PropConfig(prefix = "lambkit.auth")
public class AuthConfig {
	private String type = LambkitConsts.SERVER;
	private boolean useDefaultMapping;
	// 登录的url
	private String loginUrl = "/login";
	// 登录成功的url
	private String successUrl = "/";
	// 未登录的错误地址url
	private String unauthorizedUrl = null;// "/error/403";
//	// 服务类型，server or client
//	private String serverType = TYPE_SERVER;
//	//serverType=client时，配置server端的sso地址
//	private String serverSsoUrl;
	// 过期时间(秒)
	private int loginExpirationSecond = 3600;// 1hour
	// rememberMe的名称
	private String rememberMeCookieName = "lambkit-auth";
	// 后端提示错误的过期时间(秒)
	private int errorExpirationSecond = 60;// 1分钟
	// 默认请求头标识符
	private String header = "Authorization";
	// 默认token前缀
	private String tokenPrefix = "Lambkit@";
	// 默认私钥
	private String secret = "n1gEgB3/NiGrOxdT9IxWxA==";
	// 缓存名称
	private String cacheName = "authcache";

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isUseDefaultMapping() {
		return useDefaultMapping;
	}

	public void setUseDefaultMapping(boolean useDefaultMapping) {
		this.useDefaultMapping = useDefaultMapping;
	}

	public String getLoginUrl() {
		return loginUrl;
	}

	public void setLoginUrl(String loginUrl) {
		this.loginUrl = loginUrl;
	}

	public String getSuccessUrl() {
		return successUrl;
	}

	public void setSuccessUrl(String successUrl) {
		this.successUrl = successUrl;
	}

	public String getUnauthorizedUrl() {
		return unauthorizedUrl;
	}

	public void setUnauthorizedUrl(String unauthorizedUrl) {
		this.unauthorizedUrl = unauthorizedUrl;
	}

//	public String getServerType() {
//		return serverType;
//	}
//
//	public void setServerType(String serverType) {
//		this.serverType = serverType;
//	}

	public String getRememberMeCookieName() {
		return rememberMeCookieName;
	}

	public void setRememberMeCookieName(String rememberMeCookieName) {
		this.rememberMeCookieName = rememberMeCookieName;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getTokenPrefix() {
		return tokenPrefix;
	}

	public void setTokenPrefix(String tokenPrefix) {
		this.tokenPrefix = tokenPrefix;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getCacheName() {
		return cacheName;
	}

	public void setCacheName(String cacheName) {
		this.cacheName = cacheName;
	}

	public int getLoginExpirationSecond() {
		return loginExpirationSecond;
	}

	public void setLoginExpirationSecond(int loginExpirationSecond) {
		this.loginExpirationSecond = loginExpirationSecond;
	}

	public int getErrorExpirationSecond() {
		return errorExpirationSecond;
	}

	public void setErrorExpirationSecond(int errorExpirationSecond) {
		this.errorExpirationSecond = errorExpirationSecond;
	}

}
