package com.lambkit.auth.sms;

public interface SmsLoginService {

	int sendCode(String phoneTo);
	
	boolean verifyCode(String phoneTo, String verifyCode);
}
