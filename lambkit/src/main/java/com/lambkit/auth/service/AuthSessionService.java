package com.lambkit.auth.service;

import com.lambkit.core.http.IRequest;

/**
 * @author yangyong(孤竹行)
 */
public interface AuthSessionService {
    String getId(IRequest request);
}
