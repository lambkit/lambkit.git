/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.auth.service;

import com.lambkit.auth.AuthUser;

public interface AuthKernelService {
	/**
	 * 登录
	 */
	String login(AuthUser authUser, String password, int loginType);
	/**
	 * client无密认证
	 * @param username
	 */
	String login(AuthUser authUser);
	/**
	 * 退出
	 */
	String logout(String sessionIdOrToken);
	/**
	 * token验证用户
	 * @param sessionIdOrToken
	 * @return
	 */
	String authenticate(String sessionIdOrToken);

	/**
	 * 清空登录用户缓存
	 * @param username
	 */
	void clearCachedAuth(String username);

	AuthUser getAuth(String sessionIdOrToken);

	/**
	 * 已认证通过的用户。不包含已记住的用户，这是与user标签的区别所在。与notAuthenticated搭配使用
	 *
	 * @return 通过身份验证：true，否则false
	 */
	Boolean authenticated();
	/**
	 * 未认证通过用户，与authenticated标签相对应。与guest标签的区别是，该标签包含已记住用户。。
	 *
	 * @return 没有通过身份验证：true，否则false
	 */
	Boolean notAuthenticated();
	/**
	 * 认证通过或已记住的用户。与guset搭配使用。
	 *
	 * @return 用户：true，否则 false
	 */
	Boolean user();
	/**
	 * 验证当前用户是否为“访客”，即未认证（包含未记住）的用户。用user搭配使用
	 *
	 * @return 访客：true，否则false
	 */
	Boolean guest();
	/**
	 * 判断rule权限是否为guest权限
	 * @return
	 */
	Boolean isGuestRule(String permission);
	/**
	 * 验证当前用户是否属于该角色？
	 *
	 * @param roleid
	 *            角色id
	 * @return 属于该角色：true，否则false
	 */
	Boolean hasRole(Long roleid);
	/**
	 * 验证当前用户是否属于该角色？,使用时与lacksRole 搭配使用
	 *
	 * @param roleName
	 *            角色名
	 * @return 属于该角色：true，否则false
	 */
	Boolean hasRole(String roleName);
	/**
	 * 与hasRole标签逻辑相反，当用户不属于该角色时验证通过。
	 *
	 * @param roleName
	 *            角色名
	 * @return 不属于该角色：true，否则false
	 */
	Boolean lacksRole(String roleName);
	/**
	 * 验证当前用户是否属于以下任意一个角色。
	 *
	 * @param roleNames
	 *            角色列表
	 * @return 属于:true,否则false
	 */
	Boolean hasAnyRoles(String roleNames);
	/**
	 * 验证当前用户是否属于以下所有角色。
	 *
	 * @param roleNames
	 *            角色列表
	 * @return 属于:true,否则false
	 */
	Boolean hasAllRoles(String roleNames);
	/**
	 * 验证当前用户是否拥有指定权限
	 * @param ruleid
	 * @return
	 */
	Boolean hasRule(Long ruleid);
	/**
	 * 验证当前用户是否拥有指定权限,使用时与lacksPermission 搭配使用
	 *
	 * @param permission
	 *            权限名
	 * @return 拥有权限：true，否则false
	 */
	Boolean hasRule(String permission);
	/**
	 * 与hasPermission标签逻辑相反，当前用户没有制定权限时，验证通过。
	 *
	 * @param permission
	 *            权限名
	 * @return 拥有权限：true，否则false
	 */
	Boolean lacksRule(String permission);
	/**
	 * 验证当前用户是否属于以下任意一个权限。
	 *
	 * @param permissions
	 *            权限列表
	 * @return 属于:true,否则false
	 */
	Boolean hasAnyRules(String permissions);
	/**
	 * 验证当前用户是否属于以下所有权限。
	 * @return 属于:true,否则false
	 */
	Boolean hasAllRules(String permissions);
}
