/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.auth.service;

import com.lambkit.auth.AuthEntity;
import com.lambkit.auth.AuthUser;
import com.lambkit.auth.cache.IUserCache;

import java.util.List;

public interface AuthDataService {

	IUserCache getCache();
	/**
	 * 将用户密码加密
	 * @param password 明码,未加密
	 * @return
	 */
	String getPasswordSecurity(String username, String password);

	/**
	 * 加密
	 * @param password
	 * @param salt
	 * @return
	 */
	String encrypt(String password, String salt);

	/**
	 *加密
	 * @param password
	 * @param salt
	 * @param type
	 * @return
	 */
	String encrypt(String password, String salt, String type);

	/**
	 * 密码复杂度校验
	 * @param input
	 * @return
	 */
	boolean rexCheckPassword(String input);
	/**
	 * 获取登录的用户信息
	 * 
	 * @param userName
	 * @return
	 */
	AuthUser getAuthUser(String userName);
	/**
	 * 获取登录的用户信息
	 *
	 * @param userName
	 * @return
	 */
	AuthEntity getAuthEntity(String userName);
	/**
	 * 获取角色列表
	 * @return
	 */
	List<?> getRoles(Object userid);
	/**
	 * 获取权限列表
	 * @return
	 */
	List<?> getRules(Object userid);

	Boolean hasRole(Long userId, Long roleId);
	Boolean hasRole(Long userId, String roleName);
	Boolean lacksRole(String roleName);
	Boolean hasAnyRoles(Long userId, String roleNames);
	Boolean hasAllRoles(Long userId, String roleNames);
	Boolean hasRule(Long userId, Long ruleId);
	Boolean hasRule(Long userId, String permission);
	Boolean lacksRule(String permission);
	Boolean hasAnyRules(Long userId, String permissions);
	Boolean hasAllRules(Long userId, String permissions);
}
