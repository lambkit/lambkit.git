/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */package com.lambkit.sms.nim;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.log.StaticLog;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lambkit.core.Attr;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

public class NimMessageKit {

	public static String connect(NimConfig config, String url, Map<String, Object> formMap) throws IOException {
		if (config == null || StrUtil.isBlank(url)) {
			return null;
		}
		String appKey = config.getAppKey();
		String appSecret = config.getAppSecret();
		String nonce = "12345";
		String curTime = String.valueOf((new Date()).getTime() / 1000L);
		String checkSum = CheckSumBuilder.getCheckSum(appSecret, nonce, curTime);

		return HttpRequest.post(url)
				// 设置请求的header
				.header("AppKey", appKey)
				.header("Nonce", nonce)
				.header("CurTime", curTime)
				.header("CheckSum", checkSum)
				.header("Content-Type", "application/x-www-form-urlencoded;charset=utf-8")
				// 设置请求的参数
				.form(formMap)
				// 执行请求
				.execute()
				.body();
	}
	
	/**
	 * 发送验证码短信,
	 * 向指定的手机号码发送短信验证码。
	 * @param config
	 * @param doctorPhoneNumber
	 * @param codeLen
	 * @return
	 * http 响应:json
	 * 发送成功则返回相关信息。msg字段表示此次发送的sendid；obj字段表示此次发送的验证码。
	 */
	public static Attr sendCode(NimConfig config, String doctorPhoneNumber, String codeLen) {
		String url = "https://api.netease.im/sms/sendcode.action/";
		// 设置请求的的参数，requestBody参数
		Map<String, Object> formMap = MapUtil.newHashMap();
        /*
         * 1.如果是模板短信，请注意参数mobile是有s的，详细参数配置请参考“发送模板短信文档”
         * 2.参数格式是jsonArray的格式，例如 "['13888888888','13666666666']"
         * 3.params是根据你模板里面有几个参数，那里面的参数也是jsonArray格式
         */
		formMap.put("templateid", config.getMesssagTemplateId());
		formMap.put("mobiles", doctorPhoneNumber);
		formMap.put("codeLen", codeLen);
		
        Attr result = new Attr();
        try {
			// 执行请求
			String response = connect(config, url, formMap);
			JSONObject responseBody = JSON.parseObject(response);
			result.set("code", responseBody.getInteger("code"));
			if(responseBody.getInteger("code")==200) {
				result.set("msg", responseBody.getString("msg"));
				result.set("obj", responseBody.getString("obj"));
			}
			StaticLog.warn("NimMessageKit[sendCode] sms code is " + responseBody.getInteger("code"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result; 
	}
	
	/**
	 * 发送模板短信请求,
	 * 向手机号发送内容格式预定义的短信，整个短信的内容由模板和变量组成。
	 * @param config
	 * @param doctorPhoneNumber
	 * @param messageContent
	 * @return
	 * http 响应:json
	 * 成功则在obj中返回此次发送的sendid(long),用于查询发送结果
	 */
	public static String sendMessage(NimConfig config, String doctorPhoneNumber, String messageContent) {
		String url = "https://api.netease.im/sms/sendtemplate.action/";
		// + "?templateid=" + TEMPLATEID + "&mobiles=[\"" + doctorPhoneNumber + "\"]" + "&params=" + "[\"" + messageContent + "\"]";
		// 设置请求的的参数，requestBody参数
		Map<String, Object> formMap = MapUtil.newHashMap();
        /*
         * 1.如果是模板短信，请注意参数mobile是有s的，详细参数配置请参考“发送模板短信文档”
         * 2.参数格式是jsonArray的格式，例如 "['13888888888','13666666666']"
         * 3.params是根据你模板里面有几个参数，那里面的参数也是jsonArray格式
         */
        formMap.put("templateid", config.getMesssagTemplateId());
        formMap.put("mobiles", doctorPhoneNumber);
        formMap.put("params", messageContent);
		
        try {
			// 执行请求
			String response = connect(config, url, formMap);
			JSONObject responseBody = JSON.parseObject(response);
			if(responseBody.getInteger("code")==200) {
				return responseBody.getString("obj");
			}
			StaticLog.warn("NimMessageKit[sendMessage] sms code is " + responseBody.getInteger("code"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null; 
	}
	
	/**
	 * 查询通知类和运营类短信发送状态
	 * @param config
	 * @param sendid
	 * @return
	 * http 响应:json
	 * obj中返回JSONArray,格式如下(其中status取值:0-未发送,1-发送成功,2-发送失败,3-反垃圾)：
	 */
	public static Attr queryMessageStatus(NimConfig config, String sendid) {
		String url = "https://api.netease.im/sms/querystatus.action/";
		// + "?templateid=" + TEMPLATEID + "&mobiles=[\"" + doctorPhoneNumber + "\"]" + "&params=" + "[\"" + messageContent + "\"]";
		// 设置请求的的参数，requestBody参数
		Map<String, Object> formMap = MapUtil.newHashMap();
        /*
         * 1.如果是模板短信，请注意参数mobile是有s的，详细参数配置请参考“发送模板短信文档”
         * 2.参数格式是jsonArray的格式，例如 "['13888888888','13666666666']"
         * 3.params是根据你模板里面有几个参数，那里面的参数也是jsonArray格式
         */
        formMap.put("sendid", sendid);
		
        Attr result = new Attr();
        try {
			// 执行请求
			String response = connect(config, url, formMap);
			JSONObject responseBody = JSON.parseObject(response);
			result.set("code", responseBody.getInteger("code"));
			if(responseBody.getInteger("code")==200) {
				JSONObject info = responseBody.getJSONObject("obj");
				result.set("status", info.getInteger("code"));
				result.set("mobile", info.getString("mobile"));
				result.set("updatetime", info.getLong("updatetime"));
			}
			StaticLog.warn("NimMessageKit[sendMessage] sms code is " + responseBody.getInteger("code"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result; 
	}
}
