package com.lambkit.sms.nim;

import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Attr;
import com.lambkit.core.Lambkit;
import com.lambkit.sms.ISms;

public class NimSms implements ISms {
    @Override
    public int sendNoticeSMS(String to, String notice) {
        NimConfig nimConfig = Lambkit.config(NimConfig.class);
        String res = NimMessageKit.sendMessage(nimConfig, to, notice);
        return StrUtil.isBlank(res) ? 0 : 200;
    }

    @Override
    public int sendVerifyLoginSMS(String to) {
        String cacheVerifyCode = Lambkit.getCache().get("SMS_CACHE_PHONE", to);
        if(StrUtil.isNotBlank(cacheVerifyCode)) {
            return 10501;
        }
        // 生成六位验证码
        String code = vcode();
        // 将生成的六位验证码和传进来的手机号码存入缓存，时间5分钟
        Lambkit.getCache().put("SMS_CACHE_PHONE", to, code, 300);

        NimConfig nimConfig = Lambkit.config(NimConfig.class);
        Attr res = NimMessageKit.sendCode(nimConfig, to, this.vcode());
        return res.containsKey("code") ? res.getInt("code") : 0;
    }
}
