/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit;

/**
 * 全局常量
 */
public class LambkitConsts {

	public static final String VERSION = "2.0";

	public static final String SERVER = "server";
	public static final String CLIENT = "client";
    public static final String TOKEN = "lambkit-token";
	/**
	 * 初始化事件
	 */
	public static final String EVENT_INIT = "lambkit:init";
	/**
     * 启动完成事件
     */
    public static final String EVENT_STARTED = "lambkit:started";

	public static final String ATTR_REQUEST = "request";

	public static final String ATTR_CONTEXT_PATH = "ctx";
	
	public static final String ATTR_TEMPLATE_PATH = "TPATH";
	
	/**
	 * 节点缓存名称
	 */
	public static final String NODE_CACHE_NAME = "lambkit-node";

	/**
	 * admin类型web模板类型名称
	 */
	public static final String ADMIN_TEMPLATE_TYPE = "admin";

	/**
	 * portal类型web模板类型名称
	 */
	public static final String PORTAL_TEMPLATE_TYPE = "portal";

	public static final Object[] NULL_PARA_ARRAY = new Object[0];

	/**
	 * session的缓存名称
	 */
	public static final String SESSION_CACHE_NAME = "ehCacheSession";

	/**
	 * ehcache缓存中间件名称
	 */
	public static final String CACHE_EHCACHE = "ehcache";
	/**
     * redis缓存中间件名称
	 */
	public static final String CACHE_REDIS = "redis";

	/**
	 * 短信验证码缓存
	 */
	public static final String SMS_CACHE_PHONE = "SMS_CACHE_PHONE";

}
