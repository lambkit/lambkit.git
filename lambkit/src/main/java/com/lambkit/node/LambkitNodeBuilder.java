package com.lambkit.node;

import com.lambkit.core.Lambkit;
import com.lambkit.node.c2p.LambkitNodeC2p;
import com.lambkit.node.c2p.LambkitNodeC2pConfig;
import com.lambkit.node.c2p.LambkitNodeC2pUrlConfig;
import com.lambkit.node.s2p.LambkitNodeS2p;
import com.lambkit.node.s2p.LambkitNodeS2pConfig;

/**
 * @author yangyong(孤竹行)
 */
public class LambkitNodeBuilder {

    public LambkitNodeC2p buildC2pNode() {
        LambkitNode node = LambkitNodeManager.me().getNode();
        if(node!=null) {
            LambkitNodeC2pConfig config = Lambkit.config(LambkitNodeC2pConfig.class);
            LambkitNodeC2p c2p = new LambkitNodeC2p(node);
            c2p.setKey(config.getKey());
            c2p.setParentNodeUrl(config.getPurl());
            c2p.setLinkType(config.getLinkType());
            LambkitNodeC2pUrlConfig urlConfig = Lambkit.config(LambkitNodeC2pUrlConfig.class);
            NetUrl netUrl = new NetUrl();
            netUrl.setGroup(urlConfig.getGroup());
            netUrl.setDomain(urlConfig.getDomain());
            netUrl.setProtocal(urlConfig.getProtocal());
            //int port = urlConfig.getPort()==0 ? LambkitBeanManager.me().getPort() : urlConfig.getPort();
            int port = urlConfig.getPort();
            netUrl.setPort(port);
            c2p.setUrl(netUrl);
            return c2p;
        }
        return null;
    }

    public LambkitNodeS2p buildS2pNode() {
        LambkitNode node = LambkitNodeManager.me().getNode();
        if(node!=null) {
            LambkitNodeS2pConfig config = Lambkit.config(LambkitNodeS2pConfig.class);
            LambkitNodeS2p s2p = new LambkitNodeS2p(node);
            s2p.setKey(config.getKey());
            s2p.setParentNodeUrl(config.getSurl());
            s2p.setLinkType(config.getLinkType());
            return s2p;
        }
        return null;
    }


}
