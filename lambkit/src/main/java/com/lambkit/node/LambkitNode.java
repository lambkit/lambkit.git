package com.lambkit.node;

import java.io.Serializable;

/**
 * 网络节点
 * 
 * @author yangyong
 *
 */
public class LambkitNode implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private String id;
	/**
	 * 应用名称
	 */
	private String applicationName;
	/**
	 * 主要地址
	 */
	private NetUrl url;
	/**
	 * 树结构的层级
	 */
	private int level = 1;
	
	private String type = "normal";

	public LambkitNode() {

	}

	public LambkitNode(LambkitNode node) {
		this.id = node.getId();
		this.applicationName = node.getApplicationName();
		this.url = node.getUrl();
		this.level = node.getLevel();
		this.type = node.getType();;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public NetUrl getUrl() {
		return url;
	}

	public void setUrl(NetUrl url) {
		this.url = url;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
