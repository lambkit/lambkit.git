package com.lambkit.node;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lambkit.core.Lambkit;
import com.lambkit.node.c2p.LambkitNodeC2p;
import com.lambkit.node.c2p.LambkitNodeC2pConfig;
import com.lambkit.node.parent.LambkitNodeP2c;
import com.lambkit.node.parent.LambkitNodeP2s;
import com.lambkit.node.s2p.LambkitNodeS2p;
import com.lambkit.node.s2p.LambkitNodeS2pConfig;

public class LambkitNodeTask implements Runnable {

	@Override
	public void run() {
		//Printer.print(this, "node"("LambkitNodeTask run……");
		c2p();
		s2p();
	}

	public void c2p() {
		String url = null;
		LambkitNodeC2pConfig c2pConfig = Lambkit.config(LambkitNodeC2pConfig.class);
		String parentNodeUrl = c2pConfig.getPurl();
		if(StrUtil.isNotBlank(parentNodeUrl)) {
			url = parentNodeUrl + LambkitNodeManager.route + "/c2p";
		}
		if(StrUtil.isBlank(url)) {
			//Printer.print(this, "node"("LambkitNodeTask stop. because url is blank.");
			return;
		}

		LambkitNodeService service = LambkitNodeManager.me().getService();
		if(service!=null) {
			service.preChildPostAlive(url);
		}

		String result = null;
		LambkitNode node = LambkitNodeManager.me().getNode();
		if(node!=null && StrUtil.isNotBlank(url)) {
			LambkitNodeBuilder nodeBuilder = new LambkitNodeBuilder();
			LambkitNodeC2p c2p = nodeBuilder.buildC2pNode();
			result = HttpUtil.post(url, JSON.toJSONString(c2p));
			//Printer.print(this, "node"("LambkitNodeTask result: " + result);
			if(StrUtil.isNotBlank(result)) {
				JSONObject jsonObj = JSON.parseObject(result);
				if(jsonObj!=null && jsonObj.getInteger("code")==1 && jsonObj.containsKey("data")) {
					LambkitNodeP2c parentNode = jsonObj.getObject("data", LambkitNodeP2c.class);
					if(parentNode!=null) {
						LambkitNodeManager.me().setParentNodeP2c(parentNode);
					}
				}
			}
		}

		if(service!=null) {
			service.endChildPostAlive(result);
		}
	}

	public void s2p() {
		String url = null;
		LambkitNodeS2pConfig s2pConfig = Lambkit.config(LambkitNodeS2pConfig.class);
		if(StrUtil.isNotBlank(s2pConfig.getSurl())) {
			url = s2pConfig.getSurl() + LambkitNodeManager.route + "/s2p";
		}
		if(StrUtil.isBlank(url)) {
			//Printer.print(this, "node"("LambkitNodeTask stop. because url is blank.");
			return;
		}
		String result = null;
		LambkitNode node = LambkitNodeManager.me().getNode();
		if(node!=null && StrUtil.isNotBlank(url)) {
			LambkitNodeBuilder nodeBuilder = new LambkitNodeBuilder();
			LambkitNodeS2p s2p = nodeBuilder.buildS2pNode();
			result = HttpUtil.post(url, JSON.toJSONString(s2p));
			//Printer.print(this, "node"("LambkitNodeTask result: " + result);
			if(StrUtil.isNotBlank(result)) {
				JSONObject jsonObj = JSON.parseObject(result);
				if(jsonObj!=null && jsonObj.getInteger("code")==1 && jsonObj.containsKey("data")) {
					LambkitNodeP2s parentNode = jsonObj.getObject("data", LambkitNodeP2s.class);
					if(parentNode!=null) {
						//不用保存
					}
				}
			}
		}
	}
}
