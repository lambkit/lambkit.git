package com.lambkit.node.s2p;

import com.lambkit.core.config.annotation.PropConfig;

@PropConfig(prefix = "lambkit.node.s2p")
public class LambkitNodeS2pConfig {
	/**
	 * 父节点
	 */
	private String surl;
	/**
	 * 父子连通情况
	 */
	private int linkType = 0;// 0单向，1双向

	/**
	 * 用户的key
	 */
	private String key;
	/**
	 * 用户的密钥
	 */
	private String secret;

	public String getSurl() {
		return surl;
	}

	public void setSurl(String surl) {
		this.surl = surl;
	}

	public int getLinkType() {
		return linkType;
	}

	public void setLinkType(int linkType) {
		this.linkType = linkType;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}
}
