package com.lambkit.node.s2p;

import com.lambkit.node.LambkitNode;
import com.lambkit.node.NetNode;

/**
 * 网络节点
 * 
 * @author yangyong
 *
 */
public class LambkitNodeS2p extends LambkitNode implements NetNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	/**
	 * 用户的key
	 */
	private String key;
	/**
	 * 父节点
	 */
	private String parentNodeUrl;
	/**
	 * 父子连通情况
	 */
	private int linkType = 1;// 0单向，1双向

	public LambkitNodeS2p() {

	}

	public LambkitNodeS2p(LambkitNode node) {
		super(node);
	}

	public int getLinkType() {
		return linkType;
	}

	public void setLinkType(int linkType) {
		this.linkType = linkType;
	}

	public String getParentNodeUrl() {
		return parentNodeUrl;
	}

	public void setParentNodeUrl(String parentNodeUrl) {
		this.parentNodeUrl = parentNodeUrl;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
