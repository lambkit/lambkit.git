package com.lambkit.node;

import java.io.Serializable;

/**
 * @author yangyong(孤竹行)
 */
public interface NetNode extends Serializable {
    String getId();
    String getApplicationName();

    String getKey();

    NetUrl getUrl();
}
