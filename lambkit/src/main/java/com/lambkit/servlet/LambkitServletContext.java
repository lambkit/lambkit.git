package com.lambkit.servlet;

import com.lambkit.core.http.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class LambkitServletContext extends BaseHttpContext {

    private LambkitServletRequest request;

    private LambkitServletResponse response;

    private String target;

    public LambkitServletContext(HttpServletRequest request, HttpServletResponse response) {
        this.request = new LambkitServletRequest(request);
        this.response = new LambkitServletResponse(response);
        this.target = this.request.getRequestURI();
    }

    @Override
    public IRequest getRequest() {
        return request;
    }

    @Override
    public void setHttpServletRequest(IRequest request) {
        if(request instanceof LambkitServletRequest) {
            this.request = (LambkitServletRequest) request;
            this.target = this.request.getRequestURI();
        }
    }

    @Override
    public void setHttpServletResponse(IResponse response) {
        if(response instanceof LambkitServletResponse) {
            this.response = (LambkitServletResponse) response;
        }
    }

    @Override
    public IResponse getResponse() {
        return response;
    }

    @Override
    public ISession getSession() {
        return request.getSession(false);
    }

    @Override
    public ISession getSession(boolean create) {
        return request.getSession(create);
    }

    @Override
    public void setTarget(String pathNew) {
        target = pathNew;
    }

    @Override
    public ICookie createCookie(String name, String value) {
        return new LambkitServletCookie(name, value);
    }

    @Override
    public void setCookie(String name, String value, int maxAgeInSeconds, String path, String domain, Boolean isHttpOnly, String sameSite) {
        ICookie cookie = new LambkitServletCookie(name, value);
        cookie.setMaxAge(maxAgeInSeconds);
        // set the default path value to "/"
        if (path == null) {
            path = "/";
        }
        cookie.setPath(path);

        if (domain != null) {
            cookie.setDomain(domain);
        }
        if (isHttpOnly != null) {
            cookie.setHttpOnly(isHttpOnly);
        }
        if (sameSite != null) {
            cookie.setSameSite(sameSite);
        }
        response.addCookie(cookie);
    }

    @Override
    public void doSetCookie(String name, String value, int maxAgeInSeconds, String path, String domain, Boolean isHttpOnly) {
        ICookie cookie = new LambkitServletCookie(name, value);
        cookie.setMaxAge(maxAgeInSeconds);
        // set the default path value to "/"
        if (path == null) {
            path = "/";
        }
        cookie.setPath(path);

        if (domain != null) {
            cookie.setDomain(domain);
        }
        if (isHttpOnly != null) {
            cookie.setHttpOnly(isHttpOnly);
        }
        response.addCookie(cookie);
    }

    @Override
    public IUploadFile getFile(String name, String uploadPath, long maxPostSize) {

        return null;
    }

    @Override
    public List<IUploadFile> getFiles(String uploadPath) {

        return null;
    }
}
