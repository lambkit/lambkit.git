package com.lambkit.servlet;

import com.lambkit.core.http.ISession;

import javax.servlet.http.HttpSession;

/**
 * @author yangyong(孤竹行)
 */
public class LambkitServletSession implements ISession {
    private HttpSession session;

    public LambkitServletSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public Object getAttribute(String key) {
        return session.getAttribute(key);
    }

    @Override
    public void setAttribute(String key, Object value) {
        session.setAttribute(key, value);
    }

    @Override
    public void removeAttribute(String key) {
        session.removeAttribute(key);
    }

    @Override
    public void invalidate() {
        session.invalidate();
    }
}
