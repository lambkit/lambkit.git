package com.lambkit.generator;

import com.lambkit.core.Lambkit;
import com.lambkit.db.IDb;
import com.lambkit.db.dialect.IDialect;
import com.lambkit.generator.impl.CommonGenerator;
import com.lambkit.generator.impl.DatabaseGenerator;
import com.lambkit.generator.impl.MgrdbGenerator;
import com.lambkit.util.Printer;

import javax.sql.DataSource;
import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class Generator {
    public static void execute(String templateFolder, String templatePath, Map<String,Object> options, GeneratorConfig config) {
        //创建生成器
        IGenerator generator = create(null, options, config);
        //执行
        if(generator!=null) {
            generator.generate(templateFolder, templatePath, options);
            Printer.print(Generator.class, "generator", "-------over-------");
        } else {
            Printer.print(Generator.class, "generator", "生成器创建失败，请先初始化GeneratorManager!");
        }
    }
    public static void execute(DataSource dataSource, String templateFolder, String templatePath, Map<String,Object> options, GeneratorConfig config) {
        //创建生成器
        IGenerator generator = create(dataSource, options, config);
        //执行
        if(generator!=null) {
            generator.generate(templateFolder, templatePath, options);
            Printer.print(Generator.class, "generator", "-------over-------");
        } else {
            Printer.print(Generator.class, "generator", "生成器创建失败，请先初始化GeneratorManager!");
        }
    }

    public static void execute(IDb db, String templateFolder, String templatePath, Map<String,Object> options, GeneratorConfig config) {
        //创建生成器
        IGenerator generator = createForDb(db, options, config);
        //执行
        if(generator!=null) {
            generator.generate(templateFolder, templatePath, options);
            Printer.print(Generator.class, "generator", "-------over-------");
        } else {
            Printer.print(Generator.class, "generator", "生成器创建失败，请先初始化GeneratorManager!");
        }
    }

    public static IGenerator createGenerator(DataSource dataSource, Map<String,Object> options)  {
        return create(dataSource, options, null);
    }

    public static IGenerator createForDb(IDb db, Map<String,Object> options, GeneratorConfig config)  {
        DataSource dataSource = db.getDataSource();
        IDialect dialect = db.getDialect();
        options.put("dialect", dialect);
        return create(dataSource, options, config);
    }

    public static IGenerator create(DataSource dataSource, Map<String,Object> options, GeneratorConfig config)  {
        //创建生成器
        IGenerator generator = null;
        if(dataSource == null) {
            generator = new CommonGenerator();
        } else {
            String generatortype = options.get("type")==null ? GeneratorType.DB.getValue() : options.get("type").toString();
            GeneratorType type = GeneratorType.valueOf(generatortype.trim().toUpperCase());
            switch (type) {
                case COMMON:
                    generator = new CommonGenerator();
                    break;
                case DB:
                    generator = new DatabaseGenerator();
                    break;
                case MGRDB:
                    generator = new MgrdbGenerator();
                    break;
                default:
                    generator = Lambkit.get(generatortype.trim());
                    break;
            }
        }
        generator.context(dataSource, options, config);
        return generator;
    }
}
