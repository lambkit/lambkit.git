/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.generator.impl;

import com.lambkit.core.Lambkit;
import com.lambkit.db.meta.TableMeta;
import com.lambkit.db.mgr.ITable;
import com.lambkit.db.mgr.MgrConstants;
import com.lambkit.db.mgr.MgrTable;
import com.lambkit.db.mgr.MgrdbService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MgrdbGenerator extends DatabaseGenerator {
	@Override
	public void generate(String templateFolder, String templatePath, Map<String, Object> options) {
		// TODO Auto-generated method stub
		if(context==null) {
			return;
		}
		String where = options.get("where")!=null ? options.get("where").toString() : null;
		List<? extends ITable> tables =  Lambkit.get(MgrdbService.class).getTableDao().findByWhere(where);
		if(tables==null) {
			return;
		}
		boolean genMgrTable = !options.containsKey("genMgrTable") || !"false".equals(options.get("genMgrTable"));
		boolean eachTable = !options.containsKey("eachTable") || !"false".equals(options.get("eachTable").toString());
		if(eachTable) {
			for(ITable tb : tables) {
				String tableName = tb.getName();
				if(!genMgrTable) {
					if(tableName.startsWith("meta_") ||
							"sys_tableconfig".equals(tableName) ||
							"sys_fieldconfig".equals(tableName)) {
						continue;
					}
				}
				Map<String, Object> templateModel = createTableModel(options, tb);
				templateModel.put("tables", tables);
				context.generate(templateModel, templateFolder, templatePath);
			}
		} else {
			List<Map<String, Object>> tablesList = new ArrayList<>();
			for(ITable tb : tables) {
				String tableName = tb.getName();
				if(!genMgrTable) {
					if(tableName.startsWith("meta_") ||
							"sys_tableconfig".equals(tableName) ||
							"sys_fieldconfig".equals(tableName)) {
						continue;
					}
				}
				Map<String, Object> tableItem = createTableModel(options, tb);
				tablesList.add(tableItem);
			}
			Map<String, Object> templateModel = context.createTemplateModel(options);
			templateModel.put("tables", tables);
			templateModel.put("tablelist", tablesList);
			context.generate(templateModel, templateFolder, templatePath);
		}
	}

	@Override
	public void generate(String templateFolder, String templateFilePath, String generatFilePath, Map<String, Object> options) {
		// TODO Auto-generated method stub
		if(context==null) {
			return;
		}
		String where = options.get("where")!=null ? options.get("where").toString() : null;
		List<? extends ITable> tables =  Lambkit.get(MgrdbService.class).getTableDao().findByWhere(where);
		if(tables==null) {
			return;
		}
		boolean genMgrTable = !options.containsKey("genMgrTable") || !"false".equals(options.get("genMgrTable"));
		boolean eachTable = !options.containsKey("eachTable") || !"false".equals(options.get("eachTable").toString());
		if(eachTable) {
			for(ITable tb : tables) {
				String tableName = tb.getName();
				if(!genMgrTable) {
					if(tableName.startsWith("meta_") ||
							"sys_tableconfig".equals(tableName) ||
							"sys_fieldconfig".equals(tableName)) {
						continue;
					}
				}
				Map<String, Object> templateModel = createTableModel(options, tb);
				templateModel.put("tables", tables);
				context.generate(templateModel, templateFolder, templateFilePath, generatFilePath);
			}
		} else {
			List<Map<String, Object>> tablesList = new ArrayList<>();
			for(ITable tb : tables) {
				String tableName = tb.getName();
				if(!genMgrTable) {
					if(tableName.startsWith("meta_") ||
							"sys_tableconfig".equals(tableName) ||
							"sys_fieldconfig".equals(tableName)) {
						continue;
					}
				}
				Map<String, Object> tableItem = createTableModel(options, tb);
				tablesList.add(tableItem);
			}
			Map<String, Object> templateModel = context.createTemplateModel(options);
			templateModel.put("tables", tables);
			templateModel.put("tablelist", tablesList);
			context.generate(templateModel, templateFolder, templateFilePath, generatFilePath);
		}
	}
	
	@Override
	public Object execute(String templateFolder, String templateFilePath, Map<String, Object> options) {
		// TODO Auto-generated method stub
		if(context==null) {
			return null;
		}
		Map<String, TableMeta> tableMetas = getTableMetas();
		Map<String, Object> templateModel = context.createTemplateModel(options);
		templateModel.put("tables", tableMetas.values());
		templateModel.putAll(options);
		return context.execute(templateModel, templateFolder, templateFilePath);
	}

	public Map<String, Object> createTableModel(Map<String, Object> options, ITable tb) {
		Map<String, Object> templateModel = context.createTemplateModel(options);
		MgrTable mgrtb = Lambkit.get(MgrdbService.class).createTable(tb.getName(), MgrConstants.ALL);
		templateModel.put("model", tb);
		templateModel.put("table", mgrtb.getMeta());
		templateModel.put("modelName", mgrtb.getMeta().getModelName());
		templateModel.put("classname", mgrtb.getMeta().getModelName());
		templateModel.put("attrName", mgrtb.getMeta().getAttrName());
		templateModel.put("columns", mgrtb.getMeta().getColumnMetas());
		templateModel.put("tableName", mgrtb.getMeta().getName());
		templateModel.put("tablename", mgrtb.getMeta().getName());
		templateModel.put("primaryKey", mgrtb.getMeta().getPrimaryKey());
		templateModel.put("title", tb.getTitle());
		templateModel.put("fieldList", mgrtb.getFieldList());
		return templateModel;
	}
}
