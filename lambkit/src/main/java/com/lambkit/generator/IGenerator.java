/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.generator;

import com.lambkit.db.IDb;

import javax.sql.DataSource;
import java.util.Map;

public interface IGenerator {

	/**
	 * 环境配置
	 * @param options
	 */
	void context(DataSource dataSource, Map<String,Object> options, GeneratorConfig config);

	void context(IDb db, Map<String,Object> options, GeneratorConfig config);

	/**
	 * 生成文件
	 * @param templatePath
	 * @param options
	 */
	void generate(String templateFolder, String templatePath, Map<String, Object> options);

	void generate(String templateFolder, String templateFilePath, String generatFilePath, Map<String, Object> options);

	/**
	 * 执行模板
	 * @param templateFilePath
	 * @param options
	 * @return
	 */
	Object execute(String templateFolder, String templateFilePath, Map<String, Object> options);
}
