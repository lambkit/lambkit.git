/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.generator.template.impl;

import com.lambkit.util.FileKit;
import com.lambkit.generator.template.TemplateEngine;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.io.StringWriter;
import java.util.Iterator;
import java.util.Map;

public class VelocityTemplateEngine extends TemplateEngine {
	/**
	 * 根据模板生成代码
	 * @return
	 */
	@Override
	public String execute(String templateFolder, String templatePath, String templateType, Map<String, Object> templateModel) {
		// 实例化一个VelocityEngine对象
		VelocityEngine velocityEngine = new VelocityEngine();
		velocityEngine.setProperty(Velocity.INPUT_ENCODING, "UTF-8");
		velocityEngine.setProperty(Velocity.OUTPUT_ENCODING, "UTF-8");

		//println("模板文件夹 templateFolder 为：", templateFolder);
		//println("模板文件 templatePath 为：", templatePath);
		//println("模板文件夹类型 templateType 为：", templateType);

		String filePath = templatePath;

		if("class".equalsIgnoreCase(templateType)) {
			templateFolder = templateFolder.startsWith("class:/") ? templateFolder.substring(6) : templateFolder;
			templateFolder = templateFolder.startsWith("//") ? templateFolder.substring(1) : templateFolder;
			// 设置资源加载器为类路径加载器
			velocityEngine.setProperty("resource.loader", "classpath");
			velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
			filePath = templateFolder + templatePath;
		} else {
			// 设置velocity资源加载方式为file
			velocityEngine.setProperty("resource.loader", "file");
			// 设置velocity资源加载方式为file时的处理类
			velocityEngine.setProperty("class.resource.loader.class",
					"org.apache.velocity.runtime.resource.loader.FileResourceLoader");
			templateFolder = FileKit.replacePath(templateFolder);
			//String respath = getFolderPath(templateFolder);
			//templateFolder = templateFolder.substring(templateFolder.lastIndexOf("/") + 1);
			println("模板地址 file.resource.loader.path：", templateFolder);
			velocityEngine.setProperty("file.resource.loader.path", templateFolder);
		}
		velocityEngine.init();
		//println("模板文件 filePath 为：", filePath);
		Template template = velocityEngine.getTemplate(filePath);
		// 实例化一个VelocityContext
		VelocityContext velocityContext = new VelocityContext();
		//velocityContext.put("bean", bean);
		//velocityContext.put("classname", bean.getName());
		//velocityContext.put("date", getDate());
		Iterator<String> iter = templateModel.keySet().iterator();
		while(iter.hasNext()) {
			String skey = iter.next().toString();
			velocityContext.put(skey, templateModel.get(skey));
		}
		StringWriter stringWriter = new StringWriter();
		// 从vm目录下加载hello.vm模板,在eclipse工程中该vm目录与src目录平级
		// velocityEngine.mergeTemplate("vm/hello.vm", "gbk", velocityContext,
		// stringWriter);
		template.merge(velocityContext, stringWriter);
		return stringWriter.toString();
	}

	@Override
	public String execute(String templateContent, Map<String, Object> templateModel) {
		// 实例化一个VelocityEngine对象
		VelocityEngine velocityEngine = new VelocityEngine();
		velocityEngine.setProperty(Velocity.INPUT_ENCODING, "UTF-8");
		velocityEngine.setProperty(Velocity.OUTPUT_ENCODING, "UTF-8");
		velocityEngine.init();
		// 实例化一个VelocityContext
		VelocityContext velocityContext = new VelocityContext();
		Iterator<String> iter = templateModel.keySet().iterator();
		while(iter.hasNext()) {
			String skey = iter.next().toString();
			velocityContext.put(skey, templateModel.get(skey));
		}
		StringWriter stringWriter = new StringWriter();
		// 转换输出
		velocityEngine.evaluate(velocityContext, stringWriter, "", templateContent); // 关键方法
		return stringWriter.toString();
	}
}
