package com.lambkit.scheduled;

/**
 * @author yangyong(孤竹行)
 */
public interface ScheduleTask extends Runnable {
    abstract void stop();
}
