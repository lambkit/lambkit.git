package com.lambkit.scheduled;

/**
 * @author yangyong(孤竹行)
 */
public class ScheduleTaskInfo {
    String cron;
    Object task;
    boolean daemon;
    boolean enable;

    public ScheduleTaskInfo(String cron, Object task, boolean daemon, boolean enable) {
        this.cron = cron;
        this.task = task;
        this.daemon =daemon;
        this.enable = enable;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public Object getTask() {
        return task;
    }

    public void setTask(Object task) {
        this.task = task;
    }

    public boolean isDaemon() {
        return daemon;
    }

    public void setDaemon(boolean daemon) {
        this.daemon = daemon;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
