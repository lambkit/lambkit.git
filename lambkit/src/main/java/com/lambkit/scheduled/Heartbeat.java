package com.lambkit.scheduled;

import cn.hutool.cron.Scheduler;
import cn.hutool.log.Log;

public class Heartbeat {
	static Log log = Log.get(Heartbeat.class);

	// 执行操作
	private ScheduleTask task;

	private boolean daemon = false;

	private final Scheduler scheduler = new Scheduler();

	public Heartbeat(int fixedRateSecond, ScheduleTask task) {
		String cron = "*/" + fixedRateSecond + " * * * * *";
		scheduler.schedule(cron, task);
		this.task = task;
	}
	
	public void start() {
		scheduler.setMatchSecond(true);
		scheduler.start(daemon);
	}

	public void close() {
		task.stop();
		scheduler.stop(true);
	}

	public boolean isDaemon() {
		return daemon;
	}

	public void setDaemon(boolean daemon) {
		this.daemon = daemon;
	}
}
