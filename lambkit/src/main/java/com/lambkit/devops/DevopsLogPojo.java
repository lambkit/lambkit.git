package com.lambkit.devops;

import com.lambkit.core.Lambkit;
import com.lambkit.core.LambkitConfig;
import com.lambkit.node.LambkitNodeManager;

import java.io.Serializable;

public class DevopsLogPojo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String type = "devops";
	private String name = Lambkit.config(LambkitConfig.class).getName();
	private String node = LambkitNodeManager.me().getNode().getId();
	private String log;
	private String level;
	private long created;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}
}
