package com.lambkit.devops.monitor;

import com.lambkit.core.Lambkit;
import com.lambkit.core.LambkitConfig;
import com.lambkit.node.LambkitNodeManager;

import java.io.Serializable;
import java.util.List;

public class MonitorPojo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String type = "devops";
	private String name = Lambkit.config(LambkitConfig.class).getName();
	private String node = LambkitNodeManager.me().getNode().getId();
	private MonitorServer server;
	private List<MonitorApiPojo> system;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}

	public MonitorServer getServer() {
		return server;
	}

	public void setServer(MonitorServer server) {
		this.server = server;
	}

	public List<MonitorApiPojo> getSystem() {
		return system;
	}

	public void setSystem(List<MonitorApiPojo> system) {
		this.system = system;
	}
}
