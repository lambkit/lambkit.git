package com.lambkit.devops.monitor;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;

public class MonitorApiStatus {

	public int apiRequest(String url) {
        HttpResponse res = HttpRequest.get(url).execute();
        return res.getStatus();
    }

    public int apiRequestPost(String url) {
        HttpResponse res = HttpRequest.post(url).execute();
        return res.getStatus();
    }
}
