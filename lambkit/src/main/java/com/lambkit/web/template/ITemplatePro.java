package com.lambkit.web.template;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public interface ITemplatePro {
    String buildFolder(String templatePath, String templateFolderAbsolutePath);
    /**
     * 找出可以用来渲染的 html 模板
     *
     * @param template
     * @return
     */
    String matchTemplateFile(List<String> htmls, String template, boolean isMoblieBrowser);

    String getFolder(String folder, String mobileTpl, String wechatTpl, HttpServletRequest request);

    String getAbsolutePath(String folder, String templatePath);

    String getWebAbsolutePath(String folder, String templatePath);

    /**
     * 获得某个模块下支持的样式
     * 一般用于在后台设置
     *
     * @param prefix
     * @return
     */
    List<String> getSupportStyles(List<String> htmls, String prefix);

    void uninstall(String folder, String templatePath);
}
