package com.lambkit.web.template;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public interface ITemplateFolderPro {

    List<Template> getInstalledTemplates(String templatePath, String templateType, String templateId);

    Template getTemplate(String templatePath, String templateId);

    /**
     * html生成的时候，获取模板地址
     * @param templateFolder
     * @param request
     * @return
     */
    public String getCurrentWebPath(TemplateFolder templateFolder, HttpServletRequest request);

    public String getCurrentFilePath(TemplateFolder templateFolder, HttpServletRequest request, String file);
}
