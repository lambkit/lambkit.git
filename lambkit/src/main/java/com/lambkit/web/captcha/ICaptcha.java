package com.lambkit.web.captcha;

import com.lambkit.core.http.IController;
import com.lambkit.core.http.IRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ICaptcha {

    void render(IController controller);

    boolean validate(IRequest request, String userInputString);
}
