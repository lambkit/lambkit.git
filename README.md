## 概述

v1.0.6版本之前Lambkit是一个快速开发框架，
v1.1.0版本开始我们选择走极简自研微功能路线。

v2.0版本抛弃框架强依赖，自研通用任务处理框架，采用任务池（定时任务 + 任务队列） + 处理池（管道 + 流程） + 事件池（事件总线 + 事件处理器）的技术架构，并适配SpringBoot、JFinal、Solon等主流框架。

### 技术栈

### 微内核

极简自研微功能路线，完成任务池、处理池和事件池功能，依赖如下：

| 名称         | 作用         |
|------------|------------|
| hutool     | 工具集        |
| fastjson   | json工具     |
| oshi       | 操作系统和硬件信息库 |
| druid      | 数据库连接池     |
| sqlite     | 嵌入式数据库     |
| servlet-api | servlet支持  |

#### 集成技术

技术栈选型原则：1是选用国内开源，2是自研，3是选用国外开源，4是成熟产品

| 名称         | 作用                |
|------------|-------------------|
| smart-http | Web 服务器              |
| undertow   | Web 服务器      |
| druid      | 数据库连接池            |
| ehcache    | 缓存                |
| redis      | 缓存           |
| okhttp     | http工具            |
| guava      | 工具集               |
| hutool     | 工具集               |
| protostuff | pbf工具             |
| fastjson   | json工具            |
| oshi       | 操作系统和硬件信息库        |
| sqlite     | 嵌入式数据库            |
| groovy     | 脚本语言              |
| QLExpress  | 脚本引擎              |
| liteflow   | 规则引擎              |
| zbus       | 消息队列              |
| forest     | HTTP调用API框架       |
| sa-token   | 权限认证框架            |
| xxl-job    | 分布式定时任务           |
| postgresql | 数据库 (可用人大金仓替代)    |
| mysql      | 数据库 (可用PolarDB替代) |


### 适配框架

| 名称    | 介绍                                                                                                                                                                                                                                                         |
|-------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| JFinal | [**国产化**] JFinal是基于Java语言的极速WEB + ORM框架，以其简洁、高效著称。它采用ActiveRecord思想，使得数据库操作变得简单。JFinal还提供了丰富的插件库，支持快速开发。<br/>（开发体验好、JavaWeb极简封装、ActiveRecord和Enjoy模板超级好用）                                                                                                      |
| Solon | [**国产化**] Solon是一个启动迅速、体积小巧的Java Web框架，强调克制、简洁和开放的原则。它支持多种JDK版本，包括JDK8、JDK11、JDK17、JDK21以及GraalVM Native。Solon致力于提供一个更小、更快、更自由的开发体验，其内核大小仅为0.1MB，最小Web开发单位可达0.2MB。这意味着在某些项目切换到Solon后，应用包的大小可以缩减至原来的10%。Solon的设计不基于Servlet，但仍兼容Servlet容器，特别适合需要快速启动和低内存消耗的应用场景。 |
| Spring | Spring生态丰富，可以快速集成多种应用。                                                                                                                                                                                                                                     |
| 自研    | [**流程化开发**] 集成smarthttp和undertow两种服务器                                                                                                                                                                                                                          |

### 集成模块

| 名称         | 作用        |
|------------|-----------|
| UPMS       | 用户与权限管理系统 |
| ARMS       | 应用资源管理系统  |
| BPMS       | 业务流程管理系统  |
| CMS        | 内容管理系统    |
| DMS        | 数据管理系统    |
| OMS        | 运维管理系统    |

#### UPMS 用户与权限管理系统
- 用户管理
- 角色管理
- 权限管理

#### CMS 内容管理系统
- 分类管理
- 文章管理
- 评论管理
- 菜单管理
- 模板管理
- 广告管理
- 友情链接管理
- 

### Lambkit启动方式

**Hello World**

```java
    public class IndexController extends LambkitController {
	public void index() {
		renderText("hello world!");
	}
}
```

```java
    public class TestApplicationStart extends LambkitApplicationConfig {
	@Override
	public void configModule(LambkitModule module) {
		LambkitModule config = new LambkitModule() {
    		@Override
    		public void configRoute(Routes me) {
    			me.add("/", IndexController.class, "");
    		}
		};
		module.addModule(config);
	}
	
	public static void main(String[] args) {
		LambkitApplication.run(TestApplicationStart.class, null);
	}
}
```

右键 TestApplicationStart -> Run As -> Java Application 启动项目

### 模块

#### 模板管理

模板管理模块：支持将html模板文件存放在数据库的sys_template表中
```agsl
    public void configModule(LambkitModule module) {
	module.addModule(new SysTemplateModule());
    }
```

#### 用户与权限管理upms

用户权限管理模块：支持jwt和session两种登录管理方式。
```agsl
    public void configModule(LambkitModule module) {
	module.addModule(new UpmsModule());
    }
```

#### 配置管理setting

系统设置模块：支持系统设置的增删改查
```agsl
    public void configModule(LambkitModule module) {
	module.addModule(new SysSettingModule());
    }
```

#### 数据管理mdms

数据管理模块：支持数据库表结构管理、数据字典管理、数据字典导入导出、数据字典查询、数据字典导入导出
```agsl
    public void configModule(LambkitModule module) {
	module.addModule(new MdmsModule());
    }
```

#### 流程管理bpms

流程管理模块：
```agsl
    public void configModule(LambkitModule module) {
        module.addModule(new BpmsModule());
    }
```

#### 代码生成器

代码生成器模块：支持根据数据库表结构生成代码。
```agsl

```

#### 文件管理

文件管理模块：支持文件上传、文件下载、文件删除、文件列表、文件分片


## 更多支持

**Lambkit 官方网站：[http://www.lambkit.com](http://www.lambkit.com)**




