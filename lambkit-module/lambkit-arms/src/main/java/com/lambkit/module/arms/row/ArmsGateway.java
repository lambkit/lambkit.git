/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.arms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class ArmsGateway extends RowModel<ArmsGateway> {
	public ArmsGateway() {
		setTableName("arms_gateway");
		setPrimaryKey("gid");
	}
	public java.lang.Integer getGid() {
		return this.get("gid");
	}
	public void setGid(java.lang.Integer gid) {
		this.set("gid", gid);
	}
	public java.lang.String getRouteId() {
		return this.get("route_id");
	}
	public void setRouteId(java.lang.String routeId) {
		this.set("route_id", routeId);
	}
	public java.lang.String getType() {
		return this.get("type");
	}
	public void setType(java.lang.String type) {
		this.set("type", type);
	}
	public java.lang.String getUrlpattern() {
		return this.get("urlpattern");
	}
	public void setUrlpattern(java.lang.String urlpattern) {
		this.set("urlpattern", urlpattern);
	}
	public java.lang.String getUri() {
		return this.get("uri");
	}
	public void setUri(java.lang.String uri) {
		this.set("uri", uri);
	}
	public java.lang.Integer getWeight() {
		return this.get("weight");
	}
	public void setWeight(java.lang.Integer weight) {
		this.set("weight", weight);
	}
	public java.lang.String getLoadbalanceType() {
		return this.get("loadbalance_type");
	}
	public void setLoadbalanceType(java.lang.String loadbalanceType) {
		this.set("loadbalance_type", loadbalanceType);
	}
	public java.lang.Integer getRate() {
		return this.get("rate");
	}
	public void setRate(java.lang.Integer rate) {
		this.set("rate", rate);
	}
	public java.lang.Integer getPeriod() {
		return this.get("period");
	}
	public void setPeriod(java.lang.Integer period) {
		this.set("period", period);
	}
	public java.lang.String getLimitType() {
		return this.get("limit_type");
	}
	public void setLimitType(java.lang.String limitType) {
		this.set("limit_type", limitType);
	}
	public java.lang.String getFallbackType() {
		return this.get("fallback_type");
	}
	public void setFallbackType(java.lang.String fallbackType) {
		this.set("fallback_type", fallbackType);
	}
	public java.lang.String getFallbackimpl() {
		return this.get("fallbackimpl");
	}
	public void setFallbackimpl(java.lang.String fallbackimpl) {
		this.set("fallbackimpl", fallbackimpl);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.util.Date getUpdateTime() {
		return this.get("update_time");
	}
	public void setUpdateTime(java.util.Date updateTime) {
		this.set("update_time", updateTime);
	}
}
