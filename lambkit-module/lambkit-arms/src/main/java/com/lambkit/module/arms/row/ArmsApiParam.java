/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.arms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class ArmsApiParam extends RowModel<ArmsApiParam> {
	public ArmsApiParam() {
		setTableName("arms_api_param");
		setPrimaryKey("api_param_id");
	}
	public java.lang.Long getApiParamId() {
		return this.get("api_param_id");
	}
	public void setApiParamId(java.lang.Long apiParamId) {
		this.set("api_param_id", apiParamId);
	}
	public java.lang.Long getApiId() {
		return this.get("api_id");
	}
	public void setApiId(java.lang.Long apiId) {
		this.set("api_id", apiId);
	}
	public java.lang.Long getParamPid() {
		return this.get("param_pid");
	}
	public void setParamPid(java.lang.Long paramPid) {
		this.set("param_pid", paramPid);
	}
	public java.lang.String getParamUseType() {
		return this.get("param_use_type");
	}
	public void setParamUseType(java.lang.String paramUseType) {
		this.set("param_use_type", paramUseType);
	}
	public java.lang.String getParamName() {
		return this.get("param_name");
	}
	public void setParamName(java.lang.String paramName) {
		this.set("param_name", paramName);
	}
	public java.lang.String getParamType() {
		return this.get("param_type");
	}
	public void setParamType(java.lang.String paramType) {
		this.set("param_type", paramType);
	}
	public java.lang.String getParamTypeSecend() {
		return this.get("param_type_secend");
	}
	public void setParamTypeSecend(java.lang.String paramTypeSecend) {
		this.set("param_type_secend", paramTypeSecend);
	}
	public java.lang.Integer getParamIsRequired() {
		return this.get("param_is_required");
	}
	public void setParamIsRequired(java.lang.Integer paramIsRequired) {
		this.set("param_is_required", paramIsRequired);
	}
	public java.lang.Integer getParamLength() {
		return this.get("param_length");
	}
	public void setParamLength(java.lang.Integer paramLength) {
		this.set("param_length", paramLength);
	}
	public java.lang.String getParamValueDefault() {
		return this.get("param_value_default");
	}
	public void setParamValueDefault(java.lang.String paramValueDefault) {
		this.set("param_value_default", paramValueDefault);
	}
	public java.lang.String getParamValueExample() {
		return this.get("param_value_example");
	}
	public void setParamValueExample(java.lang.String paramValueExample) {
		this.set("param_value_example", paramValueExample);
	}
	public java.lang.String getParamDescribe() {
		return this.get("param_describe");
	}
	public void setParamDescribe(java.lang.String paramDescribe) {
		this.set("param_describe", paramDescribe);
	}
	public java.lang.Integer getParamOrders() {
		return this.get("param_orders");
	}
	public void setParamOrders(java.lang.Integer paramOrders) {
		this.set("param_orders", paramOrders);
	}
	public java.lang.Integer getParamStatus() {
		return this.get("param_status");
	}
	public void setParamStatus(java.lang.Integer paramStatus) {
		this.set("param_status", paramStatus);
	}
	public java.util.Date getParamCreateTime() {
		return this.get("param_create_time");
	}
	public void setParamCreateTime(java.util.Date paramCreateTime) {
		this.set("param_create_time", paramCreateTime);
	}
}
