/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.arms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class ArmsApiParamSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static ArmsApiParamSQL of() {
		return new ArmsApiParamSQL();
	}
	
	public static ArmsApiParamSQL by(Column column) {
		ArmsApiParamSQL that = new ArmsApiParamSQL();
		that.add(column);
        return that;
    }

    public static ArmsApiParamSQL by(String name, Object value) {
        return (ArmsApiParamSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("arms_api_param", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsApiParamSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsApiParamSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public ArmsApiParamSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public ArmsApiParamSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsApiParamSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsApiParamSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsApiParamSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsApiParamSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public ArmsApiParamSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public ArmsApiParamSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public ArmsApiParamSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public ArmsApiParamSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public ArmsApiParamSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public ArmsApiParamSQL andApiParamIdIsNull() {
		isnull("api_param_id");
		return this;
	}
	
	public ArmsApiParamSQL andApiParamIdIsNotNull() {
		notNull("api_param_id");
		return this;
	}
	
	public ArmsApiParamSQL andApiParamIdIsEmpty() {
		empty("api_param_id");
		return this;
	}

	public ArmsApiParamSQL andApiParamIdIsNotEmpty() {
		notEmpty("api_param_id");
		return this;
	}
      public ArmsApiParamSQL andApiParamIdEqualTo(java.lang.Long value) {
          addCriterion("api_param_id", value, ConditionMode.EQUAL, "apiParamId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andApiParamIdNotEqualTo(java.lang.Long value) {
          addCriterion("api_param_id", value, ConditionMode.NOT_EQUAL, "apiParamId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andApiParamIdGreaterThan(java.lang.Long value) {
          addCriterion("api_param_id", value, ConditionMode.GREATER_THEN, "apiParamId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andApiParamIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("api_param_id", value, ConditionMode.GREATER_EQUAL, "apiParamId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andApiParamIdLessThan(java.lang.Long value) {
          addCriterion("api_param_id", value, ConditionMode.LESS_THEN, "apiParamId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andApiParamIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("api_param_id", value, ConditionMode.LESS_EQUAL, "apiParamId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andApiParamIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("api_param_id", value1, value2, ConditionMode.BETWEEN, "apiParamId", "java.lang.Long", "Float");
    	  return this;
      }

      public ArmsApiParamSQL andApiParamIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("api_param_id", value1, value2, ConditionMode.NOT_BETWEEN, "apiParamId", "java.lang.Long", "Float");
          return this;
      }
        
      public ArmsApiParamSQL andApiParamIdIn(List<java.lang.Long> values) {
          addCriterion("api_param_id", values, ConditionMode.IN, "apiParamId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andApiParamIdNotIn(List<java.lang.Long> values) {
          addCriterion("api_param_id", values, ConditionMode.NOT_IN, "apiParamId", "java.lang.Long", "Float");
          return this;
      }
	public ArmsApiParamSQL andApiIdIsNull() {
		isnull("api_id");
		return this;
	}
	
	public ArmsApiParamSQL andApiIdIsNotNull() {
		notNull("api_id");
		return this;
	}
	
	public ArmsApiParamSQL andApiIdIsEmpty() {
		empty("api_id");
		return this;
	}

	public ArmsApiParamSQL andApiIdIsNotEmpty() {
		notEmpty("api_id");
		return this;
	}
      public ArmsApiParamSQL andApiIdEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andApiIdNotEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.NOT_EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andApiIdGreaterThan(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.GREATER_THEN, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andApiIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.GREATER_EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andApiIdLessThan(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.LESS_THEN, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andApiIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.LESS_EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andApiIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("api_id", value1, value2, ConditionMode.BETWEEN, "apiId", "java.lang.Long", "Float");
    	  return this;
      }

      public ArmsApiParamSQL andApiIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("api_id", value1, value2, ConditionMode.NOT_BETWEEN, "apiId", "java.lang.Long", "Float");
          return this;
      }
        
      public ArmsApiParamSQL andApiIdIn(List<java.lang.Long> values) {
          addCriterion("api_id", values, ConditionMode.IN, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andApiIdNotIn(List<java.lang.Long> values) {
          addCriterion("api_id", values, ConditionMode.NOT_IN, "apiId", "java.lang.Long", "Float");
          return this;
      }
	public ArmsApiParamSQL andParamPidIsNull() {
		isnull("param_pid");
		return this;
	}
	
	public ArmsApiParamSQL andParamPidIsNotNull() {
		notNull("param_pid");
		return this;
	}
	
	public ArmsApiParamSQL andParamPidIsEmpty() {
		empty("param_pid");
		return this;
	}

	public ArmsApiParamSQL andParamPidIsNotEmpty() {
		notEmpty("param_pid");
		return this;
	}
      public ArmsApiParamSQL andParamPidEqualTo(java.lang.Long value) {
          addCriterion("param_pid", value, ConditionMode.EQUAL, "paramPid", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamPidNotEqualTo(java.lang.Long value) {
          addCriterion("param_pid", value, ConditionMode.NOT_EQUAL, "paramPid", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamPidGreaterThan(java.lang.Long value) {
          addCriterion("param_pid", value, ConditionMode.GREATER_THEN, "paramPid", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamPidGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("param_pid", value, ConditionMode.GREATER_EQUAL, "paramPid", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamPidLessThan(java.lang.Long value) {
          addCriterion("param_pid", value, ConditionMode.LESS_THEN, "paramPid", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamPidLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("param_pid", value, ConditionMode.LESS_EQUAL, "paramPid", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamPidBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("param_pid", value1, value2, ConditionMode.BETWEEN, "paramPid", "java.lang.Long", "Float");
    	  return this;
      }

      public ArmsApiParamSQL andParamPidNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("param_pid", value1, value2, ConditionMode.NOT_BETWEEN, "paramPid", "java.lang.Long", "Float");
          return this;
      }
        
      public ArmsApiParamSQL andParamPidIn(List<java.lang.Long> values) {
          addCriterion("param_pid", values, ConditionMode.IN, "paramPid", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamPidNotIn(List<java.lang.Long> values) {
          addCriterion("param_pid", values, ConditionMode.NOT_IN, "paramPid", "java.lang.Long", "Float");
          return this;
      }
	public ArmsApiParamSQL andParamUseTypeIsNull() {
		isnull("param_use_type");
		return this;
	}
	
	public ArmsApiParamSQL andParamUseTypeIsNotNull() {
		notNull("param_use_type");
		return this;
	}
	
	public ArmsApiParamSQL andParamUseTypeIsEmpty() {
		empty("param_use_type");
		return this;
	}

	public ArmsApiParamSQL andParamUseTypeIsNotEmpty() {
		notEmpty("param_use_type");
		return this;
	}
       public ArmsApiParamSQL andParamUseTypeLike(java.lang.String value) {
    	   addCriterion("param_use_type", value, ConditionMode.FUZZY, "paramUseType", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsApiParamSQL andParamUseTypeNotLike(java.lang.String value) {
          addCriterion("param_use_type", value, ConditionMode.NOT_FUZZY, "paramUseType", "java.lang.String", "Float");
          return this;
      }
      public ArmsApiParamSQL andParamUseTypeEqualTo(java.lang.String value) {
          addCriterion("param_use_type", value, ConditionMode.EQUAL, "paramUseType", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamUseTypeNotEqualTo(java.lang.String value) {
          addCriterion("param_use_type", value, ConditionMode.NOT_EQUAL, "paramUseType", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamUseTypeGreaterThan(java.lang.String value) {
          addCriterion("param_use_type", value, ConditionMode.GREATER_THEN, "paramUseType", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamUseTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("param_use_type", value, ConditionMode.GREATER_EQUAL, "paramUseType", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamUseTypeLessThan(java.lang.String value) {
          addCriterion("param_use_type", value, ConditionMode.LESS_THEN, "paramUseType", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamUseTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("param_use_type", value, ConditionMode.LESS_EQUAL, "paramUseType", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamUseTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("param_use_type", value1, value2, ConditionMode.BETWEEN, "paramUseType", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiParamSQL andParamUseTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("param_use_type", value1, value2, ConditionMode.NOT_BETWEEN, "paramUseType", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiParamSQL andParamUseTypeIn(List<java.lang.String> values) {
          addCriterion("param_use_type", values, ConditionMode.IN, "paramUseType", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamUseTypeNotIn(List<java.lang.String> values) {
          addCriterion("param_use_type", values, ConditionMode.NOT_IN, "paramUseType", "java.lang.String", "String");
          return this;
      }
	public ArmsApiParamSQL andParamNameIsNull() {
		isnull("param_name");
		return this;
	}
	
	public ArmsApiParamSQL andParamNameIsNotNull() {
		notNull("param_name");
		return this;
	}
	
	public ArmsApiParamSQL andParamNameIsEmpty() {
		empty("param_name");
		return this;
	}

	public ArmsApiParamSQL andParamNameIsNotEmpty() {
		notEmpty("param_name");
		return this;
	}
       public ArmsApiParamSQL andParamNameLike(java.lang.String value) {
    	   addCriterion("param_name", value, ConditionMode.FUZZY, "paramName", "java.lang.String", "String");
    	   return this;
      }

      public ArmsApiParamSQL andParamNameNotLike(java.lang.String value) {
          addCriterion("param_name", value, ConditionMode.NOT_FUZZY, "paramName", "java.lang.String", "String");
          return this;
      }
      public ArmsApiParamSQL andParamNameEqualTo(java.lang.String value) {
          addCriterion("param_name", value, ConditionMode.EQUAL, "paramName", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamNameNotEqualTo(java.lang.String value) {
          addCriterion("param_name", value, ConditionMode.NOT_EQUAL, "paramName", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamNameGreaterThan(java.lang.String value) {
          addCriterion("param_name", value, ConditionMode.GREATER_THEN, "paramName", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("param_name", value, ConditionMode.GREATER_EQUAL, "paramName", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamNameLessThan(java.lang.String value) {
          addCriterion("param_name", value, ConditionMode.LESS_THEN, "paramName", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("param_name", value, ConditionMode.LESS_EQUAL, "paramName", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("param_name", value1, value2, ConditionMode.BETWEEN, "paramName", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiParamSQL andParamNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("param_name", value1, value2, ConditionMode.NOT_BETWEEN, "paramName", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiParamSQL andParamNameIn(List<java.lang.String> values) {
          addCriterion("param_name", values, ConditionMode.IN, "paramName", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamNameNotIn(List<java.lang.String> values) {
          addCriterion("param_name", values, ConditionMode.NOT_IN, "paramName", "java.lang.String", "String");
          return this;
      }
	public ArmsApiParamSQL andParamTypeIsNull() {
		isnull("param_type");
		return this;
	}
	
	public ArmsApiParamSQL andParamTypeIsNotNull() {
		notNull("param_type");
		return this;
	}
	
	public ArmsApiParamSQL andParamTypeIsEmpty() {
		empty("param_type");
		return this;
	}

	public ArmsApiParamSQL andParamTypeIsNotEmpty() {
		notEmpty("param_type");
		return this;
	}
       public ArmsApiParamSQL andParamTypeLike(java.lang.String value) {
    	   addCriterion("param_type", value, ConditionMode.FUZZY, "paramType", "java.lang.String", "String");
    	   return this;
      }

      public ArmsApiParamSQL andParamTypeNotLike(java.lang.String value) {
          addCriterion("param_type", value, ConditionMode.NOT_FUZZY, "paramType", "java.lang.String", "String");
          return this;
      }
      public ArmsApiParamSQL andParamTypeEqualTo(java.lang.String value) {
          addCriterion("param_type", value, ConditionMode.EQUAL, "paramType", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamTypeNotEqualTo(java.lang.String value) {
          addCriterion("param_type", value, ConditionMode.NOT_EQUAL, "paramType", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamTypeGreaterThan(java.lang.String value) {
          addCriterion("param_type", value, ConditionMode.GREATER_THEN, "paramType", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("param_type", value, ConditionMode.GREATER_EQUAL, "paramType", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamTypeLessThan(java.lang.String value) {
          addCriterion("param_type", value, ConditionMode.LESS_THEN, "paramType", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("param_type", value, ConditionMode.LESS_EQUAL, "paramType", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("param_type", value1, value2, ConditionMode.BETWEEN, "paramType", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiParamSQL andParamTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("param_type", value1, value2, ConditionMode.NOT_BETWEEN, "paramType", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiParamSQL andParamTypeIn(List<java.lang.String> values) {
          addCriterion("param_type", values, ConditionMode.IN, "paramType", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamTypeNotIn(List<java.lang.String> values) {
          addCriterion("param_type", values, ConditionMode.NOT_IN, "paramType", "java.lang.String", "String");
          return this;
      }
	public ArmsApiParamSQL andParamTypeSecendIsNull() {
		isnull("param_type_secend");
		return this;
	}
	
	public ArmsApiParamSQL andParamTypeSecendIsNotNull() {
		notNull("param_type_secend");
		return this;
	}
	
	public ArmsApiParamSQL andParamTypeSecendIsEmpty() {
		empty("param_type_secend");
		return this;
	}

	public ArmsApiParamSQL andParamTypeSecendIsNotEmpty() {
		notEmpty("param_type_secend");
		return this;
	}
       public ArmsApiParamSQL andParamTypeSecendLike(java.lang.String value) {
    	   addCriterion("param_type_secend", value, ConditionMode.FUZZY, "paramTypeSecend", "java.lang.String", "String");
    	   return this;
      }

      public ArmsApiParamSQL andParamTypeSecendNotLike(java.lang.String value) {
          addCriterion("param_type_secend", value, ConditionMode.NOT_FUZZY, "paramTypeSecend", "java.lang.String", "String");
          return this;
      }
      public ArmsApiParamSQL andParamTypeSecendEqualTo(java.lang.String value) {
          addCriterion("param_type_secend", value, ConditionMode.EQUAL, "paramTypeSecend", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamTypeSecendNotEqualTo(java.lang.String value) {
          addCriterion("param_type_secend", value, ConditionMode.NOT_EQUAL, "paramTypeSecend", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamTypeSecendGreaterThan(java.lang.String value) {
          addCriterion("param_type_secend", value, ConditionMode.GREATER_THEN, "paramTypeSecend", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamTypeSecendGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("param_type_secend", value, ConditionMode.GREATER_EQUAL, "paramTypeSecend", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamTypeSecendLessThan(java.lang.String value) {
          addCriterion("param_type_secend", value, ConditionMode.LESS_THEN, "paramTypeSecend", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamTypeSecendLessThanOrEqualTo(java.lang.String value) {
          addCriterion("param_type_secend", value, ConditionMode.LESS_EQUAL, "paramTypeSecend", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamTypeSecendBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("param_type_secend", value1, value2, ConditionMode.BETWEEN, "paramTypeSecend", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiParamSQL andParamTypeSecendNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("param_type_secend", value1, value2, ConditionMode.NOT_BETWEEN, "paramTypeSecend", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiParamSQL andParamTypeSecendIn(List<java.lang.String> values) {
          addCriterion("param_type_secend", values, ConditionMode.IN, "paramTypeSecend", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamTypeSecendNotIn(List<java.lang.String> values) {
          addCriterion("param_type_secend", values, ConditionMode.NOT_IN, "paramTypeSecend", "java.lang.String", "String");
          return this;
      }
	public ArmsApiParamSQL andParamIsRequiredIsNull() {
		isnull("param_is_required");
		return this;
	}
	
	public ArmsApiParamSQL andParamIsRequiredIsNotNull() {
		notNull("param_is_required");
		return this;
	}
	
	public ArmsApiParamSQL andParamIsRequiredIsEmpty() {
		empty("param_is_required");
		return this;
	}

	public ArmsApiParamSQL andParamIsRequiredIsNotEmpty() {
		notEmpty("param_is_required");
		return this;
	}
      public ArmsApiParamSQL andParamIsRequiredEqualTo(java.lang.Integer value) {
          addCriterion("param_is_required", value, ConditionMode.EQUAL, "paramIsRequired", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamIsRequiredNotEqualTo(java.lang.Integer value) {
          addCriterion("param_is_required", value, ConditionMode.NOT_EQUAL, "paramIsRequired", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamIsRequiredGreaterThan(java.lang.Integer value) {
          addCriterion("param_is_required", value, ConditionMode.GREATER_THEN, "paramIsRequired", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamIsRequiredGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("param_is_required", value, ConditionMode.GREATER_EQUAL, "paramIsRequired", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamIsRequiredLessThan(java.lang.Integer value) {
          addCriterion("param_is_required", value, ConditionMode.LESS_THEN, "paramIsRequired", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamIsRequiredLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("param_is_required", value, ConditionMode.LESS_EQUAL, "paramIsRequired", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamIsRequiredBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("param_is_required", value1, value2, ConditionMode.BETWEEN, "paramIsRequired", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsApiParamSQL andParamIsRequiredNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("param_is_required", value1, value2, ConditionMode.NOT_BETWEEN, "paramIsRequired", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsApiParamSQL andParamIsRequiredIn(List<java.lang.Integer> values) {
          addCriterion("param_is_required", values, ConditionMode.IN, "paramIsRequired", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamIsRequiredNotIn(List<java.lang.Integer> values) {
          addCriterion("param_is_required", values, ConditionMode.NOT_IN, "paramIsRequired", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsApiParamSQL andParamLengthIsNull() {
		isnull("param_length");
		return this;
	}
	
	public ArmsApiParamSQL andParamLengthIsNotNull() {
		notNull("param_length");
		return this;
	}
	
	public ArmsApiParamSQL andParamLengthIsEmpty() {
		empty("param_length");
		return this;
	}

	public ArmsApiParamSQL andParamLengthIsNotEmpty() {
		notEmpty("param_length");
		return this;
	}
      public ArmsApiParamSQL andParamLengthEqualTo(java.lang.Integer value) {
          addCriterion("param_length", value, ConditionMode.EQUAL, "paramLength", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamLengthNotEqualTo(java.lang.Integer value) {
          addCriterion("param_length", value, ConditionMode.NOT_EQUAL, "paramLength", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamLengthGreaterThan(java.lang.Integer value) {
          addCriterion("param_length", value, ConditionMode.GREATER_THEN, "paramLength", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamLengthGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("param_length", value, ConditionMode.GREATER_EQUAL, "paramLength", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamLengthLessThan(java.lang.Integer value) {
          addCriterion("param_length", value, ConditionMode.LESS_THEN, "paramLength", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamLengthLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("param_length", value, ConditionMode.LESS_EQUAL, "paramLength", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamLengthBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("param_length", value1, value2, ConditionMode.BETWEEN, "paramLength", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsApiParamSQL andParamLengthNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("param_length", value1, value2, ConditionMode.NOT_BETWEEN, "paramLength", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsApiParamSQL andParamLengthIn(List<java.lang.Integer> values) {
          addCriterion("param_length", values, ConditionMode.IN, "paramLength", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamLengthNotIn(List<java.lang.Integer> values) {
          addCriterion("param_length", values, ConditionMode.NOT_IN, "paramLength", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsApiParamSQL andParamValueDefaultIsNull() {
		isnull("param_value_default");
		return this;
	}
	
	public ArmsApiParamSQL andParamValueDefaultIsNotNull() {
		notNull("param_value_default");
		return this;
	}
	
	public ArmsApiParamSQL andParamValueDefaultIsEmpty() {
		empty("param_value_default");
		return this;
	}

	public ArmsApiParamSQL andParamValueDefaultIsNotEmpty() {
		notEmpty("param_value_default");
		return this;
	}
       public ArmsApiParamSQL andParamValueDefaultLike(java.lang.String value) {
    	   addCriterion("param_value_default", value, ConditionMode.FUZZY, "paramValueDefault", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsApiParamSQL andParamValueDefaultNotLike(java.lang.String value) {
          addCriterion("param_value_default", value, ConditionMode.NOT_FUZZY, "paramValueDefault", "java.lang.String", "Float");
          return this;
      }
      public ArmsApiParamSQL andParamValueDefaultEqualTo(java.lang.String value) {
          addCriterion("param_value_default", value, ConditionMode.EQUAL, "paramValueDefault", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamValueDefaultNotEqualTo(java.lang.String value) {
          addCriterion("param_value_default", value, ConditionMode.NOT_EQUAL, "paramValueDefault", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamValueDefaultGreaterThan(java.lang.String value) {
          addCriterion("param_value_default", value, ConditionMode.GREATER_THEN, "paramValueDefault", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamValueDefaultGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("param_value_default", value, ConditionMode.GREATER_EQUAL, "paramValueDefault", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamValueDefaultLessThan(java.lang.String value) {
          addCriterion("param_value_default", value, ConditionMode.LESS_THEN, "paramValueDefault", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamValueDefaultLessThanOrEqualTo(java.lang.String value) {
          addCriterion("param_value_default", value, ConditionMode.LESS_EQUAL, "paramValueDefault", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamValueDefaultBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("param_value_default", value1, value2, ConditionMode.BETWEEN, "paramValueDefault", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiParamSQL andParamValueDefaultNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("param_value_default", value1, value2, ConditionMode.NOT_BETWEEN, "paramValueDefault", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiParamSQL andParamValueDefaultIn(List<java.lang.String> values) {
          addCriterion("param_value_default", values, ConditionMode.IN, "paramValueDefault", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamValueDefaultNotIn(List<java.lang.String> values) {
          addCriterion("param_value_default", values, ConditionMode.NOT_IN, "paramValueDefault", "java.lang.String", "String");
          return this;
      }
	public ArmsApiParamSQL andParamValueExampleIsNull() {
		isnull("param_value_example");
		return this;
	}
	
	public ArmsApiParamSQL andParamValueExampleIsNotNull() {
		notNull("param_value_example");
		return this;
	}
	
	public ArmsApiParamSQL andParamValueExampleIsEmpty() {
		empty("param_value_example");
		return this;
	}

	public ArmsApiParamSQL andParamValueExampleIsNotEmpty() {
		notEmpty("param_value_example");
		return this;
	}
       public ArmsApiParamSQL andParamValueExampleLike(java.lang.String value) {
    	   addCriterion("param_value_example", value, ConditionMode.FUZZY, "paramValueExample", "java.lang.String", "String");
    	   return this;
      }

      public ArmsApiParamSQL andParamValueExampleNotLike(java.lang.String value) {
          addCriterion("param_value_example", value, ConditionMode.NOT_FUZZY, "paramValueExample", "java.lang.String", "String");
          return this;
      }
      public ArmsApiParamSQL andParamValueExampleEqualTo(java.lang.String value) {
          addCriterion("param_value_example", value, ConditionMode.EQUAL, "paramValueExample", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamValueExampleNotEqualTo(java.lang.String value) {
          addCriterion("param_value_example", value, ConditionMode.NOT_EQUAL, "paramValueExample", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamValueExampleGreaterThan(java.lang.String value) {
          addCriterion("param_value_example", value, ConditionMode.GREATER_THEN, "paramValueExample", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamValueExampleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("param_value_example", value, ConditionMode.GREATER_EQUAL, "paramValueExample", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamValueExampleLessThan(java.lang.String value) {
          addCriterion("param_value_example", value, ConditionMode.LESS_THEN, "paramValueExample", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamValueExampleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("param_value_example", value, ConditionMode.LESS_EQUAL, "paramValueExample", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamValueExampleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("param_value_example", value1, value2, ConditionMode.BETWEEN, "paramValueExample", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiParamSQL andParamValueExampleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("param_value_example", value1, value2, ConditionMode.NOT_BETWEEN, "paramValueExample", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiParamSQL andParamValueExampleIn(List<java.lang.String> values) {
          addCriterion("param_value_example", values, ConditionMode.IN, "paramValueExample", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamValueExampleNotIn(List<java.lang.String> values) {
          addCriterion("param_value_example", values, ConditionMode.NOT_IN, "paramValueExample", "java.lang.String", "String");
          return this;
      }
	public ArmsApiParamSQL andParamDescribeIsNull() {
		isnull("param_describe");
		return this;
	}
	
	public ArmsApiParamSQL andParamDescribeIsNotNull() {
		notNull("param_describe");
		return this;
	}
	
	public ArmsApiParamSQL andParamDescribeIsEmpty() {
		empty("param_describe");
		return this;
	}

	public ArmsApiParamSQL andParamDescribeIsNotEmpty() {
		notEmpty("param_describe");
		return this;
	}
       public ArmsApiParamSQL andParamDescribeLike(java.lang.String value) {
    	   addCriterion("param_describe", value, ConditionMode.FUZZY, "paramDescribe", "java.lang.String", "String");
    	   return this;
      }

      public ArmsApiParamSQL andParamDescribeNotLike(java.lang.String value) {
          addCriterion("param_describe", value, ConditionMode.NOT_FUZZY, "paramDescribe", "java.lang.String", "String");
          return this;
      }
      public ArmsApiParamSQL andParamDescribeEqualTo(java.lang.String value) {
          addCriterion("param_describe", value, ConditionMode.EQUAL, "paramDescribe", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamDescribeNotEqualTo(java.lang.String value) {
          addCriterion("param_describe", value, ConditionMode.NOT_EQUAL, "paramDescribe", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamDescribeGreaterThan(java.lang.String value) {
          addCriterion("param_describe", value, ConditionMode.GREATER_THEN, "paramDescribe", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamDescribeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("param_describe", value, ConditionMode.GREATER_EQUAL, "paramDescribe", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamDescribeLessThan(java.lang.String value) {
          addCriterion("param_describe", value, ConditionMode.LESS_THEN, "paramDescribe", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamDescribeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("param_describe", value, ConditionMode.LESS_EQUAL, "paramDescribe", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamDescribeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("param_describe", value1, value2, ConditionMode.BETWEEN, "paramDescribe", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiParamSQL andParamDescribeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("param_describe", value1, value2, ConditionMode.NOT_BETWEEN, "paramDescribe", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiParamSQL andParamDescribeIn(List<java.lang.String> values) {
          addCriterion("param_describe", values, ConditionMode.IN, "paramDescribe", "java.lang.String", "String");
          return this;
      }

      public ArmsApiParamSQL andParamDescribeNotIn(List<java.lang.String> values) {
          addCriterion("param_describe", values, ConditionMode.NOT_IN, "paramDescribe", "java.lang.String", "String");
          return this;
      }
	public ArmsApiParamSQL andParamOrdersIsNull() {
		isnull("param_orders");
		return this;
	}
	
	public ArmsApiParamSQL andParamOrdersIsNotNull() {
		notNull("param_orders");
		return this;
	}
	
	public ArmsApiParamSQL andParamOrdersIsEmpty() {
		empty("param_orders");
		return this;
	}

	public ArmsApiParamSQL andParamOrdersIsNotEmpty() {
		notEmpty("param_orders");
		return this;
	}
      public ArmsApiParamSQL andParamOrdersEqualTo(java.lang.Integer value) {
          addCriterion("param_orders", value, ConditionMode.EQUAL, "paramOrders", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamOrdersNotEqualTo(java.lang.Integer value) {
          addCriterion("param_orders", value, ConditionMode.NOT_EQUAL, "paramOrders", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamOrdersGreaterThan(java.lang.Integer value) {
          addCriterion("param_orders", value, ConditionMode.GREATER_THEN, "paramOrders", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamOrdersGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("param_orders", value, ConditionMode.GREATER_EQUAL, "paramOrders", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamOrdersLessThan(java.lang.Integer value) {
          addCriterion("param_orders", value, ConditionMode.LESS_THEN, "paramOrders", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamOrdersLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("param_orders", value, ConditionMode.LESS_EQUAL, "paramOrders", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamOrdersBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("param_orders", value1, value2, ConditionMode.BETWEEN, "paramOrders", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsApiParamSQL andParamOrdersNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("param_orders", value1, value2, ConditionMode.NOT_BETWEEN, "paramOrders", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsApiParamSQL andParamOrdersIn(List<java.lang.Integer> values) {
          addCriterion("param_orders", values, ConditionMode.IN, "paramOrders", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamOrdersNotIn(List<java.lang.Integer> values) {
          addCriterion("param_orders", values, ConditionMode.NOT_IN, "paramOrders", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsApiParamSQL andParamStatusIsNull() {
		isnull("param_status");
		return this;
	}
	
	public ArmsApiParamSQL andParamStatusIsNotNull() {
		notNull("param_status");
		return this;
	}
	
	public ArmsApiParamSQL andParamStatusIsEmpty() {
		empty("param_status");
		return this;
	}

	public ArmsApiParamSQL andParamStatusIsNotEmpty() {
		notEmpty("param_status");
		return this;
	}
      public ArmsApiParamSQL andParamStatusEqualTo(java.lang.Integer value) {
          addCriterion("param_status", value, ConditionMode.EQUAL, "paramStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("param_status", value, ConditionMode.NOT_EQUAL, "paramStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamStatusGreaterThan(java.lang.Integer value) {
          addCriterion("param_status", value, ConditionMode.GREATER_THEN, "paramStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("param_status", value, ConditionMode.GREATER_EQUAL, "paramStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamStatusLessThan(java.lang.Integer value) {
          addCriterion("param_status", value, ConditionMode.LESS_THEN, "paramStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("param_status", value, ConditionMode.LESS_EQUAL, "paramStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("param_status", value1, value2, ConditionMode.BETWEEN, "paramStatus", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsApiParamSQL andParamStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("param_status", value1, value2, ConditionMode.NOT_BETWEEN, "paramStatus", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsApiParamSQL andParamStatusIn(List<java.lang.Integer> values) {
          addCriterion("param_status", values, ConditionMode.IN, "paramStatus", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiParamSQL andParamStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("param_status", values, ConditionMode.NOT_IN, "paramStatus", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsApiParamSQL andParamCreateTimeIsNull() {
		isnull("param_create_time");
		return this;
	}
	
	public ArmsApiParamSQL andParamCreateTimeIsNotNull() {
		notNull("param_create_time");
		return this;
	}
	
	public ArmsApiParamSQL andParamCreateTimeIsEmpty() {
		empty("param_create_time");
		return this;
	}

	public ArmsApiParamSQL andParamCreateTimeIsNotEmpty() {
		notEmpty("param_create_time");
		return this;
	}
      public ArmsApiParamSQL andParamCreateTimeEqualTo(java.util.Date value) {
          addCriterion("param_create_time", value, ConditionMode.EQUAL, "paramCreateTime", "java.util.Date", "String");
          return this;
      }

      public ArmsApiParamSQL andParamCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("param_create_time", value, ConditionMode.NOT_EQUAL, "paramCreateTime", "java.util.Date", "String");
          return this;
      }

      public ArmsApiParamSQL andParamCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("param_create_time", value, ConditionMode.GREATER_THEN, "paramCreateTime", "java.util.Date", "String");
          return this;
      }

      public ArmsApiParamSQL andParamCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("param_create_time", value, ConditionMode.GREATER_EQUAL, "paramCreateTime", "java.util.Date", "String");
          return this;
      }

      public ArmsApiParamSQL andParamCreateTimeLessThan(java.util.Date value) {
          addCriterion("param_create_time", value, ConditionMode.LESS_THEN, "paramCreateTime", "java.util.Date", "String");
          return this;
      }

      public ArmsApiParamSQL andParamCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("param_create_time", value, ConditionMode.LESS_EQUAL, "paramCreateTime", "java.util.Date", "String");
          return this;
      }

      public ArmsApiParamSQL andParamCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("param_create_time", value1, value2, ConditionMode.BETWEEN, "paramCreateTime", "java.util.Date", "String");
    	  return this;
      }

      public ArmsApiParamSQL andParamCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("param_create_time", value1, value2, ConditionMode.NOT_BETWEEN, "paramCreateTime", "java.util.Date", "String");
          return this;
      }
        
      public ArmsApiParamSQL andParamCreateTimeIn(List<java.util.Date> values) {
          addCriterion("param_create_time", values, ConditionMode.IN, "paramCreateTime", "java.util.Date", "String");
          return this;
      }

      public ArmsApiParamSQL andParamCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("param_create_time", values, ConditionMode.NOT_IN, "paramCreateTime", "java.util.Date", "String");
          return this;
      }
}