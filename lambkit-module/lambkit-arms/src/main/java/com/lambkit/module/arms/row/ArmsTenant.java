/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.arms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class ArmsTenant extends RowModel<ArmsTenant> {
	public ArmsTenant() {
		setTableName("arms_tenant");
		setPrimaryKey("id");
	}
	public java.lang.Long getId() {
		return this.get("id");
	}
	public void setId(java.lang.Long id) {
		this.set("id", id);
	}
	public java.lang.String getTenantId() {
		return this.get("tenant_id");
	}
	public void setTenantId(java.lang.String tenantId) {
		this.set("tenant_id", tenantId);
	}
	public java.lang.String getTenantName() {
		return this.get("tenant_name");
	}
	public void setTenantName(java.lang.String tenantName) {
		this.set("tenant_name", tenantName);
	}
	public java.lang.String getDomain() {
		return this.get("domain");
	}
	public void setDomain(java.lang.String domain) {
		this.set("domain", domain);
	}
	public java.lang.String getLinkman() {
		return this.get("linkman");
	}
	public void setLinkman(java.lang.String linkman) {
		this.set("linkman", linkman);
	}
	public java.lang.String getContactNumber() {
		return this.get("contact_number");
	}
	public void setContactNumber(java.lang.String contactNumber) {
		this.set("contact_number", contactNumber);
	}
	public java.lang.String getAddress() {
		return this.get("address");
	}
	public void setAddress(java.lang.String address) {
		this.set("address", address);
	}
	public java.lang.Long getCreateUser() {
		return this.get("create_user");
	}
	public void setCreateUser(java.lang.Long createUser) {
		this.set("create_user", createUser);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
	public java.lang.Long getUpdateUser() {
		return this.get("update_user");
	}
	public void setUpdateUser(java.lang.Long updateUser) {
		this.set("update_user", updateUser);
	}
	public java.lang.Integer getTenantStatus() {
		return this.get("tenant_status");
	}
	public void setTenantStatus(java.lang.Integer tenantStatus) {
		this.set("tenant_status", tenantStatus);
	}
	public java.lang.Integer getIsDeleted() {
		return this.get("is_deleted");
	}
	public void setIsDeleted(java.lang.Integer isDeleted) {
		this.set("is_deleted", isDeleted);
	}
}
