/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.arms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class ArmsGatewaySQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static ArmsGatewaySQL of() {
		return new ArmsGatewaySQL();
	}
	
	public static ArmsGatewaySQL by(Column column) {
		ArmsGatewaySQL that = new ArmsGatewaySQL();
		that.add(column);
        return that;
    }

    public static ArmsGatewaySQL by(String name, Object value) {
        return (ArmsGatewaySQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("arms_gateway", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsGatewaySQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsGatewaySQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public ArmsGatewaySQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public ArmsGatewaySQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsGatewaySQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsGatewaySQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsGatewaySQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsGatewaySQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public ArmsGatewaySQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public ArmsGatewaySQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public ArmsGatewaySQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public ArmsGatewaySQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public ArmsGatewaySQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public ArmsGatewaySQL andGidIsNull() {
		isnull("gid");
		return this;
	}
	
	public ArmsGatewaySQL andGidIsNotNull() {
		notNull("gid");
		return this;
	}
	
	public ArmsGatewaySQL andGidIsEmpty() {
		empty("gid");
		return this;
	}

	public ArmsGatewaySQL andGidIsNotEmpty() {
		notEmpty("gid");
		return this;
	}
      public ArmsGatewaySQL andGidEqualTo(java.lang.Integer value) {
          addCriterion("gid", value, ConditionMode.EQUAL, "gid", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andGidNotEqualTo(java.lang.Integer value) {
          addCriterion("gid", value, ConditionMode.NOT_EQUAL, "gid", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andGidGreaterThan(java.lang.Integer value) {
          addCriterion("gid", value, ConditionMode.GREATER_THEN, "gid", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andGidGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("gid", value, ConditionMode.GREATER_EQUAL, "gid", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andGidLessThan(java.lang.Integer value) {
          addCriterion("gid", value, ConditionMode.LESS_THEN, "gid", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andGidLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("gid", value, ConditionMode.LESS_EQUAL, "gid", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andGidBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("gid", value1, value2, ConditionMode.BETWEEN, "gid", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsGatewaySQL andGidNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("gid", value1, value2, ConditionMode.NOT_BETWEEN, "gid", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsGatewaySQL andGidIn(List<java.lang.Integer> values) {
          addCriterion("gid", values, ConditionMode.IN, "gid", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andGidNotIn(List<java.lang.Integer> values) {
          addCriterion("gid", values, ConditionMode.NOT_IN, "gid", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsGatewaySQL andRouteIdIsNull() {
		isnull("route_id");
		return this;
	}
	
	public ArmsGatewaySQL andRouteIdIsNotNull() {
		notNull("route_id");
		return this;
	}
	
	public ArmsGatewaySQL andRouteIdIsEmpty() {
		empty("route_id");
		return this;
	}

	public ArmsGatewaySQL andRouteIdIsNotEmpty() {
		notEmpty("route_id");
		return this;
	}
       public ArmsGatewaySQL andRouteIdLike(java.lang.String value) {
    	   addCriterion("route_id", value, ConditionMode.FUZZY, "routeId", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsGatewaySQL andRouteIdNotLike(java.lang.String value) {
          addCriterion("route_id", value, ConditionMode.NOT_FUZZY, "routeId", "java.lang.String", "Float");
          return this;
      }
      public ArmsGatewaySQL andRouteIdEqualTo(java.lang.String value) {
          addCriterion("route_id", value, ConditionMode.EQUAL, "routeId", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andRouteIdNotEqualTo(java.lang.String value) {
          addCriterion("route_id", value, ConditionMode.NOT_EQUAL, "routeId", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andRouteIdGreaterThan(java.lang.String value) {
          addCriterion("route_id", value, ConditionMode.GREATER_THEN, "routeId", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andRouteIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("route_id", value, ConditionMode.GREATER_EQUAL, "routeId", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andRouteIdLessThan(java.lang.String value) {
          addCriterion("route_id", value, ConditionMode.LESS_THEN, "routeId", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andRouteIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("route_id", value, ConditionMode.LESS_EQUAL, "routeId", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andRouteIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("route_id", value1, value2, ConditionMode.BETWEEN, "routeId", "java.lang.String", "String");
    	  return this;
      }

      public ArmsGatewaySQL andRouteIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("route_id", value1, value2, ConditionMode.NOT_BETWEEN, "routeId", "java.lang.String", "String");
          return this;
      }
        
      public ArmsGatewaySQL andRouteIdIn(List<java.lang.String> values) {
          addCriterion("route_id", values, ConditionMode.IN, "routeId", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andRouteIdNotIn(List<java.lang.String> values) {
          addCriterion("route_id", values, ConditionMode.NOT_IN, "routeId", "java.lang.String", "String");
          return this;
      }
	public ArmsGatewaySQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public ArmsGatewaySQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public ArmsGatewaySQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public ArmsGatewaySQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
       public ArmsGatewaySQL andTypeLike(java.lang.String value) {
    	   addCriterion("type", value, ConditionMode.FUZZY, "type", "java.lang.String", "String");
    	   return this;
      }

      public ArmsGatewaySQL andTypeNotLike(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_FUZZY, "type", "java.lang.String", "String");
          return this;
      }
      public ArmsGatewaySQL andTypeEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andTypeNotEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andTypeGreaterThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andTypeLessThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.String", "String");
    	  return this;
      }

      public ArmsGatewaySQL andTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.String", "String");
          return this;
      }
        
      public ArmsGatewaySQL andTypeIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andTypeNotIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.String", "String");
          return this;
      }
	public ArmsGatewaySQL andUrlpatternIsNull() {
		isnull("urlpattern");
		return this;
	}
	
	public ArmsGatewaySQL andUrlpatternIsNotNull() {
		notNull("urlpattern");
		return this;
	}
	
	public ArmsGatewaySQL andUrlpatternIsEmpty() {
		empty("urlpattern");
		return this;
	}

	public ArmsGatewaySQL andUrlpatternIsNotEmpty() {
		notEmpty("urlpattern");
		return this;
	}
       public ArmsGatewaySQL andUrlpatternLike(java.lang.String value) {
    	   addCriterion("urlpattern", value, ConditionMode.FUZZY, "urlpattern", "java.lang.String", "String");
    	   return this;
      }

      public ArmsGatewaySQL andUrlpatternNotLike(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.NOT_FUZZY, "urlpattern", "java.lang.String", "String");
          return this;
      }
      public ArmsGatewaySQL andUrlpatternEqualTo(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.EQUAL, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andUrlpatternNotEqualTo(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.NOT_EQUAL, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andUrlpatternGreaterThan(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.GREATER_THEN, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andUrlpatternGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.GREATER_EQUAL, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andUrlpatternLessThan(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.LESS_THEN, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andUrlpatternLessThanOrEqualTo(java.lang.String value) {
          addCriterion("urlpattern", value, ConditionMode.LESS_EQUAL, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andUrlpatternBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("urlpattern", value1, value2, ConditionMode.BETWEEN, "urlpattern", "java.lang.String", "String");
    	  return this;
      }

      public ArmsGatewaySQL andUrlpatternNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("urlpattern", value1, value2, ConditionMode.NOT_BETWEEN, "urlpattern", "java.lang.String", "String");
          return this;
      }
        
      public ArmsGatewaySQL andUrlpatternIn(List<java.lang.String> values) {
          addCriterion("urlpattern", values, ConditionMode.IN, "urlpattern", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andUrlpatternNotIn(List<java.lang.String> values) {
          addCriterion("urlpattern", values, ConditionMode.NOT_IN, "urlpattern", "java.lang.String", "String");
          return this;
      }
	public ArmsGatewaySQL andUriIsNull() {
		isnull("uri");
		return this;
	}
	
	public ArmsGatewaySQL andUriIsNotNull() {
		notNull("uri");
		return this;
	}
	
	public ArmsGatewaySQL andUriIsEmpty() {
		empty("uri");
		return this;
	}

	public ArmsGatewaySQL andUriIsNotEmpty() {
		notEmpty("uri");
		return this;
	}
       public ArmsGatewaySQL andUriLike(java.lang.String value) {
    	   addCriterion("uri", value, ConditionMode.FUZZY, "uri", "java.lang.String", "String");
    	   return this;
      }

      public ArmsGatewaySQL andUriNotLike(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.NOT_FUZZY, "uri", "java.lang.String", "String");
          return this;
      }
      public ArmsGatewaySQL andUriEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andUriNotEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.NOT_EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andUriGreaterThan(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.GREATER_THEN, "uri", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andUriGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.GREATER_EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andUriLessThan(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.LESS_THEN, "uri", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andUriLessThanOrEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.LESS_EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andUriBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("uri", value1, value2, ConditionMode.BETWEEN, "uri", "java.lang.String", "String");
    	  return this;
      }

      public ArmsGatewaySQL andUriNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("uri", value1, value2, ConditionMode.NOT_BETWEEN, "uri", "java.lang.String", "String");
          return this;
      }
        
      public ArmsGatewaySQL andUriIn(List<java.lang.String> values) {
          addCriterion("uri", values, ConditionMode.IN, "uri", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andUriNotIn(List<java.lang.String> values) {
          addCriterion("uri", values, ConditionMode.NOT_IN, "uri", "java.lang.String", "String");
          return this;
      }
	public ArmsGatewaySQL andWeightIsNull() {
		isnull("weight");
		return this;
	}
	
	public ArmsGatewaySQL andWeightIsNotNull() {
		notNull("weight");
		return this;
	}
	
	public ArmsGatewaySQL andWeightIsEmpty() {
		empty("weight");
		return this;
	}

	public ArmsGatewaySQL andWeightIsNotEmpty() {
		notEmpty("weight");
		return this;
	}
      public ArmsGatewaySQL andWeightEqualTo(java.lang.Integer value) {
          addCriterion("weight", value, ConditionMode.EQUAL, "weight", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andWeightNotEqualTo(java.lang.Integer value) {
          addCriterion("weight", value, ConditionMode.NOT_EQUAL, "weight", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andWeightGreaterThan(java.lang.Integer value) {
          addCriterion("weight", value, ConditionMode.GREATER_THEN, "weight", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andWeightGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("weight", value, ConditionMode.GREATER_EQUAL, "weight", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andWeightLessThan(java.lang.Integer value) {
          addCriterion("weight", value, ConditionMode.LESS_THEN, "weight", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andWeightLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("weight", value, ConditionMode.LESS_EQUAL, "weight", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andWeightBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("weight", value1, value2, ConditionMode.BETWEEN, "weight", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsGatewaySQL andWeightNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("weight", value1, value2, ConditionMode.NOT_BETWEEN, "weight", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsGatewaySQL andWeightIn(List<java.lang.Integer> values) {
          addCriterion("weight", values, ConditionMode.IN, "weight", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andWeightNotIn(List<java.lang.Integer> values) {
          addCriterion("weight", values, ConditionMode.NOT_IN, "weight", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsGatewaySQL andLoadbalanceTypeIsNull() {
		isnull("loadbalance_type");
		return this;
	}
	
	public ArmsGatewaySQL andLoadbalanceTypeIsNotNull() {
		notNull("loadbalance_type");
		return this;
	}
	
	public ArmsGatewaySQL andLoadbalanceTypeIsEmpty() {
		empty("loadbalance_type");
		return this;
	}

	public ArmsGatewaySQL andLoadbalanceTypeIsNotEmpty() {
		notEmpty("loadbalance_type");
		return this;
	}
       public ArmsGatewaySQL andLoadbalanceTypeLike(java.lang.String value) {
    	   addCriterion("loadbalance_type", value, ConditionMode.FUZZY, "loadbalanceType", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsGatewaySQL andLoadbalanceTypeNotLike(java.lang.String value) {
          addCriterion("loadbalance_type", value, ConditionMode.NOT_FUZZY, "loadbalanceType", "java.lang.String", "Float");
          return this;
      }
      public ArmsGatewaySQL andLoadbalanceTypeEqualTo(java.lang.String value) {
          addCriterion("loadbalance_type", value, ConditionMode.EQUAL, "loadbalanceType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andLoadbalanceTypeNotEqualTo(java.lang.String value) {
          addCriterion("loadbalance_type", value, ConditionMode.NOT_EQUAL, "loadbalanceType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andLoadbalanceTypeGreaterThan(java.lang.String value) {
          addCriterion("loadbalance_type", value, ConditionMode.GREATER_THEN, "loadbalanceType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andLoadbalanceTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("loadbalance_type", value, ConditionMode.GREATER_EQUAL, "loadbalanceType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andLoadbalanceTypeLessThan(java.lang.String value) {
          addCriterion("loadbalance_type", value, ConditionMode.LESS_THEN, "loadbalanceType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andLoadbalanceTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("loadbalance_type", value, ConditionMode.LESS_EQUAL, "loadbalanceType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andLoadbalanceTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("loadbalance_type", value1, value2, ConditionMode.BETWEEN, "loadbalanceType", "java.lang.String", "String");
    	  return this;
      }

      public ArmsGatewaySQL andLoadbalanceTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("loadbalance_type", value1, value2, ConditionMode.NOT_BETWEEN, "loadbalanceType", "java.lang.String", "String");
          return this;
      }
        
      public ArmsGatewaySQL andLoadbalanceTypeIn(List<java.lang.String> values) {
          addCriterion("loadbalance_type", values, ConditionMode.IN, "loadbalanceType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andLoadbalanceTypeNotIn(List<java.lang.String> values) {
          addCriterion("loadbalance_type", values, ConditionMode.NOT_IN, "loadbalanceType", "java.lang.String", "String");
          return this;
      }
	public ArmsGatewaySQL andRateIsNull() {
		isnull("rate");
		return this;
	}
	
	public ArmsGatewaySQL andRateIsNotNull() {
		notNull("rate");
		return this;
	}
	
	public ArmsGatewaySQL andRateIsEmpty() {
		empty("rate");
		return this;
	}

	public ArmsGatewaySQL andRateIsNotEmpty() {
		notEmpty("rate");
		return this;
	}
      public ArmsGatewaySQL andRateEqualTo(java.lang.Integer value) {
          addCriterion("rate", value, ConditionMode.EQUAL, "rate", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andRateNotEqualTo(java.lang.Integer value) {
          addCriterion("rate", value, ConditionMode.NOT_EQUAL, "rate", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andRateGreaterThan(java.lang.Integer value) {
          addCriterion("rate", value, ConditionMode.GREATER_THEN, "rate", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andRateGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("rate", value, ConditionMode.GREATER_EQUAL, "rate", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andRateLessThan(java.lang.Integer value) {
          addCriterion("rate", value, ConditionMode.LESS_THEN, "rate", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andRateLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("rate", value, ConditionMode.LESS_EQUAL, "rate", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andRateBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("rate", value1, value2, ConditionMode.BETWEEN, "rate", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsGatewaySQL andRateNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("rate", value1, value2, ConditionMode.NOT_BETWEEN, "rate", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsGatewaySQL andRateIn(List<java.lang.Integer> values) {
          addCriterion("rate", values, ConditionMode.IN, "rate", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andRateNotIn(List<java.lang.Integer> values) {
          addCriterion("rate", values, ConditionMode.NOT_IN, "rate", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsGatewaySQL andPeriodIsNull() {
		isnull("period");
		return this;
	}
	
	public ArmsGatewaySQL andPeriodIsNotNull() {
		notNull("period");
		return this;
	}
	
	public ArmsGatewaySQL andPeriodIsEmpty() {
		empty("period");
		return this;
	}

	public ArmsGatewaySQL andPeriodIsNotEmpty() {
		notEmpty("period");
		return this;
	}
      public ArmsGatewaySQL andPeriodEqualTo(java.lang.Integer value) {
          addCriterion("period", value, ConditionMode.EQUAL, "period", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andPeriodNotEqualTo(java.lang.Integer value) {
          addCriterion("period", value, ConditionMode.NOT_EQUAL, "period", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andPeriodGreaterThan(java.lang.Integer value) {
          addCriterion("period", value, ConditionMode.GREATER_THEN, "period", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andPeriodGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("period", value, ConditionMode.GREATER_EQUAL, "period", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andPeriodLessThan(java.lang.Integer value) {
          addCriterion("period", value, ConditionMode.LESS_THEN, "period", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andPeriodLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("period", value, ConditionMode.LESS_EQUAL, "period", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andPeriodBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("period", value1, value2, ConditionMode.BETWEEN, "period", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsGatewaySQL andPeriodNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("period", value1, value2, ConditionMode.NOT_BETWEEN, "period", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsGatewaySQL andPeriodIn(List<java.lang.Integer> values) {
          addCriterion("period", values, ConditionMode.IN, "period", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andPeriodNotIn(List<java.lang.Integer> values) {
          addCriterion("period", values, ConditionMode.NOT_IN, "period", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsGatewaySQL andLimitTypeIsNull() {
		isnull("limit_type");
		return this;
	}
	
	public ArmsGatewaySQL andLimitTypeIsNotNull() {
		notNull("limit_type");
		return this;
	}
	
	public ArmsGatewaySQL andLimitTypeIsEmpty() {
		empty("limit_type");
		return this;
	}

	public ArmsGatewaySQL andLimitTypeIsNotEmpty() {
		notEmpty("limit_type");
		return this;
	}
       public ArmsGatewaySQL andLimitTypeLike(java.lang.String value) {
    	   addCriterion("limit_type", value, ConditionMode.FUZZY, "limitType", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsGatewaySQL andLimitTypeNotLike(java.lang.String value) {
          addCriterion("limit_type", value, ConditionMode.NOT_FUZZY, "limitType", "java.lang.String", "Float");
          return this;
      }
      public ArmsGatewaySQL andLimitTypeEqualTo(java.lang.String value) {
          addCriterion("limit_type", value, ConditionMode.EQUAL, "limitType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andLimitTypeNotEqualTo(java.lang.String value) {
          addCriterion("limit_type", value, ConditionMode.NOT_EQUAL, "limitType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andLimitTypeGreaterThan(java.lang.String value) {
          addCriterion("limit_type", value, ConditionMode.GREATER_THEN, "limitType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andLimitTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("limit_type", value, ConditionMode.GREATER_EQUAL, "limitType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andLimitTypeLessThan(java.lang.String value) {
          addCriterion("limit_type", value, ConditionMode.LESS_THEN, "limitType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andLimitTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("limit_type", value, ConditionMode.LESS_EQUAL, "limitType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andLimitTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("limit_type", value1, value2, ConditionMode.BETWEEN, "limitType", "java.lang.String", "String");
    	  return this;
      }

      public ArmsGatewaySQL andLimitTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("limit_type", value1, value2, ConditionMode.NOT_BETWEEN, "limitType", "java.lang.String", "String");
          return this;
      }
        
      public ArmsGatewaySQL andLimitTypeIn(List<java.lang.String> values) {
          addCriterion("limit_type", values, ConditionMode.IN, "limitType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andLimitTypeNotIn(List<java.lang.String> values) {
          addCriterion("limit_type", values, ConditionMode.NOT_IN, "limitType", "java.lang.String", "String");
          return this;
      }
	public ArmsGatewaySQL andFallbackTypeIsNull() {
		isnull("fallback_type");
		return this;
	}
	
	public ArmsGatewaySQL andFallbackTypeIsNotNull() {
		notNull("fallback_type");
		return this;
	}
	
	public ArmsGatewaySQL andFallbackTypeIsEmpty() {
		empty("fallback_type");
		return this;
	}

	public ArmsGatewaySQL andFallbackTypeIsNotEmpty() {
		notEmpty("fallback_type");
		return this;
	}
       public ArmsGatewaySQL andFallbackTypeLike(java.lang.String value) {
    	   addCriterion("fallback_type", value, ConditionMode.FUZZY, "fallbackType", "java.lang.String", "String");
    	   return this;
      }

      public ArmsGatewaySQL andFallbackTypeNotLike(java.lang.String value) {
          addCriterion("fallback_type", value, ConditionMode.NOT_FUZZY, "fallbackType", "java.lang.String", "String");
          return this;
      }
      public ArmsGatewaySQL andFallbackTypeEqualTo(java.lang.String value) {
          addCriterion("fallback_type", value, ConditionMode.EQUAL, "fallbackType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andFallbackTypeNotEqualTo(java.lang.String value) {
          addCriterion("fallback_type", value, ConditionMode.NOT_EQUAL, "fallbackType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andFallbackTypeGreaterThan(java.lang.String value) {
          addCriterion("fallback_type", value, ConditionMode.GREATER_THEN, "fallbackType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andFallbackTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("fallback_type", value, ConditionMode.GREATER_EQUAL, "fallbackType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andFallbackTypeLessThan(java.lang.String value) {
          addCriterion("fallback_type", value, ConditionMode.LESS_THEN, "fallbackType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andFallbackTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("fallback_type", value, ConditionMode.LESS_EQUAL, "fallbackType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andFallbackTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("fallback_type", value1, value2, ConditionMode.BETWEEN, "fallbackType", "java.lang.String", "String");
    	  return this;
      }

      public ArmsGatewaySQL andFallbackTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("fallback_type", value1, value2, ConditionMode.NOT_BETWEEN, "fallbackType", "java.lang.String", "String");
          return this;
      }
        
      public ArmsGatewaySQL andFallbackTypeIn(List<java.lang.String> values) {
          addCriterion("fallback_type", values, ConditionMode.IN, "fallbackType", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andFallbackTypeNotIn(List<java.lang.String> values) {
          addCriterion("fallback_type", values, ConditionMode.NOT_IN, "fallbackType", "java.lang.String", "String");
          return this;
      }
	public ArmsGatewaySQL andFallbackimplIsNull() {
		isnull("fallbackimpl");
		return this;
	}
	
	public ArmsGatewaySQL andFallbackimplIsNotNull() {
		notNull("fallbackimpl");
		return this;
	}
	
	public ArmsGatewaySQL andFallbackimplIsEmpty() {
		empty("fallbackimpl");
		return this;
	}

	public ArmsGatewaySQL andFallbackimplIsNotEmpty() {
		notEmpty("fallbackimpl");
		return this;
	}
       public ArmsGatewaySQL andFallbackimplLike(java.lang.String value) {
    	   addCriterion("fallbackimpl", value, ConditionMode.FUZZY, "fallbackimpl", "java.lang.String", "String");
    	   return this;
      }

      public ArmsGatewaySQL andFallbackimplNotLike(java.lang.String value) {
          addCriterion("fallbackimpl", value, ConditionMode.NOT_FUZZY, "fallbackimpl", "java.lang.String", "String");
          return this;
      }
      public ArmsGatewaySQL andFallbackimplEqualTo(java.lang.String value) {
          addCriterion("fallbackimpl", value, ConditionMode.EQUAL, "fallbackimpl", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andFallbackimplNotEqualTo(java.lang.String value) {
          addCriterion("fallbackimpl", value, ConditionMode.NOT_EQUAL, "fallbackimpl", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andFallbackimplGreaterThan(java.lang.String value) {
          addCriterion("fallbackimpl", value, ConditionMode.GREATER_THEN, "fallbackimpl", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andFallbackimplGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("fallbackimpl", value, ConditionMode.GREATER_EQUAL, "fallbackimpl", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andFallbackimplLessThan(java.lang.String value) {
          addCriterion("fallbackimpl", value, ConditionMode.LESS_THEN, "fallbackimpl", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andFallbackimplLessThanOrEqualTo(java.lang.String value) {
          addCriterion("fallbackimpl", value, ConditionMode.LESS_EQUAL, "fallbackimpl", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andFallbackimplBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("fallbackimpl", value1, value2, ConditionMode.BETWEEN, "fallbackimpl", "java.lang.String", "String");
    	  return this;
      }

      public ArmsGatewaySQL andFallbackimplNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("fallbackimpl", value1, value2, ConditionMode.NOT_BETWEEN, "fallbackimpl", "java.lang.String", "String");
          return this;
      }
        
      public ArmsGatewaySQL andFallbackimplIn(List<java.lang.String> values) {
          addCriterion("fallbackimpl", values, ConditionMode.IN, "fallbackimpl", "java.lang.String", "String");
          return this;
      }

      public ArmsGatewaySQL andFallbackimplNotIn(List<java.lang.String> values) {
          addCriterion("fallbackimpl", values, ConditionMode.NOT_IN, "fallbackimpl", "java.lang.String", "String");
          return this;
      }
	public ArmsGatewaySQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public ArmsGatewaySQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public ArmsGatewaySQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public ArmsGatewaySQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public ArmsGatewaySQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsGatewaySQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsGatewaySQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsGatewaySQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsGatewaySQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public ArmsGatewaySQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public ArmsGatewaySQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public ArmsGatewaySQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public ArmsGatewaySQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public ArmsGatewaySQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public ArmsGatewaySQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public ArmsGatewaySQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public ArmsGatewaySQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public ArmsGatewaySQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public ArmsGatewaySQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public ArmsGatewaySQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public ArmsGatewaySQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public ArmsGatewaySQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
}