/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.arms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class ArmsSystemSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static ArmsSystemSQL of() {
		return new ArmsSystemSQL();
	}
	
	public static ArmsSystemSQL by(Column column) {
		ArmsSystemSQL that = new ArmsSystemSQL();
		that.add(column);
        return that;
    }

    public static ArmsSystemSQL by(String name, Object value) {
        return (ArmsSystemSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("arms_system", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsSystemSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsSystemSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public ArmsSystemSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public ArmsSystemSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsSystemSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsSystemSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsSystemSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsSystemSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public ArmsSystemSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public ArmsSystemSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public ArmsSystemSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public ArmsSystemSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public ArmsSystemSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public ArmsSystemSQL andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public ArmsSystemSQL andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public ArmsSystemSQL andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public ArmsSystemSQL andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
      public ArmsSystemSQL andSystemIdEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsSystemSQL andSystemIdNotEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsSystemSQL andSystemIdGreaterThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsSystemSQL andSystemIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsSystemSQL andSystemIdLessThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsSystemSQL andSystemIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsSystemSQL andSystemIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsSystemSQL andSystemIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsSystemSQL andSystemIdIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsSystemSQL andSystemIdNotIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsSystemSQL andIconIsNull() {
		isnull("icon");
		return this;
	}
	
	public ArmsSystemSQL andIconIsNotNull() {
		notNull("icon");
		return this;
	}
	
	public ArmsSystemSQL andIconIsEmpty() {
		empty("icon");
		return this;
	}

	public ArmsSystemSQL andIconIsNotEmpty() {
		notEmpty("icon");
		return this;
	}
       public ArmsSystemSQL andIconLike(java.lang.String value) {
    	   addCriterion("icon", value, ConditionMode.FUZZY, "icon", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsSystemSQL andIconNotLike(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.NOT_FUZZY, "icon", "java.lang.String", "Float");
          return this;
      }
      public ArmsSystemSQL andIconEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andIconNotEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.NOT_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andIconGreaterThan(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.GREATER_THEN, "icon", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andIconGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.GREATER_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andIconLessThan(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.LESS_THEN, "icon", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andIconLessThanOrEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.LESS_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andIconBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("icon", value1, value2, ConditionMode.BETWEEN, "icon", "java.lang.String", "String");
    	  return this;
      }

      public ArmsSystemSQL andIconNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("icon", value1, value2, ConditionMode.NOT_BETWEEN, "icon", "java.lang.String", "String");
          return this;
      }
        
      public ArmsSystemSQL andIconIn(List<java.lang.String> values) {
          addCriterion("icon", values, ConditionMode.IN, "icon", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andIconNotIn(List<java.lang.String> values) {
          addCriterion("icon", values, ConditionMode.NOT_IN, "icon", "java.lang.String", "String");
          return this;
      }
	public ArmsSystemSQL andBannerIsNull() {
		isnull("banner");
		return this;
	}
	
	public ArmsSystemSQL andBannerIsNotNull() {
		notNull("banner");
		return this;
	}
	
	public ArmsSystemSQL andBannerIsEmpty() {
		empty("banner");
		return this;
	}

	public ArmsSystemSQL andBannerIsNotEmpty() {
		notEmpty("banner");
		return this;
	}
       public ArmsSystemSQL andBannerLike(java.lang.String value) {
    	   addCriterion("banner", value, ConditionMode.FUZZY, "banner", "java.lang.String", "String");
    	   return this;
      }

      public ArmsSystemSQL andBannerNotLike(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.NOT_FUZZY, "banner", "java.lang.String", "String");
          return this;
      }
      public ArmsSystemSQL andBannerEqualTo(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.EQUAL, "banner", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andBannerNotEqualTo(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.NOT_EQUAL, "banner", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andBannerGreaterThan(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.GREATER_THEN, "banner", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andBannerGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.GREATER_EQUAL, "banner", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andBannerLessThan(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.LESS_THEN, "banner", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andBannerLessThanOrEqualTo(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.LESS_EQUAL, "banner", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andBannerBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("banner", value1, value2, ConditionMode.BETWEEN, "banner", "java.lang.String", "String");
    	  return this;
      }

      public ArmsSystemSQL andBannerNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("banner", value1, value2, ConditionMode.NOT_BETWEEN, "banner", "java.lang.String", "String");
          return this;
      }
        
      public ArmsSystemSQL andBannerIn(List<java.lang.String> values) {
          addCriterion("banner", values, ConditionMode.IN, "banner", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andBannerNotIn(List<java.lang.String> values) {
          addCriterion("banner", values, ConditionMode.NOT_IN, "banner", "java.lang.String", "String");
          return this;
      }
	public ArmsSystemSQL andThemeIsNull() {
		isnull("theme");
		return this;
	}
	
	public ArmsSystemSQL andThemeIsNotNull() {
		notNull("theme");
		return this;
	}
	
	public ArmsSystemSQL andThemeIsEmpty() {
		empty("theme");
		return this;
	}

	public ArmsSystemSQL andThemeIsNotEmpty() {
		notEmpty("theme");
		return this;
	}
       public ArmsSystemSQL andThemeLike(java.lang.String value) {
    	   addCriterion("theme", value, ConditionMode.FUZZY, "theme", "java.lang.String", "String");
    	   return this;
      }

      public ArmsSystemSQL andThemeNotLike(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.NOT_FUZZY, "theme", "java.lang.String", "String");
          return this;
      }
      public ArmsSystemSQL andThemeEqualTo(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.EQUAL, "theme", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andThemeNotEqualTo(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.NOT_EQUAL, "theme", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andThemeGreaterThan(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.GREATER_THEN, "theme", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andThemeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.GREATER_EQUAL, "theme", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andThemeLessThan(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.LESS_THEN, "theme", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andThemeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.LESS_EQUAL, "theme", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andThemeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("theme", value1, value2, ConditionMode.BETWEEN, "theme", "java.lang.String", "String");
    	  return this;
      }

      public ArmsSystemSQL andThemeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("theme", value1, value2, ConditionMode.NOT_BETWEEN, "theme", "java.lang.String", "String");
          return this;
      }
        
      public ArmsSystemSQL andThemeIn(List<java.lang.String> values) {
          addCriterion("theme", values, ConditionMode.IN, "theme", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andThemeNotIn(List<java.lang.String> values) {
          addCriterion("theme", values, ConditionMode.NOT_IN, "theme", "java.lang.String", "String");
          return this;
      }
	public ArmsSystemSQL andBasepathIsNull() {
		isnull("basepath");
		return this;
	}
	
	public ArmsSystemSQL andBasepathIsNotNull() {
		notNull("basepath");
		return this;
	}
	
	public ArmsSystemSQL andBasepathIsEmpty() {
		empty("basepath");
		return this;
	}

	public ArmsSystemSQL andBasepathIsNotEmpty() {
		notEmpty("basepath");
		return this;
	}
       public ArmsSystemSQL andBasepathLike(java.lang.String value) {
    	   addCriterion("basepath", value, ConditionMode.FUZZY, "basepath", "java.lang.String", "String");
    	   return this;
      }

      public ArmsSystemSQL andBasepathNotLike(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.NOT_FUZZY, "basepath", "java.lang.String", "String");
          return this;
      }
      public ArmsSystemSQL andBasepathEqualTo(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.EQUAL, "basepath", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andBasepathNotEqualTo(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.NOT_EQUAL, "basepath", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andBasepathGreaterThan(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.GREATER_THEN, "basepath", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andBasepathGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.GREATER_EQUAL, "basepath", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andBasepathLessThan(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.LESS_THEN, "basepath", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andBasepathLessThanOrEqualTo(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.LESS_EQUAL, "basepath", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andBasepathBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("basepath", value1, value2, ConditionMode.BETWEEN, "basepath", "java.lang.String", "String");
    	  return this;
      }

      public ArmsSystemSQL andBasepathNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("basepath", value1, value2, ConditionMode.NOT_BETWEEN, "basepath", "java.lang.String", "String");
          return this;
      }
        
      public ArmsSystemSQL andBasepathIn(List<java.lang.String> values) {
          addCriterion("basepath", values, ConditionMode.IN, "basepath", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andBasepathNotIn(List<java.lang.String> values) {
          addCriterion("basepath", values, ConditionMode.NOT_IN, "basepath", "java.lang.String", "String");
          return this;
      }
	public ArmsSystemSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public ArmsSystemSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public ArmsSystemSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public ArmsSystemSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public ArmsSystemSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsSystemSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsSystemSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsSystemSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsSystemSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsSystemSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsSystemSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsSystemSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsSystemSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsSystemSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsSystemSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public ArmsSystemSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public ArmsSystemSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public ArmsSystemSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public ArmsSystemSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsSystemSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public ArmsSystemSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public ArmsSystemSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public ArmsSystemSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public ArmsSystemSQL andCodeIsNull() {
		isnull("code");
		return this;
	}
	
	public ArmsSystemSQL andCodeIsNotNull() {
		notNull("code");
		return this;
	}
	
	public ArmsSystemSQL andCodeIsEmpty() {
		empty("code");
		return this;
	}

	public ArmsSystemSQL andCodeIsNotEmpty() {
		notEmpty("code");
		return this;
	}
       public ArmsSystemSQL andCodeLike(java.lang.String value) {
    	   addCriterion("code", value, ConditionMode.FUZZY, "code", "java.lang.String", "String");
    	   return this;
      }

      public ArmsSystemSQL andCodeNotLike(java.lang.String value) {
          addCriterion("code", value, ConditionMode.NOT_FUZZY, "code", "java.lang.String", "String");
          return this;
      }
      public ArmsSystemSQL andCodeEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andCodeNotEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.NOT_EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andCodeGreaterThan(java.lang.String value) {
          addCriterion("code", value, ConditionMode.GREATER_THEN, "code", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.GREATER_EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andCodeLessThan(java.lang.String value) {
          addCriterion("code", value, ConditionMode.LESS_THEN, "code", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.LESS_EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("code", value1, value2, ConditionMode.BETWEEN, "code", "java.lang.String", "String");
    	  return this;
      }

      public ArmsSystemSQL andCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("code", value1, value2, ConditionMode.NOT_BETWEEN, "code", "java.lang.String", "String");
          return this;
      }
        
      public ArmsSystemSQL andCodeIn(List<java.lang.String> values) {
          addCriterion("code", values, ConditionMode.IN, "code", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andCodeNotIn(List<java.lang.String> values) {
          addCriterion("code", values, ConditionMode.NOT_IN, "code", "java.lang.String", "String");
          return this;
      }
	public ArmsSystemSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public ArmsSystemSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public ArmsSystemSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public ArmsSystemSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public ArmsSystemSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "String");
    	   return this;
      }

      public ArmsSystemSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "String");
          return this;
      }
      public ArmsSystemSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public ArmsSystemSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public ArmsSystemSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public ArmsSystemSQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public ArmsSystemSQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public ArmsSystemSQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public ArmsSystemSQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public ArmsSystemSQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public ArmsSystemSQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public ArmsSystemSQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public ArmsSystemSQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public ArmsSystemSQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public ArmsSystemSQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public ArmsSystemSQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public ArmsSystemSQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public ArmsSystemSQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public ArmsSystemSQL andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public ArmsSystemSQL andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public ArmsSystemSQL andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public ArmsSystemSQL andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public ArmsSystemSQL andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public ArmsSystemSQL andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public ArmsSystemSQL andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public ArmsSystemSQL andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public ArmsSystemSQL andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public ArmsSystemSQL andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
	public ArmsSystemSQL andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public ArmsSystemSQL andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public ArmsSystemSQL andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public ArmsSystemSQL andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
      public ArmsSystemSQL andOrdersEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public ArmsSystemSQL andOrdersNotEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public ArmsSystemSQL andOrdersGreaterThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public ArmsSystemSQL andOrdersGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public ArmsSystemSQL andOrdersLessThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public ArmsSystemSQL andOrdersLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public ArmsSystemSQL andOrdersBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.lang.Long", "Float");
    	  return this;
      }

      public ArmsSystemSQL andOrdersNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.lang.Long", "Float");
          return this;
      }
        
      public ArmsSystemSQL andOrdersIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public ArmsSystemSQL andOrdersNotIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.lang.Long", "Float");
          return this;
      }
	public ArmsSystemSQL andTenantIdIsNull() {
		isnull("tenant_id");
		return this;
	}
	
	public ArmsSystemSQL andTenantIdIsNotNull() {
		notNull("tenant_id");
		return this;
	}
	
	public ArmsSystemSQL andTenantIdIsEmpty() {
		empty("tenant_id");
		return this;
	}

	public ArmsSystemSQL andTenantIdIsNotEmpty() {
		notEmpty("tenant_id");
		return this;
	}
       public ArmsSystemSQL andTenantIdLike(java.lang.String value) {
    	   addCriterion("tenant_id", value, ConditionMode.FUZZY, "tenantId", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsSystemSQL andTenantIdNotLike(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.NOT_FUZZY, "tenantId", "java.lang.String", "Float");
          return this;
      }
      public ArmsSystemSQL andTenantIdEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andTenantIdNotEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.NOT_EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andTenantIdGreaterThan(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.GREATER_THEN, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andTenantIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.GREATER_EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andTenantIdLessThan(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.LESS_THEN, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andTenantIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.LESS_EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andTenantIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("tenant_id", value1, value2, ConditionMode.BETWEEN, "tenantId", "java.lang.String", "String");
    	  return this;
      }

      public ArmsSystemSQL andTenantIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("tenant_id", value1, value2, ConditionMode.NOT_BETWEEN, "tenantId", "java.lang.String", "String");
          return this;
      }
        
      public ArmsSystemSQL andTenantIdIn(List<java.lang.String> values) {
          addCriterion("tenant_id", values, ConditionMode.IN, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andTenantIdNotIn(List<java.lang.String> values) {
          addCriterion("tenant_id", values, ConditionMode.NOT_IN, "tenantId", "java.lang.String", "String");
          return this;
      }
	public ArmsSystemSQL andClientIdIsNull() {
		isnull("client_id");
		return this;
	}
	
	public ArmsSystemSQL andClientIdIsNotNull() {
		notNull("client_id");
		return this;
	}
	
	public ArmsSystemSQL andClientIdIsEmpty() {
		empty("client_id");
		return this;
	}

	public ArmsSystemSQL andClientIdIsNotEmpty() {
		notEmpty("client_id");
		return this;
	}
       public ArmsSystemSQL andClientIdLike(java.lang.String value) {
    	   addCriterion("client_id", value, ConditionMode.FUZZY, "clientId", "java.lang.String", "String");
    	   return this;
      }

      public ArmsSystemSQL andClientIdNotLike(java.lang.String value) {
          addCriterion("client_id", value, ConditionMode.NOT_FUZZY, "clientId", "java.lang.String", "String");
          return this;
      }
      public ArmsSystemSQL andClientIdEqualTo(java.lang.String value) {
          addCriterion("client_id", value, ConditionMode.EQUAL, "clientId", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andClientIdNotEqualTo(java.lang.String value) {
          addCriterion("client_id", value, ConditionMode.NOT_EQUAL, "clientId", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andClientIdGreaterThan(java.lang.String value) {
          addCriterion("client_id", value, ConditionMode.GREATER_THEN, "clientId", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andClientIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("client_id", value, ConditionMode.GREATER_EQUAL, "clientId", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andClientIdLessThan(java.lang.String value) {
          addCriterion("client_id", value, ConditionMode.LESS_THEN, "clientId", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andClientIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("client_id", value, ConditionMode.LESS_EQUAL, "clientId", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andClientIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("client_id", value1, value2, ConditionMode.BETWEEN, "clientId", "java.lang.String", "String");
    	  return this;
      }

      public ArmsSystemSQL andClientIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("client_id", value1, value2, ConditionMode.NOT_BETWEEN, "clientId", "java.lang.String", "String");
          return this;
      }
        
      public ArmsSystemSQL andClientIdIn(List<java.lang.String> values) {
          addCriterion("client_id", values, ConditionMode.IN, "clientId", "java.lang.String", "String");
          return this;
      }

      public ArmsSystemSQL andClientIdNotIn(List<java.lang.String> values) {
          addCriterion("client_id", values, ConditionMode.NOT_IN, "clientId", "java.lang.String", "String");
          return this;
      }
}