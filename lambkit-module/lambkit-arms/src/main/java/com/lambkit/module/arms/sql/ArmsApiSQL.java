/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.arms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class ArmsApiSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static ArmsApiSQL of() {
		return new ArmsApiSQL();
	}
	
	public static ArmsApiSQL by(Column column) {
		ArmsApiSQL that = new ArmsApiSQL();
		that.add(column);
        return that;
    }

    public static ArmsApiSQL by(String name, Object value) {
        return (ArmsApiSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("arms_api", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsApiSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsApiSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public ArmsApiSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public ArmsApiSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsApiSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsApiSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsApiSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsApiSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public ArmsApiSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public ArmsApiSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public ArmsApiSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public ArmsApiSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public ArmsApiSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public ArmsApiSQL andApiIdIsNull() {
		isnull("api_id");
		return this;
	}
	
	public ArmsApiSQL andApiIdIsNotNull() {
		notNull("api_id");
		return this;
	}
	
	public ArmsApiSQL andApiIdIsEmpty() {
		empty("api_id");
		return this;
	}

	public ArmsApiSQL andApiIdIsNotEmpty() {
		notEmpty("api_id");
		return this;
	}
      public ArmsApiSQL andApiIdEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiSQL andApiIdNotEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.NOT_EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiSQL andApiIdGreaterThan(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.GREATER_THEN, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiSQL andApiIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.GREATER_EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiSQL andApiIdLessThan(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.LESS_THEN, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiSQL andApiIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("api_id", value, ConditionMode.LESS_EQUAL, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiSQL andApiIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("api_id", value1, value2, ConditionMode.BETWEEN, "apiId", "java.lang.Long", "Float");
    	  return this;
      }

      public ArmsApiSQL andApiIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("api_id", value1, value2, ConditionMode.NOT_BETWEEN, "apiId", "java.lang.Long", "Float");
          return this;
      }
        
      public ArmsApiSQL andApiIdIn(List<java.lang.Long> values) {
          addCriterion("api_id", values, ConditionMode.IN, "apiId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiSQL andApiIdNotIn(List<java.lang.Long> values) {
          addCriterion("api_id", values, ConditionMode.NOT_IN, "apiId", "java.lang.Long", "Float");
          return this;
      }
	public ArmsApiSQL andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public ArmsApiSQL andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public ArmsApiSQL andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public ArmsApiSQL andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
      public ArmsApiSQL andSystemIdEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andSystemIdNotEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andSystemIdGreaterThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andSystemIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andSystemIdLessThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andSystemIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andSystemIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsApiSQL andSystemIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsApiSQL andSystemIdIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andSystemIdNotIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsApiSQL andUseridIsNull() {
		isnull("userid");
		return this;
	}
	
	public ArmsApiSQL andUseridIsNotNull() {
		notNull("userid");
		return this;
	}
	
	public ArmsApiSQL andUseridIsEmpty() {
		empty("userid");
		return this;
	}

	public ArmsApiSQL andUseridIsNotEmpty() {
		notEmpty("userid");
		return this;
	}
      public ArmsApiSQL andUseridEqualTo(java.lang.Long value) {
          addCriterion("userid", value, ConditionMode.EQUAL, "userid", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiSQL andUseridNotEqualTo(java.lang.Long value) {
          addCriterion("userid", value, ConditionMode.NOT_EQUAL, "userid", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiSQL andUseridGreaterThan(java.lang.Long value) {
          addCriterion("userid", value, ConditionMode.GREATER_THEN, "userid", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiSQL andUseridGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("userid", value, ConditionMode.GREATER_EQUAL, "userid", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiSQL andUseridLessThan(java.lang.Long value) {
          addCriterion("userid", value, ConditionMode.LESS_THEN, "userid", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiSQL andUseridLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("userid", value, ConditionMode.LESS_EQUAL, "userid", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiSQL andUseridBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("userid", value1, value2, ConditionMode.BETWEEN, "userid", "java.lang.Long", "Float");
    	  return this;
      }

      public ArmsApiSQL andUseridNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("userid", value1, value2, ConditionMode.NOT_BETWEEN, "userid", "java.lang.Long", "Float");
          return this;
      }
        
      public ArmsApiSQL andUseridIn(List<java.lang.Long> values) {
          addCriterion("userid", values, ConditionMode.IN, "userid", "java.lang.Long", "Float");
          return this;
      }

      public ArmsApiSQL andUseridNotIn(List<java.lang.Long> values) {
          addCriterion("userid", values, ConditionMode.NOT_IN, "userid", "java.lang.Long", "Float");
          return this;
      }
	public ArmsApiSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public ArmsApiSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public ArmsApiSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public ArmsApiSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public ArmsApiSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsApiSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "Float");
          return this;
      }
      public ArmsApiSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public ArmsApiSQL andUriIsNull() {
		isnull("uri");
		return this;
	}
	
	public ArmsApiSQL andUriIsNotNull() {
		notNull("uri");
		return this;
	}
	
	public ArmsApiSQL andUriIsEmpty() {
		empty("uri");
		return this;
	}

	public ArmsApiSQL andUriIsNotEmpty() {
		notEmpty("uri");
		return this;
	}
       public ArmsApiSQL andUriLike(java.lang.String value) {
    	   addCriterion("uri", value, ConditionMode.FUZZY, "uri", "java.lang.String", "String");
    	   return this;
      }

      public ArmsApiSQL andUriNotLike(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.NOT_FUZZY, "uri", "java.lang.String", "String");
          return this;
      }
      public ArmsApiSQL andUriEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andUriNotEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.NOT_EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andUriGreaterThan(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.GREATER_THEN, "uri", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andUriGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.GREATER_EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andUriLessThan(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.LESS_THEN, "uri", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andUriLessThanOrEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.LESS_EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andUriBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("uri", value1, value2, ConditionMode.BETWEEN, "uri", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiSQL andUriNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("uri", value1, value2, ConditionMode.NOT_BETWEEN, "uri", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiSQL andUriIn(List<java.lang.String> values) {
          addCriterion("uri", values, ConditionMode.IN, "uri", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andUriNotIn(List<java.lang.String> values) {
          addCriterion("uri", values, ConditionMode.NOT_IN, "uri", "java.lang.String", "String");
          return this;
      }
	public ArmsApiSQL andContentIsNull() {
		isnull("content");
		return this;
	}
	
	public ArmsApiSQL andContentIsNotNull() {
		notNull("content");
		return this;
	}
	
	public ArmsApiSQL andContentIsEmpty() {
		empty("content");
		return this;
	}

	public ArmsApiSQL andContentIsNotEmpty() {
		notEmpty("content");
		return this;
	}
       public ArmsApiSQL andContentLike(java.lang.String value) {
    	   addCriterion("content", value, ConditionMode.FUZZY, "content", "java.lang.String", "String");
    	   return this;
      }

      public ArmsApiSQL andContentNotLike(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_FUZZY, "content", "java.lang.String", "String");
          return this;
      }
      public ArmsApiSQL andContentEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andContentNotEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andContentGreaterThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andContentGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andContentLessThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andContentLessThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andContentBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("content", value1, value2, ConditionMode.BETWEEN, "content", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiSQL andContentNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("content", value1, value2, ConditionMode.NOT_BETWEEN, "content", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiSQL andContentIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.IN, "content", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andContentNotIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.NOT_IN, "content", "java.lang.String", "String");
          return this;
      }
	public ArmsApiSQL andExamIsNull() {
		isnull("exam");
		return this;
	}
	
	public ArmsApiSQL andExamIsNotNull() {
		notNull("exam");
		return this;
	}
	
	public ArmsApiSQL andExamIsEmpty() {
		empty("exam");
		return this;
	}

	public ArmsApiSQL andExamIsNotEmpty() {
		notEmpty("exam");
		return this;
	}
       public ArmsApiSQL andExamLike(java.lang.String value) {
    	   addCriterion("exam", value, ConditionMode.FUZZY, "exam", "java.lang.String", "String");
    	   return this;
      }

      public ArmsApiSQL andExamNotLike(java.lang.String value) {
          addCriterion("exam", value, ConditionMode.NOT_FUZZY, "exam", "java.lang.String", "String");
          return this;
      }
      public ArmsApiSQL andExamEqualTo(java.lang.String value) {
          addCriterion("exam", value, ConditionMode.EQUAL, "exam", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andExamNotEqualTo(java.lang.String value) {
          addCriterion("exam", value, ConditionMode.NOT_EQUAL, "exam", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andExamGreaterThan(java.lang.String value) {
          addCriterion("exam", value, ConditionMode.GREATER_THEN, "exam", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andExamGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("exam", value, ConditionMode.GREATER_EQUAL, "exam", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andExamLessThan(java.lang.String value) {
          addCriterion("exam", value, ConditionMode.LESS_THEN, "exam", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andExamLessThanOrEqualTo(java.lang.String value) {
          addCriterion("exam", value, ConditionMode.LESS_EQUAL, "exam", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andExamBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("exam", value1, value2, ConditionMode.BETWEEN, "exam", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiSQL andExamNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("exam", value1, value2, ConditionMode.NOT_BETWEEN, "exam", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiSQL andExamIn(List<java.lang.String> values) {
          addCriterion("exam", values, ConditionMode.IN, "exam", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andExamNotIn(List<java.lang.String> values) {
          addCriterion("exam", values, ConditionMode.NOT_IN, "exam", "java.lang.String", "String");
          return this;
      }
	public ArmsApiSQL andResultIsNull() {
		isnull("result");
		return this;
	}
	
	public ArmsApiSQL andResultIsNotNull() {
		notNull("result");
		return this;
	}
	
	public ArmsApiSQL andResultIsEmpty() {
		empty("result");
		return this;
	}

	public ArmsApiSQL andResultIsNotEmpty() {
		notEmpty("result");
		return this;
	}
       public ArmsApiSQL andResultLike(java.lang.String value) {
    	   addCriterion("result", value, ConditionMode.FUZZY, "result", "java.lang.String", "String");
    	   return this;
      }

      public ArmsApiSQL andResultNotLike(java.lang.String value) {
          addCriterion("result", value, ConditionMode.NOT_FUZZY, "result", "java.lang.String", "String");
          return this;
      }
      public ArmsApiSQL andResultEqualTo(java.lang.String value) {
          addCriterion("result", value, ConditionMode.EQUAL, "result", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andResultNotEqualTo(java.lang.String value) {
          addCriterion("result", value, ConditionMode.NOT_EQUAL, "result", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andResultGreaterThan(java.lang.String value) {
          addCriterion("result", value, ConditionMode.GREATER_THEN, "result", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andResultGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("result", value, ConditionMode.GREATER_EQUAL, "result", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andResultLessThan(java.lang.String value) {
          addCriterion("result", value, ConditionMode.LESS_THEN, "result", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andResultLessThanOrEqualTo(java.lang.String value) {
          addCriterion("result", value, ConditionMode.LESS_EQUAL, "result", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andResultBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("result", value1, value2, ConditionMode.BETWEEN, "result", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiSQL andResultNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("result", value1, value2, ConditionMode.NOT_BETWEEN, "result", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiSQL andResultIn(List<java.lang.String> values) {
          addCriterion("result", values, ConditionMode.IN, "result", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andResultNotIn(List<java.lang.String> values) {
          addCriterion("result", values, ConditionMode.NOT_IN, "result", "java.lang.String", "String");
          return this;
      }
	public ArmsApiSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public ArmsApiSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public ArmsApiSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public ArmsApiSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public ArmsApiSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsApiSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsApiSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsApiSQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public ArmsApiSQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public ArmsApiSQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public ArmsApiSQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public ArmsApiSQL andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public ArmsApiSQL andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public ArmsApiSQL andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public ArmsApiSQL andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public ArmsApiSQL andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public ArmsApiSQL andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public ArmsApiSQL andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public ArmsApiSQL andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public ArmsApiSQL andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public ArmsApiSQL andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
	public ArmsApiSQL andCatalogIsNull() {
		isnull("catalog");
		return this;
	}
	
	public ArmsApiSQL andCatalogIsNotNull() {
		notNull("catalog");
		return this;
	}
	
	public ArmsApiSQL andCatalogIsEmpty() {
		empty("catalog");
		return this;
	}

	public ArmsApiSQL andCatalogIsNotEmpty() {
		notEmpty("catalog");
		return this;
	}
       public ArmsApiSQL andCatalogLike(java.lang.String value) {
    	   addCriterion("catalog", value, ConditionMode.FUZZY, "catalog", "java.lang.String", "String");
    	   return this;
      }

      public ArmsApiSQL andCatalogNotLike(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.NOT_FUZZY, "catalog", "java.lang.String", "String");
          return this;
      }
      public ArmsApiSQL andCatalogEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andCatalogNotEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.NOT_EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andCatalogGreaterThan(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.GREATER_THEN, "catalog", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andCatalogGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.GREATER_EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andCatalogLessThan(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.LESS_THEN, "catalog", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andCatalogLessThanOrEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.LESS_EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andCatalogBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("catalog", value1, value2, ConditionMode.BETWEEN, "catalog", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiSQL andCatalogNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("catalog", value1, value2, ConditionMode.NOT_BETWEEN, "catalog", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiSQL andCatalogIn(List<java.lang.String> values) {
          addCriterion("catalog", values, ConditionMode.IN, "catalog", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andCatalogNotIn(List<java.lang.String> values) {
          addCriterion("catalog", values, ConditionMode.NOT_IN, "catalog", "java.lang.String", "String");
          return this;
      }
	public ArmsApiSQL andFlagIsNull() {
		isnull("flag");
		return this;
	}
	
	public ArmsApiSQL andFlagIsNotNull() {
		notNull("flag");
		return this;
	}
	
	public ArmsApiSQL andFlagIsEmpty() {
		empty("flag");
		return this;
	}

	public ArmsApiSQL andFlagIsNotEmpty() {
		notEmpty("flag");
		return this;
	}
       public ArmsApiSQL andFlagLike(java.lang.String value) {
    	   addCriterion("flag", value, ConditionMode.FUZZY, "flag", "java.lang.String", "String");
    	   return this;
      }

      public ArmsApiSQL andFlagNotLike(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.NOT_FUZZY, "flag", "java.lang.String", "String");
          return this;
      }
      public ArmsApiSQL andFlagEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andFlagNotEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.NOT_EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andFlagGreaterThan(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.GREATER_THEN, "flag", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andFlagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.GREATER_EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andFlagLessThan(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.LESS_THEN, "flag", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andFlagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flag", value, ConditionMode.LESS_EQUAL, "flag", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andFlagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flag", value1, value2, ConditionMode.BETWEEN, "flag", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiSQL andFlagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flag", value1, value2, ConditionMode.NOT_BETWEEN, "flag", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiSQL andFlagIn(List<java.lang.String> values) {
          addCriterion("flag", values, ConditionMode.IN, "flag", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andFlagNotIn(List<java.lang.String> values) {
          addCriterion("flag", values, ConditionMode.NOT_IN, "flag", "java.lang.String", "String");
          return this;
      }
	public ArmsApiSQL andSharedIsNull() {
		isnull("shared");
		return this;
	}
	
	public ArmsApiSQL andSharedIsNotNull() {
		notNull("shared");
		return this;
	}
	
	public ArmsApiSQL andSharedIsEmpty() {
		empty("shared");
		return this;
	}

	public ArmsApiSQL andSharedIsNotEmpty() {
		notEmpty("shared");
		return this;
	}
       public ArmsApiSQL andSharedLike(java.lang.String value) {
    	   addCriterion("shared", value, ConditionMode.FUZZY, "shared", "java.lang.String", "String");
    	   return this;
      }

      public ArmsApiSQL andSharedNotLike(java.lang.String value) {
          addCriterion("shared", value, ConditionMode.NOT_FUZZY, "shared", "java.lang.String", "String");
          return this;
      }
      public ArmsApiSQL andSharedEqualTo(java.lang.String value) {
          addCriterion("shared", value, ConditionMode.EQUAL, "shared", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andSharedNotEqualTo(java.lang.String value) {
          addCriterion("shared", value, ConditionMode.NOT_EQUAL, "shared", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andSharedGreaterThan(java.lang.String value) {
          addCriterion("shared", value, ConditionMode.GREATER_THEN, "shared", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andSharedGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("shared", value, ConditionMode.GREATER_EQUAL, "shared", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andSharedLessThan(java.lang.String value) {
          addCriterion("shared", value, ConditionMode.LESS_THEN, "shared", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andSharedLessThanOrEqualTo(java.lang.String value) {
          addCriterion("shared", value, ConditionMode.LESS_EQUAL, "shared", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andSharedBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("shared", value1, value2, ConditionMode.BETWEEN, "shared", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiSQL andSharedNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("shared", value1, value2, ConditionMode.NOT_BETWEEN, "shared", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiSQL andSharedIn(List<java.lang.String> values) {
          addCriterion("shared", values, ConditionMode.IN, "shared", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andSharedNotIn(List<java.lang.String> values) {
          addCriterion("shared", values, ConditionMode.NOT_IN, "shared", "java.lang.String", "String");
          return this;
      }
	public ArmsApiSQL andMethodIsNull() {
		isnull("method");
		return this;
	}
	
	public ArmsApiSQL andMethodIsNotNull() {
		notNull("method");
		return this;
	}
	
	public ArmsApiSQL andMethodIsEmpty() {
		empty("method");
		return this;
	}

	public ArmsApiSQL andMethodIsNotEmpty() {
		notEmpty("method");
		return this;
	}
       public ArmsApiSQL andMethodLike(java.lang.String value) {
    	   addCriterion("method", value, ConditionMode.FUZZY, "method", "java.lang.String", "String");
    	   return this;
      }

      public ArmsApiSQL andMethodNotLike(java.lang.String value) {
          addCriterion("method", value, ConditionMode.NOT_FUZZY, "method", "java.lang.String", "String");
          return this;
      }
      public ArmsApiSQL andMethodEqualTo(java.lang.String value) {
          addCriterion("method", value, ConditionMode.EQUAL, "method", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andMethodNotEqualTo(java.lang.String value) {
          addCriterion("method", value, ConditionMode.NOT_EQUAL, "method", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andMethodGreaterThan(java.lang.String value) {
          addCriterion("method", value, ConditionMode.GREATER_THEN, "method", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andMethodGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("method", value, ConditionMode.GREATER_EQUAL, "method", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andMethodLessThan(java.lang.String value) {
          addCriterion("method", value, ConditionMode.LESS_THEN, "method", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andMethodLessThanOrEqualTo(java.lang.String value) {
          addCriterion("method", value, ConditionMode.LESS_EQUAL, "method", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andMethodBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("method", value1, value2, ConditionMode.BETWEEN, "method", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiSQL andMethodNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("method", value1, value2, ConditionMode.NOT_BETWEEN, "method", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiSQL andMethodIn(List<java.lang.String> values) {
          addCriterion("method", values, ConditionMode.IN, "method", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andMethodNotIn(List<java.lang.String> values) {
          addCriterion("method", values, ConditionMode.NOT_IN, "method", "java.lang.String", "String");
          return this;
      }
	public ArmsApiSQL andPrjidIsNull() {
		isnull("prjid");
		return this;
	}
	
	public ArmsApiSQL andPrjidIsNotNull() {
		notNull("prjid");
		return this;
	}
	
	public ArmsApiSQL andPrjidIsEmpty() {
		empty("prjid");
		return this;
	}

	public ArmsApiSQL andPrjidIsNotEmpty() {
		notEmpty("prjid");
		return this;
	}
      public ArmsApiSQL andPrjidEqualTo(java.lang.Integer value) {
          addCriterion("prjid", value, ConditionMode.EQUAL, "prjid", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andPrjidNotEqualTo(java.lang.Integer value) {
          addCriterion("prjid", value, ConditionMode.NOT_EQUAL, "prjid", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andPrjidGreaterThan(java.lang.Integer value) {
          addCriterion("prjid", value, ConditionMode.GREATER_THEN, "prjid", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andPrjidGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("prjid", value, ConditionMode.GREATER_EQUAL, "prjid", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andPrjidLessThan(java.lang.Integer value) {
          addCriterion("prjid", value, ConditionMode.LESS_THEN, "prjid", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andPrjidLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("prjid", value, ConditionMode.LESS_EQUAL, "prjid", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andPrjidBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("prjid", value1, value2, ConditionMode.BETWEEN, "prjid", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsApiSQL andPrjidNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("prjid", value1, value2, ConditionMode.NOT_BETWEEN, "prjid", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsApiSQL andPrjidIn(List<java.lang.Integer> values) {
          addCriterion("prjid", values, ConditionMode.IN, "prjid", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsApiSQL andPrjidNotIn(List<java.lang.Integer> values) {
          addCriterion("prjid", values, ConditionMode.NOT_IN, "prjid", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsApiSQL andParamsIsNull() {
		isnull("params");
		return this;
	}
	
	public ArmsApiSQL andParamsIsNotNull() {
		notNull("params");
		return this;
	}
	
	public ArmsApiSQL andParamsIsEmpty() {
		empty("params");
		return this;
	}

	public ArmsApiSQL andParamsIsNotEmpty() {
		notEmpty("params");
		return this;
	}
       public ArmsApiSQL andParamsLike(java.lang.String value) {
    	   addCriterion("params", value, ConditionMode.FUZZY, "params", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsApiSQL andParamsNotLike(java.lang.String value) {
          addCriterion("params", value, ConditionMode.NOT_FUZZY, "params", "java.lang.String", "Float");
          return this;
      }
      public ArmsApiSQL andParamsEqualTo(java.lang.String value) {
          addCriterion("params", value, ConditionMode.EQUAL, "params", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andParamsNotEqualTo(java.lang.String value) {
          addCriterion("params", value, ConditionMode.NOT_EQUAL, "params", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andParamsGreaterThan(java.lang.String value) {
          addCriterion("params", value, ConditionMode.GREATER_THEN, "params", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andParamsGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("params", value, ConditionMode.GREATER_EQUAL, "params", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andParamsLessThan(java.lang.String value) {
          addCriterion("params", value, ConditionMode.LESS_THEN, "params", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andParamsLessThanOrEqualTo(java.lang.String value) {
          addCriterion("params", value, ConditionMode.LESS_EQUAL, "params", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andParamsBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("params", value1, value2, ConditionMode.BETWEEN, "params", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiSQL andParamsNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("params", value1, value2, ConditionMode.NOT_BETWEEN, "params", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiSQL andParamsIn(List<java.lang.String> values) {
          addCriterion("params", values, ConditionMode.IN, "params", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andParamsNotIn(List<java.lang.String> values) {
          addCriterion("params", values, ConditionMode.NOT_IN, "params", "java.lang.String", "String");
          return this;
      }
	public ArmsApiSQL andRetsIsNull() {
		isnull("rets");
		return this;
	}
	
	public ArmsApiSQL andRetsIsNotNull() {
		notNull("rets");
		return this;
	}
	
	public ArmsApiSQL andRetsIsEmpty() {
		empty("rets");
		return this;
	}

	public ArmsApiSQL andRetsIsNotEmpty() {
		notEmpty("rets");
		return this;
	}
       public ArmsApiSQL andRetsLike(java.lang.String value) {
    	   addCriterion("rets", value, ConditionMode.FUZZY, "rets", "java.lang.String", "String");
    	   return this;
      }

      public ArmsApiSQL andRetsNotLike(java.lang.String value) {
          addCriterion("rets", value, ConditionMode.NOT_FUZZY, "rets", "java.lang.String", "String");
          return this;
      }
      public ArmsApiSQL andRetsEqualTo(java.lang.String value) {
          addCriterion("rets", value, ConditionMode.EQUAL, "rets", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andRetsNotEqualTo(java.lang.String value) {
          addCriterion("rets", value, ConditionMode.NOT_EQUAL, "rets", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andRetsGreaterThan(java.lang.String value) {
          addCriterion("rets", value, ConditionMode.GREATER_THEN, "rets", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andRetsGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("rets", value, ConditionMode.GREATER_EQUAL, "rets", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andRetsLessThan(java.lang.String value) {
          addCriterion("rets", value, ConditionMode.LESS_THEN, "rets", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andRetsLessThanOrEqualTo(java.lang.String value) {
          addCriterion("rets", value, ConditionMode.LESS_EQUAL, "rets", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andRetsBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("rets", value1, value2, ConditionMode.BETWEEN, "rets", "java.lang.String", "String");
    	  return this;
      }

      public ArmsApiSQL andRetsNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("rets", value1, value2, ConditionMode.NOT_BETWEEN, "rets", "java.lang.String", "String");
          return this;
      }
        
      public ArmsApiSQL andRetsIn(List<java.lang.String> values) {
          addCriterion("rets", values, ConditionMode.IN, "rets", "java.lang.String", "String");
          return this;
      }

      public ArmsApiSQL andRetsNotIn(List<java.lang.String> values) {
          addCriterion("rets", values, ConditionMode.NOT_IN, "rets", "java.lang.String", "String");
          return this;
      }
}