/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.arms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class ArmsTenantUserSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static ArmsTenantUserSQL of() {
		return new ArmsTenantUserSQL();
	}
	
	public static ArmsTenantUserSQL by(Column column) {
		ArmsTenantUserSQL that = new ArmsTenantUserSQL();
		that.add(column);
        return that;
    }

    public static ArmsTenantUserSQL by(String name, Object value) {
        return (ArmsTenantUserSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("arms_tenant_user", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsTenantUserSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsTenantUserSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public ArmsTenantUserSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public ArmsTenantUserSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsTenantUserSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsTenantUserSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsTenantUserSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public ArmsTenantUserSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public ArmsTenantUserSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public ArmsTenantUserSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public ArmsTenantUserSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public ArmsTenantUserSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public ArmsTenantUserSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public ArmsTenantUserSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public ArmsTenantUserSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public ArmsTenantUserSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public ArmsTenantUserSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public ArmsTenantUserSQL andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantUserSQL andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantUserSQL andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantUserSQL andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantUserSQL andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantUserSQL andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantUserSQL andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public ArmsTenantUserSQL andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public ArmsTenantUserSQL andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantUserSQL andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public ArmsTenantUserSQL andTenantIdIsNull() {
		isnull("tenant_id");
		return this;
	}
	
	public ArmsTenantUserSQL andTenantIdIsNotNull() {
		notNull("tenant_id");
		return this;
	}
	
	public ArmsTenantUserSQL andTenantIdIsEmpty() {
		empty("tenant_id");
		return this;
	}

	public ArmsTenantUserSQL andTenantIdIsNotEmpty() {
		notEmpty("tenant_id");
		return this;
	}
       public ArmsTenantUserSQL andTenantIdLike(java.lang.String value) {
    	   addCriterion("tenant_id", value, ConditionMode.FUZZY, "tenantId", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsTenantUserSQL andTenantIdNotLike(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.NOT_FUZZY, "tenantId", "java.lang.String", "Float");
          return this;
      }
      public ArmsTenantUserSQL andTenantIdEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantUserSQL andTenantIdNotEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.NOT_EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantUserSQL andTenantIdGreaterThan(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.GREATER_THEN, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantUserSQL andTenantIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.GREATER_EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantUserSQL andTenantIdLessThan(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.LESS_THEN, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantUserSQL andTenantIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("tenant_id", value, ConditionMode.LESS_EQUAL, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantUserSQL andTenantIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("tenant_id", value1, value2, ConditionMode.BETWEEN, "tenantId", "java.lang.String", "String");
    	  return this;
      }

      public ArmsTenantUserSQL andTenantIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("tenant_id", value1, value2, ConditionMode.NOT_BETWEEN, "tenantId", "java.lang.String", "String");
          return this;
      }
        
      public ArmsTenantUserSQL andTenantIdIn(List<java.lang.String> values) {
          addCriterion("tenant_id", values, ConditionMode.IN, "tenantId", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantUserSQL andTenantIdNotIn(List<java.lang.String> values) {
          addCriterion("tenant_id", values, ConditionMode.NOT_IN, "tenantId", "java.lang.String", "String");
          return this;
      }
	public ArmsTenantUserSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public ArmsTenantUserSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public ArmsTenantUserSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public ArmsTenantUserSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public ArmsTenantUserSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantUserSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantUserSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantUserSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantUserSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantUserSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantUserSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public ArmsTenantUserSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public ArmsTenantUserSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public ArmsTenantUserSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public ArmsTenantUserSQL andUsernameIsNull() {
		isnull("username");
		return this;
	}
	
	public ArmsTenantUserSQL andUsernameIsNotNull() {
		notNull("username");
		return this;
	}
	
	public ArmsTenantUserSQL andUsernameIsEmpty() {
		empty("username");
		return this;
	}

	public ArmsTenantUserSQL andUsernameIsNotEmpty() {
		notEmpty("username");
		return this;
	}
       public ArmsTenantUserSQL andUsernameLike(java.lang.String value) {
    	   addCriterion("username", value, ConditionMode.FUZZY, "username", "java.lang.String", "Float");
    	   return this;
      }

      public ArmsTenantUserSQL andUsernameNotLike(java.lang.String value) {
          addCriterion("username", value, ConditionMode.NOT_FUZZY, "username", "java.lang.String", "Float");
          return this;
      }
      public ArmsTenantUserSQL andUsernameEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantUserSQL andUsernameNotEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.NOT_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantUserSQL andUsernameGreaterThan(java.lang.String value) {
          addCriterion("username", value, ConditionMode.GREATER_THEN, "username", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantUserSQL andUsernameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.GREATER_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantUserSQL andUsernameLessThan(java.lang.String value) {
          addCriterion("username", value, ConditionMode.LESS_THEN, "username", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantUserSQL andUsernameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.LESS_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantUserSQL andUsernameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("username", value1, value2, ConditionMode.BETWEEN, "username", "java.lang.String", "String");
    	  return this;
      }

      public ArmsTenantUserSQL andUsernameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("username", value1, value2, ConditionMode.NOT_BETWEEN, "username", "java.lang.String", "String");
          return this;
      }
        
      public ArmsTenantUserSQL andUsernameIn(List<java.lang.String> values) {
          addCriterion("username", values, ConditionMode.IN, "username", "java.lang.String", "String");
          return this;
      }

      public ArmsTenantUserSQL andUsernameNotIn(List<java.lang.String> values) {
          addCriterion("username", values, ConditionMode.NOT_IN, "username", "java.lang.String", "String");
          return this;
      }
	public ArmsTenantUserSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public ArmsTenantUserSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public ArmsTenantUserSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public ArmsTenantUserSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public ArmsTenantUserSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsTenantUserSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsTenantUserSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsTenantUserSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsTenantUserSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsTenantUserSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsTenantUserSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public ArmsTenantUserSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public ArmsTenantUserSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public ArmsTenantUserSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public ArmsTenantUserSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public ArmsTenantUserSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public ArmsTenantUserSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public ArmsTenantUserSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public ArmsTenantUserSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantUserSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantUserSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantUserSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantUserSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantUserSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantUserSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsTenantUserSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsTenantUserSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantUserSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public ArmsTenantUserSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public ArmsTenantUserSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public ArmsTenantUserSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public ArmsTenantUserSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
      public ArmsTenantUserSQL andTypeEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantUserSQL andTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantUserSQL andTypeGreaterThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantUserSQL andTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantUserSQL andTypeLessThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantUserSQL andTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantUserSQL andTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.Integer", "Float");
    	  return this;
      }

      public ArmsTenantUserSQL andTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.Integer", "Float");
          return this;
      }
        
      public ArmsTenantUserSQL andTypeIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public ArmsTenantUserSQL andTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.Integer", "Float");
          return this;
      }
}