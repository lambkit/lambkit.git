/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsCreditSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsCreditSQL of() {
		return new UpmsCreditSQL();
	}
	
	public static UpmsCreditSQL by(Column column) {
		UpmsCreditSQL that = new UpmsCreditSQL();
		that.add(column);
        return that;
    }

    public static UpmsCreditSQL by(String name, Object value) {
        return (UpmsCreditSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_credit", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsCreditSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsCreditSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsCreditSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsCreditSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsCreditSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsCreditSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsCreditSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsCreditSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsCreditSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsCreditSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsCreditSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsCreditSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsCreditSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsCreditSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public UpmsCreditSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public UpmsCreditSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public UpmsCreditSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public UpmsCreditSQL andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsCreditSQL andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsCreditSQL andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsCreditSQL andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsCreditSQL andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsCreditSQL andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsCreditSQL andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsCreditSQL andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsCreditSQL andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsCreditSQL andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public UpmsCreditSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public UpmsCreditSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public UpmsCreditSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public UpmsCreditSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public UpmsCreditSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsCreditSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsCreditSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsCreditSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsCreditSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsCreditSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsCreditSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsCreditSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsCreditSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsCreditSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsCreditSQL andCreditIsNull() {
		isnull("credit");
		return this;
	}
	
	public UpmsCreditSQL andCreditIsNotNull() {
		notNull("credit");
		return this;
	}
	
	public UpmsCreditSQL andCreditIsEmpty() {
		empty("credit");
		return this;
	}

	public UpmsCreditSQL andCreditIsNotEmpty() {
		notEmpty("credit");
		return this;
	}
      public UpmsCreditSQL andCreditEqualTo(java.lang.Integer value) {
          addCriterion("credit", value, ConditionMode.EQUAL, "credit", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsCreditSQL andCreditNotEqualTo(java.lang.Integer value) {
          addCriterion("credit", value, ConditionMode.NOT_EQUAL, "credit", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsCreditSQL andCreditGreaterThan(java.lang.Integer value) {
          addCriterion("credit", value, ConditionMode.GREATER_THEN, "credit", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsCreditSQL andCreditGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("credit", value, ConditionMode.GREATER_EQUAL, "credit", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsCreditSQL andCreditLessThan(java.lang.Integer value) {
          addCriterion("credit", value, ConditionMode.LESS_THEN, "credit", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsCreditSQL andCreditLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("credit", value, ConditionMode.LESS_EQUAL, "credit", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsCreditSQL andCreditBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("credit", value1, value2, ConditionMode.BETWEEN, "credit", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsCreditSQL andCreditNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("credit", value1, value2, ConditionMode.NOT_BETWEEN, "credit", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsCreditSQL andCreditIn(List<java.lang.Integer> values) {
          addCriterion("credit", values, ConditionMode.IN, "credit", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsCreditSQL andCreditNotIn(List<java.lang.Integer> values) {
          addCriterion("credit", values, ConditionMode.NOT_IN, "credit", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsCreditSQL andCtypeIsNull() {
		isnull("ctype");
		return this;
	}
	
	public UpmsCreditSQL andCtypeIsNotNull() {
		notNull("ctype");
		return this;
	}
	
	public UpmsCreditSQL andCtypeIsEmpty() {
		empty("ctype");
		return this;
	}

	public UpmsCreditSQL andCtypeIsNotEmpty() {
		notEmpty("ctype");
		return this;
	}
       public UpmsCreditSQL andCtypeLike(java.lang.String value) {
    	   addCriterion("ctype", value, ConditionMode.FUZZY, "ctype", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsCreditSQL andCtypeNotLike(java.lang.String value) {
          addCriterion("ctype", value, ConditionMode.NOT_FUZZY, "ctype", "java.lang.String", "Float");
          return this;
      }
      public UpmsCreditSQL andCtypeEqualTo(java.lang.String value) {
          addCriterion("ctype", value, ConditionMode.EQUAL, "ctype", "java.lang.String", "String");
          return this;
      }

      public UpmsCreditSQL andCtypeNotEqualTo(java.lang.String value) {
          addCriterion("ctype", value, ConditionMode.NOT_EQUAL, "ctype", "java.lang.String", "String");
          return this;
      }

      public UpmsCreditSQL andCtypeGreaterThan(java.lang.String value) {
          addCriterion("ctype", value, ConditionMode.GREATER_THEN, "ctype", "java.lang.String", "String");
          return this;
      }

      public UpmsCreditSQL andCtypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ctype", value, ConditionMode.GREATER_EQUAL, "ctype", "java.lang.String", "String");
          return this;
      }

      public UpmsCreditSQL andCtypeLessThan(java.lang.String value) {
          addCriterion("ctype", value, ConditionMode.LESS_THEN, "ctype", "java.lang.String", "String");
          return this;
      }

      public UpmsCreditSQL andCtypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ctype", value, ConditionMode.LESS_EQUAL, "ctype", "java.lang.String", "String");
          return this;
      }

      public UpmsCreditSQL andCtypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ctype", value1, value2, ConditionMode.BETWEEN, "ctype", "java.lang.String", "String");
    	  return this;
      }

      public UpmsCreditSQL andCtypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ctype", value1, value2, ConditionMode.NOT_BETWEEN, "ctype", "java.lang.String", "String");
          return this;
      }
        
      public UpmsCreditSQL andCtypeIn(List<java.lang.String> values) {
          addCriterion("ctype", values, ConditionMode.IN, "ctype", "java.lang.String", "String");
          return this;
      }

      public UpmsCreditSQL andCtypeNotIn(List<java.lang.String> values) {
          addCriterion("ctype", values, ConditionMode.NOT_IN, "ctype", "java.lang.String", "String");
          return this;
      }
	public UpmsCreditSQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public UpmsCreditSQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public UpmsCreditSQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public UpmsCreditSQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public UpmsCreditSQL andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsCreditSQL andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsCreditSQL andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsCreditSQL andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsCreditSQL andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsCreditSQL andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsCreditSQL andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public UpmsCreditSQL andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public UpmsCreditSQL andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsCreditSQL andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
	public UpmsCreditSQL andActionIsNull() {
		isnull("action");
		return this;
	}
	
	public UpmsCreditSQL andActionIsNotNull() {
		notNull("action");
		return this;
	}
	
	public UpmsCreditSQL andActionIsEmpty() {
		empty("action");
		return this;
	}

	public UpmsCreditSQL andActionIsNotEmpty() {
		notEmpty("action");
		return this;
	}
       public UpmsCreditSQL andActionLike(java.lang.String value) {
    	   addCriterion("action", value, ConditionMode.FUZZY, "action", "java.lang.String", "String");
    	   return this;
      }

      public UpmsCreditSQL andActionNotLike(java.lang.String value) {
          addCriterion("action", value, ConditionMode.NOT_FUZZY, "action", "java.lang.String", "String");
          return this;
      }
      public UpmsCreditSQL andActionEqualTo(java.lang.String value) {
          addCriterion("action", value, ConditionMode.EQUAL, "action", "java.lang.String", "String");
          return this;
      }

      public UpmsCreditSQL andActionNotEqualTo(java.lang.String value) {
          addCriterion("action", value, ConditionMode.NOT_EQUAL, "action", "java.lang.String", "String");
          return this;
      }

      public UpmsCreditSQL andActionGreaterThan(java.lang.String value) {
          addCriterion("action", value, ConditionMode.GREATER_THEN, "action", "java.lang.String", "String");
          return this;
      }

      public UpmsCreditSQL andActionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("action", value, ConditionMode.GREATER_EQUAL, "action", "java.lang.String", "String");
          return this;
      }

      public UpmsCreditSQL andActionLessThan(java.lang.String value) {
          addCriterion("action", value, ConditionMode.LESS_THEN, "action", "java.lang.String", "String");
          return this;
      }

      public UpmsCreditSQL andActionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("action", value, ConditionMode.LESS_EQUAL, "action", "java.lang.String", "String");
          return this;
      }

      public UpmsCreditSQL andActionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("action", value1, value2, ConditionMode.BETWEEN, "action", "java.lang.String", "String");
    	  return this;
      }

      public UpmsCreditSQL andActionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("action", value1, value2, ConditionMode.NOT_BETWEEN, "action", "java.lang.String", "String");
          return this;
      }
        
      public UpmsCreditSQL andActionIn(List<java.lang.String> values) {
          addCriterion("action", values, ConditionMode.IN, "action", "java.lang.String", "String");
          return this;
      }

      public UpmsCreditSQL andActionNotIn(List<java.lang.String> values) {
          addCriterion("action", values, ConditionMode.NOT_IN, "action", "java.lang.String", "String");
          return this;
      }
}