/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lambkit.core.Lambkit;
import com.lambkit.db.DbPool;
import com.lambkit.db.IDaoService;
import com.lambkit.db.IRowDao;
import com.lambkit.db.sql.Example;
import com.lambkit.module.upms.UpmsConfig;
import com.lambkit.module.upms.row.UpmsRolePermission;
import com.lambkit.module.upms.sql.UpmsRolePermissionSQL;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsRolePermissionService implements IDaoService<UpmsRolePermission> {
	public IRowDao<UpmsRolePermission> dao() {
		String dbPoolName = Lambkit.config(UpmsConfig.class).getDbPoolName();
		return DbPool.use(dbPoolName).dao(UpmsRolePermission.class);
	}

	public IRowDao<UpmsRolePermission> dao(String dbPoolName) {
		return DbPool.use(dbPoolName).dao(UpmsRolePermission.class);
	}

	public int rolePermission(JSONArray datas, Long id) {
		List<Long> deleteIds = new ArrayList<Long>();
		for (int i = 0; i < datas.size(); i ++) {
			JSONObject json = datas.getJSONObject(i);
			if (!json.getBoolean("checked")) {
				deleteIds.add(json.getLong("id"));
			} else {
				// 新增权限
				UpmsRolePermission upmsRolePermission = new UpmsRolePermission();
				upmsRolePermission.setRoleId(id);
				upmsRolePermission.setPermissionId(json.getLongValue("id"));
				dao().save(upmsRolePermission);
			}
		}
		// 删除权限
		if (deleteIds.size() > 0) {
			Example upmsRolePermissionExample = UpmsRolePermissionSQL.of()
					.andPermissionIdIn(deleteIds)
					.andRoleIdEqualTo(id).example();
			dao().delete(upmsRolePermissionExample);
		}
		return datas.size();
	}
}
