/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsRule extends RowModel<UpmsRule> {
	public UpmsRule() {
		setTableName("upms_rule");
		setPrimaryKey("id");
	}
	public java.lang.Long getId() {
		return this.get("id");
	}
	public void setId(java.lang.Long id) {
		this.set("id", id);
	}
	public java.lang.String getRuleName() {
		return this.get("rule_name");
	}
	public void setRuleName(java.lang.String ruleName) {
		this.set("rule_name", ruleName);
	}
	public java.lang.String getRuleCode() {
		return this.get("rule_code");
	}
	public void setRuleCode(java.lang.String ruleCode) {
		this.set("rule_code", ruleCode);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
	public java.util.Date getUpdateTime() {
		return this.get("update_time");
	}
	public void setUpdateTime(java.util.Date updateTime) {
		this.set("update_time", updateTime);
	}
	public java.lang.String getRemarks() {
		return this.get("remarks");
	}
	public void setRemarks(java.lang.String remarks) {
		this.set("remarks", remarks);
	}
	public java.lang.Integer getRuleType() {
		return this.get("rule_type");
	}
	public void setRuleType(java.lang.Integer ruleType) {
		this.set("rule_type", ruleType);
	}
	public java.lang.String getRuleContent() {
		return this.get("rule_content");
	}
	public void setRuleContent(java.lang.String ruleContent) {
		this.set("rule_content", ruleContent);
	}
	public java.lang.String getUrl() {
		return this.get("url");
	}
	public void setUrl(java.lang.String url) {
		this.set("url", url);
	}
}
