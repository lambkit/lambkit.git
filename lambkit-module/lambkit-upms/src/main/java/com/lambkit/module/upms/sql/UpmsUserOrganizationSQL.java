/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsUserOrganizationSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsUserOrganizationSQL of() {
		return new UpmsUserOrganizationSQL();
	}
	
	public static UpmsUserOrganizationSQL by(Column column) {
		UpmsUserOrganizationSQL that = new UpmsUserOrganizationSQL();
		that.add(column);
        return that;
    }

    public static UpmsUserOrganizationSQL by(String name, Object value) {
        return (UpmsUserOrganizationSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_user_organization", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserOrganizationSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserOrganizationSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsUserOrganizationSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsUserOrganizationSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserOrganizationSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserOrganizationSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserOrganizationSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserOrganizationSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsUserOrganizationSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsUserOrganizationSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsUserOrganizationSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsUserOrganizationSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsUserOrganizationSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsUserOrganizationSQL andUserOrganizationIdIsNull() {
		isnull("user_organization_id");
		return this;
	}
	
	public UpmsUserOrganizationSQL andUserOrganizationIdIsNotNull() {
		notNull("user_organization_id");
		return this;
	}
	
	public UpmsUserOrganizationSQL andUserOrganizationIdIsEmpty() {
		empty("user_organization_id");
		return this;
	}

	public UpmsUserOrganizationSQL andUserOrganizationIdIsNotEmpty() {
		notEmpty("user_organization_id");
		return this;
	}
      public UpmsUserOrganizationSQL andUserOrganizationIdEqualTo(java.lang.Long value) {
          addCriterion("user_organization_id", value, ConditionMode.EQUAL, "userOrganizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andUserOrganizationIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_organization_id", value, ConditionMode.NOT_EQUAL, "userOrganizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andUserOrganizationIdGreaterThan(java.lang.Long value) {
          addCriterion("user_organization_id", value, ConditionMode.GREATER_THEN, "userOrganizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andUserOrganizationIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_organization_id", value, ConditionMode.GREATER_EQUAL, "userOrganizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andUserOrganizationIdLessThan(java.lang.Long value) {
          addCriterion("user_organization_id", value, ConditionMode.LESS_THEN, "userOrganizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andUserOrganizationIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_organization_id", value, ConditionMode.LESS_EQUAL, "userOrganizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andUserOrganizationIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_organization_id", value1, value2, ConditionMode.BETWEEN, "userOrganizationId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserOrganizationSQL andUserOrganizationIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_organization_id", value1, value2, ConditionMode.NOT_BETWEEN, "userOrganizationId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserOrganizationSQL andUserOrganizationIdIn(List<java.lang.Long> values) {
          addCriterion("user_organization_id", values, ConditionMode.IN, "userOrganizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andUserOrganizationIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_organization_id", values, ConditionMode.NOT_IN, "userOrganizationId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserOrganizationSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public UpmsUserOrganizationSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public UpmsUserOrganizationSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public UpmsUserOrganizationSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public UpmsUserOrganizationSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserOrganizationSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserOrganizationSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserOrganizationSQL andOrganizationIdIsNull() {
		isnull("organization_id");
		return this;
	}
	
	public UpmsUserOrganizationSQL andOrganizationIdIsNotNull() {
		notNull("organization_id");
		return this;
	}
	
	public UpmsUserOrganizationSQL andOrganizationIdIsEmpty() {
		empty("organization_id");
		return this;
	}

	public UpmsUserOrganizationSQL andOrganizationIdIsNotEmpty() {
		notEmpty("organization_id");
		return this;
	}
      public UpmsUserOrganizationSQL andOrganizationIdEqualTo(java.lang.Long value) {
          addCriterion("organization_id", value, ConditionMode.EQUAL, "organizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andOrganizationIdNotEqualTo(java.lang.Long value) {
          addCriterion("organization_id", value, ConditionMode.NOT_EQUAL, "organizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andOrganizationIdGreaterThan(java.lang.Long value) {
          addCriterion("organization_id", value, ConditionMode.GREATER_THEN, "organizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andOrganizationIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("organization_id", value, ConditionMode.GREATER_EQUAL, "organizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andOrganizationIdLessThan(java.lang.Long value) {
          addCriterion("organization_id", value, ConditionMode.LESS_THEN, "organizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andOrganizationIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("organization_id", value, ConditionMode.LESS_EQUAL, "organizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andOrganizationIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("organization_id", value1, value2, ConditionMode.BETWEEN, "organizationId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserOrganizationSQL andOrganizationIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("organization_id", value1, value2, ConditionMode.NOT_BETWEEN, "organizationId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserOrganizationSQL andOrganizationIdIn(List<java.lang.Long> values) {
          addCriterion("organization_id", values, ConditionMode.IN, "organizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOrganizationSQL andOrganizationIdNotIn(List<java.lang.Long> values) {
          addCriterion("organization_id", values, ConditionMode.NOT_IN, "organizationId", "java.lang.Long", "Float");
          return this;
      }
}