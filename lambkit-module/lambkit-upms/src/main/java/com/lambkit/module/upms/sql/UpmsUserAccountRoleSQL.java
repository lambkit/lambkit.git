/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsUserAccountRoleSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsUserAccountRoleSQL of() {
		return new UpmsUserAccountRoleSQL();
	}
	
	public static UpmsUserAccountRoleSQL by(Column column) {
		UpmsUserAccountRoleSQL that = new UpmsUserAccountRoleSQL();
		that.add(column);
        return that;
    }

    public static UpmsUserAccountRoleSQL by(String name, Object value) {
        return (UpmsUserAccountRoleSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_user_account_role", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserAccountRoleSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserAccountRoleSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsUserAccountRoleSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsUserAccountRoleSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserAccountRoleSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserAccountRoleSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserAccountRoleSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserAccountRoleSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsUserAccountRoleSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsUserAccountRoleSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsUserAccountRoleSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsUserAccountRoleSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsUserAccountRoleSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsUserAccountRoleSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public UpmsUserAccountRoleSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public UpmsUserAccountRoleSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public UpmsUserAccountRoleSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public UpmsUserAccountRoleSQL andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserAccountRoleSQL andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserAccountRoleSQL andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserAccountRoleSQL andUserAccountIdIsNull() {
		isnull("user_account_id");
		return this;
	}
	
	public UpmsUserAccountRoleSQL andUserAccountIdIsNotNull() {
		notNull("user_account_id");
		return this;
	}
	
	public UpmsUserAccountRoleSQL andUserAccountIdIsEmpty() {
		empty("user_account_id");
		return this;
	}

	public UpmsUserAccountRoleSQL andUserAccountIdIsNotEmpty() {
		notEmpty("user_account_id");
		return this;
	}
      public UpmsUserAccountRoleSQL andUserAccountIdEqualTo(java.lang.Long value) {
          addCriterion("user_account_id", value, ConditionMode.EQUAL, "userAccountId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andUserAccountIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_account_id", value, ConditionMode.NOT_EQUAL, "userAccountId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andUserAccountIdGreaterThan(java.lang.Long value) {
          addCriterion("user_account_id", value, ConditionMode.GREATER_THEN, "userAccountId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andUserAccountIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_account_id", value, ConditionMode.GREATER_EQUAL, "userAccountId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andUserAccountIdLessThan(java.lang.Long value) {
          addCriterion("user_account_id", value, ConditionMode.LESS_THEN, "userAccountId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andUserAccountIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_account_id", value, ConditionMode.LESS_EQUAL, "userAccountId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andUserAccountIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_account_id", value1, value2, ConditionMode.BETWEEN, "userAccountId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserAccountRoleSQL andUserAccountIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_account_id", value1, value2, ConditionMode.NOT_BETWEEN, "userAccountId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserAccountRoleSQL andUserAccountIdIn(List<java.lang.Long> values) {
          addCriterion("user_account_id", values, ConditionMode.IN, "userAccountId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andUserAccountIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_account_id", values, ConditionMode.NOT_IN, "userAccountId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserAccountRoleSQL andRoleIdIsNull() {
		isnull("role_id");
		return this;
	}
	
	public UpmsUserAccountRoleSQL andRoleIdIsNotNull() {
		notNull("role_id");
		return this;
	}
	
	public UpmsUserAccountRoleSQL andRoleIdIsEmpty() {
		empty("role_id");
		return this;
	}

	public UpmsUserAccountRoleSQL andRoleIdIsNotEmpty() {
		notEmpty("role_id");
		return this;
	}
      public UpmsUserAccountRoleSQL andRoleIdEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andRoleIdNotEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.NOT_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andRoleIdGreaterThan(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.GREATER_THEN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andRoleIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.GREATER_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andRoleIdLessThan(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.LESS_THEN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andRoleIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.LESS_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andRoleIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("role_id", value1, value2, ConditionMode.BETWEEN, "roleId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserAccountRoleSQL andRoleIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("role_id", value1, value2, ConditionMode.NOT_BETWEEN, "roleId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserAccountRoleSQL andRoleIdIn(List<java.lang.Long> values) {
          addCriterion("role_id", values, ConditionMode.IN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountRoleSQL andRoleIdNotIn(List<java.lang.Long> values) {
          addCriterion("role_id", values, ConditionMode.NOT_IN, "roleId", "java.lang.Long", "Float");
          return this;
      }
}