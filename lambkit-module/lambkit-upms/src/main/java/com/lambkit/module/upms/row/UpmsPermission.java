/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsPermission extends RowModel<UpmsPermission> {

	public static final int STATUS_GUEST = 0;
	public static final int STATUS_LOGIN = 1;
	public static final int STATUS_RULE = 2;
	public static final int STATUS_LOCK = -1;

	public static final int TYPE_CATALOG = 0;//目录
	public static final int TYPE_MENU = 1;//菜单
	public static final int TYPE_BUTTON = 2;//按钮

	public UpmsPermission() {
		setTableName("upms_permission");
		setPrimaryKey("permission_id");
	}
	public java.lang.Long getPermissionId() {
		return this.get("permission_id");
	}
	public void setPermissionId(java.lang.Long permissionId) {
		this.set("permission_id", permissionId);
	}
	public java.lang.Long getSystemId() {
		return this.get("system_id");
	}
	public void setSystemId(java.lang.Long systemId) {
		this.set("system_id", systemId);
	}
	public java.lang.Long getPid() {
		return this.get("pid");
	}
	public void setPid(java.lang.Long pid) {
		this.set("pid", pid);
	}
	public java.lang.String getName() {
		return this.get("name");
	}
	public void setName(java.lang.String name) {
		this.set("name", name);
	}
	public java.lang.Integer getType() {
		return this.get("type");
	}
	public void setType(java.lang.Integer type) {
		this.set("type", type);
	}
	public java.lang.String getPermissionValue() {
		return this.get("permission_value");
	}
	public void setPermissionValue(java.lang.String permissionValue) {
		this.set("permission_value", permissionValue);
	}
	public java.lang.String getUri() {
		return this.get("uri");
	}
	public void setUri(java.lang.String uri) {
		this.set("uri", uri);
	}
	public java.lang.String getIcon() {
		return this.get("icon");
	}
	public void setIcon(java.lang.String icon) {
		this.set("icon", icon);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.util.Date getCtime() {
		return this.get("ctime");
	}
	public void setCtime(java.util.Date ctime) {
		this.set("ctime", ctime);
	}
	public java.lang.Long getOrders() {
		return this.get("orders");
	}
	public void setOrders(java.lang.Long orders) {
		this.set("orders", orders);
	}
}
