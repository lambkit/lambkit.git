package com.lambkit.module.upms;


import com.lambkit.auth.AuthUser;
import com.lambkit.auth.service.AuthKernelService;
import com.lambkit.module.upms.auth.cache.UpmsCache;
import com.lambkit.module.upms.row.UpmsUser;

import java.util.Map;

/**
 * UPMS实现内核
 * @author yangyong(孤竹行)
 */
public interface UpmsKernel {

    /**
     * 插件的名称
     * @return
     */
    String getName();

    AuthKernelService getAuthKernelService();

    UpmsCache getCache();

    String getSessionId();

    AuthUser getAuth();

    /**
     * 用户的ID
     * @return
     */
    String getAuthId();

    /**
     * 校验用户是否有接口访问的权限，接口存在不同技术使用的专门注解，这里需要自己去实现
     * @return
     */
    //int validation(Invocation invocation, UpmsUser upmsUser);

    boolean hasAnyRoles(UpmsUser upmsUser, String aSuper);

    boolean hasRule(UpmsUser upmsUser, Long permissionId);

    /**
     * 获取会话列表
     * @param offset
     * @param limit
     * @return
     */
    Map getActiveSessions(int offset, int limit);

    /**
     * 强制退出
     * @param ids
     * @return
     */
    int forceout(String ids);

}
