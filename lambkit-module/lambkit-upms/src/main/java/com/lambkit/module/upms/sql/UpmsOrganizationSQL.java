/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsOrganizationSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsOrganizationSQL of() {
		return new UpmsOrganizationSQL();
	}
	
	public static UpmsOrganizationSQL by(Column column) {
		UpmsOrganizationSQL that = new UpmsOrganizationSQL();
		that.add(column);
        return that;
    }

    public static UpmsOrganizationSQL by(String name, Object value) {
        return (UpmsOrganizationSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_organization", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsOrganizationSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsOrganizationSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsOrganizationSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsOrganizationSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsOrganizationSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsOrganizationSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsOrganizationSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsOrganizationSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsOrganizationSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsOrganizationSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsOrganizationSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsOrganizationSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsOrganizationSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsOrganizationSQL andOrganizationIdIsNull() {
		isnull("organization_id");
		return this;
	}
	
	public UpmsOrganizationSQL andOrganizationIdIsNotNull() {
		notNull("organization_id");
		return this;
	}
	
	public UpmsOrganizationSQL andOrganizationIdIsEmpty() {
		empty("organization_id");
		return this;
	}

	public UpmsOrganizationSQL andOrganizationIdIsNotEmpty() {
		notEmpty("organization_id");
		return this;
	}
      public UpmsOrganizationSQL andOrganizationIdEqualTo(java.lang.Long value) {
          addCriterion("organization_id", value, ConditionMode.EQUAL, "organizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andOrganizationIdNotEqualTo(java.lang.Long value) {
          addCriterion("organization_id", value, ConditionMode.NOT_EQUAL, "organizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andOrganizationIdGreaterThan(java.lang.Long value) {
          addCriterion("organization_id", value, ConditionMode.GREATER_THEN, "organizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andOrganizationIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("organization_id", value, ConditionMode.GREATER_EQUAL, "organizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andOrganizationIdLessThan(java.lang.Long value) {
          addCriterion("organization_id", value, ConditionMode.LESS_THEN, "organizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andOrganizationIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("organization_id", value, ConditionMode.LESS_EQUAL, "organizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andOrganizationIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("organization_id", value1, value2, ConditionMode.BETWEEN, "organizationId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsOrganizationSQL andOrganizationIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("organization_id", value1, value2, ConditionMode.NOT_BETWEEN, "organizationId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsOrganizationSQL andOrganizationIdIn(List<java.lang.Long> values) {
          addCriterion("organization_id", values, ConditionMode.IN, "organizationId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andOrganizationIdNotIn(List<java.lang.Long> values) {
          addCriterion("organization_id", values, ConditionMode.NOT_IN, "organizationId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsOrganizationSQL andPidIsNull() {
		isnull("pid");
		return this;
	}
	
	public UpmsOrganizationSQL andPidIsNotNull() {
		notNull("pid");
		return this;
	}
	
	public UpmsOrganizationSQL andPidIsEmpty() {
		empty("pid");
		return this;
	}

	public UpmsOrganizationSQL andPidIsNotEmpty() {
		notEmpty("pid");
		return this;
	}
      public UpmsOrganizationSQL andPidEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andPidNotEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.NOT_EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andPidGreaterThan(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.GREATER_THEN, "pid", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andPidGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.GREATER_EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andPidLessThan(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.LESS_THEN, "pid", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andPidLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.LESS_EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andPidBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("pid", value1, value2, ConditionMode.BETWEEN, "pid", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsOrganizationSQL andPidNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("pid", value1, value2, ConditionMode.NOT_BETWEEN, "pid", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsOrganizationSQL andPidIn(List<java.lang.Long> values) {
          addCriterion("pid", values, ConditionMode.IN, "pid", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andPidNotIn(List<java.lang.Long> values) {
          addCriterion("pid", values, ConditionMode.NOT_IN, "pid", "java.lang.Long", "Float");
          return this;
      }
	public UpmsOrganizationSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public UpmsOrganizationSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public UpmsOrganizationSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public UpmsOrganizationSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public UpmsOrganizationSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsOrganizationSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public UpmsOrganizationSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsOrganizationSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsOrganizationSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsOrganizationSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsOrganizationSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsOrganizationSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsOrganizationSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public UpmsOrganizationSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public UpmsOrganizationSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsOrganizationSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public UpmsOrganizationSQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public UpmsOrganizationSQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public UpmsOrganizationSQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public UpmsOrganizationSQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public UpmsOrganizationSQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public UpmsOrganizationSQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public UpmsOrganizationSQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsOrganizationSQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsOrganizationSQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsOrganizationSQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsOrganizationSQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsOrganizationSQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsOrganizationSQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public UpmsOrganizationSQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public UpmsOrganizationSQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsOrganizationSQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public UpmsOrganizationSQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public UpmsOrganizationSQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public UpmsOrganizationSQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public UpmsOrganizationSQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public UpmsOrganizationSQL andCtimeEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andCtimeNotEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andCtimeGreaterThan(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andCtimeGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andCtimeLessThan(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andCtimeLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andCtimeBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsOrganizationSQL andCtimeNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsOrganizationSQL andCtimeIn(List<java.lang.Long> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOrganizationSQL andCtimeNotIn(List<java.lang.Long> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.lang.Long", "Float");
          return this;
      }
}