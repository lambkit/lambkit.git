package com.lambkit.module.upms.kernel;

import java.io.Serializable;

/**
 * @author yangyong(孤竹行)
 */
public class UpmsVerifyResult implements Serializable {
    private String kernelName;

    private boolean allowAccess = false;

    public static UpmsVerifyResult create(String kernelName, boolean allowAccess) {
        UpmsVerifyResult result = new UpmsVerifyResult();
        result.setKernelName(kernelName);
        result.setAllowAccess(allowAccess);
        return result;
    }
    public String getKernelName() {
        return kernelName;
    }

    public void setKernelName(String kernelName) {
        this.kernelName = kernelName;
    }

    public boolean isAllowAccess() {
        return allowAccess;
    }

    public void setAllowAccess(boolean allowAccess) {
        this.allowAccess = allowAccess;
    }
}
