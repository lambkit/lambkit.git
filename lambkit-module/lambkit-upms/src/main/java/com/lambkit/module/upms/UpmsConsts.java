/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lambkit.module.upms;

/**
 * upms系统常量类 
 */
public class UpmsConsts {

	public static final String UPMS_TYPE = "lambkit.upms.type";

	//loginType = 0-account-password, 1-phone-verifyCode, 99-unpassword无密认证
	public static final int UPMS_LOGIN_TYPE_ACCOUNT = 0;
	public static final int UPMS_LOGIN_TYPE_PHONE = 1;
	public static final int UPMS_LOGIN_TYPE_UNPSWD = 99;

	// 全局会话key
	public final static String LAMBKIT_UPMS_SERVER_SESSION_ID = "lambkit-upms-server-session-id";
	// 全局会话key列表
	public final static String LAMBKIT_UPMS_SERVER_SESSION_IDS = "lambkit-upms-server-session-ids";
	// code key
	public final static String LAMBKIT_UPMS_SERVER_CODE = "lambkit-upms-server-code";
	// 局部会话key
	public final static String LAMBKIT_UPMS_CLIENT_SESSION_ID = "lambkit-upms-client-session-id";
    // 单点同一个code所有局部会话key
	public final static String LAMBKIT_UPMS_CLIENT_SESSION_IDS = "lambkit-upms-client-session-ids";
}
