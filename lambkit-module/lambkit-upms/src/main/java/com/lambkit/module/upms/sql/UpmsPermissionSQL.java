/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsPermissionSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsPermissionSQL of() {
		return new UpmsPermissionSQL();
	}
	
	public static UpmsPermissionSQL by(Column column) {
		UpmsPermissionSQL that = new UpmsPermissionSQL();
		that.add(column);
        return that;
    }

    public static UpmsPermissionSQL by(String name, Object value) {
        return (UpmsPermissionSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_permission", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsPermissionSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsPermissionSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsPermissionSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsPermissionSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsPermissionSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsPermissionSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsPermissionSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsPermissionSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsPermissionSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsPermissionSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsPermissionSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsPermissionSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsPermissionSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsPermissionSQL andPermissionIdIsNull() {
		isnull("permission_id");
		return this;
	}
	
	public UpmsPermissionSQL andPermissionIdIsNotNull() {
		notNull("permission_id");
		return this;
	}
	
	public UpmsPermissionSQL andPermissionIdIsEmpty() {
		empty("permission_id");
		return this;
	}

	public UpmsPermissionSQL andPermissionIdIsNotEmpty() {
		notEmpty("permission_id");
		return this;
	}
      public UpmsPermissionSQL andPermissionIdEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andPermissionIdNotEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.NOT_EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andPermissionIdGreaterThan(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.GREATER_THEN, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andPermissionIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.GREATER_EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andPermissionIdLessThan(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.LESS_THEN, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andPermissionIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.LESS_EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andPermissionIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("permission_id", value1, value2, ConditionMode.BETWEEN, "permissionId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsPermissionSQL andPermissionIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("permission_id", value1, value2, ConditionMode.NOT_BETWEEN, "permissionId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsPermissionSQL andPermissionIdIn(List<java.lang.Long> values) {
          addCriterion("permission_id", values, ConditionMode.IN, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andPermissionIdNotIn(List<java.lang.Long> values) {
          addCriterion("permission_id", values, ConditionMode.NOT_IN, "permissionId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsPermissionSQL andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public UpmsPermissionSQL andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public UpmsPermissionSQL andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public UpmsPermissionSQL andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
      public UpmsPermissionSQL andSystemIdEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andSystemIdNotEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andSystemIdGreaterThan(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andSystemIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andSystemIdLessThan(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andSystemIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andSystemIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsPermissionSQL andSystemIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsPermissionSQL andSystemIdIn(List<java.lang.Long> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andSystemIdNotIn(List<java.lang.Long> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsPermissionSQL andPidIsNull() {
		isnull("pid");
		return this;
	}
	
	public UpmsPermissionSQL andPidIsNotNull() {
		notNull("pid");
		return this;
	}
	
	public UpmsPermissionSQL andPidIsEmpty() {
		empty("pid");
		return this;
	}

	public UpmsPermissionSQL andPidIsNotEmpty() {
		notEmpty("pid");
		return this;
	}
      public UpmsPermissionSQL andPidEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andPidNotEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.NOT_EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andPidGreaterThan(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.GREATER_THEN, "pid", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andPidGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.GREATER_EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andPidLessThan(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.LESS_THEN, "pid", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andPidLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("pid", value, ConditionMode.LESS_EQUAL, "pid", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andPidBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("pid", value1, value2, ConditionMode.BETWEEN, "pid", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsPermissionSQL andPidNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("pid", value1, value2, ConditionMode.NOT_BETWEEN, "pid", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsPermissionSQL andPidIn(List<java.lang.Long> values) {
          addCriterion("pid", values, ConditionMode.IN, "pid", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andPidNotIn(List<java.lang.Long> values) {
          addCriterion("pid", values, ConditionMode.NOT_IN, "pid", "java.lang.Long", "Float");
          return this;
      }
	public UpmsPermissionSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public UpmsPermissionSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public UpmsPermissionSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public UpmsPermissionSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public UpmsPermissionSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsPermissionSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public UpmsPermissionSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public UpmsPermissionSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public UpmsPermissionSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public UpmsPermissionSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public UpmsPermissionSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public UpmsPermissionSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public UpmsPermissionSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
      public UpmsPermissionSQL andTypeEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsPermissionSQL andTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsPermissionSQL andTypeGreaterThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsPermissionSQL andTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsPermissionSQL andTypeLessThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsPermissionSQL andTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsPermissionSQL andTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsPermissionSQL andTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsPermissionSQL andTypeIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsPermissionSQL andTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsPermissionSQL andPermissionValueIsNull() {
		isnull("permission_value");
		return this;
	}
	
	public UpmsPermissionSQL andPermissionValueIsNotNull() {
		notNull("permission_value");
		return this;
	}
	
	public UpmsPermissionSQL andPermissionValueIsEmpty() {
		empty("permission_value");
		return this;
	}

	public UpmsPermissionSQL andPermissionValueIsNotEmpty() {
		notEmpty("permission_value");
		return this;
	}
       public UpmsPermissionSQL andPermissionValueLike(java.lang.String value) {
    	   addCriterion("permission_value", value, ConditionMode.FUZZY, "permissionValue", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsPermissionSQL andPermissionValueNotLike(java.lang.String value) {
          addCriterion("permission_value", value, ConditionMode.NOT_FUZZY, "permissionValue", "java.lang.String", "Float");
          return this;
      }
      public UpmsPermissionSQL andPermissionValueEqualTo(java.lang.String value) {
          addCriterion("permission_value", value, ConditionMode.EQUAL, "permissionValue", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andPermissionValueNotEqualTo(java.lang.String value) {
          addCriterion("permission_value", value, ConditionMode.NOT_EQUAL, "permissionValue", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andPermissionValueGreaterThan(java.lang.String value) {
          addCriterion("permission_value", value, ConditionMode.GREATER_THEN, "permissionValue", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andPermissionValueGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("permission_value", value, ConditionMode.GREATER_EQUAL, "permissionValue", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andPermissionValueLessThan(java.lang.String value) {
          addCriterion("permission_value", value, ConditionMode.LESS_THEN, "permissionValue", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andPermissionValueLessThanOrEqualTo(java.lang.String value) {
          addCriterion("permission_value", value, ConditionMode.LESS_EQUAL, "permissionValue", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andPermissionValueBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("permission_value", value1, value2, ConditionMode.BETWEEN, "permissionValue", "java.lang.String", "String");
    	  return this;
      }

      public UpmsPermissionSQL andPermissionValueNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("permission_value", value1, value2, ConditionMode.NOT_BETWEEN, "permissionValue", "java.lang.String", "String");
          return this;
      }
        
      public UpmsPermissionSQL andPermissionValueIn(List<java.lang.String> values) {
          addCriterion("permission_value", values, ConditionMode.IN, "permissionValue", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andPermissionValueNotIn(List<java.lang.String> values) {
          addCriterion("permission_value", values, ConditionMode.NOT_IN, "permissionValue", "java.lang.String", "String");
          return this;
      }
	public UpmsPermissionSQL andUriIsNull() {
		isnull("uri");
		return this;
	}
	
	public UpmsPermissionSQL andUriIsNotNull() {
		notNull("uri");
		return this;
	}
	
	public UpmsPermissionSQL andUriIsEmpty() {
		empty("uri");
		return this;
	}

	public UpmsPermissionSQL andUriIsNotEmpty() {
		notEmpty("uri");
		return this;
	}
       public UpmsPermissionSQL andUriLike(java.lang.String value) {
    	   addCriterion("uri", value, ConditionMode.FUZZY, "uri", "java.lang.String", "String");
    	   return this;
      }

      public UpmsPermissionSQL andUriNotLike(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.NOT_FUZZY, "uri", "java.lang.String", "String");
          return this;
      }
      public UpmsPermissionSQL andUriEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andUriNotEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.NOT_EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andUriGreaterThan(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.GREATER_THEN, "uri", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andUriGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.GREATER_EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andUriLessThan(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.LESS_THEN, "uri", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andUriLessThanOrEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.LESS_EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andUriBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("uri", value1, value2, ConditionMode.BETWEEN, "uri", "java.lang.String", "String");
    	  return this;
      }

      public UpmsPermissionSQL andUriNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("uri", value1, value2, ConditionMode.NOT_BETWEEN, "uri", "java.lang.String", "String");
          return this;
      }
        
      public UpmsPermissionSQL andUriIn(List<java.lang.String> values) {
          addCriterion("uri", values, ConditionMode.IN, "uri", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andUriNotIn(List<java.lang.String> values) {
          addCriterion("uri", values, ConditionMode.NOT_IN, "uri", "java.lang.String", "String");
          return this;
      }
	public UpmsPermissionSQL andIconIsNull() {
		isnull("icon");
		return this;
	}
	
	public UpmsPermissionSQL andIconIsNotNull() {
		notNull("icon");
		return this;
	}
	
	public UpmsPermissionSQL andIconIsEmpty() {
		empty("icon");
		return this;
	}

	public UpmsPermissionSQL andIconIsNotEmpty() {
		notEmpty("icon");
		return this;
	}
       public UpmsPermissionSQL andIconLike(java.lang.String value) {
    	   addCriterion("icon", value, ConditionMode.FUZZY, "icon", "java.lang.String", "String");
    	   return this;
      }

      public UpmsPermissionSQL andIconNotLike(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.NOT_FUZZY, "icon", "java.lang.String", "String");
          return this;
      }
      public UpmsPermissionSQL andIconEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andIconNotEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.NOT_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andIconGreaterThan(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.GREATER_THEN, "icon", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andIconGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.GREATER_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andIconLessThan(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.LESS_THEN, "icon", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andIconLessThanOrEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.LESS_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andIconBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("icon", value1, value2, ConditionMode.BETWEEN, "icon", "java.lang.String", "String");
    	  return this;
      }

      public UpmsPermissionSQL andIconNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("icon", value1, value2, ConditionMode.NOT_BETWEEN, "icon", "java.lang.String", "String");
          return this;
      }
        
      public UpmsPermissionSQL andIconIn(List<java.lang.String> values) {
          addCriterion("icon", values, ConditionMode.IN, "icon", "java.lang.String", "String");
          return this;
      }

      public UpmsPermissionSQL andIconNotIn(List<java.lang.String> values) {
          addCriterion("icon", values, ConditionMode.NOT_IN, "icon", "java.lang.String", "String");
          return this;
      }
	public UpmsPermissionSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public UpmsPermissionSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public UpmsPermissionSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public UpmsPermissionSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public UpmsPermissionSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsPermissionSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsPermissionSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsPermissionSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsPermissionSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsPermissionSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsPermissionSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsPermissionSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsPermissionSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsPermissionSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsPermissionSQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public UpmsPermissionSQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public UpmsPermissionSQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public UpmsPermissionSQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public UpmsPermissionSQL andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsPermissionSQL andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsPermissionSQL andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsPermissionSQL andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsPermissionSQL andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsPermissionSQL andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsPermissionSQL andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public UpmsPermissionSQL andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public UpmsPermissionSQL andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsPermissionSQL andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
	public UpmsPermissionSQL andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public UpmsPermissionSQL andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public UpmsPermissionSQL andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public UpmsPermissionSQL andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
      public UpmsPermissionSQL andOrdersEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andOrdersNotEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andOrdersGreaterThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andOrdersGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andOrdersLessThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andOrdersLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andOrdersBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsPermissionSQL andOrdersNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsPermissionSQL andOrdersIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsPermissionSQL andOrdersNotIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.lang.Long", "Float");
          return this;
      }
}