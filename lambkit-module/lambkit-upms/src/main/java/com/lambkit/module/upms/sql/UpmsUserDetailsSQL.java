/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsUserDetailsSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsUserDetailsSQL of() {
		return new UpmsUserDetailsSQL();
	}
	
	public static UpmsUserDetailsSQL by(Column column) {
		UpmsUserDetailsSQL that = new UpmsUserDetailsSQL();
		that.add(column);
        return that;
    }

    public static UpmsUserDetailsSQL by(String name, Object value) {
        return (UpmsUserDetailsSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_user_details", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserDetailsSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserDetailsSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsUserDetailsSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsUserDetailsSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserDetailsSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserDetailsSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserDetailsSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserDetailsSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsUserDetailsSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsUserDetailsSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsUserDetailsSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsUserDetailsSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsUserDetailsSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsUserDetailsSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public UpmsUserDetailsSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public UpmsUserDetailsSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public UpmsUserDetailsSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public UpmsUserDetailsSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserDetailsSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserDetailsSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserDetailsSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserDetailsSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserDetailsSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserDetailsSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserDetailsSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserDetailsSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserDetailsSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserDetailsSQL andSignatureIsNull() {
		isnull("signature");
		return this;
	}
	
	public UpmsUserDetailsSQL andSignatureIsNotNull() {
		notNull("signature");
		return this;
	}
	
	public UpmsUserDetailsSQL andSignatureIsEmpty() {
		empty("signature");
		return this;
	}

	public UpmsUserDetailsSQL andSignatureIsNotEmpty() {
		notEmpty("signature");
		return this;
	}
       public UpmsUserDetailsSQL andSignatureLike(java.lang.String value) {
    	   addCriterion("signature", value, ConditionMode.FUZZY, "signature", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsUserDetailsSQL andSignatureNotLike(java.lang.String value) {
          addCriterion("signature", value, ConditionMode.NOT_FUZZY, "signature", "java.lang.String", "Float");
          return this;
      }
      public UpmsUserDetailsSQL andSignatureEqualTo(java.lang.String value) {
          addCriterion("signature", value, ConditionMode.EQUAL, "signature", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andSignatureNotEqualTo(java.lang.String value) {
          addCriterion("signature", value, ConditionMode.NOT_EQUAL, "signature", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andSignatureGreaterThan(java.lang.String value) {
          addCriterion("signature", value, ConditionMode.GREATER_THEN, "signature", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andSignatureGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("signature", value, ConditionMode.GREATER_EQUAL, "signature", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andSignatureLessThan(java.lang.String value) {
          addCriterion("signature", value, ConditionMode.LESS_THEN, "signature", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andSignatureLessThanOrEqualTo(java.lang.String value) {
          addCriterion("signature", value, ConditionMode.LESS_EQUAL, "signature", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andSignatureBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("signature", value1, value2, ConditionMode.BETWEEN, "signature", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserDetailsSQL andSignatureNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("signature", value1, value2, ConditionMode.NOT_BETWEEN, "signature", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserDetailsSQL andSignatureIn(List<java.lang.String> values) {
          addCriterion("signature", values, ConditionMode.IN, "signature", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andSignatureNotIn(List<java.lang.String> values) {
          addCriterion("signature", values, ConditionMode.NOT_IN, "signature", "java.lang.String", "String");
          return this;
      }
	public UpmsUserDetailsSQL andRealNameIsNull() {
		isnull("real_name");
		return this;
	}
	
	public UpmsUserDetailsSQL andRealNameIsNotNull() {
		notNull("real_name");
		return this;
	}
	
	public UpmsUserDetailsSQL andRealNameIsEmpty() {
		empty("real_name");
		return this;
	}

	public UpmsUserDetailsSQL andRealNameIsNotEmpty() {
		notEmpty("real_name");
		return this;
	}
       public UpmsUserDetailsSQL andRealNameLike(java.lang.String value) {
    	   addCriterion("real_name", value, ConditionMode.FUZZY, "realName", "java.lang.String", "String");
    	   return this;
      }

      public UpmsUserDetailsSQL andRealNameNotLike(java.lang.String value) {
          addCriterion("real_name", value, ConditionMode.NOT_FUZZY, "realName", "java.lang.String", "String");
          return this;
      }
      public UpmsUserDetailsSQL andRealNameEqualTo(java.lang.String value) {
          addCriterion("real_name", value, ConditionMode.EQUAL, "realName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andRealNameNotEqualTo(java.lang.String value) {
          addCriterion("real_name", value, ConditionMode.NOT_EQUAL, "realName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andRealNameGreaterThan(java.lang.String value) {
          addCriterion("real_name", value, ConditionMode.GREATER_THEN, "realName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andRealNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("real_name", value, ConditionMode.GREATER_EQUAL, "realName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andRealNameLessThan(java.lang.String value) {
          addCriterion("real_name", value, ConditionMode.LESS_THEN, "realName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andRealNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("real_name", value, ConditionMode.LESS_EQUAL, "realName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andRealNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("real_name", value1, value2, ConditionMode.BETWEEN, "realName", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserDetailsSQL andRealNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("real_name", value1, value2, ConditionMode.NOT_BETWEEN, "realName", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserDetailsSQL andRealNameIn(List<java.lang.String> values) {
          addCriterion("real_name", values, ConditionMode.IN, "realName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andRealNameNotIn(List<java.lang.String> values) {
          addCriterion("real_name", values, ConditionMode.NOT_IN, "realName", "java.lang.String", "String");
          return this;
      }
	public UpmsUserDetailsSQL andBirthdayIsNull() {
		isnull("birthday");
		return this;
	}
	
	public UpmsUserDetailsSQL andBirthdayIsNotNull() {
		notNull("birthday");
		return this;
	}
	
	public UpmsUserDetailsSQL andBirthdayIsEmpty() {
		empty("birthday");
		return this;
	}

	public UpmsUserDetailsSQL andBirthdayIsNotEmpty() {
		notEmpty("birthday");
		return this;
	}
      public UpmsUserDetailsSQL andBirthdayEqualTo(java.util.Date value) {
          addCriterion("birthday", value, ConditionMode.EQUAL, "birthday", "java.util.Date", "String");
          return this;
      }

      public UpmsUserDetailsSQL andBirthdayNotEqualTo(java.util.Date value) {
          addCriterion("birthday", value, ConditionMode.NOT_EQUAL, "birthday", "java.util.Date", "String");
          return this;
      }

      public UpmsUserDetailsSQL andBirthdayGreaterThan(java.util.Date value) {
          addCriterion("birthday", value, ConditionMode.GREATER_THEN, "birthday", "java.util.Date", "String");
          return this;
      }

      public UpmsUserDetailsSQL andBirthdayGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("birthday", value, ConditionMode.GREATER_EQUAL, "birthday", "java.util.Date", "String");
          return this;
      }

      public UpmsUserDetailsSQL andBirthdayLessThan(java.util.Date value) {
          addCriterion("birthday", value, ConditionMode.LESS_THEN, "birthday", "java.util.Date", "String");
          return this;
      }

      public UpmsUserDetailsSQL andBirthdayLessThanOrEqualTo(java.util.Date value) {
          addCriterion("birthday", value, ConditionMode.LESS_EQUAL, "birthday", "java.util.Date", "String");
          return this;
      }

      public UpmsUserDetailsSQL andBirthdayBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("birthday", value1, value2, ConditionMode.BETWEEN, "birthday", "java.util.Date", "String");
    	  return this;
      }

      public UpmsUserDetailsSQL andBirthdayNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("birthday", value1, value2, ConditionMode.NOT_BETWEEN, "birthday", "java.util.Date", "String");
          return this;
      }
        
      public UpmsUserDetailsSQL andBirthdayIn(List<java.util.Date> values) {
          addCriterion("birthday", values, ConditionMode.IN, "birthday", "java.util.Date", "String");
          return this;
      }

      public UpmsUserDetailsSQL andBirthdayNotIn(List<java.util.Date> values) {
          addCriterion("birthday", values, ConditionMode.NOT_IN, "birthday", "java.util.Date", "String");
          return this;
      }
	public UpmsUserDetailsSQL andQuestionIsNull() {
		isnull("question");
		return this;
	}
	
	public UpmsUserDetailsSQL andQuestionIsNotNull() {
		notNull("question");
		return this;
	}
	
	public UpmsUserDetailsSQL andQuestionIsEmpty() {
		empty("question");
		return this;
	}

	public UpmsUserDetailsSQL andQuestionIsNotEmpty() {
		notEmpty("question");
		return this;
	}
       public UpmsUserDetailsSQL andQuestionLike(java.lang.String value) {
    	   addCriterion("question", value, ConditionMode.FUZZY, "question", "java.lang.String", "String");
    	   return this;
      }

      public UpmsUserDetailsSQL andQuestionNotLike(java.lang.String value) {
          addCriterion("question", value, ConditionMode.NOT_FUZZY, "question", "java.lang.String", "String");
          return this;
      }
      public UpmsUserDetailsSQL andQuestionEqualTo(java.lang.String value) {
          addCriterion("question", value, ConditionMode.EQUAL, "question", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andQuestionNotEqualTo(java.lang.String value) {
          addCriterion("question", value, ConditionMode.NOT_EQUAL, "question", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andQuestionGreaterThan(java.lang.String value) {
          addCriterion("question", value, ConditionMode.GREATER_THEN, "question", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andQuestionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("question", value, ConditionMode.GREATER_EQUAL, "question", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andQuestionLessThan(java.lang.String value) {
          addCriterion("question", value, ConditionMode.LESS_THEN, "question", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andQuestionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("question", value, ConditionMode.LESS_EQUAL, "question", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andQuestionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("question", value1, value2, ConditionMode.BETWEEN, "question", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserDetailsSQL andQuestionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("question", value1, value2, ConditionMode.NOT_BETWEEN, "question", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserDetailsSQL andQuestionIn(List<java.lang.String> values) {
          addCriterion("question", values, ConditionMode.IN, "question", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andQuestionNotIn(List<java.lang.String> values) {
          addCriterion("question", values, ConditionMode.NOT_IN, "question", "java.lang.String", "String");
          return this;
      }
	public UpmsUserDetailsSQL andAnswerIsNull() {
		isnull("answer");
		return this;
	}
	
	public UpmsUserDetailsSQL andAnswerIsNotNull() {
		notNull("answer");
		return this;
	}
	
	public UpmsUserDetailsSQL andAnswerIsEmpty() {
		empty("answer");
		return this;
	}

	public UpmsUserDetailsSQL andAnswerIsNotEmpty() {
		notEmpty("answer");
		return this;
	}
       public UpmsUserDetailsSQL andAnswerLike(java.lang.String value) {
    	   addCriterion("answer", value, ConditionMode.FUZZY, "answer", "java.lang.String", "String");
    	   return this;
      }

      public UpmsUserDetailsSQL andAnswerNotLike(java.lang.String value) {
          addCriterion("answer", value, ConditionMode.NOT_FUZZY, "answer", "java.lang.String", "String");
          return this;
      }
      public UpmsUserDetailsSQL andAnswerEqualTo(java.lang.String value) {
          addCriterion("answer", value, ConditionMode.EQUAL, "answer", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andAnswerNotEqualTo(java.lang.String value) {
          addCriterion("answer", value, ConditionMode.NOT_EQUAL, "answer", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andAnswerGreaterThan(java.lang.String value) {
          addCriterion("answer", value, ConditionMode.GREATER_THEN, "answer", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andAnswerGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("answer", value, ConditionMode.GREATER_EQUAL, "answer", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andAnswerLessThan(java.lang.String value) {
          addCriterion("answer", value, ConditionMode.LESS_THEN, "answer", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andAnswerLessThanOrEqualTo(java.lang.String value) {
          addCriterion("answer", value, ConditionMode.LESS_EQUAL, "answer", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andAnswerBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("answer", value1, value2, ConditionMode.BETWEEN, "answer", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserDetailsSQL andAnswerNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("answer", value1, value2, ConditionMode.NOT_BETWEEN, "answer", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserDetailsSQL andAnswerIn(List<java.lang.String> values) {
          addCriterion("answer", values, ConditionMode.IN, "answer", "java.lang.String", "String");
          return this;
      }

      public UpmsUserDetailsSQL andAnswerNotIn(List<java.lang.String> values) {
          addCriterion("answer", values, ConditionMode.NOT_IN, "answer", "java.lang.String", "String");
          return this;
      }
}