/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsSystemSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsSystemSQL of() {
		return new UpmsSystemSQL();
	}
	
	public static UpmsSystemSQL by(Column column) {
		UpmsSystemSQL that = new UpmsSystemSQL();
		that.add(column);
        return that;
    }

    public static UpmsSystemSQL by(String name, Object value) {
        return (UpmsSystemSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_system", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsSystemSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsSystemSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsSystemSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsSystemSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsSystemSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsSystemSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsSystemSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsSystemSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsSystemSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsSystemSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsSystemSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsSystemSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsSystemSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsSystemSQL andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public UpmsSystemSQL andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public UpmsSystemSQL andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public UpmsSystemSQL andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
      public UpmsSystemSQL andSystemIdEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsSystemSQL andSystemIdNotEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsSystemSQL andSystemIdGreaterThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsSystemSQL andSystemIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsSystemSQL andSystemIdLessThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsSystemSQL andSystemIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsSystemSQL andSystemIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsSystemSQL andSystemIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsSystemSQL andSystemIdIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsSystemSQL andSystemIdNotIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsSystemSQL andIconIsNull() {
		isnull("icon");
		return this;
	}
	
	public UpmsSystemSQL andIconIsNotNull() {
		notNull("icon");
		return this;
	}
	
	public UpmsSystemSQL andIconIsEmpty() {
		empty("icon");
		return this;
	}

	public UpmsSystemSQL andIconIsNotEmpty() {
		notEmpty("icon");
		return this;
	}
       public UpmsSystemSQL andIconLike(java.lang.String value) {
    	   addCriterion("icon", value, ConditionMode.FUZZY, "icon", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsSystemSQL andIconNotLike(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.NOT_FUZZY, "icon", "java.lang.String", "Float");
          return this;
      }
      public UpmsSystemSQL andIconEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andIconNotEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.NOT_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andIconGreaterThan(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.GREATER_THEN, "icon", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andIconGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.GREATER_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andIconLessThan(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.LESS_THEN, "icon", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andIconLessThanOrEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.LESS_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andIconBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("icon", value1, value2, ConditionMode.BETWEEN, "icon", "java.lang.String", "String");
    	  return this;
      }

      public UpmsSystemSQL andIconNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("icon", value1, value2, ConditionMode.NOT_BETWEEN, "icon", "java.lang.String", "String");
          return this;
      }
        
      public UpmsSystemSQL andIconIn(List<java.lang.String> values) {
          addCriterion("icon", values, ConditionMode.IN, "icon", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andIconNotIn(List<java.lang.String> values) {
          addCriterion("icon", values, ConditionMode.NOT_IN, "icon", "java.lang.String", "String");
          return this;
      }
	public UpmsSystemSQL andBannerIsNull() {
		isnull("banner");
		return this;
	}
	
	public UpmsSystemSQL andBannerIsNotNull() {
		notNull("banner");
		return this;
	}
	
	public UpmsSystemSQL andBannerIsEmpty() {
		empty("banner");
		return this;
	}

	public UpmsSystemSQL andBannerIsNotEmpty() {
		notEmpty("banner");
		return this;
	}
       public UpmsSystemSQL andBannerLike(java.lang.String value) {
    	   addCriterion("banner", value, ConditionMode.FUZZY, "banner", "java.lang.String", "String");
    	   return this;
      }

      public UpmsSystemSQL andBannerNotLike(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.NOT_FUZZY, "banner", "java.lang.String", "String");
          return this;
      }
      public UpmsSystemSQL andBannerEqualTo(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.EQUAL, "banner", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andBannerNotEqualTo(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.NOT_EQUAL, "banner", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andBannerGreaterThan(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.GREATER_THEN, "banner", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andBannerGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.GREATER_EQUAL, "banner", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andBannerLessThan(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.LESS_THEN, "banner", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andBannerLessThanOrEqualTo(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.LESS_EQUAL, "banner", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andBannerBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("banner", value1, value2, ConditionMode.BETWEEN, "banner", "java.lang.String", "String");
    	  return this;
      }

      public UpmsSystemSQL andBannerNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("banner", value1, value2, ConditionMode.NOT_BETWEEN, "banner", "java.lang.String", "String");
          return this;
      }
        
      public UpmsSystemSQL andBannerIn(List<java.lang.String> values) {
          addCriterion("banner", values, ConditionMode.IN, "banner", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andBannerNotIn(List<java.lang.String> values) {
          addCriterion("banner", values, ConditionMode.NOT_IN, "banner", "java.lang.String", "String");
          return this;
      }
	public UpmsSystemSQL andThemeIsNull() {
		isnull("theme");
		return this;
	}
	
	public UpmsSystemSQL andThemeIsNotNull() {
		notNull("theme");
		return this;
	}
	
	public UpmsSystemSQL andThemeIsEmpty() {
		empty("theme");
		return this;
	}

	public UpmsSystemSQL andThemeIsNotEmpty() {
		notEmpty("theme");
		return this;
	}
       public UpmsSystemSQL andThemeLike(java.lang.String value) {
    	   addCriterion("theme", value, ConditionMode.FUZZY, "theme", "java.lang.String", "String");
    	   return this;
      }

      public UpmsSystemSQL andThemeNotLike(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.NOT_FUZZY, "theme", "java.lang.String", "String");
          return this;
      }
      public UpmsSystemSQL andThemeEqualTo(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.EQUAL, "theme", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andThemeNotEqualTo(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.NOT_EQUAL, "theme", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andThemeGreaterThan(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.GREATER_THEN, "theme", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andThemeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.GREATER_EQUAL, "theme", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andThemeLessThan(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.LESS_THEN, "theme", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andThemeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.LESS_EQUAL, "theme", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andThemeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("theme", value1, value2, ConditionMode.BETWEEN, "theme", "java.lang.String", "String");
    	  return this;
      }

      public UpmsSystemSQL andThemeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("theme", value1, value2, ConditionMode.NOT_BETWEEN, "theme", "java.lang.String", "String");
          return this;
      }
        
      public UpmsSystemSQL andThemeIn(List<java.lang.String> values) {
          addCriterion("theme", values, ConditionMode.IN, "theme", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andThemeNotIn(List<java.lang.String> values) {
          addCriterion("theme", values, ConditionMode.NOT_IN, "theme", "java.lang.String", "String");
          return this;
      }
	public UpmsSystemSQL andBasepathIsNull() {
		isnull("basepath");
		return this;
	}
	
	public UpmsSystemSQL andBasepathIsNotNull() {
		notNull("basepath");
		return this;
	}
	
	public UpmsSystemSQL andBasepathIsEmpty() {
		empty("basepath");
		return this;
	}

	public UpmsSystemSQL andBasepathIsNotEmpty() {
		notEmpty("basepath");
		return this;
	}
       public UpmsSystemSQL andBasepathLike(java.lang.String value) {
    	   addCriterion("basepath", value, ConditionMode.FUZZY, "basepath", "java.lang.String", "String");
    	   return this;
      }

      public UpmsSystemSQL andBasepathNotLike(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.NOT_FUZZY, "basepath", "java.lang.String", "String");
          return this;
      }
      public UpmsSystemSQL andBasepathEqualTo(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.EQUAL, "basepath", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andBasepathNotEqualTo(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.NOT_EQUAL, "basepath", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andBasepathGreaterThan(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.GREATER_THEN, "basepath", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andBasepathGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.GREATER_EQUAL, "basepath", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andBasepathLessThan(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.LESS_THEN, "basepath", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andBasepathLessThanOrEqualTo(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.LESS_EQUAL, "basepath", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andBasepathBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("basepath", value1, value2, ConditionMode.BETWEEN, "basepath", "java.lang.String", "String");
    	  return this;
      }

      public UpmsSystemSQL andBasepathNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("basepath", value1, value2, ConditionMode.NOT_BETWEEN, "basepath", "java.lang.String", "String");
          return this;
      }
        
      public UpmsSystemSQL andBasepathIn(List<java.lang.String> values) {
          addCriterion("basepath", values, ConditionMode.IN, "basepath", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andBasepathNotIn(List<java.lang.String> values) {
          addCriterion("basepath", values, ConditionMode.NOT_IN, "basepath", "java.lang.String", "String");
          return this;
      }
	public UpmsSystemSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public UpmsSystemSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public UpmsSystemSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public UpmsSystemSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public UpmsSystemSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsSystemSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsSystemSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsSystemSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsSystemSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsSystemSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsSystemSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsSystemSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsSystemSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsSystemSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsSystemSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public UpmsSystemSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public UpmsSystemSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public UpmsSystemSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public UpmsSystemSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsSystemSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public UpmsSystemSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public UpmsSystemSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public UpmsSystemSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public UpmsSystemSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public UpmsSystemSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public UpmsSystemSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public UpmsSystemSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public UpmsSystemSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "String");
    	   return this;
      }

      public UpmsSystemSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "String");
          return this;
      }
      public UpmsSystemSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public UpmsSystemSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public UpmsSystemSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public UpmsSystemSQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public UpmsSystemSQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public UpmsSystemSQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public UpmsSystemSQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public UpmsSystemSQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public UpmsSystemSQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public UpmsSystemSQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public UpmsSystemSQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public UpmsSystemSQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsSystemSQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public UpmsSystemSQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public UpmsSystemSQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public UpmsSystemSQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public UpmsSystemSQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public UpmsSystemSQL andCtimeEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsSystemSQL andCtimeNotEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsSystemSQL andCtimeGreaterThan(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsSystemSQL andCtimeGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsSystemSQL andCtimeLessThan(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsSystemSQL andCtimeLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsSystemSQL andCtimeBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsSystemSQL andCtimeNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsSystemSQL andCtimeIn(List<java.lang.Long> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsSystemSQL andCtimeNotIn(List<java.lang.Long> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.lang.Long", "Float");
          return this;
      }
	public UpmsSystemSQL andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public UpmsSystemSQL andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public UpmsSystemSQL andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public UpmsSystemSQL andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
      public UpmsSystemSQL andOrdersEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsSystemSQL andOrdersNotEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsSystemSQL andOrdersGreaterThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsSystemSQL andOrdersGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsSystemSQL andOrdersLessThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsSystemSQL andOrdersLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsSystemSQL andOrdersBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsSystemSQL andOrdersNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsSystemSQL andOrdersIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsSystemSQL andOrdersNotIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.lang.Long", "Float");
          return this;
      }
}