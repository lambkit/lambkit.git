/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsAuthCodeSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsAuthCodeSQL of() {
		return new UpmsAuthCodeSQL();
	}
	
	public static UpmsAuthCodeSQL by(Column column) {
		UpmsAuthCodeSQL that = new UpmsAuthCodeSQL();
		that.add(column);
        return that;
    }

    public static UpmsAuthCodeSQL by(String name, Object value) {
        return (UpmsAuthCodeSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_auth_code", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAuthCodeSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAuthCodeSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsAuthCodeSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsAuthCodeSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAuthCodeSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAuthCodeSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAuthCodeSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAuthCodeSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsAuthCodeSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsAuthCodeSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsAuthCodeSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsAuthCodeSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsAuthCodeSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsAuthCodeSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public UpmsAuthCodeSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public UpmsAuthCodeSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public UpmsAuthCodeSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
       public UpmsAuthCodeSQL andIdLike(java.lang.String value) {
    	   addCriterion("id", value, ConditionMode.FUZZY, "id", "java.lang.String", "String");
    	   return this;
      }

      public UpmsAuthCodeSQL andIdNotLike(java.lang.String value) {
          addCriterion("id", value, ConditionMode.NOT_FUZZY, "id", "java.lang.String", "String");
          return this;
      }
      public UpmsAuthCodeSQL andIdEqualTo(java.lang.String value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.String", "String");
          return this;
      }

      public UpmsAuthCodeSQL andIdNotEqualTo(java.lang.String value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.String", "String");
          return this;
      }

      public UpmsAuthCodeSQL andIdGreaterThan(java.lang.String value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.String", "String");
          return this;
      }

      public UpmsAuthCodeSQL andIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.String", "String");
          return this;
      }

      public UpmsAuthCodeSQL andIdLessThan(java.lang.String value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.String", "String");
          return this;
      }

      public UpmsAuthCodeSQL andIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.String", "String");
          return this;
      }

      public UpmsAuthCodeSQL andIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.String", "String");
    	  return this;
      }

      public UpmsAuthCodeSQL andIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.String", "String");
          return this;
      }
        
      public UpmsAuthCodeSQL andIdIn(List<java.lang.String> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.String", "String");
          return this;
      }

      public UpmsAuthCodeSQL andIdNotIn(List<java.lang.String> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.String", "String");
          return this;
      }
	public UpmsAuthCodeSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public UpmsAuthCodeSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public UpmsAuthCodeSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public UpmsAuthCodeSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public UpmsAuthCodeSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsAuthCodeSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsAuthCodeSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsAuthCodeSQL andExpireAtIsNull() {
		isnull("expire_at");
		return this;
	}
	
	public UpmsAuthCodeSQL andExpireAtIsNotNull() {
		notNull("expire_at");
		return this;
	}
	
	public UpmsAuthCodeSQL andExpireAtIsEmpty() {
		empty("expire_at");
		return this;
	}

	public UpmsAuthCodeSQL andExpireAtIsNotEmpty() {
		notEmpty("expire_at");
		return this;
	}
      public UpmsAuthCodeSQL andExpireAtEqualTo(java.lang.Long value) {
          addCriterion("expire_at", value, ConditionMode.EQUAL, "expireAt", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andExpireAtNotEqualTo(java.lang.Long value) {
          addCriterion("expire_at", value, ConditionMode.NOT_EQUAL, "expireAt", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andExpireAtGreaterThan(java.lang.Long value) {
          addCriterion("expire_at", value, ConditionMode.GREATER_THEN, "expireAt", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andExpireAtGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("expire_at", value, ConditionMode.GREATER_EQUAL, "expireAt", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andExpireAtLessThan(java.lang.Long value) {
          addCriterion("expire_at", value, ConditionMode.LESS_THEN, "expireAt", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andExpireAtLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("expire_at", value, ConditionMode.LESS_EQUAL, "expireAt", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andExpireAtBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("expire_at", value1, value2, ConditionMode.BETWEEN, "expireAt", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsAuthCodeSQL andExpireAtNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("expire_at", value1, value2, ConditionMode.NOT_BETWEEN, "expireAt", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsAuthCodeSQL andExpireAtIn(List<java.lang.Long> values) {
          addCriterion("expire_at", values, ConditionMode.IN, "expireAt", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andExpireAtNotIn(List<java.lang.Long> values) {
          addCriterion("expire_at", values, ConditionMode.NOT_IN, "expireAt", "java.lang.Long", "Float");
          return this;
      }
	public UpmsAuthCodeSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public UpmsAuthCodeSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public UpmsAuthCodeSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public UpmsAuthCodeSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
      public UpmsAuthCodeSQL andTypeEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andTypeGreaterThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andTypeLessThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsAuthCodeSQL andTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsAuthCodeSQL andTypeIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsAuthCodeSQL andTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.Integer", "Float");
          return this;
      }
}