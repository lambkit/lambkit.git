/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsFavoritesSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsFavoritesSQL of() {
		return new UpmsFavoritesSQL();
	}
	
	public static UpmsFavoritesSQL by(Column column) {
		UpmsFavoritesSQL that = new UpmsFavoritesSQL();
		that.add(column);
        return that;
    }

    public static UpmsFavoritesSQL by(String name, Object value) {
        return (UpmsFavoritesSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_favorites", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsFavoritesSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsFavoritesSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsFavoritesSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsFavoritesSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsFavoritesSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsFavoritesSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsFavoritesSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsFavoritesSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsFavoritesSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsFavoritesSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsFavoritesSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsFavoritesSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsFavoritesSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsFavoritesSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public UpmsFavoritesSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public UpmsFavoritesSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public UpmsFavoritesSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public UpmsFavoritesSQL andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsFavoritesSQL andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsFavoritesSQL andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public UpmsFavoritesSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public UpmsFavoritesSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public UpmsFavoritesSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public UpmsFavoritesSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public UpmsFavoritesSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsFavoritesSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsFavoritesSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsFavoritesSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public UpmsFavoritesSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public UpmsFavoritesSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public UpmsFavoritesSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public UpmsFavoritesSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsFavoritesSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "Float");
          return this;
      }
      public UpmsFavoritesSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public UpmsFavoritesSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public UpmsFavoritesSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public UpmsFavoritesSQL andUrlIsNull() {
		isnull("url");
		return this;
	}
	
	public UpmsFavoritesSQL andUrlIsNotNull() {
		notNull("url");
		return this;
	}
	
	public UpmsFavoritesSQL andUrlIsEmpty() {
		empty("url");
		return this;
	}

	public UpmsFavoritesSQL andUrlIsNotEmpty() {
		notEmpty("url");
		return this;
	}
       public UpmsFavoritesSQL andUrlLike(java.lang.String value) {
    	   addCriterion("url", value, ConditionMode.FUZZY, "url", "java.lang.String", "String");
    	   return this;
      }

      public UpmsFavoritesSQL andUrlNotLike(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_FUZZY, "url", "java.lang.String", "String");
          return this;
      }
      public UpmsFavoritesSQL andUrlEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andUrlNotEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andUrlGreaterThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andUrlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andUrlLessThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andUrlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andUrlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("url", value1, value2, ConditionMode.BETWEEN, "url", "java.lang.String", "String");
    	  return this;
      }

      public UpmsFavoritesSQL andUrlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("url", value1, value2, ConditionMode.NOT_BETWEEN, "url", "java.lang.String", "String");
          return this;
      }
        
      public UpmsFavoritesSQL andUrlIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.IN, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andUrlNotIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.NOT_IN, "url", "java.lang.String", "String");
          return this;
      }
	public UpmsFavoritesSQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public UpmsFavoritesSQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public UpmsFavoritesSQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public UpmsFavoritesSQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public UpmsFavoritesSQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public UpmsFavoritesSQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public UpmsFavoritesSQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public UpmsFavoritesSQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public UpmsFavoritesSQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public UpmsFavoritesSQL andTagsIsNull() {
		isnull("tags");
		return this;
	}
	
	public UpmsFavoritesSQL andTagsIsNotNull() {
		notNull("tags");
		return this;
	}
	
	public UpmsFavoritesSQL andTagsIsEmpty() {
		empty("tags");
		return this;
	}

	public UpmsFavoritesSQL andTagsIsNotEmpty() {
		notEmpty("tags");
		return this;
	}
       public UpmsFavoritesSQL andTagsLike(java.lang.String value) {
    	   addCriterion("tags", value, ConditionMode.FUZZY, "tags", "java.lang.String", "String");
    	   return this;
      }

      public UpmsFavoritesSQL andTagsNotLike(java.lang.String value) {
          addCriterion("tags", value, ConditionMode.NOT_FUZZY, "tags", "java.lang.String", "String");
          return this;
      }
      public UpmsFavoritesSQL andTagsEqualTo(java.lang.String value) {
          addCriterion("tags", value, ConditionMode.EQUAL, "tags", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andTagsNotEqualTo(java.lang.String value) {
          addCriterion("tags", value, ConditionMode.NOT_EQUAL, "tags", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andTagsGreaterThan(java.lang.String value) {
          addCriterion("tags", value, ConditionMode.GREATER_THEN, "tags", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andTagsGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("tags", value, ConditionMode.GREATER_EQUAL, "tags", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andTagsLessThan(java.lang.String value) {
          addCriterion("tags", value, ConditionMode.LESS_THEN, "tags", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andTagsLessThanOrEqualTo(java.lang.String value) {
          addCriterion("tags", value, ConditionMode.LESS_EQUAL, "tags", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andTagsBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("tags", value1, value2, ConditionMode.BETWEEN, "tags", "java.lang.String", "String");
    	  return this;
      }

      public UpmsFavoritesSQL andTagsNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("tags", value1, value2, ConditionMode.NOT_BETWEEN, "tags", "java.lang.String", "String");
          return this;
      }
        
      public UpmsFavoritesSQL andTagsIn(List<java.lang.String> values) {
          addCriterion("tags", values, ConditionMode.IN, "tags", "java.lang.String", "String");
          return this;
      }

      public UpmsFavoritesSQL andTagsNotIn(List<java.lang.String> values) {
          addCriterion("tags", values, ConditionMode.NOT_IN, "tags", "java.lang.String", "String");
          return this;
      }
	public UpmsFavoritesSQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public UpmsFavoritesSQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public UpmsFavoritesSQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public UpmsFavoritesSQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public UpmsFavoritesSQL andCtimeEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andCtimeNotEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andCtimeGreaterThan(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andCtimeGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andCtimeLessThan(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andCtimeLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andCtimeBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsFavoritesSQL andCtimeNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsFavoritesSQL andCtimeIn(List<java.lang.Long> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.lang.Long", "Float");
          return this;
      }

      public UpmsFavoritesSQL andCtimeNotIn(List<java.lang.Long> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.lang.Long", "Float");
          return this;
      }
}