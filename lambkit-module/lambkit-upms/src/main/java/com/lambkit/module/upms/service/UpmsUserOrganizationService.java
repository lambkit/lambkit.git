/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.service;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Lambkit;
import com.lambkit.db.DbPool;
import com.lambkit.db.IDaoService;
import com.lambkit.db.IRowDao;
import com.lambkit.db.sql.Example;
import com.lambkit.module.upms.UpmsConfig;
import com.lambkit.module.upms.row.UpmsUserOrganization;
import com.lambkit.module.upms.sql.UpmsUserOrganizationSQL;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsUserOrganizationService implements IDaoService<UpmsUserOrganization> {
	public IRowDao<UpmsUserOrganization> dao() {
		String dbPoolName = Lambkit.config(UpmsConfig.class).getDbPoolName();
		return DbPool.use(dbPoolName).dao(UpmsUserOrganization.class);
	}

	public IRowDao<UpmsUserOrganization> dao(String dbPoolName) {
		return DbPool.use(dbPoolName).dao(UpmsUserOrganization.class);
	}

	public int organization(String[] organizationIds, Long id) {
		int result = 0;
		// 删除旧记录
		Example upmsUserOrganizationExample = UpmsUserOrganizationSQL.of()
				.andUserIdEqualTo(id).example();
		dao().delete(upmsUserOrganizationExample);
		// 增加新记录
		if (null != organizationIds) {
			for (String organizationId : organizationIds) {
				if (StrUtil.isBlank(organizationId)) {
					continue;
				}
				UpmsUserOrganization upmsUserOrganization = new UpmsUserOrganization();
				upmsUserOrganization.setUserId(id);
				upmsUserOrganization.setOrganizationId(NumberUtil.parseLong(organizationId));
				boolean flag = dao().save(upmsUserOrganization);
				result += flag ? 1 : 0;
			}
		}
		return result;
	}
}
