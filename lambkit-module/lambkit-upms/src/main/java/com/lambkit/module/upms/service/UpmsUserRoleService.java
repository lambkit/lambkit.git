/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.service;

import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Attr;
import com.lambkit.core.Lambkit;
import com.lambkit.db.DbPool;
import com.lambkit.db.IDaoService;
import com.lambkit.db.IRowDao;
import com.lambkit.db.sql.Example;
import com.lambkit.module.upms.Upms;
import com.lambkit.module.upms.UpmsConfig;
import com.lambkit.module.upms.row.UpmsRole;
import com.lambkit.module.upms.row.UpmsUserRole;
import com.lambkit.module.upms.sql.UpmsRoleSQL;
import com.lambkit.module.upms.sql.UpmsUserRoleSQL;

import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsUserRoleService implements IDaoService<UpmsUserRole> {
	public IRowDao<UpmsUserRole> dao() {
		String dbPoolName = Lambkit.config(UpmsConfig.class).getDbPoolName();
		return DbPool.use(dbPoolName).dao(UpmsUserRole.class);
	}

	public IRowDao<UpmsUserRole> dao(String dbPoolName) {
		return DbPool.use(dbPoolName).dao(UpmsUserRole.class);
	}

	public int role(String[] roleIds, Long id) {
		int result = 0;
		// 删除旧记录
		Example upmsUserRoleExample = UpmsUserRoleSQL.of()
				.andUserIdEqualTo(id).example();
		dao().delete(upmsUserRoleExample);
		// 增加新记录
		if (null != roleIds) {
			for (String roleId : roleIds) {
				if (StrUtil.isBlank(roleId)) {
					continue;
				}
				UpmsUserRole upmsUserRole = new UpmsUserRole();
				upmsUserRole.setUserId(id);
				upmsUserRole.setRoleId(Long.valueOf(roleId));
				boolean flag = dao().save(upmsUserRole);
				result += flag ? 1 : 0;
			}
		}
		return result;
	}

	public boolean hasRole(Long userId, Long roleId) {
		Example upmsUserRoleExample = UpmsUserRoleSQL.of()
				.andUserIdEqualTo(userId).andRoleIdEqualTo(roleId).example();
		UpmsUserRole urole = Lambkit.get(UpmsUserRoleService.class).dao().findFirst(upmsUserRoleExample);
		return urole !=null ? true : false;
	}

	public boolean hasRole(Long userId, String roleName) {
		Example upmsRoleExample = UpmsRoleSQL.of()
				.andNameEqualTo(roleName).example();
		UpmsRole role = Lambkit.get(UpmsRoleService.class).dao().findFirst(upmsRoleExample);
		if(role==null) {
			return false;
		}
		Example upmsUserRoleExample = UpmsUserRoleSQL.of()
				.andUserIdEqualTo(userId).andRoleIdEqualTo(role.getRoleId()).example();
		UpmsUserRole urole = Lambkit.get(UpmsUserRoleService.class).dao().findFirst(upmsUserRoleExample);
		return urole !=null ? true : false;
	}

	public boolean lacksRole(String roleName) {
		// 数据库未设定，暂时
		Example upmsRoleExample = UpmsRoleSQL.of()
				.andNameEqualTo(roleName).example();
		UpmsRole role = Lambkit.get(UpmsRoleService.class).dao().findFirst(upmsRoleExample);
		if(role==null || role.getOrders()==-1) {
			return false;
		}
		return true;
	}

	public boolean hasAnyRoles(Long userId, String roleNames) {
		roleNames = roleNames.startsWith(",") ? roleNames.substring(1) : roleNames;
		roleNames = roleNames.endsWith(",") ? roleNames.substring(0, roleNames.length()-1) : roleNames;
		Attr data = Attr.by("userId", userId).set("names", roleNames.split(","));
		List<UpmsRole> record = Upms.getApiService().find("upms.getRoleOfUserIdAndRoleNames", UpmsRole.class, data);
		return record != null && record.size() > 0 ? true : false;
	}

	public boolean hasAllRoles(Long userId, String roleNames) {
		roleNames = roleNames.startsWith(",") ? roleNames.substring(1) : roleNames;
		roleNames = roleNames.endsWith(",") ? roleNames.substring(0, roleNames.length()-1) : roleNames;
		Attr data = Attr.by("userid", userId).set("names", roleNames.split(","));
		List<UpmsRole> roles = Upms.getApiService().find("upms.getRoleOfUserIdAndRoleNames", UpmsRole.class, data);
		roleNames = "," + roleNames + ",";
		if(roles!=null && roles.size() > 0) {
			for (UpmsRole rule : roles) {
				roleNames.replace(","+rule.getName()+",", ",");
			}
			if(",".equals(roleNames)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
