/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsAppRoleSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsAppRoleSQL of() {
		return new UpmsAppRoleSQL();
	}
	
	public static UpmsAppRoleSQL by(Column column) {
		UpmsAppRoleSQL that = new UpmsAppRoleSQL();
		that.add(column);
        return that;
    }

    public static UpmsAppRoleSQL by(String name, Object value) {
        return (UpmsAppRoleSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_app_role", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppRoleSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppRoleSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsAppRoleSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsAppRoleSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppRoleSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppRoleSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppRoleSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsAppRoleSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsAppRoleSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsAppRoleSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsAppRoleSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsAppRoleSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsAppRoleSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsAppRoleSQL andAppRoleIdIsNull() {
		isnull("app_role_id");
		return this;
	}
	
	public UpmsAppRoleSQL andAppRoleIdIsNotNull() {
		notNull("app_role_id");
		return this;
	}
	
	public UpmsAppRoleSQL andAppRoleIdIsEmpty() {
		empty("app_role_id");
		return this;
	}

	public UpmsAppRoleSQL andAppRoleIdIsNotEmpty() {
		notEmpty("app_role_id");
		return this;
	}
      public UpmsAppRoleSQL andAppRoleIdEqualTo(java.lang.Long value) {
          addCriterion("app_role_id", value, ConditionMode.EQUAL, "appRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andAppRoleIdNotEqualTo(java.lang.Long value) {
          addCriterion("app_role_id", value, ConditionMode.NOT_EQUAL, "appRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andAppRoleIdGreaterThan(java.lang.Long value) {
          addCriterion("app_role_id", value, ConditionMode.GREATER_THEN, "appRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andAppRoleIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("app_role_id", value, ConditionMode.GREATER_EQUAL, "appRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andAppRoleIdLessThan(java.lang.Long value) {
          addCriterion("app_role_id", value, ConditionMode.LESS_THEN, "appRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andAppRoleIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("app_role_id", value, ConditionMode.LESS_EQUAL, "appRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andAppRoleIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("app_role_id", value1, value2, ConditionMode.BETWEEN, "appRoleId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsAppRoleSQL andAppRoleIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("app_role_id", value1, value2, ConditionMode.NOT_BETWEEN, "appRoleId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsAppRoleSQL andAppRoleIdIn(List<java.lang.Long> values) {
          addCriterion("app_role_id", values, ConditionMode.IN, "appRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andAppRoleIdNotIn(List<java.lang.Long> values) {
          addCriterion("app_role_id", values, ConditionMode.NOT_IN, "appRoleId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsAppRoleSQL andAppIdIsNull() {
		isnull("app_id");
		return this;
	}
	
	public UpmsAppRoleSQL andAppIdIsNotNull() {
		notNull("app_id");
		return this;
	}
	
	public UpmsAppRoleSQL andAppIdIsEmpty() {
		empty("app_id");
		return this;
	}

	public UpmsAppRoleSQL andAppIdIsNotEmpty() {
		notEmpty("app_id");
		return this;
	}
      public UpmsAppRoleSQL andAppIdEqualTo(java.lang.Long value) {
          addCriterion("app_id", value, ConditionMode.EQUAL, "appId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andAppIdNotEqualTo(java.lang.Long value) {
          addCriterion("app_id", value, ConditionMode.NOT_EQUAL, "appId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andAppIdGreaterThan(java.lang.Long value) {
          addCriterion("app_id", value, ConditionMode.GREATER_THEN, "appId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andAppIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("app_id", value, ConditionMode.GREATER_EQUAL, "appId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andAppIdLessThan(java.lang.Long value) {
          addCriterion("app_id", value, ConditionMode.LESS_THEN, "appId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andAppIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("app_id", value, ConditionMode.LESS_EQUAL, "appId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andAppIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("app_id", value1, value2, ConditionMode.BETWEEN, "appId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsAppRoleSQL andAppIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("app_id", value1, value2, ConditionMode.NOT_BETWEEN, "appId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsAppRoleSQL andAppIdIn(List<java.lang.Long> values) {
          addCriterion("app_id", values, ConditionMode.IN, "appId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andAppIdNotIn(List<java.lang.Long> values) {
          addCriterion("app_id", values, ConditionMode.NOT_IN, "appId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsAppRoleSQL andRoleIdIsNull() {
		isnull("role_id");
		return this;
	}
	
	public UpmsAppRoleSQL andRoleIdIsNotNull() {
		notNull("role_id");
		return this;
	}
	
	public UpmsAppRoleSQL andRoleIdIsEmpty() {
		empty("role_id");
		return this;
	}

	public UpmsAppRoleSQL andRoleIdIsNotEmpty() {
		notEmpty("role_id");
		return this;
	}
      public UpmsAppRoleSQL andRoleIdEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andRoleIdNotEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.NOT_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andRoleIdGreaterThan(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.GREATER_THEN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andRoleIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.GREATER_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andRoleIdLessThan(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.LESS_THEN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andRoleIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.LESS_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andRoleIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("role_id", value1, value2, ConditionMode.BETWEEN, "roleId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsAppRoleSQL andRoleIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("role_id", value1, value2, ConditionMode.NOT_BETWEEN, "roleId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsAppRoleSQL andRoleIdIn(List<java.lang.Long> values) {
          addCriterion("role_id", values, ConditionMode.IN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsAppRoleSQL andRoleIdNotIn(List<java.lang.Long> values) {
          addCriterion("role_id", values, ConditionMode.NOT_IN, "roleId", "java.lang.Long", "Float");
          return this;
      }
}