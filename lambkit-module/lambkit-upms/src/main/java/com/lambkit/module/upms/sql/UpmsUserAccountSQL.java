/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsUserAccountSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsUserAccountSQL of() {
		return new UpmsUserAccountSQL();
	}
	
	public static UpmsUserAccountSQL by(Column column) {
		UpmsUserAccountSQL that = new UpmsUserAccountSQL();
		that.add(column);
        return that;
    }

    public static UpmsUserAccountSQL by(String name, Object value) {
        return (UpmsUserAccountSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_user_account", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserAccountSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserAccountSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsUserAccountSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsUserAccountSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserAccountSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserAccountSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserAccountSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserAccountSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsUserAccountSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsUserAccountSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsUserAccountSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsUserAccountSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsUserAccountSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsUserAccountSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public UpmsUserAccountSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public UpmsUserAccountSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public UpmsUserAccountSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public UpmsUserAccountSQL andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountSQL andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountSQL andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountSQL andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountSQL andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountSQL andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountSQL andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserAccountSQL andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserAccountSQL andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountSQL andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserAccountSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public UpmsUserAccountSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public UpmsUserAccountSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public UpmsUserAccountSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public UpmsUserAccountSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserAccountSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserAccountSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserAccountSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserAccountSQL andPhoneIsNull() {
		isnull("phone");
		return this;
	}
	
	public UpmsUserAccountSQL andPhoneIsNotNull() {
		notNull("phone");
		return this;
	}
	
	public UpmsUserAccountSQL andPhoneIsEmpty() {
		empty("phone");
		return this;
	}

	public UpmsUserAccountSQL andPhoneIsNotEmpty() {
		notEmpty("phone");
		return this;
	}
       public UpmsUserAccountSQL andPhoneLike(java.lang.String value) {
    	   addCriterion("phone", value, ConditionMode.FUZZY, "phone", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsUserAccountSQL andPhoneNotLike(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.NOT_FUZZY, "phone", "java.lang.String", "Float");
          return this;
      }
      public UpmsUserAccountSQL andPhoneEqualTo(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.EQUAL, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andPhoneNotEqualTo(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.NOT_EQUAL, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andPhoneGreaterThan(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.GREATER_THEN, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andPhoneGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.GREATER_EQUAL, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andPhoneLessThan(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.LESS_THEN, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andPhoneLessThanOrEqualTo(java.lang.String value) {
          addCriterion("phone", value, ConditionMode.LESS_EQUAL, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andPhoneBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("phone", value1, value2, ConditionMode.BETWEEN, "phone", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserAccountSQL andPhoneNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("phone", value1, value2, ConditionMode.NOT_BETWEEN, "phone", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserAccountSQL andPhoneIn(List<java.lang.String> values) {
          addCriterion("phone", values, ConditionMode.IN, "phone", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andPhoneNotIn(List<java.lang.String> values) {
          addCriterion("phone", values, ConditionMode.NOT_IN, "phone", "java.lang.String", "String");
          return this;
      }
	public UpmsUserAccountSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public UpmsUserAccountSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public UpmsUserAccountSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public UpmsUserAccountSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
      public UpmsUserAccountSQL andTypeEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andTypeGreaterThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andTypeLessThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsUserAccountSQL andTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsUserAccountSQL andTypeIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsUserAccountSQL andUserNameIsNull() {
		isnull("user_name");
		return this;
	}
	
	public UpmsUserAccountSQL andUserNameIsNotNull() {
		notNull("user_name");
		return this;
	}
	
	public UpmsUserAccountSQL andUserNameIsEmpty() {
		empty("user_name");
		return this;
	}

	public UpmsUserAccountSQL andUserNameIsNotEmpty() {
		notEmpty("user_name");
		return this;
	}
       public UpmsUserAccountSQL andUserNameLike(java.lang.String value) {
    	   addCriterion("user_name", value, ConditionMode.FUZZY, "userName", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsUserAccountSQL andUserNameNotLike(java.lang.String value) {
          addCriterion("user_name", value, ConditionMode.NOT_FUZZY, "userName", "java.lang.String", "Float");
          return this;
      }
      public UpmsUserAccountSQL andUserNameEqualTo(java.lang.String value) {
          addCriterion("user_name", value, ConditionMode.EQUAL, "userName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andUserNameNotEqualTo(java.lang.String value) {
          addCriterion("user_name", value, ConditionMode.NOT_EQUAL, "userName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andUserNameGreaterThan(java.lang.String value) {
          addCriterion("user_name", value, ConditionMode.GREATER_THEN, "userName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andUserNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_name", value, ConditionMode.GREATER_EQUAL, "userName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andUserNameLessThan(java.lang.String value) {
          addCriterion("user_name", value, ConditionMode.LESS_THEN, "userName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andUserNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_name", value, ConditionMode.LESS_EQUAL, "userName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andUserNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_name", value1, value2, ConditionMode.BETWEEN, "userName", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserAccountSQL andUserNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_name", value1, value2, ConditionMode.NOT_BETWEEN, "userName", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserAccountSQL andUserNameIn(List<java.lang.String> values) {
          addCriterion("user_name", values, ConditionMode.IN, "userName", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andUserNameNotIn(List<java.lang.String> values) {
          addCriterion("user_name", values, ConditionMode.NOT_IN, "userName", "java.lang.String", "String");
          return this;
      }
	public UpmsUserAccountSQL andUnionidIsNull() {
		isnull("unionid");
		return this;
	}
	
	public UpmsUserAccountSQL andUnionidIsNotNull() {
		notNull("unionid");
		return this;
	}
	
	public UpmsUserAccountSQL andUnionidIsEmpty() {
		empty("unionid");
		return this;
	}

	public UpmsUserAccountSQL andUnionidIsNotEmpty() {
		notEmpty("unionid");
		return this;
	}
       public UpmsUserAccountSQL andUnionidLike(java.lang.String value) {
    	   addCriterion("unionid", value, ConditionMode.FUZZY, "unionid", "java.lang.String", "String");
    	   return this;
      }

      public UpmsUserAccountSQL andUnionidNotLike(java.lang.String value) {
          addCriterion("unionid", value, ConditionMode.NOT_FUZZY, "unionid", "java.lang.String", "String");
          return this;
      }
      public UpmsUserAccountSQL andUnionidEqualTo(java.lang.String value) {
          addCriterion("unionid", value, ConditionMode.EQUAL, "unionid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andUnionidNotEqualTo(java.lang.String value) {
          addCriterion("unionid", value, ConditionMode.NOT_EQUAL, "unionid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andUnionidGreaterThan(java.lang.String value) {
          addCriterion("unionid", value, ConditionMode.GREATER_THEN, "unionid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andUnionidGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("unionid", value, ConditionMode.GREATER_EQUAL, "unionid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andUnionidLessThan(java.lang.String value) {
          addCriterion("unionid", value, ConditionMode.LESS_THEN, "unionid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andUnionidLessThanOrEqualTo(java.lang.String value) {
          addCriterion("unionid", value, ConditionMode.LESS_EQUAL, "unionid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andUnionidBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("unionid", value1, value2, ConditionMode.BETWEEN, "unionid", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserAccountSQL andUnionidNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("unionid", value1, value2, ConditionMode.NOT_BETWEEN, "unionid", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserAccountSQL andUnionidIn(List<java.lang.String> values) {
          addCriterion("unionid", values, ConditionMode.IN, "unionid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andUnionidNotIn(List<java.lang.String> values) {
          addCriterion("unionid", values, ConditionMode.NOT_IN, "unionid", "java.lang.String", "String");
          return this;
      }
	public UpmsUserAccountSQL andOpenidIsNull() {
		isnull("openid");
		return this;
	}
	
	public UpmsUserAccountSQL andOpenidIsNotNull() {
		notNull("openid");
		return this;
	}
	
	public UpmsUserAccountSQL andOpenidIsEmpty() {
		empty("openid");
		return this;
	}

	public UpmsUserAccountSQL andOpenidIsNotEmpty() {
		notEmpty("openid");
		return this;
	}
       public UpmsUserAccountSQL andOpenidLike(java.lang.String value) {
    	   addCriterion("openid", value, ConditionMode.FUZZY, "openid", "java.lang.String", "String");
    	   return this;
      }

      public UpmsUserAccountSQL andOpenidNotLike(java.lang.String value) {
          addCriterion("openid", value, ConditionMode.NOT_FUZZY, "openid", "java.lang.String", "String");
          return this;
      }
      public UpmsUserAccountSQL andOpenidEqualTo(java.lang.String value) {
          addCriterion("openid", value, ConditionMode.EQUAL, "openid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andOpenidNotEqualTo(java.lang.String value) {
          addCriterion("openid", value, ConditionMode.NOT_EQUAL, "openid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andOpenidGreaterThan(java.lang.String value) {
          addCriterion("openid", value, ConditionMode.GREATER_THEN, "openid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andOpenidGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("openid", value, ConditionMode.GREATER_EQUAL, "openid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andOpenidLessThan(java.lang.String value) {
          addCriterion("openid", value, ConditionMode.LESS_THEN, "openid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andOpenidLessThanOrEqualTo(java.lang.String value) {
          addCriterion("openid", value, ConditionMode.LESS_EQUAL, "openid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andOpenidBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("openid", value1, value2, ConditionMode.BETWEEN, "openid", "java.lang.String", "String");
    	  return this;
      }

      public UpmsUserAccountSQL andOpenidNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("openid", value1, value2, ConditionMode.NOT_BETWEEN, "openid", "java.lang.String", "String");
          return this;
      }
        
      public UpmsUserAccountSQL andOpenidIn(List<java.lang.String> values) {
          addCriterion("openid", values, ConditionMode.IN, "openid", "java.lang.String", "String");
          return this;
      }

      public UpmsUserAccountSQL andOpenidNotIn(List<java.lang.String> values) {
          addCriterion("openid", values, ConditionMode.NOT_IN, "openid", "java.lang.String", "String");
          return this;
      }
	public UpmsUserAccountSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public UpmsUserAccountSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public UpmsUserAccountSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public UpmsUserAccountSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public UpmsUserAccountSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserAccountSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserAccountSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserAccountSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserAccountSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserAccountSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserAccountSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public UpmsUserAccountSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public UpmsUserAccountSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserAccountSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public UpmsUserAccountSQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public UpmsUserAccountSQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public UpmsUserAccountSQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public UpmsUserAccountSQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public UpmsUserAccountSQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserAccountSQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserAccountSQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserAccountSQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserAccountSQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserAccountSQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserAccountSQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public UpmsUserAccountSQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public UpmsUserAccountSQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserAccountSQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
	public UpmsUserAccountSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public UpmsUserAccountSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public UpmsUserAccountSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public UpmsUserAccountSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public UpmsUserAccountSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsUserAccountSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsUserAccountSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsUserAccountSQL andDelFlagIsNull() {
		isnull("del_flag");
		return this;
	}
	
	public UpmsUserAccountSQL andDelFlagIsNotNull() {
		notNull("del_flag");
		return this;
	}
	
	public UpmsUserAccountSQL andDelFlagIsEmpty() {
		empty("del_flag");
		return this;
	}

	public UpmsUserAccountSQL andDelFlagIsNotEmpty() {
		notEmpty("del_flag");
		return this;
	}
      public UpmsUserAccountSQL andDelFlagEqualTo(java.lang.Integer value) {
          addCriterion("del_flag", value, ConditionMode.EQUAL, "delFlag", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andDelFlagNotEqualTo(java.lang.Integer value) {
          addCriterion("del_flag", value, ConditionMode.NOT_EQUAL, "delFlag", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andDelFlagGreaterThan(java.lang.Integer value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_THEN, "delFlag", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andDelFlagGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_EQUAL, "delFlag", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andDelFlagLessThan(java.lang.Integer value) {
          addCriterion("del_flag", value, ConditionMode.LESS_THEN, "delFlag", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andDelFlagLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("del_flag", value, ConditionMode.LESS_EQUAL, "delFlag", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andDelFlagBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("del_flag", value1, value2, ConditionMode.BETWEEN, "delFlag", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsUserAccountSQL andDelFlagNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("del_flag", value1, value2, ConditionMode.NOT_BETWEEN, "delFlag", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsUserAccountSQL andDelFlagIn(List<java.lang.Integer> values) {
          addCriterion("del_flag", values, ConditionMode.IN, "delFlag", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserAccountSQL andDelFlagNotIn(List<java.lang.Integer> values) {
          addCriterion("del_flag", values, ConditionMode.NOT_IN, "delFlag", "java.lang.Integer", "Float");
          return this;
      }
}