/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsUserOauthSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsUserOauthSQL of() {
		return new UpmsUserOauthSQL();
	}
	
	public static UpmsUserOauthSQL by(Column column) {
		UpmsUserOauthSQL that = new UpmsUserOauthSQL();
		that.add(column);
        return that;
    }

    public static UpmsUserOauthSQL by(String name, Object value) {
        return (UpmsUserOauthSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_user_oauth", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserOauthSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserOauthSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsUserOauthSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsUserOauthSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserOauthSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserOauthSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserOauthSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserOauthSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsUserOauthSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsUserOauthSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsUserOauthSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsUserOauthSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsUserOauthSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsUserOauthSQL andUserOauthIdIsNull() {
		isnull("user_oauth_id");
		return this;
	}
	
	public UpmsUserOauthSQL andUserOauthIdIsNotNull() {
		notNull("user_oauth_id");
		return this;
	}
	
	public UpmsUserOauthSQL andUserOauthIdIsEmpty() {
		empty("user_oauth_id");
		return this;
	}

	public UpmsUserOauthSQL andUserOauthIdIsNotEmpty() {
		notEmpty("user_oauth_id");
		return this;
	}
      public UpmsUserOauthSQL andUserOauthIdEqualTo(java.lang.Long value) {
          addCriterion("user_oauth_id", value, ConditionMode.EQUAL, "userOauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andUserOauthIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_oauth_id", value, ConditionMode.NOT_EQUAL, "userOauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andUserOauthIdGreaterThan(java.lang.Long value) {
          addCriterion("user_oauth_id", value, ConditionMode.GREATER_THEN, "userOauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andUserOauthIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_oauth_id", value, ConditionMode.GREATER_EQUAL, "userOauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andUserOauthIdLessThan(java.lang.Long value) {
          addCriterion("user_oauth_id", value, ConditionMode.LESS_THEN, "userOauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andUserOauthIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_oauth_id", value, ConditionMode.LESS_EQUAL, "userOauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andUserOauthIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_oauth_id", value1, value2, ConditionMode.BETWEEN, "userOauthId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserOauthSQL andUserOauthIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_oauth_id", value1, value2, ConditionMode.NOT_BETWEEN, "userOauthId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserOauthSQL andUserOauthIdIn(List<java.lang.Long> values) {
          addCriterion("user_oauth_id", values, ConditionMode.IN, "userOauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andUserOauthIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_oauth_id", values, ConditionMode.NOT_IN, "userOauthId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserOauthSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public UpmsUserOauthSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public UpmsUserOauthSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public UpmsUserOauthSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public UpmsUserOauthSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserOauthSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserOauthSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserOauthSQL andOauthIdIsNull() {
		isnull("oauth_id");
		return this;
	}
	
	public UpmsUserOauthSQL andOauthIdIsNotNull() {
		notNull("oauth_id");
		return this;
	}
	
	public UpmsUserOauthSQL andOauthIdIsEmpty() {
		empty("oauth_id");
		return this;
	}

	public UpmsUserOauthSQL andOauthIdIsNotEmpty() {
		notEmpty("oauth_id");
		return this;
	}
      public UpmsUserOauthSQL andOauthIdEqualTo(java.lang.Long value) {
          addCriterion("oauth_id", value, ConditionMode.EQUAL, "oauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andOauthIdNotEqualTo(java.lang.Long value) {
          addCriterion("oauth_id", value, ConditionMode.NOT_EQUAL, "oauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andOauthIdGreaterThan(java.lang.Long value) {
          addCriterion("oauth_id", value, ConditionMode.GREATER_THEN, "oauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andOauthIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("oauth_id", value, ConditionMode.GREATER_EQUAL, "oauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andOauthIdLessThan(java.lang.Long value) {
          addCriterion("oauth_id", value, ConditionMode.LESS_THEN, "oauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andOauthIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("oauth_id", value, ConditionMode.LESS_EQUAL, "oauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andOauthIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("oauth_id", value1, value2, ConditionMode.BETWEEN, "oauthId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserOauthSQL andOauthIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("oauth_id", value1, value2, ConditionMode.NOT_BETWEEN, "oauthId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserOauthSQL andOauthIdIn(List<java.lang.Long> values) {
          addCriterion("oauth_id", values, ConditionMode.IN, "oauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserOauthSQL andOauthIdNotIn(List<java.lang.Long> values) {
          addCriterion("oauth_id", values, ConditionMode.NOT_IN, "oauthId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserOauthSQL andOpenIdIsNull() {
		isnull("open_id");
		return this;
	}
	
	public UpmsUserOauthSQL andOpenIdIsNotNull() {
		notNull("open_id");
		return this;
	}
	
	public UpmsUserOauthSQL andOpenIdIsEmpty() {
		empty("open_id");
		return this;
	}

	public UpmsUserOauthSQL andOpenIdIsNotEmpty() {
		notEmpty("open_id");
		return this;
	}
      public UpmsUserOauthSQL andOpenIdEqualTo(byte[] value) {
          addCriterion("open_id", value, ConditionMode.EQUAL, "openId", "byte[]", "String");
          return this;
      }

      public UpmsUserOauthSQL andOpenIdNotEqualTo(byte[] value) {
          addCriterion("open_id", value, ConditionMode.NOT_EQUAL, "openId", "byte[]", "String");
          return this;
      }

      public UpmsUserOauthSQL andOpenIdGreaterThan(byte[] value) {
          addCriterion("open_id", value, ConditionMode.GREATER_THEN, "openId", "byte[]", "String");
          return this;
      }

      public UpmsUserOauthSQL andOpenIdGreaterThanOrEqualTo(byte[] value) {
          addCriterion("open_id", value, ConditionMode.GREATER_EQUAL, "openId", "byte[]", "String");
          return this;
      }

      public UpmsUserOauthSQL andOpenIdLessThan(byte[] value) {
          addCriterion("open_id", value, ConditionMode.LESS_THEN, "openId", "byte[]", "String");
          return this;
      }

      public UpmsUserOauthSQL andOpenIdLessThanOrEqualTo(byte[] value) {
          addCriterion("open_id", value, ConditionMode.LESS_EQUAL, "openId", "byte[]", "String");
          return this;
      }

      public UpmsUserOauthSQL andOpenIdBetween(byte[] value1, byte[] value2) {
    	  addCriterion("open_id", value1, value2, ConditionMode.BETWEEN, "openId", "byte[]", "String");
    	  return this;
      }

      public UpmsUserOauthSQL andOpenIdNotBetween(byte[] value1, byte[] value2) {
          addCriterion("open_id", value1, value2, ConditionMode.NOT_BETWEEN, "openId", "byte[]", "String");
          return this;
      }
        
      public UpmsUserOauthSQL andOpenIdIn(List<byte[]> values) {
          addCriterion("open_id", values, ConditionMode.IN, "openId", "byte[]", "String");
          return this;
      }

      public UpmsUserOauthSQL andOpenIdNotIn(List<byte[]> values) {
          addCriterion("open_id", values, ConditionMode.NOT_IN, "openId", "byte[]", "String");
          return this;
      }
	public UpmsUserOauthSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public UpmsUserOauthSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public UpmsUserOauthSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public UpmsUserOauthSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public UpmsUserOauthSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserOauthSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserOauthSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserOauthSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserOauthSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserOauthSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserOauthSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsUserOauthSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsUserOauthSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserOauthSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsUserOauthSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public UpmsUserOauthSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public UpmsUserOauthSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public UpmsUserOauthSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public UpmsUserOauthSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserOauthSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserOauthSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserOauthSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserOauthSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserOauthSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserOauthSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public UpmsUserOauthSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public UpmsUserOauthSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsUserOauthSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
}