/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsRoleRuleSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsRoleRuleSQL of() {
		return new UpmsRoleRuleSQL();
	}
	
	public static UpmsRoleRuleSQL by(Column column) {
		UpmsRoleRuleSQL that = new UpmsRoleRuleSQL();
		that.add(column);
        return that;
    }

    public static UpmsRoleRuleSQL by(String name, Object value) {
        return (UpmsRoleRuleSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_role_rule", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRoleRuleSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRoleRuleSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsRoleRuleSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsRoleRuleSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRoleRuleSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRoleRuleSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRoleRuleSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRoleRuleSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsRoleRuleSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsRoleRuleSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsRoleRuleSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsRoleRuleSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsRoleRuleSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsRoleRuleSQL andRuleRoleIdIsNull() {
		isnull("rule_role_id");
		return this;
	}
	
	public UpmsRoleRuleSQL andRuleRoleIdIsNotNull() {
		notNull("rule_role_id");
		return this;
	}
	
	public UpmsRoleRuleSQL andRuleRoleIdIsEmpty() {
		empty("rule_role_id");
		return this;
	}

	public UpmsRoleRuleSQL andRuleRoleIdIsNotEmpty() {
		notEmpty("rule_role_id");
		return this;
	}
      public UpmsRoleRuleSQL andRuleRoleIdEqualTo(java.lang.Long value) {
          addCriterion("rule_role_id", value, ConditionMode.EQUAL, "ruleRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRuleRoleIdNotEqualTo(java.lang.Long value) {
          addCriterion("rule_role_id", value, ConditionMode.NOT_EQUAL, "ruleRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRuleRoleIdGreaterThan(java.lang.Long value) {
          addCriterion("rule_role_id", value, ConditionMode.GREATER_THEN, "ruleRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRuleRoleIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("rule_role_id", value, ConditionMode.GREATER_EQUAL, "ruleRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRuleRoleIdLessThan(java.lang.Long value) {
          addCriterion("rule_role_id", value, ConditionMode.LESS_THEN, "ruleRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRuleRoleIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("rule_role_id", value, ConditionMode.LESS_EQUAL, "ruleRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRuleRoleIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("rule_role_id", value1, value2, ConditionMode.BETWEEN, "ruleRoleId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsRoleRuleSQL andRuleRoleIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("rule_role_id", value1, value2, ConditionMode.NOT_BETWEEN, "ruleRoleId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsRoleRuleSQL andRuleRoleIdIn(List<java.lang.Long> values) {
          addCriterion("rule_role_id", values, ConditionMode.IN, "ruleRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRuleRoleIdNotIn(List<java.lang.Long> values) {
          addCriterion("rule_role_id", values, ConditionMode.NOT_IN, "ruleRoleId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsRoleRuleSQL andRoleIdIsNull() {
		isnull("role_id");
		return this;
	}
	
	public UpmsRoleRuleSQL andRoleIdIsNotNull() {
		notNull("role_id");
		return this;
	}
	
	public UpmsRoleRuleSQL andRoleIdIsEmpty() {
		empty("role_id");
		return this;
	}

	public UpmsRoleRuleSQL andRoleIdIsNotEmpty() {
		notEmpty("role_id");
		return this;
	}
      public UpmsRoleRuleSQL andRoleIdEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRoleIdNotEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.NOT_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRoleIdGreaterThan(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.GREATER_THEN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRoleIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.GREATER_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRoleIdLessThan(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.LESS_THEN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRoleIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.LESS_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRoleIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("role_id", value1, value2, ConditionMode.BETWEEN, "roleId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsRoleRuleSQL andRoleIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("role_id", value1, value2, ConditionMode.NOT_BETWEEN, "roleId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsRoleRuleSQL andRoleIdIn(List<java.lang.Long> values) {
          addCriterion("role_id", values, ConditionMode.IN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRoleIdNotIn(List<java.lang.Long> values) {
          addCriterion("role_id", values, ConditionMode.NOT_IN, "roleId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsRoleRuleSQL andRuleIdIsNull() {
		isnull("rule_id");
		return this;
	}
	
	public UpmsRoleRuleSQL andRuleIdIsNotNull() {
		notNull("rule_id");
		return this;
	}
	
	public UpmsRoleRuleSQL andRuleIdIsEmpty() {
		empty("rule_id");
		return this;
	}

	public UpmsRoleRuleSQL andRuleIdIsNotEmpty() {
		notEmpty("rule_id");
		return this;
	}
      public UpmsRoleRuleSQL andRuleIdEqualTo(java.lang.Long value) {
          addCriterion("rule_id", value, ConditionMode.EQUAL, "ruleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRuleIdNotEqualTo(java.lang.Long value) {
          addCriterion("rule_id", value, ConditionMode.NOT_EQUAL, "ruleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRuleIdGreaterThan(java.lang.Long value) {
          addCriterion("rule_id", value, ConditionMode.GREATER_THEN, "ruleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRuleIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("rule_id", value, ConditionMode.GREATER_EQUAL, "ruleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRuleIdLessThan(java.lang.Long value) {
          addCriterion("rule_id", value, ConditionMode.LESS_THEN, "ruleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRuleIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("rule_id", value, ConditionMode.LESS_EQUAL, "ruleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRuleIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("rule_id", value1, value2, ConditionMode.BETWEEN, "ruleId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsRoleRuleSQL andRuleIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("rule_id", value1, value2, ConditionMode.NOT_BETWEEN, "ruleId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsRoleRuleSQL andRuleIdIn(List<java.lang.Long> values) {
          addCriterion("rule_id", values, ConditionMode.IN, "ruleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleRuleSQL andRuleIdNotIn(List<java.lang.Long> values) {
          addCriterion("rule_id", values, ConditionMode.NOT_IN, "ruleId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsRoleRuleSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public UpmsRoleRuleSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public UpmsRoleRuleSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public UpmsRoleRuleSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public UpmsRoleRuleSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRoleRuleSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRoleRuleSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRoleRuleSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRoleRuleSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRoleRuleSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRoleRuleSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public UpmsRoleRuleSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public UpmsRoleRuleSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public UpmsRoleRuleSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
}