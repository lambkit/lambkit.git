/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsUserPermissionSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsUserPermissionSQL of() {
		return new UpmsUserPermissionSQL();
	}
	
	public static UpmsUserPermissionSQL by(Column column) {
		UpmsUserPermissionSQL that = new UpmsUserPermissionSQL();
		that.add(column);
        return that;
    }

    public static UpmsUserPermissionSQL by(String name, Object value) {
        return (UpmsUserPermissionSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_user_permission", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserPermissionSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserPermissionSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsUserPermissionSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsUserPermissionSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserPermissionSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserPermissionSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserPermissionSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserPermissionSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsUserPermissionSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsUserPermissionSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsUserPermissionSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsUserPermissionSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsUserPermissionSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsUserPermissionSQL andUserPermissionIdIsNull() {
		isnull("user_permission_id");
		return this;
	}
	
	public UpmsUserPermissionSQL andUserPermissionIdIsNotNull() {
		notNull("user_permission_id");
		return this;
	}
	
	public UpmsUserPermissionSQL andUserPermissionIdIsEmpty() {
		empty("user_permission_id");
		return this;
	}

	public UpmsUserPermissionSQL andUserPermissionIdIsNotEmpty() {
		notEmpty("user_permission_id");
		return this;
	}
      public UpmsUserPermissionSQL andUserPermissionIdEqualTo(java.lang.Long value) {
          addCriterion("user_permission_id", value, ConditionMode.EQUAL, "userPermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andUserPermissionIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_permission_id", value, ConditionMode.NOT_EQUAL, "userPermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andUserPermissionIdGreaterThan(java.lang.Long value) {
          addCriterion("user_permission_id", value, ConditionMode.GREATER_THEN, "userPermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andUserPermissionIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_permission_id", value, ConditionMode.GREATER_EQUAL, "userPermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andUserPermissionIdLessThan(java.lang.Long value) {
          addCriterion("user_permission_id", value, ConditionMode.LESS_THEN, "userPermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andUserPermissionIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_permission_id", value, ConditionMode.LESS_EQUAL, "userPermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andUserPermissionIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_permission_id", value1, value2, ConditionMode.BETWEEN, "userPermissionId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserPermissionSQL andUserPermissionIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_permission_id", value1, value2, ConditionMode.NOT_BETWEEN, "userPermissionId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserPermissionSQL andUserPermissionIdIn(List<java.lang.Long> values) {
          addCriterion("user_permission_id", values, ConditionMode.IN, "userPermissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andUserPermissionIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_permission_id", values, ConditionMode.NOT_IN, "userPermissionId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserPermissionSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public UpmsUserPermissionSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public UpmsUserPermissionSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public UpmsUserPermissionSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public UpmsUserPermissionSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserPermissionSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserPermissionSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserPermissionSQL andPermissionIdIsNull() {
		isnull("permission_id");
		return this;
	}
	
	public UpmsUserPermissionSQL andPermissionIdIsNotNull() {
		notNull("permission_id");
		return this;
	}
	
	public UpmsUserPermissionSQL andPermissionIdIsEmpty() {
		empty("permission_id");
		return this;
	}

	public UpmsUserPermissionSQL andPermissionIdIsNotEmpty() {
		notEmpty("permission_id");
		return this;
	}
      public UpmsUserPermissionSQL andPermissionIdEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andPermissionIdNotEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.NOT_EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andPermissionIdGreaterThan(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.GREATER_THEN, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andPermissionIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.GREATER_EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andPermissionIdLessThan(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.LESS_THEN, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andPermissionIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("permission_id", value, ConditionMode.LESS_EQUAL, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andPermissionIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("permission_id", value1, value2, ConditionMode.BETWEEN, "permissionId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserPermissionSQL andPermissionIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("permission_id", value1, value2, ConditionMode.NOT_BETWEEN, "permissionId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserPermissionSQL andPermissionIdIn(List<java.lang.Long> values) {
          addCriterion("permission_id", values, ConditionMode.IN, "permissionId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andPermissionIdNotIn(List<java.lang.Long> values) {
          addCriterion("permission_id", values, ConditionMode.NOT_IN, "permissionId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserPermissionSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public UpmsUserPermissionSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public UpmsUserPermissionSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public UpmsUserPermissionSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
      public UpmsUserPermissionSQL andTypeEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andTypeGreaterThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andTypeLessThan(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsUserPermissionSQL andTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsUserPermissionSQL andTypeIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsUserPermissionSQL andTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.Integer", "Float");
          return this;
      }
}