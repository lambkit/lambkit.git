package com.lambkit.module.upms.kernel.satoken;

import cn.dev33.satoken.stp.StpInterface;
import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Lambkit;
import com.lambkit.module.upms.UpmsApiService;
import com.lambkit.module.upms.row.UpmsPermission;
import com.lambkit.module.upms.row.UpmsRole;
import com.lambkit.module.upms.row.UpmsUser;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yangyong(孤竹行)
 */
public class StpInterfaceImpl implements StpInterface {
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        UpmsUser upmsUser = Lambkit.get(UpmsApiService.class).selectUpmsUserByUsername(loginId.toString());
        List<String> permissions = new ArrayList<String>();
        boolean flag = Lambkit.get(UpmsApiService.class).hasRole(upmsUser.getUserId(), "super");
        if(flag) {
            permissions.add("*");
        } else {
            List<UpmsPermission> upmsPermissions = Lambkit.get(UpmsApiService.class).selectUpmsPermissionByUpmsUserId(upmsUser.getUserId());
            if(upmsPermissions != null) {
                for (UpmsPermission upmsPermission : upmsPermissions) {
                    if (StrUtil.isNotBlank(upmsPermission.getPermissionValue())) {
                        permissions.add(upmsPermission.getPermissionValue());
                        //permissions.add(upmsPermission.getUri());
                    }
                }
            }
        }
        return permissions;
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        List<String> roles = new ArrayList<String>();
        UpmsUser upmsUser = Lambkit.get(UpmsApiService.class).selectUpmsUserByUsername(loginId.toString());
        List<UpmsRole> upmsRoles = Lambkit.get(UpmsApiService.class).selectUpmsRoleByUpmsUserId(upmsUser.getUserId());
        if(upmsRoles != null) {
            for (UpmsRole upmsRole : upmsRoles) {
                if (StrUtil.isNotBlank(upmsRole.getName())) {
                    roles.add(upmsRole.getName());
                }
            }
        }
        return roles;
    }
}
