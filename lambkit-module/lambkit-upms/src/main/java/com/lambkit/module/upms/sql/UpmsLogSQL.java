/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsLogSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsLogSQL of() {
		return new UpmsLogSQL();
	}
	
	public static UpmsLogSQL by(Column column) {
		UpmsLogSQL that = new UpmsLogSQL();
		that.add(column);
        return that;
    }

    public static UpmsLogSQL by(String name, Object value) {
        return (UpmsLogSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_log", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsLogSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsLogSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsLogSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsLogSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsLogSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsLogSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsLogSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsLogSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsLogSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsLogSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsLogSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsLogSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsLogSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsLogSQL andLogIdIsNull() {
		isnull("log_id");
		return this;
	}
	
	public UpmsLogSQL andLogIdIsNotNull() {
		notNull("log_id");
		return this;
	}
	
	public UpmsLogSQL andLogIdIsEmpty() {
		empty("log_id");
		return this;
	}

	public UpmsLogSQL andLogIdIsNotEmpty() {
		notEmpty("log_id");
		return this;
	}
      public UpmsLogSQL andLogIdEqualTo(java.lang.Long value) {
          addCriterion("log_id", value, ConditionMode.EQUAL, "logId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsLogSQL andLogIdNotEqualTo(java.lang.Long value) {
          addCriterion("log_id", value, ConditionMode.NOT_EQUAL, "logId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsLogSQL andLogIdGreaterThan(java.lang.Long value) {
          addCriterion("log_id", value, ConditionMode.GREATER_THEN, "logId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsLogSQL andLogIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("log_id", value, ConditionMode.GREATER_EQUAL, "logId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsLogSQL andLogIdLessThan(java.lang.Long value) {
          addCriterion("log_id", value, ConditionMode.LESS_THEN, "logId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsLogSQL andLogIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("log_id", value, ConditionMode.LESS_EQUAL, "logId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsLogSQL andLogIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("log_id", value1, value2, ConditionMode.BETWEEN, "logId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsLogSQL andLogIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("log_id", value1, value2, ConditionMode.NOT_BETWEEN, "logId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsLogSQL andLogIdIn(List<java.lang.Long> values) {
          addCriterion("log_id", values, ConditionMode.IN, "logId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsLogSQL andLogIdNotIn(List<java.lang.Long> values) {
          addCriterion("log_id", values, ConditionMode.NOT_IN, "logId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsLogSQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public UpmsLogSQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public UpmsLogSQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public UpmsLogSQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public UpmsLogSQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsLogSQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "Float");
          return this;
      }
      public UpmsLogSQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public UpmsLogSQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public UpmsLogSQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public UpmsLogSQL andUsernameIsNull() {
		isnull("username");
		return this;
	}
	
	public UpmsLogSQL andUsernameIsNotNull() {
		notNull("username");
		return this;
	}
	
	public UpmsLogSQL andUsernameIsEmpty() {
		empty("username");
		return this;
	}

	public UpmsLogSQL andUsernameIsNotEmpty() {
		notEmpty("username");
		return this;
	}
       public UpmsLogSQL andUsernameLike(java.lang.String value) {
    	   addCriterion("username", value, ConditionMode.FUZZY, "username", "java.lang.String", "String");
    	   return this;
      }

      public UpmsLogSQL andUsernameNotLike(java.lang.String value) {
          addCriterion("username", value, ConditionMode.NOT_FUZZY, "username", "java.lang.String", "String");
          return this;
      }
      public UpmsLogSQL andUsernameEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUsernameNotEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.NOT_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUsernameGreaterThan(java.lang.String value) {
          addCriterion("username", value, ConditionMode.GREATER_THEN, "username", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUsernameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.GREATER_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUsernameLessThan(java.lang.String value) {
          addCriterion("username", value, ConditionMode.LESS_THEN, "username", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUsernameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.LESS_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUsernameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("username", value1, value2, ConditionMode.BETWEEN, "username", "java.lang.String", "String");
    	  return this;
      }

      public UpmsLogSQL andUsernameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("username", value1, value2, ConditionMode.NOT_BETWEEN, "username", "java.lang.String", "String");
          return this;
      }
        
      public UpmsLogSQL andUsernameIn(List<java.lang.String> values) {
          addCriterion("username", values, ConditionMode.IN, "username", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUsernameNotIn(List<java.lang.String> values) {
          addCriterion("username", values, ConditionMode.NOT_IN, "username", "java.lang.String", "String");
          return this;
      }
	public UpmsLogSQL andStartTimeIsNull() {
		isnull("start_time");
		return this;
	}
	
	public UpmsLogSQL andStartTimeIsNotNull() {
		notNull("start_time");
		return this;
	}
	
	public UpmsLogSQL andStartTimeIsEmpty() {
		empty("start_time");
		return this;
	}

	public UpmsLogSQL andStartTimeIsNotEmpty() {
		notEmpty("start_time");
		return this;
	}
      public UpmsLogSQL andStartTimeEqualTo(java.util.Date value) {
          addCriterion("start_time", value, ConditionMode.EQUAL, "startTime", "java.util.Date", "String");
          return this;
      }

      public UpmsLogSQL andStartTimeNotEqualTo(java.util.Date value) {
          addCriterion("start_time", value, ConditionMode.NOT_EQUAL, "startTime", "java.util.Date", "String");
          return this;
      }

      public UpmsLogSQL andStartTimeGreaterThan(java.util.Date value) {
          addCriterion("start_time", value, ConditionMode.GREATER_THEN, "startTime", "java.util.Date", "String");
          return this;
      }

      public UpmsLogSQL andStartTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("start_time", value, ConditionMode.GREATER_EQUAL, "startTime", "java.util.Date", "String");
          return this;
      }

      public UpmsLogSQL andStartTimeLessThan(java.util.Date value) {
          addCriterion("start_time", value, ConditionMode.LESS_THEN, "startTime", "java.util.Date", "String");
          return this;
      }

      public UpmsLogSQL andStartTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("start_time", value, ConditionMode.LESS_EQUAL, "startTime", "java.util.Date", "String");
          return this;
      }

      public UpmsLogSQL andStartTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("start_time", value1, value2, ConditionMode.BETWEEN, "startTime", "java.util.Date", "String");
    	  return this;
      }

      public UpmsLogSQL andStartTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("start_time", value1, value2, ConditionMode.NOT_BETWEEN, "startTime", "java.util.Date", "String");
          return this;
      }
        
      public UpmsLogSQL andStartTimeIn(List<java.util.Date> values) {
          addCriterion("start_time", values, ConditionMode.IN, "startTime", "java.util.Date", "String");
          return this;
      }

      public UpmsLogSQL andStartTimeNotIn(List<java.util.Date> values) {
          addCriterion("start_time", values, ConditionMode.NOT_IN, "startTime", "java.util.Date", "String");
          return this;
      }
	public UpmsLogSQL andSpendTimeIsNull() {
		isnull("spend_time");
		return this;
	}
	
	public UpmsLogSQL andSpendTimeIsNotNull() {
		notNull("spend_time");
		return this;
	}
	
	public UpmsLogSQL andSpendTimeIsEmpty() {
		empty("spend_time");
		return this;
	}

	public UpmsLogSQL andSpendTimeIsNotEmpty() {
		notEmpty("spend_time");
		return this;
	}
      public UpmsLogSQL andSpendTimeEqualTo(java.lang.Integer value) {
          addCriterion("spend_time", value, ConditionMode.EQUAL, "spendTime", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsLogSQL andSpendTimeNotEqualTo(java.lang.Integer value) {
          addCriterion("spend_time", value, ConditionMode.NOT_EQUAL, "spendTime", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsLogSQL andSpendTimeGreaterThan(java.lang.Integer value) {
          addCriterion("spend_time", value, ConditionMode.GREATER_THEN, "spendTime", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsLogSQL andSpendTimeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("spend_time", value, ConditionMode.GREATER_EQUAL, "spendTime", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsLogSQL andSpendTimeLessThan(java.lang.Integer value) {
          addCriterion("spend_time", value, ConditionMode.LESS_THEN, "spendTime", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsLogSQL andSpendTimeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("spend_time", value, ConditionMode.LESS_EQUAL, "spendTime", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsLogSQL andSpendTimeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("spend_time", value1, value2, ConditionMode.BETWEEN, "spendTime", "java.lang.Integer", "Float");
    	  return this;
      }

      public UpmsLogSQL andSpendTimeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("spend_time", value1, value2, ConditionMode.NOT_BETWEEN, "spendTime", "java.lang.Integer", "Float");
          return this;
      }
        
      public UpmsLogSQL andSpendTimeIn(List<java.lang.Integer> values) {
          addCriterion("spend_time", values, ConditionMode.IN, "spendTime", "java.lang.Integer", "Float");
          return this;
      }

      public UpmsLogSQL andSpendTimeNotIn(List<java.lang.Integer> values) {
          addCriterion("spend_time", values, ConditionMode.NOT_IN, "spendTime", "java.lang.Integer", "Float");
          return this;
      }
	public UpmsLogSQL andBasePathIsNull() {
		isnull("base_path");
		return this;
	}
	
	public UpmsLogSQL andBasePathIsNotNull() {
		notNull("base_path");
		return this;
	}
	
	public UpmsLogSQL andBasePathIsEmpty() {
		empty("base_path");
		return this;
	}

	public UpmsLogSQL andBasePathIsNotEmpty() {
		notEmpty("base_path");
		return this;
	}
       public UpmsLogSQL andBasePathLike(java.lang.String value) {
    	   addCriterion("base_path", value, ConditionMode.FUZZY, "basePath", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsLogSQL andBasePathNotLike(java.lang.String value) {
          addCriterion("base_path", value, ConditionMode.NOT_FUZZY, "basePath", "java.lang.String", "Float");
          return this;
      }
      public UpmsLogSQL andBasePathEqualTo(java.lang.String value) {
          addCriterion("base_path", value, ConditionMode.EQUAL, "basePath", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andBasePathNotEqualTo(java.lang.String value) {
          addCriterion("base_path", value, ConditionMode.NOT_EQUAL, "basePath", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andBasePathGreaterThan(java.lang.String value) {
          addCriterion("base_path", value, ConditionMode.GREATER_THEN, "basePath", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andBasePathGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("base_path", value, ConditionMode.GREATER_EQUAL, "basePath", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andBasePathLessThan(java.lang.String value) {
          addCriterion("base_path", value, ConditionMode.LESS_THEN, "basePath", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andBasePathLessThanOrEqualTo(java.lang.String value) {
          addCriterion("base_path", value, ConditionMode.LESS_EQUAL, "basePath", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andBasePathBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("base_path", value1, value2, ConditionMode.BETWEEN, "basePath", "java.lang.String", "String");
    	  return this;
      }

      public UpmsLogSQL andBasePathNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("base_path", value1, value2, ConditionMode.NOT_BETWEEN, "basePath", "java.lang.String", "String");
          return this;
      }
        
      public UpmsLogSQL andBasePathIn(List<java.lang.String> values) {
          addCriterion("base_path", values, ConditionMode.IN, "basePath", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andBasePathNotIn(List<java.lang.String> values) {
          addCriterion("base_path", values, ConditionMode.NOT_IN, "basePath", "java.lang.String", "String");
          return this;
      }
	public UpmsLogSQL andUriIsNull() {
		isnull("uri");
		return this;
	}
	
	public UpmsLogSQL andUriIsNotNull() {
		notNull("uri");
		return this;
	}
	
	public UpmsLogSQL andUriIsEmpty() {
		empty("uri");
		return this;
	}

	public UpmsLogSQL andUriIsNotEmpty() {
		notEmpty("uri");
		return this;
	}
       public UpmsLogSQL andUriLike(java.lang.String value) {
    	   addCriterion("uri", value, ConditionMode.FUZZY, "uri", "java.lang.String", "String");
    	   return this;
      }

      public UpmsLogSQL andUriNotLike(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.NOT_FUZZY, "uri", "java.lang.String", "String");
          return this;
      }
      public UpmsLogSQL andUriEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUriNotEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.NOT_EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUriGreaterThan(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.GREATER_THEN, "uri", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUriGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.GREATER_EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUriLessThan(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.LESS_THEN, "uri", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUriLessThanOrEqualTo(java.lang.String value) {
          addCriterion("uri", value, ConditionMode.LESS_EQUAL, "uri", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUriBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("uri", value1, value2, ConditionMode.BETWEEN, "uri", "java.lang.String", "String");
    	  return this;
      }

      public UpmsLogSQL andUriNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("uri", value1, value2, ConditionMode.NOT_BETWEEN, "uri", "java.lang.String", "String");
          return this;
      }
        
      public UpmsLogSQL andUriIn(List<java.lang.String> values) {
          addCriterion("uri", values, ConditionMode.IN, "uri", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUriNotIn(List<java.lang.String> values) {
          addCriterion("uri", values, ConditionMode.NOT_IN, "uri", "java.lang.String", "String");
          return this;
      }
	public UpmsLogSQL andUrlIsNull() {
		isnull("url");
		return this;
	}
	
	public UpmsLogSQL andUrlIsNotNull() {
		notNull("url");
		return this;
	}
	
	public UpmsLogSQL andUrlIsEmpty() {
		empty("url");
		return this;
	}

	public UpmsLogSQL andUrlIsNotEmpty() {
		notEmpty("url");
		return this;
	}
       public UpmsLogSQL andUrlLike(java.lang.String value) {
    	   addCriterion("url", value, ConditionMode.FUZZY, "url", "java.lang.String", "String");
    	   return this;
      }

      public UpmsLogSQL andUrlNotLike(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_FUZZY, "url", "java.lang.String", "String");
          return this;
      }
      public UpmsLogSQL andUrlEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUrlNotEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUrlGreaterThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUrlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUrlLessThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUrlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUrlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("url", value1, value2, ConditionMode.BETWEEN, "url", "java.lang.String", "String");
    	  return this;
      }

      public UpmsLogSQL andUrlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("url", value1, value2, ConditionMode.NOT_BETWEEN, "url", "java.lang.String", "String");
          return this;
      }
        
      public UpmsLogSQL andUrlIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.IN, "url", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUrlNotIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.NOT_IN, "url", "java.lang.String", "String");
          return this;
      }
	public UpmsLogSQL andMethodIsNull() {
		isnull("method");
		return this;
	}
	
	public UpmsLogSQL andMethodIsNotNull() {
		notNull("method");
		return this;
	}
	
	public UpmsLogSQL andMethodIsEmpty() {
		empty("method");
		return this;
	}

	public UpmsLogSQL andMethodIsNotEmpty() {
		notEmpty("method");
		return this;
	}
       public UpmsLogSQL andMethodLike(java.lang.String value) {
    	   addCriterion("method", value, ConditionMode.FUZZY, "method", "java.lang.String", "String");
    	   return this;
      }

      public UpmsLogSQL andMethodNotLike(java.lang.String value) {
          addCriterion("method", value, ConditionMode.NOT_FUZZY, "method", "java.lang.String", "String");
          return this;
      }
      public UpmsLogSQL andMethodEqualTo(java.lang.String value) {
          addCriterion("method", value, ConditionMode.EQUAL, "method", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andMethodNotEqualTo(java.lang.String value) {
          addCriterion("method", value, ConditionMode.NOT_EQUAL, "method", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andMethodGreaterThan(java.lang.String value) {
          addCriterion("method", value, ConditionMode.GREATER_THEN, "method", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andMethodGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("method", value, ConditionMode.GREATER_EQUAL, "method", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andMethodLessThan(java.lang.String value) {
          addCriterion("method", value, ConditionMode.LESS_THEN, "method", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andMethodLessThanOrEqualTo(java.lang.String value) {
          addCriterion("method", value, ConditionMode.LESS_EQUAL, "method", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andMethodBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("method", value1, value2, ConditionMode.BETWEEN, "method", "java.lang.String", "String");
    	  return this;
      }

      public UpmsLogSQL andMethodNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("method", value1, value2, ConditionMode.NOT_BETWEEN, "method", "java.lang.String", "String");
          return this;
      }
        
      public UpmsLogSQL andMethodIn(List<java.lang.String> values) {
          addCriterion("method", values, ConditionMode.IN, "method", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andMethodNotIn(List<java.lang.String> values) {
          addCriterion("method", values, ConditionMode.NOT_IN, "method", "java.lang.String", "String");
          return this;
      }
	public UpmsLogSQL andParameterIsNull() {
		isnull("parameter");
		return this;
	}
	
	public UpmsLogSQL andParameterIsNotNull() {
		notNull("parameter");
		return this;
	}
	
	public UpmsLogSQL andParameterIsEmpty() {
		empty("parameter");
		return this;
	}

	public UpmsLogSQL andParameterIsNotEmpty() {
		notEmpty("parameter");
		return this;
	}
       public UpmsLogSQL andParameterLike(java.lang.String value) {
    	   addCriterion("parameter", value, ConditionMode.FUZZY, "parameter", "java.lang.String", "String");
    	   return this;
      }

      public UpmsLogSQL andParameterNotLike(java.lang.String value) {
          addCriterion("parameter", value, ConditionMode.NOT_FUZZY, "parameter", "java.lang.String", "String");
          return this;
      }
      public UpmsLogSQL andParameterEqualTo(java.lang.String value) {
          addCriterion("parameter", value, ConditionMode.EQUAL, "parameter", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andParameterNotEqualTo(java.lang.String value) {
          addCriterion("parameter", value, ConditionMode.NOT_EQUAL, "parameter", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andParameterGreaterThan(java.lang.String value) {
          addCriterion("parameter", value, ConditionMode.GREATER_THEN, "parameter", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andParameterGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("parameter", value, ConditionMode.GREATER_EQUAL, "parameter", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andParameterLessThan(java.lang.String value) {
          addCriterion("parameter", value, ConditionMode.LESS_THEN, "parameter", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andParameterLessThanOrEqualTo(java.lang.String value) {
          addCriterion("parameter", value, ConditionMode.LESS_EQUAL, "parameter", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andParameterBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("parameter", value1, value2, ConditionMode.BETWEEN, "parameter", "java.lang.String", "String");
    	  return this;
      }

      public UpmsLogSQL andParameterNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("parameter", value1, value2, ConditionMode.NOT_BETWEEN, "parameter", "java.lang.String", "String");
          return this;
      }
        
      public UpmsLogSQL andParameterIn(List<java.lang.String> values) {
          addCriterion("parameter", values, ConditionMode.IN, "parameter", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andParameterNotIn(List<java.lang.String> values) {
          addCriterion("parameter", values, ConditionMode.NOT_IN, "parameter", "java.lang.String", "String");
          return this;
      }
	public UpmsLogSQL andUserAgentIsNull() {
		isnull("user_agent");
		return this;
	}
	
	public UpmsLogSQL andUserAgentIsNotNull() {
		notNull("user_agent");
		return this;
	}
	
	public UpmsLogSQL andUserAgentIsEmpty() {
		empty("user_agent");
		return this;
	}

	public UpmsLogSQL andUserAgentIsNotEmpty() {
		notEmpty("user_agent");
		return this;
	}
       public UpmsLogSQL andUserAgentLike(java.lang.String value) {
    	   addCriterion("user_agent", value, ConditionMode.FUZZY, "userAgent", "java.lang.String", "String");
    	   return this;
      }

      public UpmsLogSQL andUserAgentNotLike(java.lang.String value) {
          addCriterion("user_agent", value, ConditionMode.NOT_FUZZY, "userAgent", "java.lang.String", "String");
          return this;
      }
      public UpmsLogSQL andUserAgentEqualTo(java.lang.String value) {
          addCriterion("user_agent", value, ConditionMode.EQUAL, "userAgent", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUserAgentNotEqualTo(java.lang.String value) {
          addCriterion("user_agent", value, ConditionMode.NOT_EQUAL, "userAgent", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUserAgentGreaterThan(java.lang.String value) {
          addCriterion("user_agent", value, ConditionMode.GREATER_THEN, "userAgent", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUserAgentGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_agent", value, ConditionMode.GREATER_EQUAL, "userAgent", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUserAgentLessThan(java.lang.String value) {
          addCriterion("user_agent", value, ConditionMode.LESS_THEN, "userAgent", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUserAgentLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_agent", value, ConditionMode.LESS_EQUAL, "userAgent", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUserAgentBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_agent", value1, value2, ConditionMode.BETWEEN, "userAgent", "java.lang.String", "String");
    	  return this;
      }

      public UpmsLogSQL andUserAgentNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_agent", value1, value2, ConditionMode.NOT_BETWEEN, "userAgent", "java.lang.String", "String");
          return this;
      }
        
      public UpmsLogSQL andUserAgentIn(List<java.lang.String> values) {
          addCriterion("user_agent", values, ConditionMode.IN, "userAgent", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andUserAgentNotIn(List<java.lang.String> values) {
          addCriterion("user_agent", values, ConditionMode.NOT_IN, "userAgent", "java.lang.String", "String");
          return this;
      }
	public UpmsLogSQL andIpIsNull() {
		isnull("ip");
		return this;
	}
	
	public UpmsLogSQL andIpIsNotNull() {
		notNull("ip");
		return this;
	}
	
	public UpmsLogSQL andIpIsEmpty() {
		empty("ip");
		return this;
	}

	public UpmsLogSQL andIpIsNotEmpty() {
		notEmpty("ip");
		return this;
	}
       public UpmsLogSQL andIpLike(java.lang.String value) {
    	   addCriterion("ip", value, ConditionMode.FUZZY, "ip", "java.lang.String", "String");
    	   return this;
      }

      public UpmsLogSQL andIpNotLike(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.NOT_FUZZY, "ip", "java.lang.String", "String");
          return this;
      }
      public UpmsLogSQL andIpEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andIpNotEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.NOT_EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andIpGreaterThan(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.GREATER_THEN, "ip", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andIpGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.GREATER_EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andIpLessThan(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.LESS_THEN, "ip", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andIpLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ip", value, ConditionMode.LESS_EQUAL, "ip", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andIpBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ip", value1, value2, ConditionMode.BETWEEN, "ip", "java.lang.String", "String");
    	  return this;
      }

      public UpmsLogSQL andIpNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ip", value1, value2, ConditionMode.NOT_BETWEEN, "ip", "java.lang.String", "String");
          return this;
      }
        
      public UpmsLogSQL andIpIn(List<java.lang.String> values) {
          addCriterion("ip", values, ConditionMode.IN, "ip", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andIpNotIn(List<java.lang.String> values) {
          addCriterion("ip", values, ConditionMode.NOT_IN, "ip", "java.lang.String", "String");
          return this;
      }
	public UpmsLogSQL andResultIsNull() {
		isnull("result");
		return this;
	}
	
	public UpmsLogSQL andResultIsNotNull() {
		notNull("result");
		return this;
	}
	
	public UpmsLogSQL andResultIsEmpty() {
		empty("result");
		return this;
	}

	public UpmsLogSQL andResultIsNotEmpty() {
		notEmpty("result");
		return this;
	}
       public UpmsLogSQL andResultLike(java.lang.String value) {
    	   addCriterion("result", value, ConditionMode.FUZZY, "result", "java.lang.String", "String");
    	   return this;
      }

      public UpmsLogSQL andResultNotLike(java.lang.String value) {
          addCriterion("result", value, ConditionMode.NOT_FUZZY, "result", "java.lang.String", "String");
          return this;
      }
      public UpmsLogSQL andResultEqualTo(java.lang.String value) {
          addCriterion("result", value, ConditionMode.EQUAL, "result", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andResultNotEqualTo(java.lang.String value) {
          addCriterion("result", value, ConditionMode.NOT_EQUAL, "result", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andResultGreaterThan(java.lang.String value) {
          addCriterion("result", value, ConditionMode.GREATER_THEN, "result", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andResultGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("result", value, ConditionMode.GREATER_EQUAL, "result", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andResultLessThan(java.lang.String value) {
          addCriterion("result", value, ConditionMode.LESS_THEN, "result", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andResultLessThanOrEqualTo(java.lang.String value) {
          addCriterion("result", value, ConditionMode.LESS_EQUAL, "result", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andResultBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("result", value1, value2, ConditionMode.BETWEEN, "result", "java.lang.String", "String");
    	  return this;
      }

      public UpmsLogSQL andResultNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("result", value1, value2, ConditionMode.NOT_BETWEEN, "result", "java.lang.String", "String");
          return this;
      }
        
      public UpmsLogSQL andResultIn(List<java.lang.String> values) {
          addCriterion("result", values, ConditionMode.IN, "result", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andResultNotIn(List<java.lang.String> values) {
          addCriterion("result", values, ConditionMode.NOT_IN, "result", "java.lang.String", "String");
          return this;
      }
	public UpmsLogSQL andPermissionsIsNull() {
		isnull("permissions");
		return this;
	}
	
	public UpmsLogSQL andPermissionsIsNotNull() {
		notNull("permissions");
		return this;
	}
	
	public UpmsLogSQL andPermissionsIsEmpty() {
		empty("permissions");
		return this;
	}

	public UpmsLogSQL andPermissionsIsNotEmpty() {
		notEmpty("permissions");
		return this;
	}
       public UpmsLogSQL andPermissionsLike(java.lang.String value) {
    	   addCriterion("permissions", value, ConditionMode.FUZZY, "permissions", "java.lang.String", "String");
    	   return this;
      }

      public UpmsLogSQL andPermissionsNotLike(java.lang.String value) {
          addCriterion("permissions", value, ConditionMode.NOT_FUZZY, "permissions", "java.lang.String", "String");
          return this;
      }
      public UpmsLogSQL andPermissionsEqualTo(java.lang.String value) {
          addCriterion("permissions", value, ConditionMode.EQUAL, "permissions", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andPermissionsNotEqualTo(java.lang.String value) {
          addCriterion("permissions", value, ConditionMode.NOT_EQUAL, "permissions", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andPermissionsGreaterThan(java.lang.String value) {
          addCriterion("permissions", value, ConditionMode.GREATER_THEN, "permissions", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andPermissionsGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("permissions", value, ConditionMode.GREATER_EQUAL, "permissions", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andPermissionsLessThan(java.lang.String value) {
          addCriterion("permissions", value, ConditionMode.LESS_THEN, "permissions", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andPermissionsLessThanOrEqualTo(java.lang.String value) {
          addCriterion("permissions", value, ConditionMode.LESS_EQUAL, "permissions", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andPermissionsBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("permissions", value1, value2, ConditionMode.BETWEEN, "permissions", "java.lang.String", "String");
    	  return this;
      }

      public UpmsLogSQL andPermissionsNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("permissions", value1, value2, ConditionMode.NOT_BETWEEN, "permissions", "java.lang.String", "String");
          return this;
      }
        
      public UpmsLogSQL andPermissionsIn(List<java.lang.String> values) {
          addCriterion("permissions", values, ConditionMode.IN, "permissions", "java.lang.String", "String");
          return this;
      }

      public UpmsLogSQL andPermissionsNotIn(List<java.lang.String> values) {
          addCriterion("permissions", values, ConditionMode.NOT_IN, "permissions", "java.lang.String", "String");
          return this;
      }
}