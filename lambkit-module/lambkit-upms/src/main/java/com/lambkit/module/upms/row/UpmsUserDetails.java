/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsUserDetails extends RowModel<UpmsUserDetails> {
	public UpmsUserDetails() {
		setTableName("upms_user_details");
		setPrimaryKey("user_id");
	}
	public java.lang.Long getUserId() {
		return this.get("user_id");
	}
	public void setUserId(java.lang.Long userId) {
		this.set("user_id", userId);
	}
	public java.lang.String getSignature() {
		return this.get("signature");
	}
	public void setSignature(java.lang.String signature) {
		this.set("signature", signature);
	}
	public java.lang.String getRealName() {
		return this.get("real_name");
	}
	public void setRealName(java.lang.String realName) {
		this.set("real_name", realName);
	}
	public java.util.Date getBirthday() {
		return this.get("birthday");
	}
	public void setBirthday(java.util.Date birthday) {
		this.set("birthday", birthday);
	}
	public java.lang.String getQuestion() {
		return this.get("question");
	}
	public void setQuestion(java.lang.String question) {
		this.set("question", question);
	}
	public java.lang.String getAnswer() {
		return this.get("answer");
	}
	public void setAnswer(java.lang.String answer) {
		this.set("answer", answer);
	}
}
