/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms;

import com.lambkit.LambkitConsts;
import com.lambkit.core.config.annotation.PropConfig;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

@PropConfig(prefix="lambkit.upms")
public class UpmsConfig {
	private String version = "1.0";
	/**
	 * 数据库连接池名称
	 */
	private String dbPoolName;
	/**
	 * serverType=client时，配置server端的sso地址
	 */
	private String serverSsoUrl;
	/**
	 * client的应用ID
	 */
	private String appId;
	/**
	 * 所需要启动的插件， satoken, session, jwt
	 */
	private String kernel = "satoken";

	/**
	 * session id 名称，cookie的名称
	 */
	private String sessionIdName = "lambkit-upms-server-session-id";

	/**
	 * 短信验证方式
	 */
	private String sms = "huawei";
	/**
	 * 缓存类型
	 */
	private String cacheType = LambkitConsts.CACHE_EHCACHE;

	/**
	 * 敏感信息存储方式，normal、ASE、RSA
	 */
	private String sensitiveInfoStore = "normal";

	/**
	 * 密码加密方式
	 */
	private String passwordEncrypt;

	/**
	 * 用户登录锁定的错误次数
	 */
	private int accountLockerNum = 6;

	/**
	 * 激活用户权限
	 */
	private boolean activeUpmsPermission = true;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDbPoolName() {
		return dbPoolName;
	}

	public void setDbPoolName(String dbPoolName) {
		this.dbPoolName = dbPoolName;
	}

	public String getSms() {
		return sms;
	}

	public void setSms(String sms) {
		this.sms = sms;
	}

	public String getCacheType() {
		return cacheType;
	}

	public void setCacheType(String cacheType) {
		this.cacheType = cacheType;
	}

	public String getServerSsoUrl() {
		return serverSsoUrl;
	}

	public void setServerSsoUrl(String serverSsoUrl) {
		this.serverSsoUrl = serverSsoUrl;
	}

	public String getKernel() {
		return kernel;
	}

	public void setKernel(String kernel) {
		this.kernel = kernel;
	}

	public String getSessionIdName() {
		return sessionIdName;
	}

	public void setSessionIdName(String sessionIdName) {
		this.sessionIdName = sessionIdName;
	}

	public String getSensitiveInfoStore() {
		return sensitiveInfoStore;
	}

	public void setSensitiveInfoStore(String sensitiveInfoStore) {
		this.sensitiveInfoStore = sensitiveInfoStore;
	}

	public String getPasswordEncrypt() {
		return passwordEncrypt;
	}

	public void setPasswordEncrypt(String passwordEncrypt) {
		this.passwordEncrypt = passwordEncrypt;
	}

	public int getAccountLockerNum() {
		return accountLockerNum;
	}

	public void setAccountLockerNum(int accountLockerNum) {
		this.accountLockerNum = accountLockerNum;
	}

	public boolean isActiveUpmsPermission() {
		return activeUpmsPermission;
	}

	public void setActiveUpmsPermission(boolean activeUpmsPermission) {
		this.activeUpmsPermission = activeUpmsPermission;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}
}
