/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsRoleSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsRoleSQL of() {
		return new UpmsRoleSQL();
	}
	
	public static UpmsRoleSQL by(Column column) {
		UpmsRoleSQL that = new UpmsRoleSQL();
		that.add(column);
        return that;
    }

    public static UpmsRoleSQL by(String name, Object value) {
        return (UpmsRoleSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_role", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRoleSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRoleSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsRoleSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsRoleSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRoleSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRoleSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRoleSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsRoleSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsRoleSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsRoleSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsRoleSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsRoleSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsRoleSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsRoleSQL andRoleIdIsNull() {
		isnull("role_id");
		return this;
	}
	
	public UpmsRoleSQL andRoleIdIsNotNull() {
		notNull("role_id");
		return this;
	}
	
	public UpmsRoleSQL andRoleIdIsEmpty() {
		empty("role_id");
		return this;
	}

	public UpmsRoleSQL andRoleIdIsNotEmpty() {
		notEmpty("role_id");
		return this;
	}
      public UpmsRoleSQL andRoleIdEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleSQL andRoleIdNotEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.NOT_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleSQL andRoleIdGreaterThan(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.GREATER_THEN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleSQL andRoleIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.GREATER_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleSQL andRoleIdLessThan(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.LESS_THEN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleSQL andRoleIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.LESS_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleSQL andRoleIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("role_id", value1, value2, ConditionMode.BETWEEN, "roleId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsRoleSQL andRoleIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("role_id", value1, value2, ConditionMode.NOT_BETWEEN, "roleId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsRoleSQL andRoleIdIn(List<java.lang.Long> values) {
          addCriterion("role_id", values, ConditionMode.IN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleSQL andRoleIdNotIn(List<java.lang.Long> values) {
          addCriterion("role_id", values, ConditionMode.NOT_IN, "roleId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsRoleSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public UpmsRoleSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public UpmsRoleSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public UpmsRoleSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public UpmsRoleSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsRoleSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public UpmsRoleSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public UpmsRoleSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public UpmsRoleSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public UpmsRoleSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public UpmsRoleSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public UpmsRoleSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public UpmsRoleSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public UpmsRoleSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "String");
    	   return this;
      }

      public UpmsRoleSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "String");
          return this;
      }
      public UpmsRoleSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public UpmsRoleSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public UpmsRoleSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public UpmsRoleSQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public UpmsRoleSQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public UpmsRoleSQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public UpmsRoleSQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public UpmsRoleSQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public UpmsRoleSQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public UpmsRoleSQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public UpmsRoleSQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public UpmsRoleSQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public UpmsRoleSQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public UpmsRoleSQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public UpmsRoleSQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public UpmsRoleSQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public UpmsRoleSQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public UpmsRoleSQL andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsRoleSQL andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsRoleSQL andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsRoleSQL andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsRoleSQL andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsRoleSQL andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsRoleSQL andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public UpmsRoleSQL andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public UpmsRoleSQL andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public UpmsRoleSQL andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
	public UpmsRoleSQL andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public UpmsRoleSQL andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public UpmsRoleSQL andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public UpmsRoleSQL andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
      public UpmsRoleSQL andOrdersEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleSQL andOrdersNotEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleSQL andOrdersGreaterThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleSQL andOrdersGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleSQL andOrdersLessThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleSQL andOrdersLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleSQL andOrdersBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsRoleSQL andOrdersNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsRoleSQL andOrdersIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public UpmsRoleSQL andOrdersNotIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.lang.Long", "Float");
          return this;
      }
}