/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsOauthSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsOauthSQL of() {
		return new UpmsOauthSQL();
	}
	
	public static UpmsOauthSQL by(Column column) {
		UpmsOauthSQL that = new UpmsOauthSQL();
		that.add(column);
        return that;
    }

    public static UpmsOauthSQL by(String name, Object value) {
        return (UpmsOauthSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_oauth", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsOauthSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsOauthSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsOauthSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsOauthSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsOauthSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsOauthSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsOauthSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsOauthSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsOauthSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsOauthSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsOauthSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsOauthSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsOauthSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsOauthSQL andOauthIdIsNull() {
		isnull("oauth_id");
		return this;
	}
	
	public UpmsOauthSQL andOauthIdIsNotNull() {
		notNull("oauth_id");
		return this;
	}
	
	public UpmsOauthSQL andOauthIdIsEmpty() {
		empty("oauth_id");
		return this;
	}

	public UpmsOauthSQL andOauthIdIsNotEmpty() {
		notEmpty("oauth_id");
		return this;
	}
      public UpmsOauthSQL andOauthIdEqualTo(java.lang.Long value) {
          addCriterion("oauth_id", value, ConditionMode.EQUAL, "oauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOauthSQL andOauthIdNotEqualTo(java.lang.Long value) {
          addCriterion("oauth_id", value, ConditionMode.NOT_EQUAL, "oauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOauthSQL andOauthIdGreaterThan(java.lang.Long value) {
          addCriterion("oauth_id", value, ConditionMode.GREATER_THEN, "oauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOauthSQL andOauthIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("oauth_id", value, ConditionMode.GREATER_EQUAL, "oauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOauthSQL andOauthIdLessThan(java.lang.Long value) {
          addCriterion("oauth_id", value, ConditionMode.LESS_THEN, "oauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOauthSQL andOauthIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("oauth_id", value, ConditionMode.LESS_EQUAL, "oauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOauthSQL andOauthIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("oauth_id", value1, value2, ConditionMode.BETWEEN, "oauthId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsOauthSQL andOauthIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("oauth_id", value1, value2, ConditionMode.NOT_BETWEEN, "oauthId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsOauthSQL andOauthIdIn(List<java.lang.Long> values) {
          addCriterion("oauth_id", values, ConditionMode.IN, "oauthId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsOauthSQL andOauthIdNotIn(List<java.lang.Long> values) {
          addCriterion("oauth_id", values, ConditionMode.NOT_IN, "oauthId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsOauthSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public UpmsOauthSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public UpmsOauthSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public UpmsOauthSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public UpmsOauthSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public UpmsOauthSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public UpmsOauthSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsOauthSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsOauthSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsOauthSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsOauthSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsOauthSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsOauthSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public UpmsOauthSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public UpmsOauthSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public UpmsOauthSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
}