/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.upms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class UpmsUserRoleSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static UpmsUserRoleSQL of() {
		return new UpmsUserRoleSQL();
	}
	
	public static UpmsUserRoleSQL by(Column column) {
		UpmsUserRoleSQL that = new UpmsUserRoleSQL();
		that.add(column);
        return that;
    }

    public static UpmsUserRoleSQL by(String name, Object value) {
        return (UpmsUserRoleSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("upms_user_role", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserRoleSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserRoleSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public UpmsUserRoleSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public UpmsUserRoleSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserRoleSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserRoleSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserRoleSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public UpmsUserRoleSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public UpmsUserRoleSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public UpmsUserRoleSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public UpmsUserRoleSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public UpmsUserRoleSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public UpmsUserRoleSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public UpmsUserRoleSQL andUserRoleIdIsNull() {
		isnull("user_role_id");
		return this;
	}
	
	public UpmsUserRoleSQL andUserRoleIdIsNotNull() {
		notNull("user_role_id");
		return this;
	}
	
	public UpmsUserRoleSQL andUserRoleIdIsEmpty() {
		empty("user_role_id");
		return this;
	}

	public UpmsUserRoleSQL andUserRoleIdIsNotEmpty() {
		notEmpty("user_role_id");
		return this;
	}
      public UpmsUserRoleSQL andUserRoleIdEqualTo(java.lang.Long value) {
          addCriterion("user_role_id", value, ConditionMode.EQUAL, "userRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andUserRoleIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_role_id", value, ConditionMode.NOT_EQUAL, "userRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andUserRoleIdGreaterThan(java.lang.Long value) {
          addCriterion("user_role_id", value, ConditionMode.GREATER_THEN, "userRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andUserRoleIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_role_id", value, ConditionMode.GREATER_EQUAL, "userRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andUserRoleIdLessThan(java.lang.Long value) {
          addCriterion("user_role_id", value, ConditionMode.LESS_THEN, "userRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andUserRoleIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_role_id", value, ConditionMode.LESS_EQUAL, "userRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andUserRoleIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_role_id", value1, value2, ConditionMode.BETWEEN, "userRoleId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserRoleSQL andUserRoleIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_role_id", value1, value2, ConditionMode.NOT_BETWEEN, "userRoleId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserRoleSQL andUserRoleIdIn(List<java.lang.Long> values) {
          addCriterion("user_role_id", values, ConditionMode.IN, "userRoleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andUserRoleIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_role_id", values, ConditionMode.NOT_IN, "userRoleId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserRoleSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public UpmsUserRoleSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public UpmsUserRoleSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public UpmsUserRoleSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public UpmsUserRoleSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserRoleSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserRoleSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public UpmsUserRoleSQL andRoleIdIsNull() {
		isnull("role_id");
		return this;
	}
	
	public UpmsUserRoleSQL andRoleIdIsNotNull() {
		notNull("role_id");
		return this;
	}
	
	public UpmsUserRoleSQL andRoleIdIsEmpty() {
		empty("role_id");
		return this;
	}

	public UpmsUserRoleSQL andRoleIdIsNotEmpty() {
		notEmpty("role_id");
		return this;
	}
      public UpmsUserRoleSQL andRoleIdEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andRoleIdNotEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.NOT_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andRoleIdGreaterThan(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.GREATER_THEN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andRoleIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.GREATER_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andRoleIdLessThan(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.LESS_THEN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andRoleIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("role_id", value, ConditionMode.LESS_EQUAL, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andRoleIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("role_id", value1, value2, ConditionMode.BETWEEN, "roleId", "java.lang.Long", "Float");
    	  return this;
      }

      public UpmsUserRoleSQL andRoleIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("role_id", value1, value2, ConditionMode.NOT_BETWEEN, "roleId", "java.lang.Long", "Float");
          return this;
      }
        
      public UpmsUserRoleSQL andRoleIdIn(List<java.lang.Long> values) {
          addCriterion("role_id", values, ConditionMode.IN, "roleId", "java.lang.Long", "Float");
          return this;
      }

      public UpmsUserRoleSQL andRoleIdNotIn(List<java.lang.Long> values) {
          addCriterion("role_id", values, ConditionMode.NOT_IN, "roleId", "java.lang.Long", "Float");
          return this;
      }
}