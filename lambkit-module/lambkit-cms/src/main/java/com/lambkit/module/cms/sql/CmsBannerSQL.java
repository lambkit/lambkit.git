/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsBannerSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsBannerSQL of() {
		return new CmsBannerSQL();
	}
	
	public static CmsBannerSQL by(Column column) {
		CmsBannerSQL that = new CmsBannerSQL();
		that.add(column);
        return that;
    }

    public static CmsBannerSQL by(String name, Object value) {
        return (CmsBannerSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_banner", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsBannerSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsBannerSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsBannerSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsBannerSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsBannerSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsBannerSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsBannerSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsBannerSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsBannerSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsBannerSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsBannerSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsBannerSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsBannerSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsBannerSQL andBannerIdIsNull() {
		isnull("banner_id");
		return this;
	}
	
	public CmsBannerSQL andBannerIdIsNotNull() {
		notNull("banner_id");
		return this;
	}
	
	public CmsBannerSQL andBannerIdIsEmpty() {
		empty("banner_id");
		return this;
	}

	public CmsBannerSQL andBannerIdIsNotEmpty() {
		notEmpty("banner_id");
		return this;
	}
      public CmsBannerSQL andBannerIdEqualTo(java.lang.Long value) {
          addCriterion("banner_id", value, ConditionMode.EQUAL, "bannerId", "java.lang.Long", "Float");
          return this;
      }

      public CmsBannerSQL andBannerIdNotEqualTo(java.lang.Long value) {
          addCriterion("banner_id", value, ConditionMode.NOT_EQUAL, "bannerId", "java.lang.Long", "Float");
          return this;
      }

      public CmsBannerSQL andBannerIdGreaterThan(java.lang.Long value) {
          addCriterion("banner_id", value, ConditionMode.GREATER_THEN, "bannerId", "java.lang.Long", "Float");
          return this;
      }

      public CmsBannerSQL andBannerIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("banner_id", value, ConditionMode.GREATER_EQUAL, "bannerId", "java.lang.Long", "Float");
          return this;
      }

      public CmsBannerSQL andBannerIdLessThan(java.lang.Long value) {
          addCriterion("banner_id", value, ConditionMode.LESS_THEN, "bannerId", "java.lang.Long", "Float");
          return this;
      }

      public CmsBannerSQL andBannerIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("banner_id", value, ConditionMode.LESS_EQUAL, "bannerId", "java.lang.Long", "Float");
          return this;
      }

      public CmsBannerSQL andBannerIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("banner_id", value1, value2, ConditionMode.BETWEEN, "bannerId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsBannerSQL andBannerIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("banner_id", value1, value2, ConditionMode.NOT_BETWEEN, "bannerId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsBannerSQL andBannerIdIn(List<java.lang.Long> values) {
          addCriterion("banner_id", values, ConditionMode.IN, "bannerId", "java.lang.Long", "Float");
          return this;
      }

      public CmsBannerSQL andBannerIdNotIn(List<java.lang.Long> values) {
          addCriterion("banner_id", values, ConditionMode.NOT_IN, "bannerId", "java.lang.Long", "Float");
          return this;
      }
	public CmsBannerSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public CmsBannerSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public CmsBannerSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public CmsBannerSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public CmsBannerSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "Float");
    	   return this;
      }

      public CmsBannerSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "Float");
          return this;
      }
      public CmsBannerSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public CmsBannerSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public CmsBannerSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public CmsBannerSQL andUrlIsNull() {
		isnull("url");
		return this;
	}
	
	public CmsBannerSQL andUrlIsNotNull() {
		notNull("url");
		return this;
	}
	
	public CmsBannerSQL andUrlIsEmpty() {
		empty("url");
		return this;
	}

	public CmsBannerSQL andUrlIsNotEmpty() {
		notEmpty("url");
		return this;
	}
       public CmsBannerSQL andUrlLike(java.lang.String value) {
    	   addCriterion("url", value, ConditionMode.FUZZY, "url", "java.lang.String", "String");
    	   return this;
      }

      public CmsBannerSQL andUrlNotLike(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_FUZZY, "url", "java.lang.String", "String");
          return this;
      }
      public CmsBannerSQL andUrlEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andUrlNotEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andUrlGreaterThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andUrlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andUrlLessThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andUrlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andUrlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("url", value1, value2, ConditionMode.BETWEEN, "url", "java.lang.String", "String");
    	  return this;
      }

      public CmsBannerSQL andUrlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("url", value1, value2, ConditionMode.NOT_BETWEEN, "url", "java.lang.String", "String");
          return this;
      }
        
      public CmsBannerSQL andUrlIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.IN, "url", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andUrlNotIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.NOT_IN, "url", "java.lang.String", "String");
          return this;
      }
	public CmsBannerSQL andTargetIsNull() {
		isnull("target");
		return this;
	}
	
	public CmsBannerSQL andTargetIsNotNull() {
		notNull("target");
		return this;
	}
	
	public CmsBannerSQL andTargetIsEmpty() {
		empty("target");
		return this;
	}

	public CmsBannerSQL andTargetIsNotEmpty() {
		notEmpty("target");
		return this;
	}
      public CmsBannerSQL andTargetEqualTo(java.lang.Integer value) {
          addCriterion("target", value, ConditionMode.EQUAL, "target", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andTargetNotEqualTo(java.lang.Integer value) {
          addCriterion("target", value, ConditionMode.NOT_EQUAL, "target", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andTargetGreaterThan(java.lang.Integer value) {
          addCriterion("target", value, ConditionMode.GREATER_THEN, "target", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andTargetGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("target", value, ConditionMode.GREATER_EQUAL, "target", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andTargetLessThan(java.lang.Integer value) {
          addCriterion("target", value, ConditionMode.LESS_THEN, "target", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andTargetLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("target", value, ConditionMode.LESS_EQUAL, "target", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andTargetBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("target", value1, value2, ConditionMode.BETWEEN, "target", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsBannerSQL andTargetNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("target", value1, value2, ConditionMode.NOT_BETWEEN, "target", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsBannerSQL andTargetIn(List<java.lang.Integer> values) {
          addCriterion("target", values, ConditionMode.IN, "target", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andTargetNotIn(List<java.lang.Integer> values) {
          addCriterion("target", values, ConditionMode.NOT_IN, "target", "java.lang.Integer", "Float");
          return this;
      }
	public CmsBannerSQL andGroupIdIsNull() {
		isnull("group_id");
		return this;
	}
	
	public CmsBannerSQL andGroupIdIsNotNull() {
		notNull("group_id");
		return this;
	}
	
	public CmsBannerSQL andGroupIdIsEmpty() {
		empty("group_id");
		return this;
	}

	public CmsBannerSQL andGroupIdIsNotEmpty() {
		notEmpty("group_id");
		return this;
	}
      public CmsBannerSQL andGroupIdEqualTo(java.lang.Integer value) {
          addCriterion("group_id", value, ConditionMode.EQUAL, "groupId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andGroupIdNotEqualTo(java.lang.Integer value) {
          addCriterion("group_id", value, ConditionMode.NOT_EQUAL, "groupId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andGroupIdGreaterThan(java.lang.Integer value) {
          addCriterion("group_id", value, ConditionMode.GREATER_THEN, "groupId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andGroupIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("group_id", value, ConditionMode.GREATER_EQUAL, "groupId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andGroupIdLessThan(java.lang.Integer value) {
          addCriterion("group_id", value, ConditionMode.LESS_THEN, "groupId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andGroupIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("group_id", value, ConditionMode.LESS_EQUAL, "groupId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andGroupIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("group_id", value1, value2, ConditionMode.BETWEEN, "groupId", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsBannerSQL andGroupIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("group_id", value1, value2, ConditionMode.NOT_BETWEEN, "groupId", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsBannerSQL andGroupIdIn(List<java.lang.Integer> values) {
          addCriterion("group_id", values, ConditionMode.IN, "groupId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andGroupIdNotIn(List<java.lang.Integer> values) {
          addCriterion("group_id", values, ConditionMode.NOT_IN, "groupId", "java.lang.Integer", "Float");
          return this;
      }
	public CmsBannerSQL andPicIsNull() {
		isnull("pic");
		return this;
	}
	
	public CmsBannerSQL andPicIsNotNull() {
		notNull("pic");
		return this;
	}
	
	public CmsBannerSQL andPicIsEmpty() {
		empty("pic");
		return this;
	}

	public CmsBannerSQL andPicIsNotEmpty() {
		notEmpty("pic");
		return this;
	}
       public CmsBannerSQL andPicLike(java.lang.String value) {
    	   addCriterion("pic", value, ConditionMode.FUZZY, "pic", "java.lang.String", "Float");
    	   return this;
      }

      public CmsBannerSQL andPicNotLike(java.lang.String value) {
          addCriterion("pic", value, ConditionMode.NOT_FUZZY, "pic", "java.lang.String", "Float");
          return this;
      }
      public CmsBannerSQL andPicEqualTo(java.lang.String value) {
          addCriterion("pic", value, ConditionMode.EQUAL, "pic", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andPicNotEqualTo(java.lang.String value) {
          addCriterion("pic", value, ConditionMode.NOT_EQUAL, "pic", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andPicGreaterThan(java.lang.String value) {
          addCriterion("pic", value, ConditionMode.GREATER_THEN, "pic", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andPicGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("pic", value, ConditionMode.GREATER_EQUAL, "pic", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andPicLessThan(java.lang.String value) {
          addCriterion("pic", value, ConditionMode.LESS_THEN, "pic", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andPicLessThanOrEqualTo(java.lang.String value) {
          addCriterion("pic", value, ConditionMode.LESS_EQUAL, "pic", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andPicBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("pic", value1, value2, ConditionMode.BETWEEN, "pic", "java.lang.String", "String");
    	  return this;
      }

      public CmsBannerSQL andPicNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("pic", value1, value2, ConditionMode.NOT_BETWEEN, "pic", "java.lang.String", "String");
          return this;
      }
        
      public CmsBannerSQL andPicIn(List<java.lang.String> values) {
          addCriterion("pic", values, ConditionMode.IN, "pic", "java.lang.String", "String");
          return this;
      }

      public CmsBannerSQL andPicNotIn(List<java.lang.String> values) {
          addCriterion("pic", values, ConditionMode.NOT_IN, "pic", "java.lang.String", "String");
          return this;
      }
	public CmsBannerSQL andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public CmsBannerSQL andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public CmsBannerSQL andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public CmsBannerSQL andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
      public CmsBannerSQL andOrdersEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andOrdersNotEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andOrdersGreaterThan(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andOrdersGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andOrdersLessThan(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andOrdersLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andOrdersBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsBannerSQL andOrdersNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsBannerSQL andOrdersIn(List<java.lang.Integer> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andOrdersNotIn(List<java.lang.Integer> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.lang.Integer", "Float");
          return this;
      }
	public CmsBannerSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public CmsBannerSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public CmsBannerSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public CmsBannerSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public CmsBannerSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsBannerSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsBannerSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsBannerSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
}