/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsDictSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsDictSQL of() {
		return new CmsDictSQL();
	}
	
	public static CmsDictSQL by(Column column) {
		CmsDictSQL that = new CmsDictSQL();
		that.add(column);
        return that;
    }

    public static CmsDictSQL by(String name, Object value) {
        return (CmsDictSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_dict", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsDictSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsDictSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsDictSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsDictSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsDictSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsDictSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsDictSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsDictSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsDictSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsDictSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsDictSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsDictSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsDictSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsDictSQL andDictIdIsNull() {
		isnull("dict_id");
		return this;
	}
	
	public CmsDictSQL andDictIdIsNotNull() {
		notNull("dict_id");
		return this;
	}
	
	public CmsDictSQL andDictIdIsEmpty() {
		empty("dict_id");
		return this;
	}

	public CmsDictSQL andDictIdIsNotEmpty() {
		notEmpty("dict_id");
		return this;
	}
      public CmsDictSQL andDictIdEqualTo(java.lang.Long value) {
          addCriterion("dict_id", value, ConditionMode.EQUAL, "dictId", "java.lang.Long", "Float");
          return this;
      }

      public CmsDictSQL andDictIdNotEqualTo(java.lang.Long value) {
          addCriterion("dict_id", value, ConditionMode.NOT_EQUAL, "dictId", "java.lang.Long", "Float");
          return this;
      }

      public CmsDictSQL andDictIdGreaterThan(java.lang.Long value) {
          addCriterion("dict_id", value, ConditionMode.GREATER_THEN, "dictId", "java.lang.Long", "Float");
          return this;
      }

      public CmsDictSQL andDictIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("dict_id", value, ConditionMode.GREATER_EQUAL, "dictId", "java.lang.Long", "Float");
          return this;
      }

      public CmsDictSQL andDictIdLessThan(java.lang.Long value) {
          addCriterion("dict_id", value, ConditionMode.LESS_THEN, "dictId", "java.lang.Long", "Float");
          return this;
      }

      public CmsDictSQL andDictIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("dict_id", value, ConditionMode.LESS_EQUAL, "dictId", "java.lang.Long", "Float");
          return this;
      }

      public CmsDictSQL andDictIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("dict_id", value1, value2, ConditionMode.BETWEEN, "dictId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsDictSQL andDictIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("dict_id", value1, value2, ConditionMode.NOT_BETWEEN, "dictId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsDictSQL andDictIdIn(List<java.lang.Long> values) {
          addCriterion("dict_id", values, ConditionMode.IN, "dictId", "java.lang.Long", "Float");
          return this;
      }

      public CmsDictSQL andDictIdNotIn(List<java.lang.Long> values) {
          addCriterion("dict_id", values, ConditionMode.NOT_IN, "dictId", "java.lang.Long", "Float");
          return this;
      }
	public CmsDictSQL andDictKeyIsNull() {
		isnull("dict_key");
		return this;
	}
	
	public CmsDictSQL andDictKeyIsNotNull() {
		notNull("dict_key");
		return this;
	}
	
	public CmsDictSQL andDictKeyIsEmpty() {
		empty("dict_key");
		return this;
	}

	public CmsDictSQL andDictKeyIsNotEmpty() {
		notEmpty("dict_key");
		return this;
	}
       public CmsDictSQL andDictKeyLike(java.lang.String value) {
    	   addCriterion("dict_key", value, ConditionMode.FUZZY, "dictKey", "java.lang.String", "Float");
    	   return this;
      }

      public CmsDictSQL andDictKeyNotLike(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.NOT_FUZZY, "dictKey", "java.lang.String", "Float");
          return this;
      }
      public CmsDictSQL andDictKeyEqualTo(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.EQUAL, "dictKey", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictKeyNotEqualTo(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.NOT_EQUAL, "dictKey", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictKeyGreaterThan(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.GREATER_THEN, "dictKey", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictKeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.GREATER_EQUAL, "dictKey", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictKeyLessThan(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.LESS_THEN, "dictKey", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictKeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.LESS_EQUAL, "dictKey", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictKeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("dict_key", value1, value2, ConditionMode.BETWEEN, "dictKey", "java.lang.String", "String");
    	  return this;
      }

      public CmsDictSQL andDictKeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("dict_key", value1, value2, ConditionMode.NOT_BETWEEN, "dictKey", "java.lang.String", "String");
          return this;
      }
        
      public CmsDictSQL andDictKeyIn(List<java.lang.String> values) {
          addCriterion("dict_key", values, ConditionMode.IN, "dictKey", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictKeyNotIn(List<java.lang.String> values) {
          addCriterion("dict_key", values, ConditionMode.NOT_IN, "dictKey", "java.lang.String", "String");
          return this;
      }
	public CmsDictSQL andDictValueIsNull() {
		isnull("dict_value");
		return this;
	}
	
	public CmsDictSQL andDictValueIsNotNull() {
		notNull("dict_value");
		return this;
	}
	
	public CmsDictSQL andDictValueIsEmpty() {
		empty("dict_value");
		return this;
	}

	public CmsDictSQL andDictValueIsNotEmpty() {
		notEmpty("dict_value");
		return this;
	}
       public CmsDictSQL andDictValueLike(java.lang.String value) {
    	   addCriterion("dict_value", value, ConditionMode.FUZZY, "dictValue", "java.lang.String", "String");
    	   return this;
      }

      public CmsDictSQL andDictValueNotLike(java.lang.String value) {
          addCriterion("dict_value", value, ConditionMode.NOT_FUZZY, "dictValue", "java.lang.String", "String");
          return this;
      }
      public CmsDictSQL andDictValueEqualTo(java.lang.String value) {
          addCriterion("dict_value", value, ConditionMode.EQUAL, "dictValue", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictValueNotEqualTo(java.lang.String value) {
          addCriterion("dict_value", value, ConditionMode.NOT_EQUAL, "dictValue", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictValueGreaterThan(java.lang.String value) {
          addCriterion("dict_value", value, ConditionMode.GREATER_THEN, "dictValue", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictValueGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("dict_value", value, ConditionMode.GREATER_EQUAL, "dictValue", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictValueLessThan(java.lang.String value) {
          addCriterion("dict_value", value, ConditionMode.LESS_THEN, "dictValue", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictValueLessThanOrEqualTo(java.lang.String value) {
          addCriterion("dict_value", value, ConditionMode.LESS_EQUAL, "dictValue", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictValueBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("dict_value", value1, value2, ConditionMode.BETWEEN, "dictValue", "java.lang.String", "String");
    	  return this;
      }

      public CmsDictSQL andDictValueNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("dict_value", value1, value2, ConditionMode.NOT_BETWEEN, "dictValue", "java.lang.String", "String");
          return this;
      }
        
      public CmsDictSQL andDictValueIn(List<java.lang.String> values) {
          addCriterion("dict_value", values, ConditionMode.IN, "dictValue", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictValueNotIn(List<java.lang.String> values) {
          addCriterion("dict_value", values, ConditionMode.NOT_IN, "dictValue", "java.lang.String", "String");
          return this;
      }
	public CmsDictSQL andDictTitleIsNull() {
		isnull("dict_title");
		return this;
	}
	
	public CmsDictSQL andDictTitleIsNotNull() {
		notNull("dict_title");
		return this;
	}
	
	public CmsDictSQL andDictTitleIsEmpty() {
		empty("dict_title");
		return this;
	}

	public CmsDictSQL andDictTitleIsNotEmpty() {
		notEmpty("dict_title");
		return this;
	}
       public CmsDictSQL andDictTitleLike(java.lang.String value) {
    	   addCriterion("dict_title", value, ConditionMode.FUZZY, "dictTitle", "java.lang.String", "String");
    	   return this;
      }

      public CmsDictSQL andDictTitleNotLike(java.lang.String value) {
          addCriterion("dict_title", value, ConditionMode.NOT_FUZZY, "dictTitle", "java.lang.String", "String");
          return this;
      }
      public CmsDictSQL andDictTitleEqualTo(java.lang.String value) {
          addCriterion("dict_title", value, ConditionMode.EQUAL, "dictTitle", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictTitleNotEqualTo(java.lang.String value) {
          addCriterion("dict_title", value, ConditionMode.NOT_EQUAL, "dictTitle", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictTitleGreaterThan(java.lang.String value) {
          addCriterion("dict_title", value, ConditionMode.GREATER_THEN, "dictTitle", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("dict_title", value, ConditionMode.GREATER_EQUAL, "dictTitle", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictTitleLessThan(java.lang.String value) {
          addCriterion("dict_title", value, ConditionMode.LESS_THEN, "dictTitle", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("dict_title", value, ConditionMode.LESS_EQUAL, "dictTitle", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("dict_title", value1, value2, ConditionMode.BETWEEN, "dictTitle", "java.lang.String", "String");
    	  return this;
      }

      public CmsDictSQL andDictTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("dict_title", value1, value2, ConditionMode.NOT_BETWEEN, "dictTitle", "java.lang.String", "String");
          return this;
      }
        
      public CmsDictSQL andDictTitleIn(List<java.lang.String> values) {
          addCriterion("dict_title", values, ConditionMode.IN, "dictTitle", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andDictTitleNotIn(List<java.lang.String> values) {
          addCriterion("dict_title", values, ConditionMode.NOT_IN, "dictTitle", "java.lang.String", "String");
          return this;
      }
	public CmsDictSQL andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public CmsDictSQL andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public CmsDictSQL andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public CmsDictSQL andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
      public CmsDictSQL andOrdersEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andOrdersNotEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andOrdersGreaterThan(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andOrdersGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andOrdersLessThan(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andOrdersLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andOrdersBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsDictSQL andOrdersNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsDictSQL andOrdersIn(List<java.lang.Integer> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andOrdersNotIn(List<java.lang.Integer> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.lang.Integer", "Float");
          return this;
      }
	public CmsDictSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public CmsDictSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public CmsDictSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public CmsDictSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public CmsDictSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsDictSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsDictSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public CmsDictSQL andDictDefaultIsNull() {
		isnull("dict_default");
		return this;
	}
	
	public CmsDictSQL andDictDefaultIsNotNull() {
		notNull("dict_default");
		return this;
	}
	
	public CmsDictSQL andDictDefaultIsEmpty() {
		empty("dict_default");
		return this;
	}

	public CmsDictSQL andDictDefaultIsNotEmpty() {
		notEmpty("dict_default");
		return this;
	}
      public CmsDictSQL andDictDefaultEqualTo(java.lang.Integer value) {
          addCriterion("dict_default", value, ConditionMode.EQUAL, "dictDefault", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andDictDefaultNotEqualTo(java.lang.Integer value) {
          addCriterion("dict_default", value, ConditionMode.NOT_EQUAL, "dictDefault", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andDictDefaultGreaterThan(java.lang.Integer value) {
          addCriterion("dict_default", value, ConditionMode.GREATER_THEN, "dictDefault", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andDictDefaultGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("dict_default", value, ConditionMode.GREATER_EQUAL, "dictDefault", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andDictDefaultLessThan(java.lang.Integer value) {
          addCriterion("dict_default", value, ConditionMode.LESS_THEN, "dictDefault", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andDictDefaultLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("dict_default", value, ConditionMode.LESS_EQUAL, "dictDefault", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andDictDefaultBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("dict_default", value1, value2, ConditionMode.BETWEEN, "dictDefault", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsDictSQL andDictDefaultNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("dict_default", value1, value2, ConditionMode.NOT_BETWEEN, "dictDefault", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsDictSQL andDictDefaultIn(List<java.lang.Integer> values) {
          addCriterion("dict_default", values, ConditionMode.IN, "dictDefault", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictSQL andDictDefaultNotIn(List<java.lang.Integer> values) {
          addCriterion("dict_default", values, ConditionMode.NOT_IN, "dictDefault", "java.lang.Integer", "Float");
          return this;
      }
	public CmsDictSQL andViewStyleIsNull() {
		isnull("view_style");
		return this;
	}
	
	public CmsDictSQL andViewStyleIsNotNull() {
		notNull("view_style");
		return this;
	}
	
	public CmsDictSQL andViewStyleIsEmpty() {
		empty("view_style");
		return this;
	}

	public CmsDictSQL andViewStyleIsNotEmpty() {
		notEmpty("view_style");
		return this;
	}
       public CmsDictSQL andViewStyleLike(java.lang.String value) {
    	   addCriterion("view_style", value, ConditionMode.FUZZY, "viewStyle", "java.lang.String", "Float");
    	   return this;
      }

      public CmsDictSQL andViewStyleNotLike(java.lang.String value) {
          addCriterion("view_style", value, ConditionMode.NOT_FUZZY, "viewStyle", "java.lang.String", "Float");
          return this;
      }
      public CmsDictSQL andViewStyleEqualTo(java.lang.String value) {
          addCriterion("view_style", value, ConditionMode.EQUAL, "viewStyle", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andViewStyleNotEqualTo(java.lang.String value) {
          addCriterion("view_style", value, ConditionMode.NOT_EQUAL, "viewStyle", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andViewStyleGreaterThan(java.lang.String value) {
          addCriterion("view_style", value, ConditionMode.GREATER_THEN, "viewStyle", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andViewStyleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("view_style", value, ConditionMode.GREATER_EQUAL, "viewStyle", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andViewStyleLessThan(java.lang.String value) {
          addCriterion("view_style", value, ConditionMode.LESS_THEN, "viewStyle", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andViewStyleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("view_style", value, ConditionMode.LESS_EQUAL, "viewStyle", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andViewStyleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("view_style", value1, value2, ConditionMode.BETWEEN, "viewStyle", "java.lang.String", "String");
    	  return this;
      }

      public CmsDictSQL andViewStyleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("view_style", value1, value2, ConditionMode.NOT_BETWEEN, "viewStyle", "java.lang.String", "String");
          return this;
      }
        
      public CmsDictSQL andViewStyleIn(List<java.lang.String> values) {
          addCriterion("view_style", values, ConditionMode.IN, "viewStyle", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andViewStyleNotIn(List<java.lang.String> values) {
          addCriterion("view_style", values, ConditionMode.NOT_IN, "viewStyle", "java.lang.String", "String");
          return this;
      }
	public CmsDictSQL andViewTypeIsNull() {
		isnull("view_type");
		return this;
	}
	
	public CmsDictSQL andViewTypeIsNotNull() {
		notNull("view_type");
		return this;
	}
	
	public CmsDictSQL andViewTypeIsEmpty() {
		empty("view_type");
		return this;
	}

	public CmsDictSQL andViewTypeIsNotEmpty() {
		notEmpty("view_type");
		return this;
	}
       public CmsDictSQL andViewTypeLike(java.lang.String value) {
    	   addCriterion("view_type", value, ConditionMode.FUZZY, "viewType", "java.lang.String", "String");
    	   return this;
      }

      public CmsDictSQL andViewTypeNotLike(java.lang.String value) {
          addCriterion("view_type", value, ConditionMode.NOT_FUZZY, "viewType", "java.lang.String", "String");
          return this;
      }
      public CmsDictSQL andViewTypeEqualTo(java.lang.String value) {
          addCriterion("view_type", value, ConditionMode.EQUAL, "viewType", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andViewTypeNotEqualTo(java.lang.String value) {
          addCriterion("view_type", value, ConditionMode.NOT_EQUAL, "viewType", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andViewTypeGreaterThan(java.lang.String value) {
          addCriterion("view_type", value, ConditionMode.GREATER_THEN, "viewType", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andViewTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("view_type", value, ConditionMode.GREATER_EQUAL, "viewType", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andViewTypeLessThan(java.lang.String value) {
          addCriterion("view_type", value, ConditionMode.LESS_THEN, "viewType", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andViewTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("view_type", value, ConditionMode.LESS_EQUAL, "viewType", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andViewTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("view_type", value1, value2, ConditionMode.BETWEEN, "viewType", "java.lang.String", "String");
    	  return this;
      }

      public CmsDictSQL andViewTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("view_type", value1, value2, ConditionMode.NOT_BETWEEN, "viewType", "java.lang.String", "String");
          return this;
      }
        
      public CmsDictSQL andViewTypeIn(List<java.lang.String> values) {
          addCriterion("view_type", values, ConditionMode.IN, "viewType", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andViewTypeNotIn(List<java.lang.String> values) {
          addCriterion("view_type", values, ConditionMode.NOT_IN, "viewType", "java.lang.String", "String");
          return this;
      }
	public CmsDictSQL andRemarkIsNull() {
		isnull("remark");
		return this;
	}
	
	public CmsDictSQL andRemarkIsNotNull() {
		notNull("remark");
		return this;
	}
	
	public CmsDictSQL andRemarkIsEmpty() {
		empty("remark");
		return this;
	}

	public CmsDictSQL andRemarkIsNotEmpty() {
		notEmpty("remark");
		return this;
	}
       public CmsDictSQL andRemarkLike(java.lang.String value) {
    	   addCriterion("remark", value, ConditionMode.FUZZY, "remark", "java.lang.String", "String");
    	   return this;
      }

      public CmsDictSQL andRemarkNotLike(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.NOT_FUZZY, "remark", "java.lang.String", "String");
          return this;
      }
      public CmsDictSQL andRemarkEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andRemarkNotEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.NOT_EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andRemarkGreaterThan(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.GREATER_THEN, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andRemarkGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.GREATER_EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andRemarkLessThan(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.LESS_THEN, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andRemarkLessThanOrEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.LESS_EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andRemarkBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("remark", value1, value2, ConditionMode.BETWEEN, "remark", "java.lang.String", "String");
    	  return this;
      }

      public CmsDictSQL andRemarkNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("remark", value1, value2, ConditionMode.NOT_BETWEEN, "remark", "java.lang.String", "String");
          return this;
      }
        
      public CmsDictSQL andRemarkIn(List<java.lang.String> values) {
          addCriterion("remark", values, ConditionMode.IN, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsDictSQL andRemarkNotIn(List<java.lang.String> values) {
          addCriterion("remark", values, ConditionMode.NOT_IN, "remark", "java.lang.String", "String");
          return this;
      }
	public CmsDictSQL andCreatedIsNull() {
		isnull("created");
		return this;
	}
	
	public CmsDictSQL andCreatedIsNotNull() {
		notNull("created");
		return this;
	}
	
	public CmsDictSQL andCreatedIsEmpty() {
		empty("created");
		return this;
	}

	public CmsDictSQL andCreatedIsNotEmpty() {
		notEmpty("created");
		return this;
	}
      public CmsDictSQL andCreatedEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsDictSQL andCreatedNotEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.NOT_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsDictSQL andCreatedGreaterThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public CmsDictSQL andCreatedGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsDictSQL andCreatedLessThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public CmsDictSQL andCreatedLessThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsDictSQL andCreatedBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("created", value1, value2, ConditionMode.BETWEEN, "created", "java.util.Date", "String");
    	  return this;
      }

      public CmsDictSQL andCreatedNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("created", value1, value2, ConditionMode.NOT_BETWEEN, "created", "java.util.Date", "String");
          return this;
      }
        
      public CmsDictSQL andCreatedIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.IN, "created", "java.util.Date", "String");
          return this;
      }

      public CmsDictSQL andCreatedNotIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.NOT_IN, "created", "java.util.Date", "String");
          return this;
      }
}