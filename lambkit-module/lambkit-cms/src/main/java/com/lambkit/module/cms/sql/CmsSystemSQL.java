/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsSystemSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsSystemSQL of() {
		return new CmsSystemSQL();
	}
	
	public static CmsSystemSQL by(Column column) {
		CmsSystemSQL that = new CmsSystemSQL();
		that.add(column);
        return that;
    }

    public static CmsSystemSQL by(String name, Object value) {
        return (CmsSystemSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_system", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSystemSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSystemSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsSystemSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsSystemSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSystemSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSystemSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSystemSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSystemSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsSystemSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsSystemSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsSystemSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsSystemSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsSystemSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsSystemSQL andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public CmsSystemSQL andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public CmsSystemSQL andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public CmsSystemSQL andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
      public CmsSystemSQL andSystemIdEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSystemSQL andSystemIdNotEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSystemSQL andSystemIdGreaterThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSystemSQL andSystemIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSystemSQL andSystemIdLessThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSystemSQL andSystemIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSystemSQL andSystemIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsSystemSQL andSystemIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsSystemSQL andSystemIdIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSystemSQL andSystemIdNotIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
	public CmsSystemSQL andIconIsNull() {
		isnull("icon");
		return this;
	}
	
	public CmsSystemSQL andIconIsNotNull() {
		notNull("icon");
		return this;
	}
	
	public CmsSystemSQL andIconIsEmpty() {
		empty("icon");
		return this;
	}

	public CmsSystemSQL andIconIsNotEmpty() {
		notEmpty("icon");
		return this;
	}
       public CmsSystemSQL andIconLike(java.lang.String value) {
    	   addCriterion("icon", value, ConditionMode.FUZZY, "icon", "java.lang.String", "Float");
    	   return this;
      }

      public CmsSystemSQL andIconNotLike(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.NOT_FUZZY, "icon", "java.lang.String", "Float");
          return this;
      }
      public CmsSystemSQL andIconEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andIconNotEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.NOT_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andIconGreaterThan(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.GREATER_THEN, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andIconGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.GREATER_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andIconLessThan(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.LESS_THEN, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andIconLessThanOrEqualTo(java.lang.String value) {
          addCriterion("icon", value, ConditionMode.LESS_EQUAL, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andIconBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("icon", value1, value2, ConditionMode.BETWEEN, "icon", "java.lang.String", "String");
    	  return this;
      }

      public CmsSystemSQL andIconNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("icon", value1, value2, ConditionMode.NOT_BETWEEN, "icon", "java.lang.String", "String");
          return this;
      }
        
      public CmsSystemSQL andIconIn(List<java.lang.String> values) {
          addCriterion("icon", values, ConditionMode.IN, "icon", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andIconNotIn(List<java.lang.String> values) {
          addCriterion("icon", values, ConditionMode.NOT_IN, "icon", "java.lang.String", "String");
          return this;
      }
	public CmsSystemSQL andBannerIsNull() {
		isnull("banner");
		return this;
	}
	
	public CmsSystemSQL andBannerIsNotNull() {
		notNull("banner");
		return this;
	}
	
	public CmsSystemSQL andBannerIsEmpty() {
		empty("banner");
		return this;
	}

	public CmsSystemSQL andBannerIsNotEmpty() {
		notEmpty("banner");
		return this;
	}
       public CmsSystemSQL andBannerLike(java.lang.String value) {
    	   addCriterion("banner", value, ConditionMode.FUZZY, "banner", "java.lang.String", "String");
    	   return this;
      }

      public CmsSystemSQL andBannerNotLike(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.NOT_FUZZY, "banner", "java.lang.String", "String");
          return this;
      }
      public CmsSystemSQL andBannerEqualTo(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.EQUAL, "banner", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andBannerNotEqualTo(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.NOT_EQUAL, "banner", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andBannerGreaterThan(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.GREATER_THEN, "banner", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andBannerGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.GREATER_EQUAL, "banner", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andBannerLessThan(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.LESS_THEN, "banner", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andBannerLessThanOrEqualTo(java.lang.String value) {
          addCriterion("banner", value, ConditionMode.LESS_EQUAL, "banner", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andBannerBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("banner", value1, value2, ConditionMode.BETWEEN, "banner", "java.lang.String", "String");
    	  return this;
      }

      public CmsSystemSQL andBannerNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("banner", value1, value2, ConditionMode.NOT_BETWEEN, "banner", "java.lang.String", "String");
          return this;
      }
        
      public CmsSystemSQL andBannerIn(List<java.lang.String> values) {
          addCriterion("banner", values, ConditionMode.IN, "banner", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andBannerNotIn(List<java.lang.String> values) {
          addCriterion("banner", values, ConditionMode.NOT_IN, "banner", "java.lang.String", "String");
          return this;
      }
	public CmsSystemSQL andThemeIsNull() {
		isnull("theme");
		return this;
	}
	
	public CmsSystemSQL andThemeIsNotNull() {
		notNull("theme");
		return this;
	}
	
	public CmsSystemSQL andThemeIsEmpty() {
		empty("theme");
		return this;
	}

	public CmsSystemSQL andThemeIsNotEmpty() {
		notEmpty("theme");
		return this;
	}
       public CmsSystemSQL andThemeLike(java.lang.String value) {
    	   addCriterion("theme", value, ConditionMode.FUZZY, "theme", "java.lang.String", "String");
    	   return this;
      }

      public CmsSystemSQL andThemeNotLike(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.NOT_FUZZY, "theme", "java.lang.String", "String");
          return this;
      }
      public CmsSystemSQL andThemeEqualTo(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.EQUAL, "theme", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andThemeNotEqualTo(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.NOT_EQUAL, "theme", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andThemeGreaterThan(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.GREATER_THEN, "theme", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andThemeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.GREATER_EQUAL, "theme", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andThemeLessThan(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.LESS_THEN, "theme", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andThemeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("theme", value, ConditionMode.LESS_EQUAL, "theme", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andThemeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("theme", value1, value2, ConditionMode.BETWEEN, "theme", "java.lang.String", "String");
    	  return this;
      }

      public CmsSystemSQL andThemeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("theme", value1, value2, ConditionMode.NOT_BETWEEN, "theme", "java.lang.String", "String");
          return this;
      }
        
      public CmsSystemSQL andThemeIn(List<java.lang.String> values) {
          addCriterion("theme", values, ConditionMode.IN, "theme", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andThemeNotIn(List<java.lang.String> values) {
          addCriterion("theme", values, ConditionMode.NOT_IN, "theme", "java.lang.String", "String");
          return this;
      }
	public CmsSystemSQL andBasepathIsNull() {
		isnull("basepath");
		return this;
	}
	
	public CmsSystemSQL andBasepathIsNotNull() {
		notNull("basepath");
		return this;
	}
	
	public CmsSystemSQL andBasepathIsEmpty() {
		empty("basepath");
		return this;
	}

	public CmsSystemSQL andBasepathIsNotEmpty() {
		notEmpty("basepath");
		return this;
	}
       public CmsSystemSQL andBasepathLike(java.lang.String value) {
    	   addCriterion("basepath", value, ConditionMode.FUZZY, "basepath", "java.lang.String", "String");
    	   return this;
      }

      public CmsSystemSQL andBasepathNotLike(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.NOT_FUZZY, "basepath", "java.lang.String", "String");
          return this;
      }
      public CmsSystemSQL andBasepathEqualTo(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.EQUAL, "basepath", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andBasepathNotEqualTo(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.NOT_EQUAL, "basepath", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andBasepathGreaterThan(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.GREATER_THEN, "basepath", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andBasepathGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.GREATER_EQUAL, "basepath", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andBasepathLessThan(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.LESS_THEN, "basepath", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andBasepathLessThanOrEqualTo(java.lang.String value) {
          addCriterion("basepath", value, ConditionMode.LESS_EQUAL, "basepath", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andBasepathBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("basepath", value1, value2, ConditionMode.BETWEEN, "basepath", "java.lang.String", "String");
    	  return this;
      }

      public CmsSystemSQL andBasepathNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("basepath", value1, value2, ConditionMode.NOT_BETWEEN, "basepath", "java.lang.String", "String");
          return this;
      }
        
      public CmsSystemSQL andBasepathIn(List<java.lang.String> values) {
          addCriterion("basepath", values, ConditionMode.IN, "basepath", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andBasepathNotIn(List<java.lang.String> values) {
          addCriterion("basepath", values, ConditionMode.NOT_IN, "basepath", "java.lang.String", "String");
          return this;
      }
	public CmsSystemSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public CmsSystemSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public CmsSystemSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public CmsSystemSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public CmsSystemSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSystemSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSystemSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSystemSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSystemSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSystemSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSystemSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsSystemSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsSystemSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSystemSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public CmsSystemSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public CmsSystemSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public CmsSystemSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public CmsSystemSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public CmsSystemSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public CmsSystemSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public CmsSystemSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public CmsSystemSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public CmsSystemSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public CmsSystemSQL andCodeIsNull() {
		isnull("code");
		return this;
	}
	
	public CmsSystemSQL andCodeIsNotNull() {
		notNull("code");
		return this;
	}
	
	public CmsSystemSQL andCodeIsEmpty() {
		empty("code");
		return this;
	}

	public CmsSystemSQL andCodeIsNotEmpty() {
		notEmpty("code");
		return this;
	}
       public CmsSystemSQL andCodeLike(java.lang.String value) {
    	   addCriterion("code", value, ConditionMode.FUZZY, "code", "java.lang.String", "String");
    	   return this;
      }

      public CmsSystemSQL andCodeNotLike(java.lang.String value) {
          addCriterion("code", value, ConditionMode.NOT_FUZZY, "code", "java.lang.String", "String");
          return this;
      }
      public CmsSystemSQL andCodeEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andCodeNotEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.NOT_EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andCodeGreaterThan(java.lang.String value) {
          addCriterion("code", value, ConditionMode.GREATER_THEN, "code", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.GREATER_EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andCodeLessThan(java.lang.String value) {
          addCriterion("code", value, ConditionMode.LESS_THEN, "code", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("code", value, ConditionMode.LESS_EQUAL, "code", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("code", value1, value2, ConditionMode.BETWEEN, "code", "java.lang.String", "String");
    	  return this;
      }

      public CmsSystemSQL andCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("code", value1, value2, ConditionMode.NOT_BETWEEN, "code", "java.lang.String", "String");
          return this;
      }
        
      public CmsSystemSQL andCodeIn(List<java.lang.String> values) {
          addCriterion("code", values, ConditionMode.IN, "code", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andCodeNotIn(List<java.lang.String> values) {
          addCriterion("code", values, ConditionMode.NOT_IN, "code", "java.lang.String", "String");
          return this;
      }
	public CmsSystemSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public CmsSystemSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public CmsSystemSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public CmsSystemSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public CmsSystemSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "String");
    	   return this;
      }

      public CmsSystemSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "String");
          return this;
      }
      public CmsSystemSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public CmsSystemSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public CmsSystemSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public CmsSystemSQL andDescriptionIsNull() {
		isnull("description");
		return this;
	}
	
	public CmsSystemSQL andDescriptionIsNotNull() {
		notNull("description");
		return this;
	}
	
	public CmsSystemSQL andDescriptionIsEmpty() {
		empty("description");
		return this;
	}

	public CmsSystemSQL andDescriptionIsNotEmpty() {
		notEmpty("description");
		return this;
	}
       public CmsSystemSQL andDescriptionLike(java.lang.String value) {
    	   addCriterion("description", value, ConditionMode.FUZZY, "description", "java.lang.String", "String");
    	   return this;
      }

      public CmsSystemSQL andDescriptionNotLike(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_FUZZY, "description", "java.lang.String", "String");
          return this;
      }
      public CmsSystemSQL andDescriptionEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andDescriptionNotEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.NOT_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andDescriptionGreaterThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andDescriptionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.GREATER_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andDescriptionLessThan(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_THEN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andDescriptionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("description", value, ConditionMode.LESS_EQUAL, "description", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andDescriptionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("description", value1, value2, ConditionMode.BETWEEN, "description", "java.lang.String", "String");
    	  return this;
      }

      public CmsSystemSQL andDescriptionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("description", value1, value2, ConditionMode.NOT_BETWEEN, "description", "java.lang.String", "String");
          return this;
      }
        
      public CmsSystemSQL andDescriptionIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.IN, "description", "java.lang.String", "String");
          return this;
      }

      public CmsSystemSQL andDescriptionNotIn(List<java.lang.String> values) {
          addCriterion("description", values, ConditionMode.NOT_IN, "description", "java.lang.String", "String");
          return this;
      }
	public CmsSystemSQL andCtimeIsNull() {
		isnull("ctime");
		return this;
	}
	
	public CmsSystemSQL andCtimeIsNotNull() {
		notNull("ctime");
		return this;
	}
	
	public CmsSystemSQL andCtimeIsEmpty() {
		empty("ctime");
		return this;
	}

	public CmsSystemSQL andCtimeIsNotEmpty() {
		notEmpty("ctime");
		return this;
	}
      public CmsSystemSQL andCtimeEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsSystemSQL andCtimeNotEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.NOT_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsSystemSQL andCtimeGreaterThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsSystemSQL andCtimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.GREATER_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsSystemSQL andCtimeLessThan(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_THEN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsSystemSQL andCtimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("ctime", value, ConditionMode.LESS_EQUAL, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsSystemSQL andCtimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("ctime", value1, value2, ConditionMode.BETWEEN, "ctime", "java.util.Date", "String");
    	  return this;
      }

      public CmsSystemSQL andCtimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("ctime", value1, value2, ConditionMode.NOT_BETWEEN, "ctime", "java.util.Date", "String");
          return this;
      }
        
      public CmsSystemSQL andCtimeIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.IN, "ctime", "java.util.Date", "String");
          return this;
      }

      public CmsSystemSQL andCtimeNotIn(List<java.util.Date> values) {
          addCriterion("ctime", values, ConditionMode.NOT_IN, "ctime", "java.util.Date", "String");
          return this;
      }
	public CmsSystemSQL andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public CmsSystemSQL andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public CmsSystemSQL andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public CmsSystemSQL andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
      public CmsSystemSQL andOrdersEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemSQL andOrdersNotEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemSQL andOrdersGreaterThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemSQL andOrdersGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemSQL andOrdersLessThan(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemSQL andOrdersLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemSQL andOrdersBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsSystemSQL andOrdersNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsSystemSQL andOrdersIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.lang.Long", "Float");
          return this;
      }

      public CmsSystemSQL andOrdersNotIn(List<java.lang.Long> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.lang.Long", "Float");
          return this;
      }
}