/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsAreaSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsAreaSQL of() {
		return new CmsAreaSQL();
	}
	
	public static CmsAreaSQL by(Column column) {
		CmsAreaSQL that = new CmsAreaSQL();
		that.add(column);
        return that;
    }

    public static CmsAreaSQL by(String name, Object value) {
        return (CmsAreaSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_area", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAreaSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAreaSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsAreaSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsAreaSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAreaSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAreaSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAreaSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAreaSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsAreaSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsAreaSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsAreaSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsAreaSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsAreaSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsAreaSQL andEAreaCodeIsNull() {
		isnull("e_area_code");
		return this;
	}
	
	public CmsAreaSQL andEAreaCodeIsNotNull() {
		notNull("e_area_code");
		return this;
	}
	
	public CmsAreaSQL andEAreaCodeIsEmpty() {
		empty("e_area_code");
		return this;
	}

	public CmsAreaSQL andEAreaCodeIsNotEmpty() {
		notEmpty("e_area_code");
		return this;
	}
       public CmsAreaSQL andEAreaCodeLike(java.lang.String value) {
    	   addCriterion("e_area_code", value, ConditionMode.FUZZY, "eAreaCode", "java.lang.String", "String");
    	   return this;
      }

      public CmsAreaSQL andEAreaCodeNotLike(java.lang.String value) {
          addCriterion("e_area_code", value, ConditionMode.NOT_FUZZY, "eAreaCode", "java.lang.String", "String");
          return this;
      }
      public CmsAreaSQL andEAreaCodeEqualTo(java.lang.String value) {
          addCriterion("e_area_code", value, ConditionMode.EQUAL, "eAreaCode", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEAreaCodeNotEqualTo(java.lang.String value) {
          addCriterion("e_area_code", value, ConditionMode.NOT_EQUAL, "eAreaCode", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEAreaCodeGreaterThan(java.lang.String value) {
          addCriterion("e_area_code", value, ConditionMode.GREATER_THEN, "eAreaCode", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEAreaCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("e_area_code", value, ConditionMode.GREATER_EQUAL, "eAreaCode", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEAreaCodeLessThan(java.lang.String value) {
          addCriterion("e_area_code", value, ConditionMode.LESS_THEN, "eAreaCode", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEAreaCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("e_area_code", value, ConditionMode.LESS_EQUAL, "eAreaCode", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEAreaCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("e_area_code", value1, value2, ConditionMode.BETWEEN, "eAreaCode", "java.lang.String", "String");
    	  return this;
      }

      public CmsAreaSQL andEAreaCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("e_area_code", value1, value2, ConditionMode.NOT_BETWEEN, "eAreaCode", "java.lang.String", "String");
          return this;
      }
        
      public CmsAreaSQL andEAreaCodeIn(List<java.lang.String> values) {
          addCriterion("e_area_code", values, ConditionMode.IN, "eAreaCode", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEAreaCodeNotIn(List<java.lang.String> values) {
          addCriterion("e_area_code", values, ConditionMode.NOT_IN, "eAreaCode", "java.lang.String", "String");
          return this;
      }
	public CmsAreaSQL andEAreaNameIsNull() {
		isnull("e_area_name");
		return this;
	}
	
	public CmsAreaSQL andEAreaNameIsNotNull() {
		notNull("e_area_name");
		return this;
	}
	
	public CmsAreaSQL andEAreaNameIsEmpty() {
		empty("e_area_name");
		return this;
	}

	public CmsAreaSQL andEAreaNameIsNotEmpty() {
		notEmpty("e_area_name");
		return this;
	}
       public CmsAreaSQL andEAreaNameLike(java.lang.String value) {
    	   addCriterion("e_area_name", value, ConditionMode.FUZZY, "eAreaName", "java.lang.String", "String");
    	   return this;
      }

      public CmsAreaSQL andEAreaNameNotLike(java.lang.String value) {
          addCriterion("e_area_name", value, ConditionMode.NOT_FUZZY, "eAreaName", "java.lang.String", "String");
          return this;
      }
      public CmsAreaSQL andEAreaNameEqualTo(java.lang.String value) {
          addCriterion("e_area_name", value, ConditionMode.EQUAL, "eAreaName", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEAreaNameNotEqualTo(java.lang.String value) {
          addCriterion("e_area_name", value, ConditionMode.NOT_EQUAL, "eAreaName", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEAreaNameGreaterThan(java.lang.String value) {
          addCriterion("e_area_name", value, ConditionMode.GREATER_THEN, "eAreaName", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEAreaNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("e_area_name", value, ConditionMode.GREATER_EQUAL, "eAreaName", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEAreaNameLessThan(java.lang.String value) {
          addCriterion("e_area_name", value, ConditionMode.LESS_THEN, "eAreaName", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEAreaNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("e_area_name", value, ConditionMode.LESS_EQUAL, "eAreaName", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEAreaNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("e_area_name", value1, value2, ConditionMode.BETWEEN, "eAreaName", "java.lang.String", "String");
    	  return this;
      }

      public CmsAreaSQL andEAreaNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("e_area_name", value1, value2, ConditionMode.NOT_BETWEEN, "eAreaName", "java.lang.String", "String");
          return this;
      }
        
      public CmsAreaSQL andEAreaNameIn(List<java.lang.String> values) {
          addCriterion("e_area_name", values, ConditionMode.IN, "eAreaName", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEAreaNameNotIn(List<java.lang.String> values) {
          addCriterion("e_area_name", values, ConditionMode.NOT_IN, "eAreaName", "java.lang.String", "String");
          return this;
      }
	public CmsAreaSQL andEPidIsNull() {
		isnull("e_pid");
		return this;
	}
	
	public CmsAreaSQL andEPidIsNotNull() {
		notNull("e_pid");
		return this;
	}
	
	public CmsAreaSQL andEPidIsEmpty() {
		empty("e_pid");
		return this;
	}

	public CmsAreaSQL andEPidIsNotEmpty() {
		notEmpty("e_pid");
		return this;
	}
       public CmsAreaSQL andEPidLike(java.lang.String value) {
    	   addCriterion("e_pid", value, ConditionMode.FUZZY, "ePid", "java.lang.String", "String");
    	   return this;
      }

      public CmsAreaSQL andEPidNotLike(java.lang.String value) {
          addCriterion("e_pid", value, ConditionMode.NOT_FUZZY, "ePid", "java.lang.String", "String");
          return this;
      }
      public CmsAreaSQL andEPidEqualTo(java.lang.String value) {
          addCriterion("e_pid", value, ConditionMode.EQUAL, "ePid", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEPidNotEqualTo(java.lang.String value) {
          addCriterion("e_pid", value, ConditionMode.NOT_EQUAL, "ePid", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEPidGreaterThan(java.lang.String value) {
          addCriterion("e_pid", value, ConditionMode.GREATER_THEN, "ePid", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEPidGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("e_pid", value, ConditionMode.GREATER_EQUAL, "ePid", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEPidLessThan(java.lang.String value) {
          addCriterion("e_pid", value, ConditionMode.LESS_THEN, "ePid", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEPidLessThanOrEqualTo(java.lang.String value) {
          addCriterion("e_pid", value, ConditionMode.LESS_EQUAL, "ePid", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEPidBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("e_pid", value1, value2, ConditionMode.BETWEEN, "ePid", "java.lang.String", "String");
    	  return this;
      }

      public CmsAreaSQL andEPidNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("e_pid", value1, value2, ConditionMode.NOT_BETWEEN, "ePid", "java.lang.String", "String");
          return this;
      }
        
      public CmsAreaSQL andEPidIn(List<java.lang.String> values) {
          addCriterion("e_pid", values, ConditionMode.IN, "ePid", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andEPidNotIn(List<java.lang.String> values) {
          addCriterion("e_pid", values, ConditionMode.NOT_IN, "ePid", "java.lang.String", "String");
          return this;
      }
	public CmsAreaSQL andEOrderIsNull() {
		isnull("e_order");
		return this;
	}
	
	public CmsAreaSQL andEOrderIsNotNull() {
		notNull("e_order");
		return this;
	}
	
	public CmsAreaSQL andEOrderIsEmpty() {
		empty("e_order");
		return this;
	}

	public CmsAreaSQL andEOrderIsNotEmpty() {
		notEmpty("e_order");
		return this;
	}
      public CmsAreaSQL andEOrderEqualTo(java.lang.Integer value) {
          addCriterion("e_order", value, ConditionMode.EQUAL, "eOrder", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAreaSQL andEOrderNotEqualTo(java.lang.Integer value) {
          addCriterion("e_order", value, ConditionMode.NOT_EQUAL, "eOrder", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAreaSQL andEOrderGreaterThan(java.lang.Integer value) {
          addCriterion("e_order", value, ConditionMode.GREATER_THEN, "eOrder", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAreaSQL andEOrderGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("e_order", value, ConditionMode.GREATER_EQUAL, "eOrder", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAreaSQL andEOrderLessThan(java.lang.Integer value) {
          addCriterion("e_order", value, ConditionMode.LESS_THEN, "eOrder", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAreaSQL andEOrderLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("e_order", value, ConditionMode.LESS_EQUAL, "eOrder", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAreaSQL andEOrderBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("e_order", value1, value2, ConditionMode.BETWEEN, "eOrder", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsAreaSQL andEOrderNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("e_order", value1, value2, ConditionMode.NOT_BETWEEN, "eOrder", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsAreaSQL andEOrderIn(List<java.lang.Integer> values) {
          addCriterion("e_order", values, ConditionMode.IN, "eOrder", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAreaSQL andEOrderNotIn(List<java.lang.Integer> values) {
          addCriterion("e_order", values, ConditionMode.NOT_IN, "eOrder", "java.lang.Integer", "Float");
          return this;
      }
	public CmsAreaSQL andEGradeIsNull() {
		isnull("e_grade");
		return this;
	}
	
	public CmsAreaSQL andEGradeIsNotNull() {
		notNull("e_grade");
		return this;
	}
	
	public CmsAreaSQL andEGradeIsEmpty() {
		empty("e_grade");
		return this;
	}

	public CmsAreaSQL andEGradeIsNotEmpty() {
		notEmpty("e_grade");
		return this;
	}
      public CmsAreaSQL andEGradeEqualTo(java.lang.Integer value) {
          addCriterion("e_grade", value, ConditionMode.EQUAL, "eGrade", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAreaSQL andEGradeNotEqualTo(java.lang.Integer value) {
          addCriterion("e_grade", value, ConditionMode.NOT_EQUAL, "eGrade", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAreaSQL andEGradeGreaterThan(java.lang.Integer value) {
          addCriterion("e_grade", value, ConditionMode.GREATER_THEN, "eGrade", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAreaSQL andEGradeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("e_grade", value, ConditionMode.GREATER_EQUAL, "eGrade", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAreaSQL andEGradeLessThan(java.lang.Integer value) {
          addCriterion("e_grade", value, ConditionMode.LESS_THEN, "eGrade", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAreaSQL andEGradeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("e_grade", value, ConditionMode.LESS_EQUAL, "eGrade", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAreaSQL andEGradeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("e_grade", value1, value2, ConditionMode.BETWEEN, "eGrade", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsAreaSQL andEGradeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("e_grade", value1, value2, ConditionMode.NOT_BETWEEN, "eGrade", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsAreaSQL andEGradeIn(List<java.lang.Integer> values) {
          addCriterion("e_grade", values, ConditionMode.IN, "eGrade", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAreaSQL andEGradeNotIn(List<java.lang.Integer> values) {
          addCriterion("e_grade", values, ConditionMode.NOT_IN, "eGrade", "java.lang.Integer", "Float");
          return this;
      }
	public CmsAreaSQL andRemarkIsNull() {
		isnull("remark");
		return this;
	}
	
	public CmsAreaSQL andRemarkIsNotNull() {
		notNull("remark");
		return this;
	}
	
	public CmsAreaSQL andRemarkIsEmpty() {
		empty("remark");
		return this;
	}

	public CmsAreaSQL andRemarkIsNotEmpty() {
		notEmpty("remark");
		return this;
	}
       public CmsAreaSQL andRemarkLike(java.lang.String value) {
    	   addCriterion("remark", value, ConditionMode.FUZZY, "remark", "java.lang.String", "Float");
    	   return this;
      }

      public CmsAreaSQL andRemarkNotLike(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.NOT_FUZZY, "remark", "java.lang.String", "Float");
          return this;
      }
      public CmsAreaSQL andRemarkEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andRemarkNotEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.NOT_EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andRemarkGreaterThan(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.GREATER_THEN, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andRemarkGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.GREATER_EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andRemarkLessThan(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.LESS_THEN, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andRemarkLessThanOrEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.LESS_EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andRemarkBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("remark", value1, value2, ConditionMode.BETWEEN, "remark", "java.lang.String", "String");
    	  return this;
      }

      public CmsAreaSQL andRemarkNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("remark", value1, value2, ConditionMode.NOT_BETWEEN, "remark", "java.lang.String", "String");
          return this;
      }
        
      public CmsAreaSQL andRemarkIn(List<java.lang.String> values) {
          addCriterion("remark", values, ConditionMode.IN, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsAreaSQL andRemarkNotIn(List<java.lang.String> values) {
          addCriterion("remark", values, ConditionMode.NOT_IN, "remark", "java.lang.String", "String");
          return this;
      }
}