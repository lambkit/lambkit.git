/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.service;

import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Lambkit;
import com.lambkit.db.DbPool;
import com.lambkit.db.IDaoService;
import com.lambkit.db.IRowDao;
import com.lambkit.module.cms.CmsConfig;
import com.lambkit.module.cms.row.CmsSetting;
import com.lambkit.module.cms.sql.CmsSettingSQL;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsSettingService implements IDaoService<CmsSetting> {
	public IRowDao<CmsSetting> dao() {
		String dbPoolName = Lambkit.config(CmsConfig.class).getDbPoolName();
		return DbPool.use(dbPoolName).dao(CmsSetting.class);
	}

	public IRowDao<CmsSetting> dao(String dbPoolName) {
		return DbPool.use(dbPoolName).dao(CmsSetting.class);
	}


	public CmsSetting findByKey(String key) {
		return dao().findFirst(CmsSettingSQL.of().andSettingKeyEqualTo(key).example());
	}

	public String get(String key) {
		CmsSetting setting = dao().findFirst(CmsSettingSQL.of().andSettingKeyEqualTo(key).example());
		return setting!=null ? setting.getSettingValue() : null;
	}

	public void remove(String key) {
		dao().delete(CmsSettingSQL.of().andSettingKeyEqualTo(key).example());
		Lambkit.configCenter().clearConfig();
	}

	public void put(String key, String value) {
		if(StrUtil.isBlank(key) || StrUtil.isBlank(value)) {
			return;
		}
		if(StrUtil.isBlank(value)) {
			remove(key);
			return;
		}
		CmsSetting setting = dao().findFirst(CmsSettingSQL.of().andSettingKeyEqualTo(key).example());
		if(setting==null) {
			setting = new CmsSetting();
		}
		setting.setSettingKey(key);
		setting.setSettingValue(value);
		dao().saveOrUpdate(setting);
	}
}
