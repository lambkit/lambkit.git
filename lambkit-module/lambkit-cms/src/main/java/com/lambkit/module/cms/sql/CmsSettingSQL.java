/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsSettingSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsSettingSQL of() {
		return new CmsSettingSQL();
	}
	
	public static CmsSettingSQL by(Column column) {
		CmsSettingSQL that = new CmsSettingSQL();
		that.add(column);
        return that;
    }

    public static CmsSettingSQL by(String name, Object value) {
        return (CmsSettingSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_setting", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSettingSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSettingSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsSettingSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsSettingSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSettingSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSettingSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSettingSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsSettingSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsSettingSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsSettingSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsSettingSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsSettingSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsSettingSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsSettingSQL andSettingIdIsNull() {
		isnull("setting_id");
		return this;
	}
	
	public CmsSettingSQL andSettingIdIsNotNull() {
		notNull("setting_id");
		return this;
	}
	
	public CmsSettingSQL andSettingIdIsEmpty() {
		empty("setting_id");
		return this;
	}

	public CmsSettingSQL andSettingIdIsNotEmpty() {
		notEmpty("setting_id");
		return this;
	}
      public CmsSettingSQL andSettingIdEqualTo(java.lang.Integer value) {
          addCriterion("setting_id", value, ConditionMode.EQUAL, "settingId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSettingSQL andSettingIdNotEqualTo(java.lang.Integer value) {
          addCriterion("setting_id", value, ConditionMode.NOT_EQUAL, "settingId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSettingSQL andSettingIdGreaterThan(java.lang.Integer value) {
          addCriterion("setting_id", value, ConditionMode.GREATER_THEN, "settingId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSettingSQL andSettingIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("setting_id", value, ConditionMode.GREATER_EQUAL, "settingId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSettingSQL andSettingIdLessThan(java.lang.Integer value) {
          addCriterion("setting_id", value, ConditionMode.LESS_THEN, "settingId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSettingSQL andSettingIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("setting_id", value, ConditionMode.LESS_EQUAL, "settingId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSettingSQL andSettingIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("setting_id", value1, value2, ConditionMode.BETWEEN, "settingId", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsSettingSQL andSettingIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("setting_id", value1, value2, ConditionMode.NOT_BETWEEN, "settingId", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsSettingSQL andSettingIdIn(List<java.lang.Integer> values) {
          addCriterion("setting_id", values, ConditionMode.IN, "settingId", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSettingSQL andSettingIdNotIn(List<java.lang.Integer> values) {
          addCriterion("setting_id", values, ConditionMode.NOT_IN, "settingId", "java.lang.Integer", "Float");
          return this;
      }
	public CmsSettingSQL andSettingKeyIsNull() {
		isnull("setting_key");
		return this;
	}
	
	public CmsSettingSQL andSettingKeyIsNotNull() {
		notNull("setting_key");
		return this;
	}
	
	public CmsSettingSQL andSettingKeyIsEmpty() {
		empty("setting_key");
		return this;
	}

	public CmsSettingSQL andSettingKeyIsNotEmpty() {
		notEmpty("setting_key");
		return this;
	}
       public CmsSettingSQL andSettingKeyLike(java.lang.String value) {
    	   addCriterion("setting_key", value, ConditionMode.FUZZY, "settingKey", "java.lang.String", "Float");
    	   return this;
      }

      public CmsSettingSQL andSettingKeyNotLike(java.lang.String value) {
          addCriterion("setting_key", value, ConditionMode.NOT_FUZZY, "settingKey", "java.lang.String", "Float");
          return this;
      }
      public CmsSettingSQL andSettingKeyEqualTo(java.lang.String value) {
          addCriterion("setting_key", value, ConditionMode.EQUAL, "settingKey", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingKeyNotEqualTo(java.lang.String value) {
          addCriterion("setting_key", value, ConditionMode.NOT_EQUAL, "settingKey", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingKeyGreaterThan(java.lang.String value) {
          addCriterion("setting_key", value, ConditionMode.GREATER_THEN, "settingKey", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingKeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("setting_key", value, ConditionMode.GREATER_EQUAL, "settingKey", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingKeyLessThan(java.lang.String value) {
          addCriterion("setting_key", value, ConditionMode.LESS_THEN, "settingKey", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingKeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("setting_key", value, ConditionMode.LESS_EQUAL, "settingKey", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingKeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("setting_key", value1, value2, ConditionMode.BETWEEN, "settingKey", "java.lang.String", "String");
    	  return this;
      }

      public CmsSettingSQL andSettingKeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("setting_key", value1, value2, ConditionMode.NOT_BETWEEN, "settingKey", "java.lang.String", "String");
          return this;
      }
        
      public CmsSettingSQL andSettingKeyIn(List<java.lang.String> values) {
          addCriterion("setting_key", values, ConditionMode.IN, "settingKey", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingKeyNotIn(List<java.lang.String> values) {
          addCriterion("setting_key", values, ConditionMode.NOT_IN, "settingKey", "java.lang.String", "String");
          return this;
      }
	public CmsSettingSQL andSettingValueIsNull() {
		isnull("setting_value");
		return this;
	}
	
	public CmsSettingSQL andSettingValueIsNotNull() {
		notNull("setting_value");
		return this;
	}
	
	public CmsSettingSQL andSettingValueIsEmpty() {
		empty("setting_value");
		return this;
	}

	public CmsSettingSQL andSettingValueIsNotEmpty() {
		notEmpty("setting_value");
		return this;
	}
       public CmsSettingSQL andSettingValueLike(java.lang.String value) {
    	   addCriterion("setting_value", value, ConditionMode.FUZZY, "settingValue", "java.lang.String", "String");
    	   return this;
      }

      public CmsSettingSQL andSettingValueNotLike(java.lang.String value) {
          addCriterion("setting_value", value, ConditionMode.NOT_FUZZY, "settingValue", "java.lang.String", "String");
          return this;
      }
      public CmsSettingSQL andSettingValueEqualTo(java.lang.String value) {
          addCriterion("setting_value", value, ConditionMode.EQUAL, "settingValue", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingValueNotEqualTo(java.lang.String value) {
          addCriterion("setting_value", value, ConditionMode.NOT_EQUAL, "settingValue", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingValueGreaterThan(java.lang.String value) {
          addCriterion("setting_value", value, ConditionMode.GREATER_THEN, "settingValue", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingValueGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("setting_value", value, ConditionMode.GREATER_EQUAL, "settingValue", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingValueLessThan(java.lang.String value) {
          addCriterion("setting_value", value, ConditionMode.LESS_THEN, "settingValue", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingValueLessThanOrEqualTo(java.lang.String value) {
          addCriterion("setting_value", value, ConditionMode.LESS_EQUAL, "settingValue", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingValueBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("setting_value", value1, value2, ConditionMode.BETWEEN, "settingValue", "java.lang.String", "String");
    	  return this;
      }

      public CmsSettingSQL andSettingValueNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("setting_value", value1, value2, ConditionMode.NOT_BETWEEN, "settingValue", "java.lang.String", "String");
          return this;
      }
        
      public CmsSettingSQL andSettingValueIn(List<java.lang.String> values) {
          addCriterion("setting_value", values, ConditionMode.IN, "settingValue", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingValueNotIn(List<java.lang.String> values) {
          addCriterion("setting_value", values, ConditionMode.NOT_IN, "settingValue", "java.lang.String", "String");
          return this;
      }
	public CmsSettingSQL andSettingNameIsNull() {
		isnull("setting_name");
		return this;
	}
	
	public CmsSettingSQL andSettingNameIsNotNull() {
		notNull("setting_name");
		return this;
	}
	
	public CmsSettingSQL andSettingNameIsEmpty() {
		empty("setting_name");
		return this;
	}

	public CmsSettingSQL andSettingNameIsNotEmpty() {
		notEmpty("setting_name");
		return this;
	}
       public CmsSettingSQL andSettingNameLike(java.lang.String value) {
    	   addCriterion("setting_name", value, ConditionMode.FUZZY, "settingName", "java.lang.String", "String");
    	   return this;
      }

      public CmsSettingSQL andSettingNameNotLike(java.lang.String value) {
          addCriterion("setting_name", value, ConditionMode.NOT_FUZZY, "settingName", "java.lang.String", "String");
          return this;
      }
      public CmsSettingSQL andSettingNameEqualTo(java.lang.String value) {
          addCriterion("setting_name", value, ConditionMode.EQUAL, "settingName", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingNameNotEqualTo(java.lang.String value) {
          addCriterion("setting_name", value, ConditionMode.NOT_EQUAL, "settingName", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingNameGreaterThan(java.lang.String value) {
          addCriterion("setting_name", value, ConditionMode.GREATER_THEN, "settingName", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("setting_name", value, ConditionMode.GREATER_EQUAL, "settingName", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingNameLessThan(java.lang.String value) {
          addCriterion("setting_name", value, ConditionMode.LESS_THEN, "settingName", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("setting_name", value, ConditionMode.LESS_EQUAL, "settingName", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("setting_name", value1, value2, ConditionMode.BETWEEN, "settingName", "java.lang.String", "String");
    	  return this;
      }

      public CmsSettingSQL andSettingNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("setting_name", value1, value2, ConditionMode.NOT_BETWEEN, "settingName", "java.lang.String", "String");
          return this;
      }
        
      public CmsSettingSQL andSettingNameIn(List<java.lang.String> values) {
          addCriterion("setting_name", values, ConditionMode.IN, "settingName", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andSettingNameNotIn(List<java.lang.String> values) {
          addCriterion("setting_name", values, ConditionMode.NOT_IN, "settingName", "java.lang.String", "String");
          return this;
      }
	public CmsSettingSQL andBisysIsNull() {
		isnull("bisys");
		return this;
	}
	
	public CmsSettingSQL andBisysIsNotNull() {
		notNull("bisys");
		return this;
	}
	
	public CmsSettingSQL andBisysIsEmpty() {
		empty("bisys");
		return this;
	}

	public CmsSettingSQL andBisysIsNotEmpty() {
		notEmpty("bisys");
		return this;
	}
      public CmsSettingSQL andBisysEqualTo(java.lang.Integer value) {
          addCriterion("bisys", value, ConditionMode.EQUAL, "bisys", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSettingSQL andBisysNotEqualTo(java.lang.Integer value) {
          addCriterion("bisys", value, ConditionMode.NOT_EQUAL, "bisys", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSettingSQL andBisysGreaterThan(java.lang.Integer value) {
          addCriterion("bisys", value, ConditionMode.GREATER_THEN, "bisys", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSettingSQL andBisysGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("bisys", value, ConditionMode.GREATER_EQUAL, "bisys", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSettingSQL andBisysLessThan(java.lang.Integer value) {
          addCriterion("bisys", value, ConditionMode.LESS_THEN, "bisys", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSettingSQL andBisysLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("bisys", value, ConditionMode.LESS_EQUAL, "bisys", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSettingSQL andBisysBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("bisys", value1, value2, ConditionMode.BETWEEN, "bisys", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsSettingSQL andBisysNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("bisys", value1, value2, ConditionMode.NOT_BETWEEN, "bisys", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsSettingSQL andBisysIn(List<java.lang.Integer> values) {
          addCriterion("bisys", values, ConditionMode.IN, "bisys", "java.lang.Integer", "Float");
          return this;
      }

      public CmsSettingSQL andBisysNotIn(List<java.lang.Integer> values) {
          addCriterion("bisys", values, ConditionMode.NOT_IN, "bisys", "java.lang.Integer", "Float");
          return this;
      }
	public CmsSettingSQL andRemarkIsNull() {
		isnull("remark");
		return this;
	}
	
	public CmsSettingSQL andRemarkIsNotNull() {
		notNull("remark");
		return this;
	}
	
	public CmsSettingSQL andRemarkIsEmpty() {
		empty("remark");
		return this;
	}

	public CmsSettingSQL andRemarkIsNotEmpty() {
		notEmpty("remark");
		return this;
	}
       public CmsSettingSQL andRemarkLike(java.lang.String value) {
    	   addCriterion("remark", value, ConditionMode.FUZZY, "remark", "java.lang.String", "Float");
    	   return this;
      }

      public CmsSettingSQL andRemarkNotLike(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.NOT_FUZZY, "remark", "java.lang.String", "Float");
          return this;
      }
      public CmsSettingSQL andRemarkEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andRemarkNotEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.NOT_EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andRemarkGreaterThan(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.GREATER_THEN, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andRemarkGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.GREATER_EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andRemarkLessThan(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.LESS_THEN, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andRemarkLessThanOrEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.LESS_EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andRemarkBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("remark", value1, value2, ConditionMode.BETWEEN, "remark", "java.lang.String", "String");
    	  return this;
      }

      public CmsSettingSQL andRemarkNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("remark", value1, value2, ConditionMode.NOT_BETWEEN, "remark", "java.lang.String", "String");
          return this;
      }
        
      public CmsSettingSQL andRemarkIn(List<java.lang.String> values) {
          addCriterion("remark", values, ConditionMode.IN, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsSettingSQL andRemarkNotIn(List<java.lang.String> values) {
          addCriterion("remark", values, ConditionMode.NOT_IN, "remark", "java.lang.String", "String");
          return this;
      }
	public CmsSettingSQL andCreatedIsNull() {
		isnull("created");
		return this;
	}
	
	public CmsSettingSQL andCreatedIsNotNull() {
		notNull("created");
		return this;
	}
	
	public CmsSettingSQL andCreatedIsEmpty() {
		empty("created");
		return this;
	}

	public CmsSettingSQL andCreatedIsNotEmpty() {
		notEmpty("created");
		return this;
	}
      public CmsSettingSQL andCreatedEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsSettingSQL andCreatedNotEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.NOT_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsSettingSQL andCreatedGreaterThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public CmsSettingSQL andCreatedGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsSettingSQL andCreatedLessThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public CmsSettingSQL andCreatedLessThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsSettingSQL andCreatedBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("created", value1, value2, ConditionMode.BETWEEN, "created", "java.util.Date", "String");
    	  return this;
      }

      public CmsSettingSQL andCreatedNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("created", value1, value2, ConditionMode.NOT_BETWEEN, "created", "java.util.Date", "String");
          return this;
      }
        
      public CmsSettingSQL andCreatedIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.IN, "created", "java.util.Date", "String");
          return this;
      }

      public CmsSettingSQL andCreatedNotIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.NOT_IN, "created", "java.util.Date", "String");
          return this;
      }
}