package com.lambkit.module.cms;

import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Lambkit;
import com.lambkit.core.config.ConfigCenter;
import com.lambkit.db.sql.Example;
import com.lambkit.db.sql.GroupBy;
import com.lambkit.module.cms.row.CmsSetting;
import com.lambkit.module.cms.service.CmsSettingService;
import com.lambkit.module.cms.sql.CmsSettingSQL;

import java.util.ArrayList;
import java.util.List;

public class CmsSettingConfigCenter implements ConfigCenter {

	private CmsSettingService service;

	public CmsSettingConfigCenter() {
		service = Lambkit.get(CmsSettingService.class);
	}

	@Override
	public String getName() {
		return "CommonSetting";
	}
	
	@Override
	public String getValue(String key) {
		return service.get(key);
	}

	@Override
	public boolean containsKey(String key) {
		String value = service.get(key);
		return StrUtil.isNotBlank(value);
	}

	@Override
	public List<String> getKeys(String prefix) {
		GroupBy groupBy = new GroupBy("setting_key", null);
		Example example = CmsSettingSQL.of().andSettingKeyLike(prefix + "%").example().setGroupBy(groupBy).setLoadColumns("setting_key");
		List<CmsSetting> records = service.dao().find(example);
		List<String> keyList = new ArrayList<String>();
		for(CmsSetting record : records) {
			keyList.add(record.getSettingKey());
		}
		return keyList;
	}

	@Override
	public boolean refresh() {
		return false;
	}

	@Override
	public void setValue(String name, String key, String value) {
		if(StrUtil.isBlank(name) || getName().equals(name)) {
			service.put(key, value);
		}
	}

	@Override
	public void removeValue(String key) {
		service.remove(key);
	}
}
