/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsTag extends RowModel<CmsTag> {
	public CmsTag() {
		setTableName("cms_tag");
		setPrimaryKey("tag_id");
	}
	public java.lang.Integer getTagId() {
		return this.get("tag_id");
	}
	public void setTagId(java.lang.Integer tagId) {
		this.set("tag_id", tagId);
	}
	public java.lang.String getName() {
		return this.get("name");
	}
	public void setName(java.lang.String name) {
		this.set("name", name);
	}
	public java.lang.String getDescription() {
		return this.get("description");
	}
	public void setDescription(java.lang.String description) {
		this.set("description", description);
	}
	public java.lang.String getIcon() {
		return this.get("icon");
	}
	public void setIcon(java.lang.String icon) {
		this.set("icon", icon);
	}
	public java.lang.Integer getType() {
		return this.get("type");
	}
	public void setType(java.lang.Integer type) {
		this.set("type", type);
	}
	public java.lang.String getAlias() {
		return this.get("alias");
	}
	public void setAlias(java.lang.String alias) {
		this.set("alias", alias);
	}
	public java.lang.Long getUserId() {
		return this.get("user_id");
	}
	public void setUserId(java.lang.Long userId) {
		this.set("user_id", userId);
	}
	public java.lang.Integer getSystemId() {
		return this.get("system_id");
	}
	public void setSystemId(java.lang.Integer systemId) {
		this.set("system_id", systemId);
	}
	public java.util.Date getCtime() {
		return this.get("ctime");
	}
	public void setCtime(java.util.Date ctime) {
		this.set("ctime", ctime);
	}
	public java.lang.Long getOrders() {
		return this.get("orders");
	}
	public void setOrders(java.lang.Long orders) {
		this.set("orders", orders);
	}
}
