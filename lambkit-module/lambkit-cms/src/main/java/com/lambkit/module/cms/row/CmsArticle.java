/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsArticle extends RowModel<CmsArticle> {
	public CmsArticle() {
		setTableName("cms_article");
		setPrimaryKey("article_id");
	}
	public java.lang.Long getArticleId() {
		return this.get("article_id");
	}
	public void setArticleId(java.lang.Long articleId) {
		this.set("article_id", articleId);
	}
	public java.lang.Integer getTopicId() {
		return this.get("topic_id");
	}
	public void setTopicId(java.lang.Integer topicId) {
		this.set("topic_id", topicId);
	}
	public java.lang.String getTitle() {
		return this.get("title");
	}
	public void setTitle(java.lang.String title) {
		this.set("title", title);
	}
	public java.lang.String getAuthor() {
		return this.get("author");
	}
	public void setAuthor(java.lang.String author) {
		this.set("author", author);
	}
	public java.lang.String getFromurl() {
		return this.get("fromurl");
	}
	public void setFromurl(java.lang.String fromurl) {
		this.set("fromurl", fromurl);
	}
	public java.lang.String getImage() {
		return this.get("image");
	}
	public void setImage(java.lang.String image) {
		this.set("image", image);
	}
	public java.lang.String getKeywords() {
		return this.get("keywords");
	}
	public void setKeywords(java.lang.String keywords) {
		this.set("keywords", keywords);
	}
	public java.lang.String getDescription() {
		return this.get("description");
	}
	public void setDescription(java.lang.String description) {
		this.set("description", description);
	}
	public java.lang.Integer getType() {
		return this.get("type");
	}
	public void setType(java.lang.Integer type) {
		this.set("type", type);
	}
	public java.lang.String getTag() {
		return this.get("tag");
	}
	public void setTag(java.lang.String tag) {
		this.set("tag", tag);
	}
	public java.lang.String getAlias() {
		return this.get("alias");
	}
	public void setAlias(java.lang.String alias) {
		this.set("alias", alias);
	}
	public java.lang.String getStyle() {
		return this.get("style");
	}
	public void setStyle(java.lang.String style) {
		this.set("style", style);
	}
	public java.lang.Integer getAllowcomments() {
		return this.get("allowcomments");
	}
	public void setAllowcomments(java.lang.Integer allowcomments) {
		this.set("allowcomments", allowcomments);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.lang.String getContent() {
		return this.get("content");
	}
	public void setContent(java.lang.String content) {
		this.set("content", content);
	}
	public java.lang.String getEditMode() {
		return this.get("edit_mode");
	}
	public void setEditMode(java.lang.String editMode) {
		this.set("edit_mode", editMode);
	}
	public java.lang.Long getUserId() {
		return this.get("user_id");
	}
	public void setUserId(java.lang.Long userId) {
		this.set("user_id", userId);
	}
	public java.lang.Integer getReadnumber() {
		return this.get("readnumber");
	}
	public void setReadnumber(java.lang.Integer readnumber) {
		this.set("readnumber", readnumber);
	}
	public java.lang.Integer getTop() {
		return this.get("top");
	}
	public void setTop(java.lang.Integer top) {
		this.set("top", top);
	}
	public java.lang.Integer getSystemId() {
		return this.get("system_id");
	}
	public void setSystemId(java.lang.Integer systemId) {
		this.set("system_id", systemId);
	}
	public java.util.Date getCtime() {
		return this.get("ctime");
	}
	public void setCtime(java.util.Date ctime) {
		this.set("ctime", ctime);
	}
	public java.lang.Long getOrders() {
		return this.get("orders");
	}
	public void setOrders(java.lang.Long orders) {
		this.set("orders", orders);
	}
}
