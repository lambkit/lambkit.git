/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsAttachment extends RowModel<CmsAttachment> {
	public CmsAttachment() {
		setTableName("cms_attachment");
		setPrimaryKey("attachment_id");
	}
	public java.lang.Integer getAttachmentId() {
		return this.get("attachment_id");
	}
	public void setAttachmentId(java.lang.Integer attachmentId) {
		this.set("attachment_id", attachmentId);
	}
	public java.lang.Long getUserId() {
		return this.get("user_id");
	}
	public void setUserId(java.lang.Long userId) {
		this.set("user_id", userId);
	}
	public java.lang.String getTitle() {
		return this.get("title");
	}
	public void setTitle(java.lang.String title) {
		this.set("title", title);
	}
	public java.lang.String getDescription() {
		return this.get("description");
	}
	public void setDescription(java.lang.String description) {
		this.set("description", description);
	}
	public java.lang.String getPath() {
		return this.get("path");
	}
	public void setPath(java.lang.String path) {
		this.set("path", path);
	}
	public java.lang.String getMimeType() {
		return this.get("mime_type");
	}
	public void setMimeType(java.lang.String mimeType) {
		this.set("mime_type", mimeType);
	}
	public java.lang.String getSuffix() {
		return this.get("suffix");
	}
	public void setSuffix(java.lang.String suffix) {
		this.set("suffix", suffix);
	}
	public java.lang.String getType() {
		return this.get("type");
	}
	public void setType(java.lang.String type) {
		this.set("type", type);
	}
	public java.lang.String getFlag() {
		return this.get("flag");
	}
	public void setFlag(java.lang.String flag) {
		this.set("flag", flag);
	}
	public java.lang.Integer getOrderNumber() {
		return this.get("order_number");
	}
	public void setOrderNumber(java.lang.Integer orderNumber) {
		this.set("order_number", orderNumber);
	}
	public java.lang.Integer getAccessible() {
		return this.get("accessible");
	}
	public void setAccessible(java.lang.Integer accessible) {
		this.set("accessible", accessible);
	}
	public java.util.Date getCreated() {
		return this.get("created");
	}
	public void setCreated(java.util.Date created) {
		this.set("created", created);
	}
	public java.util.Date getModified() {
		return this.get("modified");
	}
	public void setModified(java.util.Date modified) {
		this.set("modified", modified);
	}
}
