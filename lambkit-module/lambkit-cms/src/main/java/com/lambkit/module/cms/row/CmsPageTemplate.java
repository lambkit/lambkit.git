/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsPageTemplate extends RowModel<CmsPageTemplate> {
	public CmsPageTemplate() {
		setTableName("cms_page_template");
		setPrimaryKey("temp_id");
	}
	public java.lang.Long getTempId() {
		return this.get("temp_id");
	}
	public void setTempId(java.lang.Long tempId) {
		this.set("temp_id", tempId);
	}
	public java.lang.String getName() {
		return this.get("name");
	}
	public void setName(java.lang.String name) {
		this.set("name", name);
	}
	public java.lang.String getTitle() {
		return this.get("title");
	}
	public void setTitle(java.lang.String title) {
		this.set("title", title);
	}
	public java.lang.String getCatalog() {
		return this.get("catalog");
	}
	public void setCatalog(java.lang.String catalog) {
		this.set("catalog", catalog);
	}
	public java.lang.String getType() {
		return this.get("type");
	}
	public void setType(java.lang.String type) {
		this.set("type", type);
	}
	public java.lang.String getContent() {
		return this.get("content");
	}
	public void setContent(java.lang.String content) {
		this.set("content", content);
	}
	public java.lang.String getEditMode() {
		return this.get("edit_mode");
	}
	public void setEditMode(java.lang.String editMode) {
		this.set("edit_mode", editMode);
	}
	public java.lang.String getDescription() {
		return this.get("description");
	}
	public void setDescription(java.lang.String description) {
		this.set("description", description);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.util.Date getUpdateTime() {
		return this.get("update_time");
	}
	public void setUpdateTime(java.util.Date updateTime) {
		this.set("update_time", updateTime);
	}
	public java.lang.Long getUserId() {
		return this.get("user_id");
	}
	public void setUserId(java.lang.Long userId) {
		this.set("user_id", userId);
	}
}
