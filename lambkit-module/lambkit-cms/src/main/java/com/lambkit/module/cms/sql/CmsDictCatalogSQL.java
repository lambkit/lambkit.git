/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsDictCatalogSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsDictCatalogSQL of() {
		return new CmsDictCatalogSQL();
	}
	
	public static CmsDictCatalogSQL by(Column column) {
		CmsDictCatalogSQL that = new CmsDictCatalogSQL();
		that.add(column);
        return that;
    }

    public static CmsDictCatalogSQL by(String name, Object value) {
        return (CmsDictCatalogSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_dict_catalog", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsDictCatalogSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsDictCatalogSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsDictCatalogSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsDictCatalogSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsDictCatalogSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsDictCatalogSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsDictCatalogSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsDictCatalogSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsDictCatalogSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsDictCatalogSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsDictCatalogSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsDictCatalogSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsDictCatalogSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsDictCatalogSQL andDictCatalogIdIsNull() {
		isnull("dict_catalog_id");
		return this;
	}
	
	public CmsDictCatalogSQL andDictCatalogIdIsNotNull() {
		notNull("dict_catalog_id");
		return this;
	}
	
	public CmsDictCatalogSQL andDictCatalogIdIsEmpty() {
		empty("dict_catalog_id");
		return this;
	}

	public CmsDictCatalogSQL andDictCatalogIdIsNotEmpty() {
		notEmpty("dict_catalog_id");
		return this;
	}
      public CmsDictCatalogSQL andDictCatalogIdEqualTo(java.lang.Long value) {
          addCriterion("dict_catalog_id", value, ConditionMode.EQUAL, "dictCatalogId", "java.lang.Long", "Float");
          return this;
      }

      public CmsDictCatalogSQL andDictCatalogIdNotEqualTo(java.lang.Long value) {
          addCriterion("dict_catalog_id", value, ConditionMode.NOT_EQUAL, "dictCatalogId", "java.lang.Long", "Float");
          return this;
      }

      public CmsDictCatalogSQL andDictCatalogIdGreaterThan(java.lang.Long value) {
          addCriterion("dict_catalog_id", value, ConditionMode.GREATER_THEN, "dictCatalogId", "java.lang.Long", "Float");
          return this;
      }

      public CmsDictCatalogSQL andDictCatalogIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("dict_catalog_id", value, ConditionMode.GREATER_EQUAL, "dictCatalogId", "java.lang.Long", "Float");
          return this;
      }

      public CmsDictCatalogSQL andDictCatalogIdLessThan(java.lang.Long value) {
          addCriterion("dict_catalog_id", value, ConditionMode.LESS_THEN, "dictCatalogId", "java.lang.Long", "Float");
          return this;
      }

      public CmsDictCatalogSQL andDictCatalogIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("dict_catalog_id", value, ConditionMode.LESS_EQUAL, "dictCatalogId", "java.lang.Long", "Float");
          return this;
      }

      public CmsDictCatalogSQL andDictCatalogIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("dict_catalog_id", value1, value2, ConditionMode.BETWEEN, "dictCatalogId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsDictCatalogSQL andDictCatalogIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("dict_catalog_id", value1, value2, ConditionMode.NOT_BETWEEN, "dictCatalogId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsDictCatalogSQL andDictCatalogIdIn(List<java.lang.Long> values) {
          addCriterion("dict_catalog_id", values, ConditionMode.IN, "dictCatalogId", "java.lang.Long", "Float");
          return this;
      }

      public CmsDictCatalogSQL andDictCatalogIdNotIn(List<java.lang.Long> values) {
          addCriterion("dict_catalog_id", values, ConditionMode.NOT_IN, "dictCatalogId", "java.lang.Long", "Float");
          return this;
      }
	public CmsDictCatalogSQL andDictNameIsNull() {
		isnull("dict_name");
		return this;
	}
	
	public CmsDictCatalogSQL andDictNameIsNotNull() {
		notNull("dict_name");
		return this;
	}
	
	public CmsDictCatalogSQL andDictNameIsEmpty() {
		empty("dict_name");
		return this;
	}

	public CmsDictCatalogSQL andDictNameIsNotEmpty() {
		notEmpty("dict_name");
		return this;
	}
       public CmsDictCatalogSQL andDictNameLike(java.lang.String value) {
    	   addCriterion("dict_name", value, ConditionMode.FUZZY, "dictName", "java.lang.String", "Float");
    	   return this;
      }

      public CmsDictCatalogSQL andDictNameNotLike(java.lang.String value) {
          addCriterion("dict_name", value, ConditionMode.NOT_FUZZY, "dictName", "java.lang.String", "Float");
          return this;
      }
      public CmsDictCatalogSQL andDictNameEqualTo(java.lang.String value) {
          addCriterion("dict_name", value, ConditionMode.EQUAL, "dictName", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andDictNameNotEqualTo(java.lang.String value) {
          addCriterion("dict_name", value, ConditionMode.NOT_EQUAL, "dictName", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andDictNameGreaterThan(java.lang.String value) {
          addCriterion("dict_name", value, ConditionMode.GREATER_THEN, "dictName", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andDictNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("dict_name", value, ConditionMode.GREATER_EQUAL, "dictName", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andDictNameLessThan(java.lang.String value) {
          addCriterion("dict_name", value, ConditionMode.LESS_THEN, "dictName", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andDictNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("dict_name", value, ConditionMode.LESS_EQUAL, "dictName", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andDictNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("dict_name", value1, value2, ConditionMode.BETWEEN, "dictName", "java.lang.String", "String");
    	  return this;
      }

      public CmsDictCatalogSQL andDictNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("dict_name", value1, value2, ConditionMode.NOT_BETWEEN, "dictName", "java.lang.String", "String");
          return this;
      }
        
      public CmsDictCatalogSQL andDictNameIn(List<java.lang.String> values) {
          addCriterion("dict_name", values, ConditionMode.IN, "dictName", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andDictNameNotIn(List<java.lang.String> values) {
          addCriterion("dict_name", values, ConditionMode.NOT_IN, "dictName", "java.lang.String", "String");
          return this;
      }
	public CmsDictCatalogSQL andDictKeyIsNull() {
		isnull("dict_key");
		return this;
	}
	
	public CmsDictCatalogSQL andDictKeyIsNotNull() {
		notNull("dict_key");
		return this;
	}
	
	public CmsDictCatalogSQL andDictKeyIsEmpty() {
		empty("dict_key");
		return this;
	}

	public CmsDictCatalogSQL andDictKeyIsNotEmpty() {
		notEmpty("dict_key");
		return this;
	}
       public CmsDictCatalogSQL andDictKeyLike(java.lang.String value) {
    	   addCriterion("dict_key", value, ConditionMode.FUZZY, "dictKey", "java.lang.String", "String");
    	   return this;
      }

      public CmsDictCatalogSQL andDictKeyNotLike(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.NOT_FUZZY, "dictKey", "java.lang.String", "String");
          return this;
      }
      public CmsDictCatalogSQL andDictKeyEqualTo(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.EQUAL, "dictKey", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andDictKeyNotEqualTo(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.NOT_EQUAL, "dictKey", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andDictKeyGreaterThan(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.GREATER_THEN, "dictKey", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andDictKeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.GREATER_EQUAL, "dictKey", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andDictKeyLessThan(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.LESS_THEN, "dictKey", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andDictKeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.LESS_EQUAL, "dictKey", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andDictKeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("dict_key", value1, value2, ConditionMode.BETWEEN, "dictKey", "java.lang.String", "String");
    	  return this;
      }

      public CmsDictCatalogSQL andDictKeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("dict_key", value1, value2, ConditionMode.NOT_BETWEEN, "dictKey", "java.lang.String", "String");
          return this;
      }
        
      public CmsDictCatalogSQL andDictKeyIn(List<java.lang.String> values) {
          addCriterion("dict_key", values, ConditionMode.IN, "dictKey", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andDictKeyNotIn(List<java.lang.String> values) {
          addCriterion("dict_key", values, ConditionMode.NOT_IN, "dictKey", "java.lang.String", "String");
          return this;
      }
	public CmsDictCatalogSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public CmsDictCatalogSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public CmsDictCatalogSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public CmsDictCatalogSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public CmsDictCatalogSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictCatalogSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictCatalogSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictCatalogSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictCatalogSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictCatalogSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictCatalogSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsDictCatalogSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsDictCatalogSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public CmsDictCatalogSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public CmsDictCatalogSQL andRemarkIsNull() {
		isnull("remark");
		return this;
	}
	
	public CmsDictCatalogSQL andRemarkIsNotNull() {
		notNull("remark");
		return this;
	}
	
	public CmsDictCatalogSQL andRemarkIsEmpty() {
		empty("remark");
		return this;
	}

	public CmsDictCatalogSQL andRemarkIsNotEmpty() {
		notEmpty("remark");
		return this;
	}
       public CmsDictCatalogSQL andRemarkLike(java.lang.String value) {
    	   addCriterion("remark", value, ConditionMode.FUZZY, "remark", "java.lang.String", "Float");
    	   return this;
      }

      public CmsDictCatalogSQL andRemarkNotLike(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.NOT_FUZZY, "remark", "java.lang.String", "Float");
          return this;
      }
      public CmsDictCatalogSQL andRemarkEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andRemarkNotEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.NOT_EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andRemarkGreaterThan(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.GREATER_THEN, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andRemarkGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.GREATER_EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andRemarkLessThan(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.LESS_THEN, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andRemarkLessThanOrEqualTo(java.lang.String value) {
          addCriterion("remark", value, ConditionMode.LESS_EQUAL, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andRemarkBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("remark", value1, value2, ConditionMode.BETWEEN, "remark", "java.lang.String", "String");
    	  return this;
      }

      public CmsDictCatalogSQL andRemarkNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("remark", value1, value2, ConditionMode.NOT_BETWEEN, "remark", "java.lang.String", "String");
          return this;
      }
        
      public CmsDictCatalogSQL andRemarkIn(List<java.lang.String> values) {
          addCriterion("remark", values, ConditionMode.IN, "remark", "java.lang.String", "String");
          return this;
      }

      public CmsDictCatalogSQL andRemarkNotIn(List<java.lang.String> values) {
          addCriterion("remark", values, ConditionMode.NOT_IN, "remark", "java.lang.String", "String");
          return this;
      }
	public CmsDictCatalogSQL andCreatedIsNull() {
		isnull("created");
		return this;
	}
	
	public CmsDictCatalogSQL andCreatedIsNotNull() {
		notNull("created");
		return this;
	}
	
	public CmsDictCatalogSQL andCreatedIsEmpty() {
		empty("created");
		return this;
	}

	public CmsDictCatalogSQL andCreatedIsNotEmpty() {
		notEmpty("created");
		return this;
	}
      public CmsDictCatalogSQL andCreatedEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsDictCatalogSQL andCreatedNotEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.NOT_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsDictCatalogSQL andCreatedGreaterThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public CmsDictCatalogSQL andCreatedGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsDictCatalogSQL andCreatedLessThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public CmsDictCatalogSQL andCreatedLessThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public CmsDictCatalogSQL andCreatedBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("created", value1, value2, ConditionMode.BETWEEN, "created", "java.util.Date", "String");
    	  return this;
      }

      public CmsDictCatalogSQL andCreatedNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("created", value1, value2, ConditionMode.NOT_BETWEEN, "created", "java.util.Date", "String");
          return this;
      }
        
      public CmsDictCatalogSQL andCreatedIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.IN, "created", "java.util.Date", "String");
          return this;
      }

      public CmsDictCatalogSQL andCreatedNotIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.NOT_IN, "created", "java.util.Date", "String");
          return this;
      }
}