/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsAccessSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsAccessSQL of() {
		return new CmsAccessSQL();
	}
	
	public static CmsAccessSQL by(Column column) {
		CmsAccessSQL that = new CmsAccessSQL();
		that.add(column);
        return that;
    }

    public static CmsAccessSQL by(String name, Object value) {
        return (CmsAccessSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_access", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAccessSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAccessSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsAccessSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsAccessSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAccessSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAccessSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAccessSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsAccessSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsAccessSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsAccessSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsAccessSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsAccessSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsAccessSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsAccessSQL andAccessIdIsNull() {
		isnull("access_id");
		return this;
	}
	
	public CmsAccessSQL andAccessIdIsNotNull() {
		notNull("access_id");
		return this;
	}
	
	public CmsAccessSQL andAccessIdIsEmpty() {
		empty("access_id");
		return this;
	}

	public CmsAccessSQL andAccessIdIsNotEmpty() {
		notEmpty("access_id");
		return this;
	}
      public CmsAccessSQL andAccessIdEqualTo(java.lang.Long value) {
          addCriterion("access_id", value, ConditionMode.EQUAL, "accessId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andAccessIdNotEqualTo(java.lang.Long value) {
          addCriterion("access_id", value, ConditionMode.NOT_EQUAL, "accessId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andAccessIdGreaterThan(java.lang.Long value) {
          addCriterion("access_id", value, ConditionMode.GREATER_THEN, "accessId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andAccessIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("access_id", value, ConditionMode.GREATER_EQUAL, "accessId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andAccessIdLessThan(java.lang.Long value) {
          addCriterion("access_id", value, ConditionMode.LESS_THEN, "accessId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andAccessIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("access_id", value, ConditionMode.LESS_EQUAL, "accessId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andAccessIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("access_id", value1, value2, ConditionMode.BETWEEN, "accessId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsAccessSQL andAccessIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("access_id", value1, value2, ConditionMode.NOT_BETWEEN, "accessId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsAccessSQL andAccessIdIn(List<java.lang.Long> values) {
          addCriterion("access_id", values, ConditionMode.IN, "accessId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andAccessIdNotIn(List<java.lang.Long> values) {
          addCriterion("access_id", values, ConditionMode.NOT_IN, "accessId", "java.lang.Long", "Float");
          return this;
      }
	public CmsAccessSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public CmsAccessSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public CmsAccessSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public CmsAccessSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
      public CmsAccessSQL andUserIdEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andUserIdGreaterThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andUserIdLessThan(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsAccessSQL andUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsAccessSQL andUserIdIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.Long", "Float");
          return this;
      }
	public CmsAccessSQL andReftypeIsNull() {
		isnull("reftype");
		return this;
	}
	
	public CmsAccessSQL andReftypeIsNotNull() {
		notNull("reftype");
		return this;
	}
	
	public CmsAccessSQL andReftypeIsEmpty() {
		empty("reftype");
		return this;
	}

	public CmsAccessSQL andReftypeIsNotEmpty() {
		notEmpty("reftype");
		return this;
	}
      public CmsAccessSQL andReftypeEqualTo(java.lang.Integer value) {
          addCriterion("reftype", value, ConditionMode.EQUAL, "reftype", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAccessSQL andReftypeNotEqualTo(java.lang.Integer value) {
          addCriterion("reftype", value, ConditionMode.NOT_EQUAL, "reftype", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAccessSQL andReftypeGreaterThan(java.lang.Integer value) {
          addCriterion("reftype", value, ConditionMode.GREATER_THEN, "reftype", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAccessSQL andReftypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("reftype", value, ConditionMode.GREATER_EQUAL, "reftype", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAccessSQL andReftypeLessThan(java.lang.Integer value) {
          addCriterion("reftype", value, ConditionMode.LESS_THEN, "reftype", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAccessSQL andReftypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("reftype", value, ConditionMode.LESS_EQUAL, "reftype", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAccessSQL andReftypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("reftype", value1, value2, ConditionMode.BETWEEN, "reftype", "java.lang.Integer", "Float");
    	  return this;
      }

      public CmsAccessSQL andReftypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("reftype", value1, value2, ConditionMode.NOT_BETWEEN, "reftype", "java.lang.Integer", "Float");
          return this;
      }
        
      public CmsAccessSQL andReftypeIn(List<java.lang.Integer> values) {
          addCriterion("reftype", values, ConditionMode.IN, "reftype", "java.lang.Integer", "Float");
          return this;
      }

      public CmsAccessSQL andReftypeNotIn(List<java.lang.Integer> values) {
          addCriterion("reftype", values, ConditionMode.NOT_IN, "reftype", "java.lang.Integer", "Float");
          return this;
      }
	public CmsAccessSQL andRefidIsNull() {
		isnull("refid");
		return this;
	}
	
	public CmsAccessSQL andRefidIsNotNull() {
		notNull("refid");
		return this;
	}
	
	public CmsAccessSQL andRefidIsEmpty() {
		empty("refid");
		return this;
	}

	public CmsAccessSQL andRefidIsNotEmpty() {
		notEmpty("refid");
		return this;
	}
      public CmsAccessSQL andRefidEqualTo(java.lang.Long value) {
          addCriterion("refid", value, ConditionMode.EQUAL, "refid", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andRefidNotEqualTo(java.lang.Long value) {
          addCriterion("refid", value, ConditionMode.NOT_EQUAL, "refid", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andRefidGreaterThan(java.lang.Long value) {
          addCriterion("refid", value, ConditionMode.GREATER_THEN, "refid", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andRefidGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("refid", value, ConditionMode.GREATER_EQUAL, "refid", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andRefidLessThan(java.lang.Long value) {
          addCriterion("refid", value, ConditionMode.LESS_THEN, "refid", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andRefidLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("refid", value, ConditionMode.LESS_EQUAL, "refid", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andRefidBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("refid", value1, value2, ConditionMode.BETWEEN, "refid", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsAccessSQL andRefidNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("refid", value1, value2, ConditionMode.NOT_BETWEEN, "refid", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsAccessSQL andRefidIn(List<java.lang.Long> values) {
          addCriterion("refid", values, ConditionMode.IN, "refid", "java.lang.Long", "Float");
          return this;
      }

      public CmsAccessSQL andRefidNotIn(List<java.lang.Long> values) {
          addCriterion("refid", values, ConditionMode.NOT_IN, "refid", "java.lang.Long", "Float");
          return this;
      }
	public CmsAccessSQL andAccessTimeIsNull() {
		isnull("access_time");
		return this;
	}
	
	public CmsAccessSQL andAccessTimeIsNotNull() {
		notNull("access_time");
		return this;
	}
	
	public CmsAccessSQL andAccessTimeIsEmpty() {
		empty("access_time");
		return this;
	}

	public CmsAccessSQL andAccessTimeIsNotEmpty() {
		notEmpty("access_time");
		return this;
	}
      public CmsAccessSQL andAccessTimeEqualTo(java.util.Date value) {
          addCriterion("access_time", value, ConditionMode.EQUAL, "accessTime", "java.util.Date", "String");
          return this;
      }

      public CmsAccessSQL andAccessTimeNotEqualTo(java.util.Date value) {
          addCriterion("access_time", value, ConditionMode.NOT_EQUAL, "accessTime", "java.util.Date", "String");
          return this;
      }

      public CmsAccessSQL andAccessTimeGreaterThan(java.util.Date value) {
          addCriterion("access_time", value, ConditionMode.GREATER_THEN, "accessTime", "java.util.Date", "String");
          return this;
      }

      public CmsAccessSQL andAccessTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("access_time", value, ConditionMode.GREATER_EQUAL, "accessTime", "java.util.Date", "String");
          return this;
      }

      public CmsAccessSQL andAccessTimeLessThan(java.util.Date value) {
          addCriterion("access_time", value, ConditionMode.LESS_THEN, "accessTime", "java.util.Date", "String");
          return this;
      }

      public CmsAccessSQL andAccessTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("access_time", value, ConditionMode.LESS_EQUAL, "accessTime", "java.util.Date", "String");
          return this;
      }

      public CmsAccessSQL andAccessTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("access_time", value1, value2, ConditionMode.BETWEEN, "accessTime", "java.util.Date", "String");
    	  return this;
      }

      public CmsAccessSQL andAccessTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("access_time", value1, value2, ConditionMode.NOT_BETWEEN, "accessTime", "java.util.Date", "String");
          return this;
      }
        
      public CmsAccessSQL andAccessTimeIn(List<java.util.Date> values) {
          addCriterion("access_time", values, ConditionMode.IN, "accessTime", "java.util.Date", "String");
          return this;
      }

      public CmsAccessSQL andAccessTimeNotIn(List<java.util.Date> values) {
          addCriterion("access_time", values, ConditionMode.NOT_IN, "accessTime", "java.util.Date", "String");
          return this;
      }
	public CmsAccessSQL andAccessTypeIsNull() {
		isnull("access_type");
		return this;
	}
	
	public CmsAccessSQL andAccessTypeIsNotNull() {
		notNull("access_type");
		return this;
	}
	
	public CmsAccessSQL andAccessTypeIsEmpty() {
		empty("access_type");
		return this;
	}

	public CmsAccessSQL andAccessTypeIsNotEmpty() {
		notEmpty("access_type");
		return this;
	}
       public CmsAccessSQL andAccessTypeLike(java.lang.String value) {
    	   addCriterion("access_type", value, ConditionMode.FUZZY, "accessType", "java.lang.String", "String");
    	   return this;
      }

      public CmsAccessSQL andAccessTypeNotLike(java.lang.String value) {
          addCriterion("access_type", value, ConditionMode.NOT_FUZZY, "accessType", "java.lang.String", "String");
          return this;
      }
      public CmsAccessSQL andAccessTypeEqualTo(java.lang.String value) {
          addCriterion("access_type", value, ConditionMode.EQUAL, "accessType", "java.lang.String", "String");
          return this;
      }

      public CmsAccessSQL andAccessTypeNotEqualTo(java.lang.String value) {
          addCriterion("access_type", value, ConditionMode.NOT_EQUAL, "accessType", "java.lang.String", "String");
          return this;
      }

      public CmsAccessSQL andAccessTypeGreaterThan(java.lang.String value) {
          addCriterion("access_type", value, ConditionMode.GREATER_THEN, "accessType", "java.lang.String", "String");
          return this;
      }

      public CmsAccessSQL andAccessTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("access_type", value, ConditionMode.GREATER_EQUAL, "accessType", "java.lang.String", "String");
          return this;
      }

      public CmsAccessSQL andAccessTypeLessThan(java.lang.String value) {
          addCriterion("access_type", value, ConditionMode.LESS_THEN, "accessType", "java.lang.String", "String");
          return this;
      }

      public CmsAccessSQL andAccessTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("access_type", value, ConditionMode.LESS_EQUAL, "accessType", "java.lang.String", "String");
          return this;
      }

      public CmsAccessSQL andAccessTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("access_type", value1, value2, ConditionMode.BETWEEN, "accessType", "java.lang.String", "String");
    	  return this;
      }

      public CmsAccessSQL andAccessTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("access_type", value1, value2, ConditionMode.NOT_BETWEEN, "accessType", "java.lang.String", "String");
          return this;
      }
        
      public CmsAccessSQL andAccessTypeIn(List<java.lang.String> values) {
          addCriterion("access_type", values, ConditionMode.IN, "accessType", "java.lang.String", "String");
          return this;
      }

      public CmsAccessSQL andAccessTypeNotIn(List<java.lang.String> values) {
          addCriterion("access_type", values, ConditionMode.NOT_IN, "accessType", "java.lang.String", "String");
          return this;
      }
}