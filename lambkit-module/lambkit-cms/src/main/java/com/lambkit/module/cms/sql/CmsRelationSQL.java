/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsRelationSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static CmsRelationSQL of() {
		return new CmsRelationSQL();
	}
	
	public static CmsRelationSQL by(Column column) {
		CmsRelationSQL that = new CmsRelationSQL();
		that.add(column);
        return that;
    }

    public static CmsRelationSQL by(String name, Object value) {
        return (CmsRelationSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("cms_relation", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public CmsRelationSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public CmsRelationSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public CmsRelationSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public CmsRelationSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsRelationSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsRelationSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public CmsRelationSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public CmsRelationSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public CmsRelationSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public CmsRelationSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public CmsRelationSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public CmsRelationSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public CmsRelationSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public CmsRelationSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public CmsRelationSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public CmsRelationSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public CmsRelationSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public CmsRelationSQL andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsRelationSQL andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsRelationSQL andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public CmsRelationSQL andMainIdIsNull() {
		isnull("main_id");
		return this;
	}
	
	public CmsRelationSQL andMainIdIsNotNull() {
		notNull("main_id");
		return this;
	}
	
	public CmsRelationSQL andMainIdIsEmpty() {
		empty("main_id");
		return this;
	}

	public CmsRelationSQL andMainIdIsNotEmpty() {
		notEmpty("main_id");
		return this;
	}
      public CmsRelationSQL andMainIdEqualTo(java.lang.Long value) {
          addCriterion("main_id", value, ConditionMode.EQUAL, "mainId", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andMainIdNotEqualTo(java.lang.Long value) {
          addCriterion("main_id", value, ConditionMode.NOT_EQUAL, "mainId", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andMainIdGreaterThan(java.lang.Long value) {
          addCriterion("main_id", value, ConditionMode.GREATER_THEN, "mainId", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andMainIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("main_id", value, ConditionMode.GREATER_EQUAL, "mainId", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andMainIdLessThan(java.lang.Long value) {
          addCriterion("main_id", value, ConditionMode.LESS_THEN, "mainId", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andMainIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("main_id", value, ConditionMode.LESS_EQUAL, "mainId", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andMainIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("main_id", value1, value2, ConditionMode.BETWEEN, "mainId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsRelationSQL andMainIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("main_id", value1, value2, ConditionMode.NOT_BETWEEN, "mainId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsRelationSQL andMainIdIn(List<java.lang.Long> values) {
          addCriterion("main_id", values, ConditionMode.IN, "mainId", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andMainIdNotIn(List<java.lang.Long> values) {
          addCriterion("main_id", values, ConditionMode.NOT_IN, "mainId", "java.lang.Long", "Float");
          return this;
      }
	public CmsRelationSQL andMainTypeIsNull() {
		isnull("main_type");
		return this;
	}
	
	public CmsRelationSQL andMainTypeIsNotNull() {
		notNull("main_type");
		return this;
	}
	
	public CmsRelationSQL andMainTypeIsEmpty() {
		empty("main_type");
		return this;
	}

	public CmsRelationSQL andMainTypeIsNotEmpty() {
		notEmpty("main_type");
		return this;
	}
       public CmsRelationSQL andMainTypeLike(java.lang.String value) {
    	   addCriterion("main_type", value, ConditionMode.FUZZY, "mainType", "java.lang.String", "Float");
    	   return this;
      }

      public CmsRelationSQL andMainTypeNotLike(java.lang.String value) {
          addCriterion("main_type", value, ConditionMode.NOT_FUZZY, "mainType", "java.lang.String", "Float");
          return this;
      }
      public CmsRelationSQL andMainTypeEqualTo(java.lang.String value) {
          addCriterion("main_type", value, ConditionMode.EQUAL, "mainType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andMainTypeNotEqualTo(java.lang.String value) {
          addCriterion("main_type", value, ConditionMode.NOT_EQUAL, "mainType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andMainTypeGreaterThan(java.lang.String value) {
          addCriterion("main_type", value, ConditionMode.GREATER_THEN, "mainType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andMainTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("main_type", value, ConditionMode.GREATER_EQUAL, "mainType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andMainTypeLessThan(java.lang.String value) {
          addCriterion("main_type", value, ConditionMode.LESS_THEN, "mainType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andMainTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("main_type", value, ConditionMode.LESS_EQUAL, "mainType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andMainTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("main_type", value1, value2, ConditionMode.BETWEEN, "mainType", "java.lang.String", "String");
    	  return this;
      }

      public CmsRelationSQL andMainTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("main_type", value1, value2, ConditionMode.NOT_BETWEEN, "mainType", "java.lang.String", "String");
          return this;
      }
        
      public CmsRelationSQL andMainTypeIn(List<java.lang.String> values) {
          addCriterion("main_type", values, ConditionMode.IN, "mainType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andMainTypeNotIn(List<java.lang.String> values) {
          addCriterion("main_type", values, ConditionMode.NOT_IN, "mainType", "java.lang.String", "String");
          return this;
      }
	public CmsRelationSQL andRelationIdIsNull() {
		isnull("relation_id");
		return this;
	}
	
	public CmsRelationSQL andRelationIdIsNotNull() {
		notNull("relation_id");
		return this;
	}
	
	public CmsRelationSQL andRelationIdIsEmpty() {
		empty("relation_id");
		return this;
	}

	public CmsRelationSQL andRelationIdIsNotEmpty() {
		notEmpty("relation_id");
		return this;
	}
      public CmsRelationSQL andRelationIdEqualTo(java.lang.Long value) {
          addCriterion("relation_id", value, ConditionMode.EQUAL, "relationId", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andRelationIdNotEqualTo(java.lang.Long value) {
          addCriterion("relation_id", value, ConditionMode.NOT_EQUAL, "relationId", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andRelationIdGreaterThan(java.lang.Long value) {
          addCriterion("relation_id", value, ConditionMode.GREATER_THEN, "relationId", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andRelationIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("relation_id", value, ConditionMode.GREATER_EQUAL, "relationId", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andRelationIdLessThan(java.lang.Long value) {
          addCriterion("relation_id", value, ConditionMode.LESS_THEN, "relationId", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andRelationIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("relation_id", value, ConditionMode.LESS_EQUAL, "relationId", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andRelationIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("relation_id", value1, value2, ConditionMode.BETWEEN, "relationId", "java.lang.Long", "Float");
    	  return this;
      }

      public CmsRelationSQL andRelationIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("relation_id", value1, value2, ConditionMode.NOT_BETWEEN, "relationId", "java.lang.Long", "Float");
          return this;
      }
        
      public CmsRelationSQL andRelationIdIn(List<java.lang.Long> values) {
          addCriterion("relation_id", values, ConditionMode.IN, "relationId", "java.lang.Long", "Float");
          return this;
      }

      public CmsRelationSQL andRelationIdNotIn(List<java.lang.Long> values) {
          addCriterion("relation_id", values, ConditionMode.NOT_IN, "relationId", "java.lang.Long", "Float");
          return this;
      }
	public CmsRelationSQL andRelationTypeIsNull() {
		isnull("relation_type");
		return this;
	}
	
	public CmsRelationSQL andRelationTypeIsNotNull() {
		notNull("relation_type");
		return this;
	}
	
	public CmsRelationSQL andRelationTypeIsEmpty() {
		empty("relation_type");
		return this;
	}

	public CmsRelationSQL andRelationTypeIsNotEmpty() {
		notEmpty("relation_type");
		return this;
	}
       public CmsRelationSQL andRelationTypeLike(java.lang.String value) {
    	   addCriterion("relation_type", value, ConditionMode.FUZZY, "relationType", "java.lang.String", "Float");
    	   return this;
      }

      public CmsRelationSQL andRelationTypeNotLike(java.lang.String value) {
          addCriterion("relation_type", value, ConditionMode.NOT_FUZZY, "relationType", "java.lang.String", "Float");
          return this;
      }
      public CmsRelationSQL andRelationTypeEqualTo(java.lang.String value) {
          addCriterion("relation_type", value, ConditionMode.EQUAL, "relationType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andRelationTypeNotEqualTo(java.lang.String value) {
          addCriterion("relation_type", value, ConditionMode.NOT_EQUAL, "relationType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andRelationTypeGreaterThan(java.lang.String value) {
          addCriterion("relation_type", value, ConditionMode.GREATER_THEN, "relationType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andRelationTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("relation_type", value, ConditionMode.GREATER_EQUAL, "relationType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andRelationTypeLessThan(java.lang.String value) {
          addCriterion("relation_type", value, ConditionMode.LESS_THEN, "relationType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andRelationTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("relation_type", value, ConditionMode.LESS_EQUAL, "relationType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andRelationTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("relation_type", value1, value2, ConditionMode.BETWEEN, "relationType", "java.lang.String", "String");
    	  return this;
      }

      public CmsRelationSQL andRelationTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("relation_type", value1, value2, ConditionMode.NOT_BETWEEN, "relationType", "java.lang.String", "String");
          return this;
      }
        
      public CmsRelationSQL andRelationTypeIn(List<java.lang.String> values) {
          addCriterion("relation_type", values, ConditionMode.IN, "relationType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andRelationTypeNotIn(List<java.lang.String> values) {
          addCriterion("relation_type", values, ConditionMode.NOT_IN, "relationType", "java.lang.String", "String");
          return this;
      }
	public CmsRelationSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public CmsRelationSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public CmsRelationSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public CmsRelationSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public CmsRelationSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public CmsRelationSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public CmsRelationSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public CmsRelationSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public CmsRelationSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public CmsRelationSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public CmsRelationSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public CmsRelationSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public CmsRelationSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public CmsRelationSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public CmsRelationSQL andAssociatedTypeIsNull() {
		isnull("associated_type");
		return this;
	}
	
	public CmsRelationSQL andAssociatedTypeIsNotNull() {
		notNull("associated_type");
		return this;
	}
	
	public CmsRelationSQL andAssociatedTypeIsEmpty() {
		empty("associated_type");
		return this;
	}

	public CmsRelationSQL andAssociatedTypeIsNotEmpty() {
		notEmpty("associated_type");
		return this;
	}
       public CmsRelationSQL andAssociatedTypeLike(java.lang.String value) {
    	   addCriterion("associated_type", value, ConditionMode.FUZZY, "associatedType", "java.lang.String", "String");
    	   return this;
      }

      public CmsRelationSQL andAssociatedTypeNotLike(java.lang.String value) {
          addCriterion("associated_type", value, ConditionMode.NOT_FUZZY, "associatedType", "java.lang.String", "String");
          return this;
      }
      public CmsRelationSQL andAssociatedTypeEqualTo(java.lang.String value) {
          addCriterion("associated_type", value, ConditionMode.EQUAL, "associatedType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andAssociatedTypeNotEqualTo(java.lang.String value) {
          addCriterion("associated_type", value, ConditionMode.NOT_EQUAL, "associatedType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andAssociatedTypeGreaterThan(java.lang.String value) {
          addCriterion("associated_type", value, ConditionMode.GREATER_THEN, "associatedType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andAssociatedTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("associated_type", value, ConditionMode.GREATER_EQUAL, "associatedType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andAssociatedTypeLessThan(java.lang.String value) {
          addCriterion("associated_type", value, ConditionMode.LESS_THEN, "associatedType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andAssociatedTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("associated_type", value, ConditionMode.LESS_EQUAL, "associatedType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andAssociatedTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("associated_type", value1, value2, ConditionMode.BETWEEN, "associatedType", "java.lang.String", "String");
    	  return this;
      }

      public CmsRelationSQL andAssociatedTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("associated_type", value1, value2, ConditionMode.NOT_BETWEEN, "associatedType", "java.lang.String", "String");
          return this;
      }
        
      public CmsRelationSQL andAssociatedTypeIn(List<java.lang.String> values) {
          addCriterion("associated_type", values, ConditionMode.IN, "associatedType", "java.lang.String", "String");
          return this;
      }

      public CmsRelationSQL andAssociatedTypeNotIn(List<java.lang.String> values) {
          addCriterion("associated_type", values, ConditionMode.NOT_IN, "associatedType", "java.lang.String", "String");
          return this;
      }
}