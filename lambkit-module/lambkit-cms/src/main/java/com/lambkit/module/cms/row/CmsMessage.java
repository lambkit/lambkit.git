/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.cms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class CmsMessage extends RowModel<CmsMessage> {
	public CmsMessage() {
		setTableName("cms_message");
		setPrimaryKey("message_id");
	}
	public java.lang.Integer getMessageId() {
		return this.get("message_id");
	}
	public void setMessageId(java.lang.Integer messageId) {
		this.set("message_id", messageId);
	}
	public java.lang.String getMessageTitle() {
		return this.get("message_title");
	}
	public void setMessageTitle(java.lang.String messageTitle) {
		this.set("message_title", messageTitle);
	}
	public java.lang.String getMessageContent() {
		return this.get("message_content");
	}
	public void setMessageContent(java.lang.String messageContent) {
		this.set("message_content", messageContent);
	}
	public java.lang.Long getMessageToUserId() {
		return this.get("message_to_user_id");
	}
	public void setMessageToUserId(java.lang.Long messageToUserId) {
		this.set("message_to_user_id", messageToUserId);
	}
	public java.lang.Integer getMessageIsRead() {
		return this.get("message_is_read");
	}
	public void setMessageIsRead(java.lang.Integer messageIsRead) {
		this.set("message_is_read", messageIsRead);
	}
	public java.util.Date getMessageTime() {
		return this.get("message_time");
	}
	public void setMessageTime(java.util.Date messageTime) {
		this.set("message_time", messageTime);
	}
	public java.lang.Long getMessageFromUserId() {
		return this.get("message_from_user_id");
	}
	public void setMessageFromUserId(java.lang.Long messageFromUserId) {
		this.set("message_from_user_id", messageFromUserId);
	}
	public java.lang.String getMessageRefType() {
		return this.get("message_ref_type");
	}
	public void setMessageRefType(java.lang.String messageRefType) {
		this.set("message_ref_type", messageRefType);
	}
	public java.lang.Long getMessageRefId() {
		return this.get("message_ref_id");
	}
	public void setMessageRefId(java.lang.Long messageRefId) {
		this.set("message_ref_id", messageRefId);
	}
	public java.lang.String getMessageFromUserPhone() {
		return this.get("message_from_user_phone");
	}
	public void setMessageFromUserPhone(java.lang.String messageFromUserPhone) {
		this.set("message_from_user_phone", messageFromUserPhone);
	}
	public java.lang.String getMessageFromUserName() {
		return this.get("message_from_user_name");
	}
	public void setMessageFromUserName(java.lang.String messageFromUserName) {
		this.set("message_from_user_name", messageFromUserName);
	}
}
