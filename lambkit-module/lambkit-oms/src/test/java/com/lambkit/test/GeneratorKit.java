package com.lambkit.test;

import cn.hutool.db.dialect.DriverNamePool;
import com.alibaba.druid.pool.DruidDataSource;
import com.lambkit.db.hutool.HutoolDb;
import com.lambkit.generator.Generator;
import com.lambkit.generator.GeneratorConfig;
import com.lambkit.generator.GeneratorType;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yangyong(孤竹行)
 */
public class GeneratorKit {
    public static void main(String[] args) {
        DruidDataSource ds2 = new DruidDataSource();
        ds2.setUrl("jdbc:postgresql://localhost:5432/lambkit2");
        ds2.setUsername("postgres");
        ds2.setPassword("spFAyCBalefzzoBK");
        HutoolDb hutoolDb = new HutoolDb(ds2, DriverNamePool.DRIVER_POSTGRESQL);
        //
        GeneratorConfig config = new GeneratorConfig();
        // 模板地址
        String templatePath = "/template/jfinal";
        // 模板所在目录
        String rootPath = System.getProperty("user.dir");
        // 生成java代码的存放地址
        config.setOutRootDir(rootPath + "/lambkit-module/lambkit-oms/src/main/java");
        // 生成前端文件文件夹
        config.setWebpages("app");
        // 表格配置方式
        //config.setMgrdb("dmsMgrdb");
        // 选择一种模板语言
        config.setEngine(GeneratorConfig.TYPE_JFINAL);
        // 选择一种处理引擎
        config.setType(GeneratorType.DB);
        // 包地址
        config.setBasepackage("com.lambkit.module.oms");
        // 配置
        Map<String, Object> options = new HashMap<String, Object>();
        // 需要去掉的前缀
        options.put("tableRemovePrefixes", "lk_");

        options.put("includedTables", "oms_access_log," +
                "oms_action_log," +
                "oms_api_log," +
                "oms_devops_api," +
                "oms_devops_log," +
                "oms_devops_server," +
                "oms_devops_warn," +
                "oms_log," +
                "oms_pv_log," +
                "oms_uv_log");
        options.put("moduleName", "Oms");
        options.put("propName", "oms");

        // 不进行生成的模板
        options.put("excludedTemplate", "#(moduleName)Config.java, #(classname).java,SQL.java,Module.java");

        // 代码生成
        Generator.execute(hutoolDb, rootPath, templatePath, options, config);
    }
}
