/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsDevopsLogSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static OmsDevopsLogSQL of() {
		return new OmsDevopsLogSQL();
	}
	
	public static OmsDevopsLogSQL by(Column column) {
		OmsDevopsLogSQL that = new OmsDevopsLogSQL();
		that.add(column);
        return that;
    }

    public static OmsDevopsLogSQL by(String name, Object value) {
        return (OmsDevopsLogSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("oms_devops_log", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsLogSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsLogSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public OmsDevopsLogSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public OmsDevopsLogSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsLogSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsLogSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsLogSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsLogSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public OmsDevopsLogSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public OmsDevopsLogSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public OmsDevopsLogSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public OmsDevopsLogSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public OmsDevopsLogSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public OmsDevopsLogSQL andDevopsLogIdIsNull() {
		isnull("devops_log_id");
		return this;
	}
	
	public OmsDevopsLogSQL andDevopsLogIdIsNotNull() {
		notNull("devops_log_id");
		return this;
	}
	
	public OmsDevopsLogSQL andDevopsLogIdIsEmpty() {
		empty("devops_log_id");
		return this;
	}

	public OmsDevopsLogSQL andDevopsLogIdIsNotEmpty() {
		notEmpty("devops_log_id");
		return this;
	}
      public OmsDevopsLogSQL andDevopsLogIdEqualTo(java.lang.Long value) {
          addCriterion("devops_log_id", value, ConditionMode.EQUAL, "devopsLogId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsLogSQL andDevopsLogIdNotEqualTo(java.lang.Long value) {
          addCriterion("devops_log_id", value, ConditionMode.NOT_EQUAL, "devopsLogId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsLogSQL andDevopsLogIdGreaterThan(java.lang.Long value) {
          addCriterion("devops_log_id", value, ConditionMode.GREATER_THEN, "devopsLogId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsLogSQL andDevopsLogIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("devops_log_id", value, ConditionMode.GREATER_EQUAL, "devopsLogId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsLogSQL andDevopsLogIdLessThan(java.lang.Long value) {
          addCriterion("devops_log_id", value, ConditionMode.LESS_THEN, "devopsLogId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsLogSQL andDevopsLogIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("devops_log_id", value, ConditionMode.LESS_EQUAL, "devopsLogId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsLogSQL andDevopsLogIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("devops_log_id", value1, value2, ConditionMode.BETWEEN, "devopsLogId", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsDevopsLogSQL andDevopsLogIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("devops_log_id", value1, value2, ConditionMode.NOT_BETWEEN, "devopsLogId", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsDevopsLogSQL andDevopsLogIdIn(List<java.lang.Long> values) {
          addCriterion("devops_log_id", values, ConditionMode.IN, "devopsLogId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsLogSQL andDevopsLogIdNotIn(List<java.lang.Long> values) {
          addCriterion("devops_log_id", values, ConditionMode.NOT_IN, "devopsLogId", "java.lang.Long", "Float");
          return this;
      }
	public OmsDevopsLogSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public OmsDevopsLogSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public OmsDevopsLogSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public OmsDevopsLogSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public OmsDevopsLogSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public OmsDevopsLogSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public OmsDevopsLogSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsLogSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsLogSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsLogSQL andNodeIsNull() {
		isnull("node");
		return this;
	}
	
	public OmsDevopsLogSQL andNodeIsNotNull() {
		notNull("node");
		return this;
	}
	
	public OmsDevopsLogSQL andNodeIsEmpty() {
		empty("node");
		return this;
	}

	public OmsDevopsLogSQL andNodeIsNotEmpty() {
		notEmpty("node");
		return this;
	}
       public OmsDevopsLogSQL andNodeLike(java.lang.String value) {
    	   addCriterion("node", value, ConditionMode.FUZZY, "node", "java.lang.String", "String");
    	   return this;
      }

      public OmsDevopsLogSQL andNodeNotLike(java.lang.String value) {
          addCriterion("node", value, ConditionMode.NOT_FUZZY, "node", "java.lang.String", "String");
          return this;
      }
      public OmsDevopsLogSQL andNodeEqualTo(java.lang.String value) {
          addCriterion("node", value, ConditionMode.EQUAL, "node", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andNodeNotEqualTo(java.lang.String value) {
          addCriterion("node", value, ConditionMode.NOT_EQUAL, "node", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andNodeGreaterThan(java.lang.String value) {
          addCriterion("node", value, ConditionMode.GREATER_THEN, "node", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andNodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("node", value, ConditionMode.GREATER_EQUAL, "node", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andNodeLessThan(java.lang.String value) {
          addCriterion("node", value, ConditionMode.LESS_THEN, "node", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andNodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("node", value, ConditionMode.LESS_EQUAL, "node", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andNodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("node", value1, value2, ConditionMode.BETWEEN, "node", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsLogSQL andNodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("node", value1, value2, ConditionMode.NOT_BETWEEN, "node", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsLogSQL andNodeIn(List<java.lang.String> values) {
          addCriterion("node", values, ConditionMode.IN, "node", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andNodeNotIn(List<java.lang.String> values) {
          addCriterion("node", values, ConditionMode.NOT_IN, "node", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsLogSQL andLevelIsNull() {
		isnull("level");
		return this;
	}
	
	public OmsDevopsLogSQL andLevelIsNotNull() {
		notNull("level");
		return this;
	}
	
	public OmsDevopsLogSQL andLevelIsEmpty() {
		empty("level");
		return this;
	}

	public OmsDevopsLogSQL andLevelIsNotEmpty() {
		notEmpty("level");
		return this;
	}
       public OmsDevopsLogSQL andLevelLike(java.lang.String value) {
    	   addCriterion("level", value, ConditionMode.FUZZY, "level", "java.lang.String", "String");
    	   return this;
      }

      public OmsDevopsLogSQL andLevelNotLike(java.lang.String value) {
          addCriterion("level", value, ConditionMode.NOT_FUZZY, "level", "java.lang.String", "String");
          return this;
      }
      public OmsDevopsLogSQL andLevelEqualTo(java.lang.String value) {
          addCriterion("level", value, ConditionMode.EQUAL, "level", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andLevelNotEqualTo(java.lang.String value) {
          addCriterion("level", value, ConditionMode.NOT_EQUAL, "level", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andLevelGreaterThan(java.lang.String value) {
          addCriterion("level", value, ConditionMode.GREATER_THEN, "level", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andLevelGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("level", value, ConditionMode.GREATER_EQUAL, "level", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andLevelLessThan(java.lang.String value) {
          addCriterion("level", value, ConditionMode.LESS_THEN, "level", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andLevelLessThanOrEqualTo(java.lang.String value) {
          addCriterion("level", value, ConditionMode.LESS_EQUAL, "level", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andLevelBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("level", value1, value2, ConditionMode.BETWEEN, "level", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsLogSQL andLevelNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("level", value1, value2, ConditionMode.NOT_BETWEEN, "level", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsLogSQL andLevelIn(List<java.lang.String> values) {
          addCriterion("level", values, ConditionMode.IN, "level", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andLevelNotIn(List<java.lang.String> values) {
          addCriterion("level", values, ConditionMode.NOT_IN, "level", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsLogSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public OmsDevopsLogSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public OmsDevopsLogSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public OmsDevopsLogSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
       public OmsDevopsLogSQL andTypeLike(java.lang.String value) {
    	   addCriterion("type", value, ConditionMode.FUZZY, "type", "java.lang.String", "String");
    	   return this;
      }

      public OmsDevopsLogSQL andTypeNotLike(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_FUZZY, "type", "java.lang.String", "String");
          return this;
      }
      public OmsDevopsLogSQL andTypeEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andTypeNotEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andTypeGreaterThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andTypeLessThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsLogSQL andTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsLogSQL andTypeIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andTypeNotIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsLogSQL andLogIsNull() {
		isnull("log");
		return this;
	}
	
	public OmsDevopsLogSQL andLogIsNotNull() {
		notNull("log");
		return this;
	}
	
	public OmsDevopsLogSQL andLogIsEmpty() {
		empty("log");
		return this;
	}

	public OmsDevopsLogSQL andLogIsNotEmpty() {
		notEmpty("log");
		return this;
	}
       public OmsDevopsLogSQL andLogLike(java.lang.String value) {
    	   addCriterion("log", value, ConditionMode.FUZZY, "log", "java.lang.String", "String");
    	   return this;
      }

      public OmsDevopsLogSQL andLogNotLike(java.lang.String value) {
          addCriterion("log", value, ConditionMode.NOT_FUZZY, "log", "java.lang.String", "String");
          return this;
      }
      public OmsDevopsLogSQL andLogEqualTo(java.lang.String value) {
          addCriterion("log", value, ConditionMode.EQUAL, "log", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andLogNotEqualTo(java.lang.String value) {
          addCriterion("log", value, ConditionMode.NOT_EQUAL, "log", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andLogGreaterThan(java.lang.String value) {
          addCriterion("log", value, ConditionMode.GREATER_THEN, "log", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andLogGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("log", value, ConditionMode.GREATER_EQUAL, "log", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andLogLessThan(java.lang.String value) {
          addCriterion("log", value, ConditionMode.LESS_THEN, "log", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andLogLessThanOrEqualTo(java.lang.String value) {
          addCriterion("log", value, ConditionMode.LESS_EQUAL, "log", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andLogBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("log", value1, value2, ConditionMode.BETWEEN, "log", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsLogSQL andLogNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("log", value1, value2, ConditionMode.NOT_BETWEEN, "log", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsLogSQL andLogIn(List<java.lang.String> values) {
          addCriterion("log", values, ConditionMode.IN, "log", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsLogSQL andLogNotIn(List<java.lang.String> values) {
          addCriterion("log", values, ConditionMode.NOT_IN, "log", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsLogSQL andCreatedIsNull() {
		isnull("created");
		return this;
	}
	
	public OmsDevopsLogSQL andCreatedIsNotNull() {
		notNull("created");
		return this;
	}
	
	public OmsDevopsLogSQL andCreatedIsEmpty() {
		empty("created");
		return this;
	}

	public OmsDevopsLogSQL andCreatedIsNotEmpty() {
		notEmpty("created");
		return this;
	}
      public OmsDevopsLogSQL andCreatedEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public OmsDevopsLogSQL andCreatedNotEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.NOT_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public OmsDevopsLogSQL andCreatedGreaterThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public OmsDevopsLogSQL andCreatedGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.GREATER_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public OmsDevopsLogSQL andCreatedLessThan(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_THEN, "created", "java.util.Date", "String");
          return this;
      }

      public OmsDevopsLogSQL andCreatedLessThanOrEqualTo(java.util.Date value) {
          addCriterion("created", value, ConditionMode.LESS_EQUAL, "created", "java.util.Date", "String");
          return this;
      }

      public OmsDevopsLogSQL andCreatedBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("created", value1, value2, ConditionMode.BETWEEN, "created", "java.util.Date", "String");
    	  return this;
      }

      public OmsDevopsLogSQL andCreatedNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("created", value1, value2, ConditionMode.NOT_BETWEEN, "created", "java.util.Date", "String");
          return this;
      }
        
      public OmsDevopsLogSQL andCreatedIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.IN, "created", "java.util.Date", "String");
          return this;
      }

      public OmsDevopsLogSQL andCreatedNotIn(List<java.util.Date> values) {
          addCriterion("created", values, ConditionMode.NOT_IN, "created", "java.util.Date", "String");
          return this;
      }
}