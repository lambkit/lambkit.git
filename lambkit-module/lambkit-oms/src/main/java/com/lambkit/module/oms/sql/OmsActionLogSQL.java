/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsActionLogSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static OmsActionLogSQL of() {
		return new OmsActionLogSQL();
	}
	
	public static OmsActionLogSQL by(Column column) {
		OmsActionLogSQL that = new OmsActionLogSQL();
		that.add(column);
        return that;
    }

    public static OmsActionLogSQL by(String name, Object value) {
        return (OmsActionLogSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("oms_action_log", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public OmsActionLogSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public OmsActionLogSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public OmsActionLogSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public OmsActionLogSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsActionLogSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsActionLogSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsActionLogSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsActionLogSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public OmsActionLogSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public OmsActionLogSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public OmsActionLogSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public OmsActionLogSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public OmsActionLogSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public OmsActionLogSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public OmsActionLogSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public OmsActionLogSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public OmsActionLogSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public OmsActionLogSQL andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsActionLogSQL andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsActionLogSQL andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public OmsActionLogSQL andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public OmsActionLogSQL andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public OmsActionLogSQL andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public OmsActionLogSQL andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
      public OmsActionLogSQL andSystemIdEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andSystemIdNotEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andSystemIdGreaterThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andSystemIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andSystemIdLessThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andSystemIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andSystemIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Integer", "Float");
    	  return this;
      }

      public OmsActionLogSQL andSystemIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
        
      public OmsActionLogSQL andSystemIdIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andSystemIdNotIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
	public OmsActionLogSQL andUpmsUserIdIsNull() {
		isnull("upms_user_id");
		return this;
	}
	
	public OmsActionLogSQL andUpmsUserIdIsNotNull() {
		notNull("upms_user_id");
		return this;
	}
	
	public OmsActionLogSQL andUpmsUserIdIsEmpty() {
		empty("upms_user_id");
		return this;
	}

	public OmsActionLogSQL andUpmsUserIdIsNotEmpty() {
		notEmpty("upms_user_id");
		return this;
	}
      public OmsActionLogSQL andUpmsUserIdEqualTo(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.EQUAL, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andUpmsUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.NOT_EQUAL, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andUpmsUserIdGreaterThan(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.GREATER_THEN, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andUpmsUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.GREATER_EQUAL, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andUpmsUserIdLessThan(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.LESS_THEN, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andUpmsUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.LESS_EQUAL, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andUpmsUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("upms_user_id", value1, value2, ConditionMode.BETWEEN, "upmsUserId", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsActionLogSQL andUpmsUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("upms_user_id", value1, value2, ConditionMode.NOT_BETWEEN, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsActionLogSQL andUpmsUserIdIn(List<java.lang.Long> values) {
          addCriterion("upms_user_id", values, ConditionMode.IN, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andUpmsUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("upms_user_id", values, ConditionMode.NOT_IN, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }
	public OmsActionLogSQL andSessionIdIsNull() {
		isnull("session_id");
		return this;
	}
	
	public OmsActionLogSQL andSessionIdIsNotNull() {
		notNull("session_id");
		return this;
	}
	
	public OmsActionLogSQL andSessionIdIsEmpty() {
		empty("session_id");
		return this;
	}

	public OmsActionLogSQL andSessionIdIsNotEmpty() {
		notEmpty("session_id");
		return this;
	}
       public OmsActionLogSQL andSessionIdLike(java.lang.String value) {
    	   addCriterion("session_id", value, ConditionMode.FUZZY, "sessionId", "java.lang.String", "Float");
    	   return this;
      }

      public OmsActionLogSQL andSessionIdNotLike(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.NOT_FUZZY, "sessionId", "java.lang.String", "Float");
          return this;
      }
      public OmsActionLogSQL andSessionIdEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andSessionIdNotEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.NOT_EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andSessionIdGreaterThan(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.GREATER_THEN, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andSessionIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.GREATER_EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andSessionIdLessThan(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.LESS_THEN, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andSessionIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.LESS_EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andSessionIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("session_id", value1, value2, ConditionMode.BETWEEN, "sessionId", "java.lang.String", "String");
    	  return this;
      }

      public OmsActionLogSQL andSessionIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("session_id", value1, value2, ConditionMode.NOT_BETWEEN, "sessionId", "java.lang.String", "String");
          return this;
      }
        
      public OmsActionLogSQL andSessionIdIn(List<java.lang.String> values) {
          addCriterion("session_id", values, ConditionMode.IN, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andSessionIdNotIn(List<java.lang.String> values) {
          addCriterion("session_id", values, ConditionMode.NOT_IN, "sessionId", "java.lang.String", "String");
          return this;
      }
	public OmsActionLogSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public OmsActionLogSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public OmsActionLogSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public OmsActionLogSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public OmsActionLogSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsActionLogSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsActionLogSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsActionLogSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsActionLogSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsActionLogSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsActionLogSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public OmsActionLogSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public OmsActionLogSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsActionLogSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public OmsActionLogSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public OmsActionLogSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public OmsActionLogSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public OmsActionLogSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
       public OmsActionLogSQL andUserIdLike(java.lang.String value) {
    	   addCriterion("user_id", value, ConditionMode.FUZZY, "userId", "java.lang.String", "String");
    	   return this;
      }

      public OmsActionLogSQL andUserIdNotLike(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.NOT_FUZZY, "userId", "java.lang.String", "String");
          return this;
      }
      public OmsActionLogSQL andUserIdEqualTo(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andUserIdNotEqualTo(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andUserIdGreaterThan(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andUserIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andUserIdLessThan(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andUserIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andUserIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.String", "String");
    	  return this;
      }

      public OmsActionLogSQL andUserIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.String", "String");
          return this;
      }
        
      public OmsActionLogSQL andUserIdIn(List<java.lang.String> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andUserIdNotIn(List<java.lang.String> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.String", "String");
          return this;
      }
	public OmsActionLogSQL andUserRoleIsNull() {
		isnull("user_role");
		return this;
	}
	
	public OmsActionLogSQL andUserRoleIsNotNull() {
		notNull("user_role");
		return this;
	}
	
	public OmsActionLogSQL andUserRoleIsEmpty() {
		empty("user_role");
		return this;
	}

	public OmsActionLogSQL andUserRoleIsNotEmpty() {
		notEmpty("user_role");
		return this;
	}
       public OmsActionLogSQL andUserRoleLike(java.lang.String value) {
    	   addCriterion("user_role", value, ConditionMode.FUZZY, "userRole", "java.lang.String", "String");
    	   return this;
      }

      public OmsActionLogSQL andUserRoleNotLike(java.lang.String value) {
          addCriterion("user_role", value, ConditionMode.NOT_FUZZY, "userRole", "java.lang.String", "String");
          return this;
      }
      public OmsActionLogSQL andUserRoleEqualTo(java.lang.String value) {
          addCriterion("user_role", value, ConditionMode.EQUAL, "userRole", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andUserRoleNotEqualTo(java.lang.String value) {
          addCriterion("user_role", value, ConditionMode.NOT_EQUAL, "userRole", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andUserRoleGreaterThan(java.lang.String value) {
          addCriterion("user_role", value, ConditionMode.GREATER_THEN, "userRole", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andUserRoleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_role", value, ConditionMode.GREATER_EQUAL, "userRole", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andUserRoleLessThan(java.lang.String value) {
          addCriterion("user_role", value, ConditionMode.LESS_THEN, "userRole", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andUserRoleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_role", value, ConditionMode.LESS_EQUAL, "userRole", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andUserRoleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_role", value1, value2, ConditionMode.BETWEEN, "userRole", "java.lang.String", "String");
    	  return this;
      }

      public OmsActionLogSQL andUserRoleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_role", value1, value2, ConditionMode.NOT_BETWEEN, "userRole", "java.lang.String", "String");
          return this;
      }
        
      public OmsActionLogSQL andUserRoleIn(List<java.lang.String> values) {
          addCriterion("user_role", values, ConditionMode.IN, "userRole", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andUserRoleNotIn(List<java.lang.String> values) {
          addCriterion("user_role", values, ConditionMode.NOT_IN, "userRole", "java.lang.String", "String");
          return this;
      }
	public OmsActionLogSQL andAreaCodeIsNull() {
		isnull("area_code");
		return this;
	}
	
	public OmsActionLogSQL andAreaCodeIsNotNull() {
		notNull("area_code");
		return this;
	}
	
	public OmsActionLogSQL andAreaCodeIsEmpty() {
		empty("area_code");
		return this;
	}

	public OmsActionLogSQL andAreaCodeIsNotEmpty() {
		notEmpty("area_code");
		return this;
	}
       public OmsActionLogSQL andAreaCodeLike(java.lang.String value) {
    	   addCriterion("area_code", value, ConditionMode.FUZZY, "areaCode", "java.lang.String", "String");
    	   return this;
      }

      public OmsActionLogSQL andAreaCodeNotLike(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.NOT_FUZZY, "areaCode", "java.lang.String", "String");
          return this;
      }
      public OmsActionLogSQL andAreaCodeEqualTo(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.EQUAL, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andAreaCodeNotEqualTo(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.NOT_EQUAL, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andAreaCodeGreaterThan(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.GREATER_THEN, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andAreaCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.GREATER_EQUAL, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andAreaCodeLessThan(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.LESS_THEN, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andAreaCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.LESS_EQUAL, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andAreaCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("area_code", value1, value2, ConditionMode.BETWEEN, "areaCode", "java.lang.String", "String");
    	  return this;
      }

      public OmsActionLogSQL andAreaCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("area_code", value1, value2, ConditionMode.NOT_BETWEEN, "areaCode", "java.lang.String", "String");
          return this;
      }
        
      public OmsActionLogSQL andAreaCodeIn(List<java.lang.String> values) {
          addCriterion("area_code", values, ConditionMode.IN, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsActionLogSQL andAreaCodeNotIn(List<java.lang.String> values) {
          addCriterion("area_code", values, ConditionMode.NOT_IN, "areaCode", "java.lang.String", "String");
          return this;
      }
	public OmsActionLogSQL andActionTypeIsNull() {
		isnull("action_type");
		return this;
	}
	
	public OmsActionLogSQL andActionTypeIsNotNull() {
		notNull("action_type");
		return this;
	}
	
	public OmsActionLogSQL andActionTypeIsEmpty() {
		empty("action_type");
		return this;
	}

	public OmsActionLogSQL andActionTypeIsNotEmpty() {
		notEmpty("action_type");
		return this;
	}
      public OmsActionLogSQL andActionTypeEqualTo(java.lang.Integer value) {
          addCriterion("action_type", value, ConditionMode.EQUAL, "actionType", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("action_type", value, ConditionMode.NOT_EQUAL, "actionType", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionTypeGreaterThan(java.lang.Integer value) {
          addCriterion("action_type", value, ConditionMode.GREATER_THEN, "actionType", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("action_type", value, ConditionMode.GREATER_EQUAL, "actionType", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionTypeLessThan(java.lang.Integer value) {
          addCriterion("action_type", value, ConditionMode.LESS_THEN, "actionType", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("action_type", value, ConditionMode.LESS_EQUAL, "actionType", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("action_type", value1, value2, ConditionMode.BETWEEN, "actionType", "java.lang.Integer", "Float");
    	  return this;
      }

      public OmsActionLogSQL andActionTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("action_type", value1, value2, ConditionMode.NOT_BETWEEN, "actionType", "java.lang.Integer", "Float");
          return this;
      }
        
      public OmsActionLogSQL andActionTypeIn(List<java.lang.Integer> values) {
          addCriterion("action_type", values, ConditionMode.IN, "actionType", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("action_type", values, ConditionMode.NOT_IN, "actionType", "java.lang.Integer", "Float");
          return this;
      }
	public OmsActionLogSQL andActionIdIsNull() {
		isnull("action_id");
		return this;
	}
	
	public OmsActionLogSQL andActionIdIsNotNull() {
		notNull("action_id");
		return this;
	}
	
	public OmsActionLogSQL andActionIdIsEmpty() {
		empty("action_id");
		return this;
	}

	public OmsActionLogSQL andActionIdIsNotEmpty() {
		notEmpty("action_id");
		return this;
	}
      public OmsActionLogSQL andActionIdEqualTo(java.lang.Integer value) {
          addCriterion("action_id", value, ConditionMode.EQUAL, "actionId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionIdNotEqualTo(java.lang.Integer value) {
          addCriterion("action_id", value, ConditionMode.NOT_EQUAL, "actionId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionIdGreaterThan(java.lang.Integer value) {
          addCriterion("action_id", value, ConditionMode.GREATER_THEN, "actionId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("action_id", value, ConditionMode.GREATER_EQUAL, "actionId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionIdLessThan(java.lang.Integer value) {
          addCriterion("action_id", value, ConditionMode.LESS_THEN, "actionId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("action_id", value, ConditionMode.LESS_EQUAL, "actionId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("action_id", value1, value2, ConditionMode.BETWEEN, "actionId", "java.lang.Integer", "Float");
    	  return this;
      }

      public OmsActionLogSQL andActionIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("action_id", value1, value2, ConditionMode.NOT_BETWEEN, "actionId", "java.lang.Integer", "Float");
          return this;
      }
        
      public OmsActionLogSQL andActionIdIn(List<java.lang.Integer> values) {
          addCriterion("action_id", values, ConditionMode.IN, "actionId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionIdNotIn(List<java.lang.Integer> values) {
          addCriterion("action_id", values, ConditionMode.NOT_IN, "actionId", "java.lang.Integer", "Float");
          return this;
      }
	public OmsActionLogSQL andActionTimeIsNull() {
		isnull("action_time");
		return this;
	}
	
	public OmsActionLogSQL andActionTimeIsNotNull() {
		notNull("action_time");
		return this;
	}
	
	public OmsActionLogSQL andActionTimeIsEmpty() {
		empty("action_time");
		return this;
	}

	public OmsActionLogSQL andActionTimeIsNotEmpty() {
		notEmpty("action_time");
		return this;
	}
      public OmsActionLogSQL andActionTimeEqualTo(java.util.Date value) {
          addCriterion("action_time", value, ConditionMode.EQUAL, "actionTime", "java.util.Date", "String");
          return this;
      }

      public OmsActionLogSQL andActionTimeNotEqualTo(java.util.Date value) {
          addCriterion("action_time", value, ConditionMode.NOT_EQUAL, "actionTime", "java.util.Date", "String");
          return this;
      }

      public OmsActionLogSQL andActionTimeGreaterThan(java.util.Date value) {
          addCriterion("action_time", value, ConditionMode.GREATER_THEN, "actionTime", "java.util.Date", "String");
          return this;
      }

      public OmsActionLogSQL andActionTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("action_time", value, ConditionMode.GREATER_EQUAL, "actionTime", "java.util.Date", "String");
          return this;
      }

      public OmsActionLogSQL andActionTimeLessThan(java.util.Date value) {
          addCriterion("action_time", value, ConditionMode.LESS_THEN, "actionTime", "java.util.Date", "String");
          return this;
      }

      public OmsActionLogSQL andActionTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("action_time", value, ConditionMode.LESS_EQUAL, "actionTime", "java.util.Date", "String");
          return this;
      }

      public OmsActionLogSQL andActionTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("action_time", value1, value2, ConditionMode.BETWEEN, "actionTime", "java.util.Date", "String");
    	  return this;
      }

      public OmsActionLogSQL andActionTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("action_time", value1, value2, ConditionMode.NOT_BETWEEN, "actionTime", "java.util.Date", "String");
          return this;
      }
        
      public OmsActionLogSQL andActionTimeIn(List<java.util.Date> values) {
          addCriterion("action_time", values, ConditionMode.IN, "actionTime", "java.util.Date", "String");
          return this;
      }

      public OmsActionLogSQL andActionTimeNotIn(List<java.util.Date> values) {
          addCriterion("action_time", values, ConditionMode.NOT_IN, "actionTime", "java.util.Date", "String");
          return this;
      }
	public OmsActionLogSQL andActionDurationIsNull() {
		isnull("action_duration");
		return this;
	}
	
	public OmsActionLogSQL andActionDurationIsNotNull() {
		notNull("action_duration");
		return this;
	}
	
	public OmsActionLogSQL andActionDurationIsEmpty() {
		empty("action_duration");
		return this;
	}

	public OmsActionLogSQL andActionDurationIsNotEmpty() {
		notEmpty("action_duration");
		return this;
	}
      public OmsActionLogSQL andActionDurationEqualTo(java.lang.Integer value) {
          addCriterion("action_duration", value, ConditionMode.EQUAL, "actionDuration", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionDurationNotEqualTo(java.lang.Integer value) {
          addCriterion("action_duration", value, ConditionMode.NOT_EQUAL, "actionDuration", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionDurationGreaterThan(java.lang.Integer value) {
          addCriterion("action_duration", value, ConditionMode.GREATER_THEN, "actionDuration", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionDurationGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("action_duration", value, ConditionMode.GREATER_EQUAL, "actionDuration", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionDurationLessThan(java.lang.Integer value) {
          addCriterion("action_duration", value, ConditionMode.LESS_THEN, "actionDuration", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionDurationLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("action_duration", value, ConditionMode.LESS_EQUAL, "actionDuration", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionDurationBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("action_duration", value1, value2, ConditionMode.BETWEEN, "actionDuration", "java.lang.Integer", "Float");
    	  return this;
      }

      public OmsActionLogSQL andActionDurationNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("action_duration", value1, value2, ConditionMode.NOT_BETWEEN, "actionDuration", "java.lang.Integer", "Float");
          return this;
      }
        
      public OmsActionLogSQL andActionDurationIn(List<java.lang.Integer> values) {
          addCriterion("action_duration", values, ConditionMode.IN, "actionDuration", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionDurationNotIn(List<java.lang.Integer> values) {
          addCriterion("action_duration", values, ConditionMode.NOT_IN, "actionDuration", "java.lang.Integer", "Float");
          return this;
      }
	public OmsActionLogSQL andActionStatusIsNull() {
		isnull("action_status");
		return this;
	}
	
	public OmsActionLogSQL andActionStatusIsNotNull() {
		notNull("action_status");
		return this;
	}
	
	public OmsActionLogSQL andActionStatusIsEmpty() {
		empty("action_status");
		return this;
	}

	public OmsActionLogSQL andActionStatusIsNotEmpty() {
		notEmpty("action_status");
		return this;
	}
      public OmsActionLogSQL andActionStatusEqualTo(java.lang.Integer value) {
          addCriterion("action_status", value, ConditionMode.EQUAL, "actionStatus", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("action_status", value, ConditionMode.NOT_EQUAL, "actionStatus", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionStatusGreaterThan(java.lang.Integer value) {
          addCriterion("action_status", value, ConditionMode.GREATER_THEN, "actionStatus", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("action_status", value, ConditionMode.GREATER_EQUAL, "actionStatus", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionStatusLessThan(java.lang.Integer value) {
          addCriterion("action_status", value, ConditionMode.LESS_THEN, "actionStatus", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("action_status", value, ConditionMode.LESS_EQUAL, "actionStatus", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("action_status", value1, value2, ConditionMode.BETWEEN, "actionStatus", "java.lang.Integer", "Float");
    	  return this;
      }

      public OmsActionLogSQL andActionStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("action_status", value1, value2, ConditionMode.NOT_BETWEEN, "actionStatus", "java.lang.Integer", "Float");
          return this;
      }
        
      public OmsActionLogSQL andActionStatusIn(List<java.lang.Integer> values) {
          addCriterion("action_status", values, ConditionMode.IN, "actionStatus", "java.lang.Integer", "Float");
          return this;
      }

      public OmsActionLogSQL andActionStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("action_status", values, ConditionMode.NOT_IN, "actionStatus", "java.lang.Integer", "Float");
          return this;
      }
	public OmsActionLogSQL andBusinessIdIsNull() {
		isnull("business_id");
		return this;
	}
	
	public OmsActionLogSQL andBusinessIdIsNotNull() {
		notNull("business_id");
		return this;
	}
	
	public OmsActionLogSQL andBusinessIdIsEmpty() {
		empty("business_id");
		return this;
	}

	public OmsActionLogSQL andBusinessIdIsNotEmpty() {
		notEmpty("business_id");
		return this;
	}
      public OmsActionLogSQL andBusinessIdEqualTo(java.lang.Long value) {
          addCriterion("business_id", value, ConditionMode.EQUAL, "businessId", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andBusinessIdNotEqualTo(java.lang.Long value) {
          addCriterion("business_id", value, ConditionMode.NOT_EQUAL, "businessId", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andBusinessIdGreaterThan(java.lang.Long value) {
          addCriterion("business_id", value, ConditionMode.GREATER_THEN, "businessId", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andBusinessIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("business_id", value, ConditionMode.GREATER_EQUAL, "businessId", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andBusinessIdLessThan(java.lang.Long value) {
          addCriterion("business_id", value, ConditionMode.LESS_THEN, "businessId", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andBusinessIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("business_id", value, ConditionMode.LESS_EQUAL, "businessId", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andBusinessIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("business_id", value1, value2, ConditionMode.BETWEEN, "businessId", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsActionLogSQL andBusinessIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("business_id", value1, value2, ConditionMode.NOT_BETWEEN, "businessId", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsActionLogSQL andBusinessIdIn(List<java.lang.Long> values) {
          addCriterion("business_id", values, ConditionMode.IN, "businessId", "java.lang.Long", "Float");
          return this;
      }

      public OmsActionLogSQL andBusinessIdNotIn(List<java.lang.Long> values) {
          addCriterion("business_id", values, ConditionMode.NOT_IN, "businessId", "java.lang.Long", "Float");
          return this;
      }
}