/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsActionLog extends RowModel<OmsActionLog> {
	public OmsActionLog() {
		setTableName("oms_action_log");
		setPrimaryKey("id");
	}
	public java.lang.Long getId() {
		return this.get("id");
	}
	public void setId(java.lang.Long id) {
		this.set("id", id);
	}
	public java.lang.Integer getSystemId() {
		return this.get("system_id");
	}
	public void setSystemId(java.lang.Integer systemId) {
		this.set("system_id", systemId);
	}
	public java.lang.Long getUpmsUserId() {
		return this.get("upms_user_id");
	}
	public void setUpmsUserId(java.lang.Long upmsUserId) {
		this.set("upms_user_id", upmsUserId);
	}
	public java.lang.String getSessionId() {
		return this.get("session_id");
	}
	public void setSessionId(java.lang.String sessionId) {
		this.set("session_id", sessionId);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
	public java.lang.String getUserId() {
		return this.get("user_id");
	}
	public void setUserId(java.lang.String userId) {
		this.set("user_id", userId);
	}
	public java.lang.String getUserRole() {
		return this.get("user_role");
	}
	public void setUserRole(java.lang.String userRole) {
		this.set("user_role", userRole);
	}
	public java.lang.String getAreaCode() {
		return this.get("area_code");
	}
	public void setAreaCode(java.lang.String areaCode) {
		this.set("area_code", areaCode);
	}
	public java.lang.Integer getActionType() {
		return this.get("action_type");
	}
	public void setActionType(java.lang.Integer actionType) {
		this.set("action_type", actionType);
	}
	public java.lang.Integer getActionId() {
		return this.get("action_id");
	}
	public void setActionId(java.lang.Integer actionId) {
		this.set("action_id", actionId);
	}
	public java.util.Date getActionTime() {
		return this.get("action_time");
	}
	public void setActionTime(java.util.Date actionTime) {
		this.set("action_time", actionTime);
	}
	public java.lang.Integer getActionDuration() {
		return this.get("action_duration");
	}
	public void setActionDuration(java.lang.Integer actionDuration) {
		this.set("action_duration", actionDuration);
	}
	public java.lang.Integer getActionStatus() {
		return this.get("action_status");
	}
	public void setActionStatus(java.lang.Integer actionStatus) {
		this.set("action_status", actionStatus);
	}
	public java.lang.Long getBusinessId() {
		return this.get("business_id");
	}
	public void setBusinessId(java.lang.Long businessId) {
		this.set("business_id", businessId);
	}
}
