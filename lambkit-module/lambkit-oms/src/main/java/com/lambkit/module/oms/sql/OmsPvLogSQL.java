/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsPvLogSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static OmsPvLogSQL of() {
		return new OmsPvLogSQL();
	}
	
	public static OmsPvLogSQL by(Column column) {
		OmsPvLogSQL that = new OmsPvLogSQL();
		that.add(column);
        return that;
    }

    public static OmsPvLogSQL by(String name, Object value) {
        return (OmsPvLogSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("oms_pv_log", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public OmsPvLogSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public OmsPvLogSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public OmsPvLogSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public OmsPvLogSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsPvLogSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsPvLogSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsPvLogSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsPvLogSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public OmsPvLogSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public OmsPvLogSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public OmsPvLogSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public OmsPvLogSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public OmsPvLogSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public OmsPvLogSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public OmsPvLogSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public OmsPvLogSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public OmsPvLogSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public OmsPvLogSQL andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsPvLogSQL andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsPvLogSQL andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsPvLogSQL andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsPvLogSQL andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsPvLogSQL andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsPvLogSQL andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsPvLogSQL andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsPvLogSQL andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public OmsPvLogSQL andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public OmsPvLogSQL andUpmsUserIdIsNull() {
		isnull("upms_user_id");
		return this;
	}
	
	public OmsPvLogSQL andUpmsUserIdIsNotNull() {
		notNull("upms_user_id");
		return this;
	}
	
	public OmsPvLogSQL andUpmsUserIdIsEmpty() {
		empty("upms_user_id");
		return this;
	}

	public OmsPvLogSQL andUpmsUserIdIsNotEmpty() {
		notEmpty("upms_user_id");
		return this;
	}
      public OmsPvLogSQL andUpmsUserIdEqualTo(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.EQUAL, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsPvLogSQL andUpmsUserIdNotEqualTo(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.NOT_EQUAL, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsPvLogSQL andUpmsUserIdGreaterThan(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.GREATER_THEN, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsPvLogSQL andUpmsUserIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.GREATER_EQUAL, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsPvLogSQL andUpmsUserIdLessThan(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.LESS_THEN, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsPvLogSQL andUpmsUserIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("upms_user_id", value, ConditionMode.LESS_EQUAL, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsPvLogSQL andUpmsUserIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("upms_user_id", value1, value2, ConditionMode.BETWEEN, "upmsUserId", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsPvLogSQL andUpmsUserIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("upms_user_id", value1, value2, ConditionMode.NOT_BETWEEN, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsPvLogSQL andUpmsUserIdIn(List<java.lang.Long> values) {
          addCriterion("upms_user_id", values, ConditionMode.IN, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }

      public OmsPvLogSQL andUpmsUserIdNotIn(List<java.lang.Long> values) {
          addCriterion("upms_user_id", values, ConditionMode.NOT_IN, "upmsUserId", "java.lang.Long", "Float");
          return this;
      }
	public OmsPvLogSQL andSessionIdIsNull() {
		isnull("session_id");
		return this;
	}
	
	public OmsPvLogSQL andSessionIdIsNotNull() {
		notNull("session_id");
		return this;
	}
	
	public OmsPvLogSQL andSessionIdIsEmpty() {
		empty("session_id");
		return this;
	}

	public OmsPvLogSQL andSessionIdIsNotEmpty() {
		notEmpty("session_id");
		return this;
	}
       public OmsPvLogSQL andSessionIdLike(java.lang.String value) {
    	   addCriterion("session_id", value, ConditionMode.FUZZY, "sessionId", "java.lang.String", "Float");
    	   return this;
      }

      public OmsPvLogSQL andSessionIdNotLike(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.NOT_FUZZY, "sessionId", "java.lang.String", "Float");
          return this;
      }
      public OmsPvLogSQL andSessionIdEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andSessionIdNotEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.NOT_EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andSessionIdGreaterThan(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.GREATER_THEN, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andSessionIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.GREATER_EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andSessionIdLessThan(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.LESS_THEN, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andSessionIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("session_id", value, ConditionMode.LESS_EQUAL, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andSessionIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("session_id", value1, value2, ConditionMode.BETWEEN, "sessionId", "java.lang.String", "String");
    	  return this;
      }

      public OmsPvLogSQL andSessionIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("session_id", value1, value2, ConditionMode.NOT_BETWEEN, "sessionId", "java.lang.String", "String");
          return this;
      }
        
      public OmsPvLogSQL andSessionIdIn(List<java.lang.String> values) {
          addCriterion("session_id", values, ConditionMode.IN, "sessionId", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andSessionIdNotIn(List<java.lang.String> values) {
          addCriterion("session_id", values, ConditionMode.NOT_IN, "sessionId", "java.lang.String", "String");
          return this;
      }
	public OmsPvLogSQL andUserIdIsNull() {
		isnull("user_id");
		return this;
	}
	
	public OmsPvLogSQL andUserIdIsNotNull() {
		notNull("user_id");
		return this;
	}
	
	public OmsPvLogSQL andUserIdIsEmpty() {
		empty("user_id");
		return this;
	}

	public OmsPvLogSQL andUserIdIsNotEmpty() {
		notEmpty("user_id");
		return this;
	}
       public OmsPvLogSQL andUserIdLike(java.lang.String value) {
    	   addCriterion("user_id", value, ConditionMode.FUZZY, "userId", "java.lang.String", "String");
    	   return this;
      }

      public OmsPvLogSQL andUserIdNotLike(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.NOT_FUZZY, "userId", "java.lang.String", "String");
          return this;
      }
      public OmsPvLogSQL andUserIdEqualTo(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.EQUAL, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andUserIdNotEqualTo(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.NOT_EQUAL, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andUserIdGreaterThan(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.GREATER_THEN, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andUserIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.GREATER_EQUAL, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andUserIdLessThan(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.LESS_THEN, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andUserIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_id", value, ConditionMode.LESS_EQUAL, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andUserIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_id", value1, value2, ConditionMode.BETWEEN, "userId", "java.lang.String", "String");
    	  return this;
      }

      public OmsPvLogSQL andUserIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_id", value1, value2, ConditionMode.NOT_BETWEEN, "userId", "java.lang.String", "String");
          return this;
      }
        
      public OmsPvLogSQL andUserIdIn(List<java.lang.String> values) {
          addCriterion("user_id", values, ConditionMode.IN, "userId", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andUserIdNotIn(List<java.lang.String> values) {
          addCriterion("user_id", values, ConditionMode.NOT_IN, "userId", "java.lang.String", "String");
          return this;
      }
	public OmsPvLogSQL andViewPathIsNull() {
		isnull("view_path");
		return this;
	}
	
	public OmsPvLogSQL andViewPathIsNotNull() {
		notNull("view_path");
		return this;
	}
	
	public OmsPvLogSQL andViewPathIsEmpty() {
		empty("view_path");
		return this;
	}

	public OmsPvLogSQL andViewPathIsNotEmpty() {
		notEmpty("view_path");
		return this;
	}
       public OmsPvLogSQL andViewPathLike(java.lang.String value) {
    	   addCriterion("view_path", value, ConditionMode.FUZZY, "viewPath", "java.lang.String", "String");
    	   return this;
      }

      public OmsPvLogSQL andViewPathNotLike(java.lang.String value) {
          addCriterion("view_path", value, ConditionMode.NOT_FUZZY, "viewPath", "java.lang.String", "String");
          return this;
      }
      public OmsPvLogSQL andViewPathEqualTo(java.lang.String value) {
          addCriterion("view_path", value, ConditionMode.EQUAL, "viewPath", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andViewPathNotEqualTo(java.lang.String value) {
          addCriterion("view_path", value, ConditionMode.NOT_EQUAL, "viewPath", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andViewPathGreaterThan(java.lang.String value) {
          addCriterion("view_path", value, ConditionMode.GREATER_THEN, "viewPath", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andViewPathGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("view_path", value, ConditionMode.GREATER_EQUAL, "viewPath", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andViewPathLessThan(java.lang.String value) {
          addCriterion("view_path", value, ConditionMode.LESS_THEN, "viewPath", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andViewPathLessThanOrEqualTo(java.lang.String value) {
          addCriterion("view_path", value, ConditionMode.LESS_EQUAL, "viewPath", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andViewPathBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("view_path", value1, value2, ConditionMode.BETWEEN, "viewPath", "java.lang.String", "String");
    	  return this;
      }

      public OmsPvLogSQL andViewPathNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("view_path", value1, value2, ConditionMode.NOT_BETWEEN, "viewPath", "java.lang.String", "String");
          return this;
      }
        
      public OmsPvLogSQL andViewPathIn(List<java.lang.String> values) {
          addCriterion("view_path", values, ConditionMode.IN, "viewPath", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andViewPathNotIn(List<java.lang.String> values) {
          addCriterion("view_path", values, ConditionMode.NOT_IN, "viewPath", "java.lang.String", "String");
          return this;
      }
	public OmsPvLogSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public OmsPvLogSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public OmsPvLogSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public OmsPvLogSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public OmsPvLogSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsPvLogSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsPvLogSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsPvLogSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsPvLogSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsPvLogSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsPvLogSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public OmsPvLogSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public OmsPvLogSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public OmsPvLogSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public OmsPvLogSQL andAreaCodeIsNull() {
		isnull("area_code");
		return this;
	}
	
	public OmsPvLogSQL andAreaCodeIsNotNull() {
		notNull("area_code");
		return this;
	}
	
	public OmsPvLogSQL andAreaCodeIsEmpty() {
		empty("area_code");
		return this;
	}

	public OmsPvLogSQL andAreaCodeIsNotEmpty() {
		notEmpty("area_code");
		return this;
	}
       public OmsPvLogSQL andAreaCodeLike(java.lang.String value) {
    	   addCriterion("area_code", value, ConditionMode.FUZZY, "areaCode", "java.lang.String", "String");
    	   return this;
      }

      public OmsPvLogSQL andAreaCodeNotLike(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.NOT_FUZZY, "areaCode", "java.lang.String", "String");
          return this;
      }
      public OmsPvLogSQL andAreaCodeEqualTo(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.EQUAL, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andAreaCodeNotEqualTo(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.NOT_EQUAL, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andAreaCodeGreaterThan(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.GREATER_THEN, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andAreaCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.GREATER_EQUAL, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andAreaCodeLessThan(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.LESS_THEN, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andAreaCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("area_code", value, ConditionMode.LESS_EQUAL, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andAreaCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("area_code", value1, value2, ConditionMode.BETWEEN, "areaCode", "java.lang.String", "String");
    	  return this;
      }

      public OmsPvLogSQL andAreaCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("area_code", value1, value2, ConditionMode.NOT_BETWEEN, "areaCode", "java.lang.String", "String");
          return this;
      }
        
      public OmsPvLogSQL andAreaCodeIn(List<java.lang.String> values) {
          addCriterion("area_code", values, ConditionMode.IN, "areaCode", "java.lang.String", "String");
          return this;
      }

      public OmsPvLogSQL andAreaCodeNotIn(List<java.lang.String> values) {
          addCriterion("area_code", values, ConditionMode.NOT_IN, "areaCode", "java.lang.String", "String");
          return this;
      }
	public OmsPvLogSQL andSystemIdIsNull() {
		isnull("system_id");
		return this;
	}
	
	public OmsPvLogSQL andSystemIdIsNotNull() {
		notNull("system_id");
		return this;
	}
	
	public OmsPvLogSQL andSystemIdIsEmpty() {
		empty("system_id");
		return this;
	}

	public OmsPvLogSQL andSystemIdIsNotEmpty() {
		notEmpty("system_id");
		return this;
	}
      public OmsPvLogSQL andSystemIdEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsPvLogSQL andSystemIdNotEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.NOT_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsPvLogSQL andSystemIdGreaterThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsPvLogSQL andSystemIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.GREATER_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsPvLogSQL andSystemIdLessThan(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_THEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsPvLogSQL andSystemIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("system_id", value, ConditionMode.LESS_EQUAL, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsPvLogSQL andSystemIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("system_id", value1, value2, ConditionMode.BETWEEN, "systemId", "java.lang.Integer", "Float");
    	  return this;
      }

      public OmsPvLogSQL andSystemIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("system_id", value1, value2, ConditionMode.NOT_BETWEEN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
        
      public OmsPvLogSQL andSystemIdIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }

      public OmsPvLogSQL andSystemIdNotIn(List<java.lang.Integer> values) {
          addCriterion("system_id", values, ConditionMode.NOT_IN, "systemId", "java.lang.Integer", "Float");
          return this;
      }
}