/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsLog extends RowModel<OmsLog> {
	public OmsLog() {
		setTableName("oms_log");
		setPrimaryKey("log_id");
	}
	public java.lang.Integer getLogId() {
		return this.get("log_id");
	}
	public void setLogId(java.lang.Integer logId) {
		this.set("log_id", logId);
	}
	public java.lang.String getDescription() {
		return this.get("description");
	}
	public void setDescription(java.lang.String description) {
		this.set("description", description);
	}
	public java.lang.String getAction() {
		return this.get("action");
	}
	public void setAction(java.lang.String action) {
		this.set("action", action);
	}
	public java.lang.String getFtable() {
		return this.get("ftable");
	}
	public void setFtable(java.lang.String ftable) {
		this.set("ftable", ftable);
	}
	public java.lang.String getFcolumn() {
		return this.get("fcolumn");
	}
	public void setFcolumn(java.lang.String fcolumn) {
		this.set("fcolumn", fcolumn);
	}
	public java.util.Date getFtime() {
		return this.get("ftime");
	}
	public void setFtime(java.util.Date ftime) {
		this.set("ftime", ftime);
	}
	public java.lang.String getUsername() {
		return this.get("username");
	}
	public void setUsername(java.lang.String username) {
		this.set("username", username);
	}
}
