/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsDevopsServerSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static OmsDevopsServerSQL of() {
		return new OmsDevopsServerSQL();
	}
	
	public static OmsDevopsServerSQL by(Column column) {
		OmsDevopsServerSQL that = new OmsDevopsServerSQL();
		that.add(column);
        return that;
    }

    public static OmsDevopsServerSQL by(String name, Object value) {
        return (OmsDevopsServerSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("oms_devops_server", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsServerSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsServerSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public OmsDevopsServerSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public OmsDevopsServerSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsServerSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsServerSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsServerSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public OmsDevopsServerSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public OmsDevopsServerSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public OmsDevopsServerSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public OmsDevopsServerSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public OmsDevopsServerSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public OmsDevopsServerSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public OmsDevopsServerSQL andDevopsServerIdIsNull() {
		isnull("devops_server_id");
		return this;
	}
	
	public OmsDevopsServerSQL andDevopsServerIdIsNotNull() {
		notNull("devops_server_id");
		return this;
	}
	
	public OmsDevopsServerSQL andDevopsServerIdIsEmpty() {
		empty("devops_server_id");
		return this;
	}

	public OmsDevopsServerSQL andDevopsServerIdIsNotEmpty() {
		notEmpty("devops_server_id");
		return this;
	}
      public OmsDevopsServerSQL andDevopsServerIdEqualTo(java.lang.Long value) {
          addCriterion("devops_server_id", value, ConditionMode.EQUAL, "devopsServerId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsServerSQL andDevopsServerIdNotEqualTo(java.lang.Long value) {
          addCriterion("devops_server_id", value, ConditionMode.NOT_EQUAL, "devopsServerId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsServerSQL andDevopsServerIdGreaterThan(java.lang.Long value) {
          addCriterion("devops_server_id", value, ConditionMode.GREATER_THEN, "devopsServerId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsServerSQL andDevopsServerIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("devops_server_id", value, ConditionMode.GREATER_EQUAL, "devopsServerId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsServerSQL andDevopsServerIdLessThan(java.lang.Long value) {
          addCriterion("devops_server_id", value, ConditionMode.LESS_THEN, "devopsServerId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsServerSQL andDevopsServerIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("devops_server_id", value, ConditionMode.LESS_EQUAL, "devopsServerId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsServerSQL andDevopsServerIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("devops_server_id", value1, value2, ConditionMode.BETWEEN, "devopsServerId", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsDevopsServerSQL andDevopsServerIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("devops_server_id", value1, value2, ConditionMode.NOT_BETWEEN, "devopsServerId", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsDevopsServerSQL andDevopsServerIdIn(List<java.lang.Long> values) {
          addCriterion("devops_server_id", values, ConditionMode.IN, "devopsServerId", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsServerSQL andDevopsServerIdNotIn(List<java.lang.Long> values) {
          addCriterion("devops_server_id", values, ConditionMode.NOT_IN, "devopsServerId", "java.lang.Long", "Float");
          return this;
      }
	public OmsDevopsServerSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public OmsDevopsServerSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public OmsDevopsServerSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public OmsDevopsServerSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public OmsDevopsServerSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public OmsDevopsServerSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public OmsDevopsServerSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsServerSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsServerSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsServerSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public OmsDevopsServerSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public OmsDevopsServerSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public OmsDevopsServerSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
       public OmsDevopsServerSQL andTypeLike(java.lang.String value) {
    	   addCriterion("type", value, ConditionMode.FUZZY, "type", "java.lang.String", "String");
    	   return this;
      }

      public OmsDevopsServerSQL andTypeNotLike(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_FUZZY, "type", "java.lang.String", "String");
          return this;
      }
      public OmsDevopsServerSQL andTypeEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andTypeNotEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andTypeGreaterThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andTypeLessThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsServerSQL andTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsServerSQL andTypeIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andTypeNotIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsServerSQL andCreatedIsNull() {
		isnull("created");
		return this;
	}
	
	public OmsDevopsServerSQL andCreatedIsNotNull() {
		notNull("created");
		return this;
	}
	
	public OmsDevopsServerSQL andCreatedIsEmpty() {
		empty("created");
		return this;
	}

	public OmsDevopsServerSQL andCreatedIsNotEmpty() {
		notEmpty("created");
		return this;
	}
      public OmsDevopsServerSQL andCreatedEqualTo(java.lang.Long value) {
          addCriterion("created", value, ConditionMode.EQUAL, "created", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsServerSQL andCreatedNotEqualTo(java.lang.Long value) {
          addCriterion("created", value, ConditionMode.NOT_EQUAL, "created", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsServerSQL andCreatedGreaterThan(java.lang.Long value) {
          addCriterion("created", value, ConditionMode.GREATER_THEN, "created", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsServerSQL andCreatedGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("created", value, ConditionMode.GREATER_EQUAL, "created", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsServerSQL andCreatedLessThan(java.lang.Long value) {
          addCriterion("created", value, ConditionMode.LESS_THEN, "created", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsServerSQL andCreatedLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("created", value, ConditionMode.LESS_EQUAL, "created", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsServerSQL andCreatedBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("created", value1, value2, ConditionMode.BETWEEN, "created", "java.lang.Long", "Float");
    	  return this;
      }

      public OmsDevopsServerSQL andCreatedNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("created", value1, value2, ConditionMode.NOT_BETWEEN, "created", "java.lang.Long", "Float");
          return this;
      }
        
      public OmsDevopsServerSQL andCreatedIn(List<java.lang.Long> values) {
          addCriterion("created", values, ConditionMode.IN, "created", "java.lang.Long", "Float");
          return this;
      }

      public OmsDevopsServerSQL andCreatedNotIn(List<java.lang.Long> values) {
          addCriterion("created", values, ConditionMode.NOT_IN, "created", "java.lang.Long", "Float");
          return this;
      }
	public OmsDevopsServerSQL andRecordIsNull() {
		isnull("record");
		return this;
	}
	
	public OmsDevopsServerSQL andRecordIsNotNull() {
		notNull("record");
		return this;
	}
	
	public OmsDevopsServerSQL andRecordIsEmpty() {
		empty("record");
		return this;
	}

	public OmsDevopsServerSQL andRecordIsNotEmpty() {
		notEmpty("record");
		return this;
	}
       public OmsDevopsServerSQL andRecordLike(java.lang.String value) {
    	   addCriterion("record", value, ConditionMode.FUZZY, "record", "java.lang.String", "Float");
    	   return this;
      }

      public OmsDevopsServerSQL andRecordNotLike(java.lang.String value) {
          addCriterion("record", value, ConditionMode.NOT_FUZZY, "record", "java.lang.String", "Float");
          return this;
      }
      public OmsDevopsServerSQL andRecordEqualTo(java.lang.String value) {
          addCriterion("record", value, ConditionMode.EQUAL, "record", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andRecordNotEqualTo(java.lang.String value) {
          addCriterion("record", value, ConditionMode.NOT_EQUAL, "record", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andRecordGreaterThan(java.lang.String value) {
          addCriterion("record", value, ConditionMode.GREATER_THEN, "record", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andRecordGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("record", value, ConditionMode.GREATER_EQUAL, "record", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andRecordLessThan(java.lang.String value) {
          addCriterion("record", value, ConditionMode.LESS_THEN, "record", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andRecordLessThanOrEqualTo(java.lang.String value) {
          addCriterion("record", value, ConditionMode.LESS_EQUAL, "record", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andRecordBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("record", value1, value2, ConditionMode.BETWEEN, "record", "java.lang.String", "String");
    	  return this;
      }

      public OmsDevopsServerSQL andRecordNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("record", value1, value2, ConditionMode.NOT_BETWEEN, "record", "java.lang.String", "String");
          return this;
      }
        
      public OmsDevopsServerSQL andRecordIn(List<java.lang.String> values) {
          addCriterion("record", values, ConditionMode.IN, "record", "java.lang.String", "String");
          return this;
      }

      public OmsDevopsServerSQL andRecordNotIn(List<java.lang.String> values) {
          addCriterion("record", values, ConditionMode.NOT_IN, "record", "java.lang.String", "String");
          return this;
      }
	public OmsDevopsServerSQL andCpuIsNull() {
		isnull("cpu");
		return this;
	}
	
	public OmsDevopsServerSQL andCpuIsNotNull() {
		notNull("cpu");
		return this;
	}
	
	public OmsDevopsServerSQL andCpuIsEmpty() {
		empty("cpu");
		return this;
	}

	public OmsDevopsServerSQL andCpuIsNotEmpty() {
		notEmpty("cpu");
		return this;
	}
      public OmsDevopsServerSQL andCpuEqualTo(java.lang.Float value) {
          addCriterion("cpu", value, ConditionMode.EQUAL, "cpu", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andCpuNotEqualTo(java.lang.Float value) {
          addCriterion("cpu", value, ConditionMode.NOT_EQUAL, "cpu", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andCpuGreaterThan(java.lang.Float value) {
          addCriterion("cpu", value, ConditionMode.GREATER_THEN, "cpu", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andCpuGreaterThanOrEqualTo(java.lang.Float value) {
          addCriterion("cpu", value, ConditionMode.GREATER_EQUAL, "cpu", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andCpuLessThan(java.lang.Float value) {
          addCriterion("cpu", value, ConditionMode.LESS_THEN, "cpu", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andCpuLessThanOrEqualTo(java.lang.Float value) {
          addCriterion("cpu", value, ConditionMode.LESS_EQUAL, "cpu", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andCpuBetween(java.lang.Float value1, java.lang.Float value2) {
    	  addCriterion("cpu", value1, value2, ConditionMode.BETWEEN, "cpu", "java.lang.Float", "Float");
    	  return this;
      }

      public OmsDevopsServerSQL andCpuNotBetween(java.lang.Float value1, java.lang.Float value2) {
          addCriterion("cpu", value1, value2, ConditionMode.NOT_BETWEEN, "cpu", "java.lang.Float", "Float");
          return this;
      }
        
      public OmsDevopsServerSQL andCpuIn(List<java.lang.Float> values) {
          addCriterion("cpu", values, ConditionMode.IN, "cpu", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andCpuNotIn(List<java.lang.Float> values) {
          addCriterion("cpu", values, ConditionMode.NOT_IN, "cpu", "java.lang.Float", "Float");
          return this;
      }
	public OmsDevopsServerSQL andMemoryIsNull() {
		isnull("memory");
		return this;
	}
	
	public OmsDevopsServerSQL andMemoryIsNotNull() {
		notNull("memory");
		return this;
	}
	
	public OmsDevopsServerSQL andMemoryIsEmpty() {
		empty("memory");
		return this;
	}

	public OmsDevopsServerSQL andMemoryIsNotEmpty() {
		notEmpty("memory");
		return this;
	}
      public OmsDevopsServerSQL andMemoryEqualTo(java.lang.Float value) {
          addCriterion("memory", value, ConditionMode.EQUAL, "memory", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andMemoryNotEqualTo(java.lang.Float value) {
          addCriterion("memory", value, ConditionMode.NOT_EQUAL, "memory", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andMemoryGreaterThan(java.lang.Float value) {
          addCriterion("memory", value, ConditionMode.GREATER_THEN, "memory", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andMemoryGreaterThanOrEqualTo(java.lang.Float value) {
          addCriterion("memory", value, ConditionMode.GREATER_EQUAL, "memory", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andMemoryLessThan(java.lang.Float value) {
          addCriterion("memory", value, ConditionMode.LESS_THEN, "memory", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andMemoryLessThanOrEqualTo(java.lang.Float value) {
          addCriterion("memory", value, ConditionMode.LESS_EQUAL, "memory", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andMemoryBetween(java.lang.Float value1, java.lang.Float value2) {
    	  addCriterion("memory", value1, value2, ConditionMode.BETWEEN, "memory", "java.lang.Float", "Float");
    	  return this;
      }

      public OmsDevopsServerSQL andMemoryNotBetween(java.lang.Float value1, java.lang.Float value2) {
          addCriterion("memory", value1, value2, ConditionMode.NOT_BETWEEN, "memory", "java.lang.Float", "Float");
          return this;
      }
        
      public OmsDevopsServerSQL andMemoryIn(List<java.lang.Float> values) {
          addCriterion("memory", values, ConditionMode.IN, "memory", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andMemoryNotIn(List<java.lang.Float> values) {
          addCriterion("memory", values, ConditionMode.NOT_IN, "memory", "java.lang.Float", "Float");
          return this;
      }
	public OmsDevopsServerSQL andDiskIsNull() {
		isnull("disk");
		return this;
	}
	
	public OmsDevopsServerSQL andDiskIsNotNull() {
		notNull("disk");
		return this;
	}
	
	public OmsDevopsServerSQL andDiskIsEmpty() {
		empty("disk");
		return this;
	}

	public OmsDevopsServerSQL andDiskIsNotEmpty() {
		notEmpty("disk");
		return this;
	}
      public OmsDevopsServerSQL andDiskEqualTo(java.lang.Float value) {
          addCriterion("disk", value, ConditionMode.EQUAL, "disk", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andDiskNotEqualTo(java.lang.Float value) {
          addCriterion("disk", value, ConditionMode.NOT_EQUAL, "disk", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andDiskGreaterThan(java.lang.Float value) {
          addCriterion("disk", value, ConditionMode.GREATER_THEN, "disk", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andDiskGreaterThanOrEqualTo(java.lang.Float value) {
          addCriterion("disk", value, ConditionMode.GREATER_EQUAL, "disk", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andDiskLessThan(java.lang.Float value) {
          addCriterion("disk", value, ConditionMode.LESS_THEN, "disk", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andDiskLessThanOrEqualTo(java.lang.Float value) {
          addCriterion("disk", value, ConditionMode.LESS_EQUAL, "disk", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andDiskBetween(java.lang.Float value1, java.lang.Float value2) {
    	  addCriterion("disk", value1, value2, ConditionMode.BETWEEN, "disk", "java.lang.Float", "Float");
    	  return this;
      }

      public OmsDevopsServerSQL andDiskNotBetween(java.lang.Float value1, java.lang.Float value2) {
          addCriterion("disk", value1, value2, ConditionMode.NOT_BETWEEN, "disk", "java.lang.Float", "Float");
          return this;
      }
        
      public OmsDevopsServerSQL andDiskIn(List<java.lang.Float> values) {
          addCriterion("disk", values, ConditionMode.IN, "disk", "java.lang.Float", "Float");
          return this;
      }

      public OmsDevopsServerSQL andDiskNotIn(List<java.lang.Float> values) {
          addCriterion("disk", values, ConditionMode.NOT_IN, "disk", "java.lang.Float", "Float");
          return this;
      }
}