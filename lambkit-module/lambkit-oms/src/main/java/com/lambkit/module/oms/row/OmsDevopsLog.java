/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.oms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class OmsDevopsLog extends RowModel<OmsDevopsLog> {
	public OmsDevopsLog() {
		setTableName("oms_devops_log");
		setPrimaryKey("devops_log_id");
	}
	public java.lang.Long getDevopsLogId() {
		return this.get("devops_log_id");
	}
	public void setDevopsLogId(java.lang.Long devopsLogId) {
		this.set("devops_log_id", devopsLogId);
	}
	public java.lang.String getName() {
		return this.get("name");
	}
	public void setName(java.lang.String name) {
		this.set("name", name);
	}
	public java.lang.String getNode() {
		return this.get("node");
	}
	public void setNode(java.lang.String node) {
		this.set("node", node);
	}
	public java.lang.String getLevel() {
		return this.get("level");
	}
	public void setLevel(java.lang.String level) {
		this.set("level", level);
	}
	public java.lang.String getType() {
		return this.get("type");
	}
	public void setType(java.lang.String type) {
		this.set("type", type);
	}
	public java.lang.String getLog() {
		return this.get("log");
	}
	public void setLog(java.lang.String log) {
		this.set("log", log);
	}
	public java.util.Date getCreated() {
		return this.get("created");
	}
	public void setCreated(java.util.Date created) {
		this.set("created", created);
	}
}
