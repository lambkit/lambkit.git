/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceTask extends RowModel<BpmsServiceTask> {
	public BpmsServiceTask() {
		setTableName("bpms_service_task");
		setPrimaryKey("flow_task_id");
	}
	public java.lang.Long getFlowTaskId() {
		return this.get("flow_task_id");
	}
	public void setFlowTaskId(java.lang.Long flowTaskId) {
		this.set("flow_task_id", flowTaskId);
	}
	public java.lang.String getFlowCode() {
		return this.get("flow_code");
	}
	public void setFlowCode(java.lang.String flowCode) {
		this.set("flow_code", flowCode);
	}
	public java.lang.String getInstanceCode() {
		return this.get("instance_code");
	}
	public void setInstanceCode(java.lang.String instanceCode) {
		this.set("instance_code", instanceCode);
	}
	public java.lang.String getFlowNodeCode() {
		return this.get("flow_node_code");
	}
	public void setFlowNodeCode(java.lang.String flowNodeCode) {
		this.set("flow_node_code", flowNodeCode);
	}
	public java.lang.String getFlowTaskCode() {
		return this.get("flow_task_code");
	}
	public void setFlowTaskCode(java.lang.String flowTaskCode) {
		this.set("flow_task_code", flowTaskCode);
	}
	public java.lang.String getFlowTaskName() {
		return this.get("flow_task_name");
	}
	public void setFlowTaskName(java.lang.String flowTaskName) {
		this.set("flow_task_name", flowTaskName);
	}
	public java.lang.Integer getFlowTaskType() {
		return this.get("flow_task_type");
	}
	public void setFlowTaskType(java.lang.Integer flowTaskType) {
		this.set("flow_task_type", flowTaskType);
	}
	public java.lang.Integer getFlowStatus() {
		return this.get("flow_status");
	}
	public void setFlowStatus(java.lang.Integer flowStatus) {
		this.set("flow_status", flowStatus);
	}
	public java.lang.String getApprover() {
		return this.get("approver");
	}
	public void setApprover(java.lang.String approver) {
		this.set("approver", approver);
	}
	public java.lang.String getAssignee() {
		return this.get("assignee");
	}
	public void setAssignee(java.lang.String assignee) {
		this.set("assignee", assignee);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
	public java.util.Date getUpdateTime() {
		return this.get("update_time");
	}
	public void setUpdateTime(java.util.Date updateTime) {
		this.set("update_time", updateTime);
	}
	public java.lang.String getDelFlag() {
		return this.get("del_flag");
	}
	public void setDelFlag(java.lang.String delFlag) {
		this.set("del_flag", delFlag);
	}
	public java.lang.String getUserCode() {
		return this.get("user_code");
	}
	public void setUserCode(java.lang.String userCode) {
		this.set("user_code", userCode);
	}
}
