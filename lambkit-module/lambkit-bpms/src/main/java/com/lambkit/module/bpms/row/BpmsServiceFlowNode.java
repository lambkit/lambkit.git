/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceFlowNode extends RowModel<BpmsServiceFlowNode> {
	public BpmsServiceFlowNode() {
		setTableName("bpms_service_flow_node");
		setPrimaryKey("flow_node_id");
	}
	public java.lang.Long getFlowNodeId() {
		return this.get("flow_node_id");
	}
	public void setFlowNodeId(java.lang.Long flowNodeId) {
		this.set("flow_node_id", flowNodeId);
	}
	public java.lang.String getFlowCode() {
		return this.get("flow_code");
	}
	public void setFlowCode(java.lang.String flowCode) {
		this.set("flow_code", flowCode);
	}
	public java.lang.String getNodeCode() {
		return this.get("node_code");
	}
	public void setNodeCode(java.lang.String nodeCode) {
		this.set("node_code", nodeCode);
	}
	public java.lang.String getFlowNodeCode() {
		return this.get("flow_node_code");
	}
	public void setFlowNodeCode(java.lang.String flowNodeCode) {
		this.set("flow_node_code", flowNodeCode);
	}
	public java.lang.Integer getFlowNodeType() {
		return this.get("flow_node_type");
	}
	public void setFlowNodeType(java.lang.Integer flowNodeType) {
		this.set("flow_node_type", flowNodeType);
	}
	public java.lang.String getFlowNodeName() {
		return this.get("flow_node_name");
	}
	public void setFlowNodeName(java.lang.String flowNodeName) {
		this.set("flow_node_name", flowNodeName);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
	public java.util.Date getUpdateTime() {
		return this.get("update_time");
	}
	public void setUpdateTime(java.util.Date updateTime) {
		this.set("update_time", updateTime);
	}
	public java.lang.String getDelFlag() {
		return this.get("del_flag");
	}
	public void setDelFlag(java.lang.String delFlag) {
		this.set("del_flag", delFlag);
	}
	public java.lang.String getUserCode() {
		return this.get("user_code");
	}
	public void setUserCode(java.lang.String userCode) {
		this.set("user_code", userCode);
	}
}
