/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceTaskSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static BpmsServiceTaskSQL of() {
		return new BpmsServiceTaskSQL();
	}
	
	public static BpmsServiceTaskSQL by(Column column) {
		BpmsServiceTaskSQL that = new BpmsServiceTaskSQL();
		that.add(column);
        return that;
    }

    public static BpmsServiceTaskSQL by(String name, Object value) {
        return (BpmsServiceTaskSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("bpms_service_task", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceTaskSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceTaskSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public BpmsServiceTaskSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public BpmsServiceTaskSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceTaskSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceTaskSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceTaskSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceTaskSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public BpmsServiceTaskSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public BpmsServiceTaskSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public BpmsServiceTaskSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public BpmsServiceTaskSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public BpmsServiceTaskSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public BpmsServiceTaskSQL andFlowTaskIdIsNull() {
		isnull("flow_task_id");
		return this;
	}
	
	public BpmsServiceTaskSQL andFlowTaskIdIsNotNull() {
		notNull("flow_task_id");
		return this;
	}
	
	public BpmsServiceTaskSQL andFlowTaskIdIsEmpty() {
		empty("flow_task_id");
		return this;
	}

	public BpmsServiceTaskSQL andFlowTaskIdIsNotEmpty() {
		notEmpty("flow_task_id");
		return this;
	}
      public BpmsServiceTaskSQL andFlowTaskIdEqualTo(java.lang.Long value) {
          addCriterion("flow_task_id", value, ConditionMode.EQUAL, "flowTaskId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskIdNotEqualTo(java.lang.Long value) {
          addCriterion("flow_task_id", value, ConditionMode.NOT_EQUAL, "flowTaskId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskIdGreaterThan(java.lang.Long value) {
          addCriterion("flow_task_id", value, ConditionMode.GREATER_THEN, "flowTaskId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("flow_task_id", value, ConditionMode.GREATER_EQUAL, "flowTaskId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskIdLessThan(java.lang.Long value) {
          addCriterion("flow_task_id", value, ConditionMode.LESS_THEN, "flowTaskId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("flow_task_id", value, ConditionMode.LESS_EQUAL, "flowTaskId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("flow_task_id", value1, value2, ConditionMode.BETWEEN, "flowTaskId", "java.lang.Long", "Float");
    	  return this;
      }

      public BpmsServiceTaskSQL andFlowTaskIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("flow_task_id", value1, value2, ConditionMode.NOT_BETWEEN, "flowTaskId", "java.lang.Long", "Float");
          return this;
      }
        
      public BpmsServiceTaskSQL andFlowTaskIdIn(List<java.lang.Long> values) {
          addCriterion("flow_task_id", values, ConditionMode.IN, "flowTaskId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskIdNotIn(List<java.lang.Long> values) {
          addCriterion("flow_task_id", values, ConditionMode.NOT_IN, "flowTaskId", "java.lang.Long", "Float");
          return this;
      }
	public BpmsServiceTaskSQL andFlowCodeIsNull() {
		isnull("flow_code");
		return this;
	}
	
	public BpmsServiceTaskSQL andFlowCodeIsNotNull() {
		notNull("flow_code");
		return this;
	}
	
	public BpmsServiceTaskSQL andFlowCodeIsEmpty() {
		empty("flow_code");
		return this;
	}

	public BpmsServiceTaskSQL andFlowCodeIsNotEmpty() {
		notEmpty("flow_code");
		return this;
	}
       public BpmsServiceTaskSQL andFlowCodeLike(java.lang.String value) {
    	   addCriterion("flow_code", value, ConditionMode.FUZZY, "flowCode", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceTaskSQL andFlowCodeNotLike(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.NOT_FUZZY, "flowCode", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceTaskSQL andFlowCodeEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowCodeNotEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.NOT_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowCodeGreaterThan(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.GREATER_THEN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.GREATER_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowCodeLessThan(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.LESS_THEN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.LESS_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flow_code", value1, value2, ConditionMode.BETWEEN, "flowCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceTaskSQL andFlowCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flow_code", value1, value2, ConditionMode.NOT_BETWEEN, "flowCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceTaskSQL andFlowCodeIn(List<java.lang.String> values) {
          addCriterion("flow_code", values, ConditionMode.IN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowCodeNotIn(List<java.lang.String> values) {
          addCriterion("flow_code", values, ConditionMode.NOT_IN, "flowCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceTaskSQL andInstanceCodeIsNull() {
		isnull("instance_code");
		return this;
	}
	
	public BpmsServiceTaskSQL andInstanceCodeIsNotNull() {
		notNull("instance_code");
		return this;
	}
	
	public BpmsServiceTaskSQL andInstanceCodeIsEmpty() {
		empty("instance_code");
		return this;
	}

	public BpmsServiceTaskSQL andInstanceCodeIsNotEmpty() {
		notEmpty("instance_code");
		return this;
	}
       public BpmsServiceTaskSQL andInstanceCodeLike(java.lang.String value) {
    	   addCriterion("instance_code", value, ConditionMode.FUZZY, "instanceCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceTaskSQL andInstanceCodeNotLike(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.NOT_FUZZY, "instanceCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceTaskSQL andInstanceCodeEqualTo(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.EQUAL, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andInstanceCodeNotEqualTo(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.NOT_EQUAL, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andInstanceCodeGreaterThan(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.GREATER_THEN, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andInstanceCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.GREATER_EQUAL, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andInstanceCodeLessThan(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.LESS_THEN, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andInstanceCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("instance_code", value, ConditionMode.LESS_EQUAL, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andInstanceCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("instance_code", value1, value2, ConditionMode.BETWEEN, "instanceCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceTaskSQL andInstanceCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("instance_code", value1, value2, ConditionMode.NOT_BETWEEN, "instanceCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceTaskSQL andInstanceCodeIn(List<java.lang.String> values) {
          addCriterion("instance_code", values, ConditionMode.IN, "instanceCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andInstanceCodeNotIn(List<java.lang.String> values) {
          addCriterion("instance_code", values, ConditionMode.NOT_IN, "instanceCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceTaskSQL andFlowNodeCodeIsNull() {
		isnull("flow_node_code");
		return this;
	}
	
	public BpmsServiceTaskSQL andFlowNodeCodeIsNotNull() {
		notNull("flow_node_code");
		return this;
	}
	
	public BpmsServiceTaskSQL andFlowNodeCodeIsEmpty() {
		empty("flow_node_code");
		return this;
	}

	public BpmsServiceTaskSQL andFlowNodeCodeIsNotEmpty() {
		notEmpty("flow_node_code");
		return this;
	}
       public BpmsServiceTaskSQL andFlowNodeCodeLike(java.lang.String value) {
    	   addCriterion("flow_node_code", value, ConditionMode.FUZZY, "flowNodeCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceTaskSQL andFlowNodeCodeNotLike(java.lang.String value) {
          addCriterion("flow_node_code", value, ConditionMode.NOT_FUZZY, "flowNodeCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceTaskSQL andFlowNodeCodeEqualTo(java.lang.String value) {
          addCriterion("flow_node_code", value, ConditionMode.EQUAL, "flowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowNodeCodeNotEqualTo(java.lang.String value) {
          addCriterion("flow_node_code", value, ConditionMode.NOT_EQUAL, "flowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowNodeCodeGreaterThan(java.lang.String value) {
          addCriterion("flow_node_code", value, ConditionMode.GREATER_THEN, "flowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowNodeCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_node_code", value, ConditionMode.GREATER_EQUAL, "flowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowNodeCodeLessThan(java.lang.String value) {
          addCriterion("flow_node_code", value, ConditionMode.LESS_THEN, "flowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowNodeCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_node_code", value, ConditionMode.LESS_EQUAL, "flowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowNodeCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flow_node_code", value1, value2, ConditionMode.BETWEEN, "flowNodeCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceTaskSQL andFlowNodeCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flow_node_code", value1, value2, ConditionMode.NOT_BETWEEN, "flowNodeCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceTaskSQL andFlowNodeCodeIn(List<java.lang.String> values) {
          addCriterion("flow_node_code", values, ConditionMode.IN, "flowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowNodeCodeNotIn(List<java.lang.String> values) {
          addCriterion("flow_node_code", values, ConditionMode.NOT_IN, "flowNodeCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceTaskSQL andFlowTaskCodeIsNull() {
		isnull("flow_task_code");
		return this;
	}
	
	public BpmsServiceTaskSQL andFlowTaskCodeIsNotNull() {
		notNull("flow_task_code");
		return this;
	}
	
	public BpmsServiceTaskSQL andFlowTaskCodeIsEmpty() {
		empty("flow_task_code");
		return this;
	}

	public BpmsServiceTaskSQL andFlowTaskCodeIsNotEmpty() {
		notEmpty("flow_task_code");
		return this;
	}
       public BpmsServiceTaskSQL andFlowTaskCodeLike(java.lang.String value) {
    	   addCriterion("flow_task_code", value, ConditionMode.FUZZY, "flowTaskCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceTaskSQL andFlowTaskCodeNotLike(java.lang.String value) {
          addCriterion("flow_task_code", value, ConditionMode.NOT_FUZZY, "flowTaskCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceTaskSQL andFlowTaskCodeEqualTo(java.lang.String value) {
          addCriterion("flow_task_code", value, ConditionMode.EQUAL, "flowTaskCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskCodeNotEqualTo(java.lang.String value) {
          addCriterion("flow_task_code", value, ConditionMode.NOT_EQUAL, "flowTaskCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskCodeGreaterThan(java.lang.String value) {
          addCriterion("flow_task_code", value, ConditionMode.GREATER_THEN, "flowTaskCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_task_code", value, ConditionMode.GREATER_EQUAL, "flowTaskCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskCodeLessThan(java.lang.String value) {
          addCriterion("flow_task_code", value, ConditionMode.LESS_THEN, "flowTaskCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_task_code", value, ConditionMode.LESS_EQUAL, "flowTaskCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flow_task_code", value1, value2, ConditionMode.BETWEEN, "flowTaskCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceTaskSQL andFlowTaskCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flow_task_code", value1, value2, ConditionMode.NOT_BETWEEN, "flowTaskCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceTaskSQL andFlowTaskCodeIn(List<java.lang.String> values) {
          addCriterion("flow_task_code", values, ConditionMode.IN, "flowTaskCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskCodeNotIn(List<java.lang.String> values) {
          addCriterion("flow_task_code", values, ConditionMode.NOT_IN, "flowTaskCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceTaskSQL andFlowTaskNameIsNull() {
		isnull("flow_task_name");
		return this;
	}
	
	public BpmsServiceTaskSQL andFlowTaskNameIsNotNull() {
		notNull("flow_task_name");
		return this;
	}
	
	public BpmsServiceTaskSQL andFlowTaskNameIsEmpty() {
		empty("flow_task_name");
		return this;
	}

	public BpmsServiceTaskSQL andFlowTaskNameIsNotEmpty() {
		notEmpty("flow_task_name");
		return this;
	}
       public BpmsServiceTaskSQL andFlowTaskNameLike(java.lang.String value) {
    	   addCriterion("flow_task_name", value, ConditionMode.FUZZY, "flowTaskName", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceTaskSQL andFlowTaskNameNotLike(java.lang.String value) {
          addCriterion("flow_task_name", value, ConditionMode.NOT_FUZZY, "flowTaskName", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceTaskSQL andFlowTaskNameEqualTo(java.lang.String value) {
          addCriterion("flow_task_name", value, ConditionMode.EQUAL, "flowTaskName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskNameNotEqualTo(java.lang.String value) {
          addCriterion("flow_task_name", value, ConditionMode.NOT_EQUAL, "flowTaskName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskNameGreaterThan(java.lang.String value) {
          addCriterion("flow_task_name", value, ConditionMode.GREATER_THEN, "flowTaskName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_task_name", value, ConditionMode.GREATER_EQUAL, "flowTaskName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskNameLessThan(java.lang.String value) {
          addCriterion("flow_task_name", value, ConditionMode.LESS_THEN, "flowTaskName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_task_name", value, ConditionMode.LESS_EQUAL, "flowTaskName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flow_task_name", value1, value2, ConditionMode.BETWEEN, "flowTaskName", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceTaskSQL andFlowTaskNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flow_task_name", value1, value2, ConditionMode.NOT_BETWEEN, "flowTaskName", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceTaskSQL andFlowTaskNameIn(List<java.lang.String> values) {
          addCriterion("flow_task_name", values, ConditionMode.IN, "flowTaskName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskNameNotIn(List<java.lang.String> values) {
          addCriterion("flow_task_name", values, ConditionMode.NOT_IN, "flowTaskName", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceTaskSQL andFlowTaskTypeIsNull() {
		isnull("flow_task_type");
		return this;
	}
	
	public BpmsServiceTaskSQL andFlowTaskTypeIsNotNull() {
		notNull("flow_task_type");
		return this;
	}
	
	public BpmsServiceTaskSQL andFlowTaskTypeIsEmpty() {
		empty("flow_task_type");
		return this;
	}

	public BpmsServiceTaskSQL andFlowTaskTypeIsNotEmpty() {
		notEmpty("flow_task_type");
		return this;
	}
      public BpmsServiceTaskSQL andFlowTaskTypeEqualTo(java.lang.Integer value) {
          addCriterion("flow_task_type", value, ConditionMode.EQUAL, "flowTaskType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("flow_task_type", value, ConditionMode.NOT_EQUAL, "flowTaskType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskTypeGreaterThan(java.lang.Integer value) {
          addCriterion("flow_task_type", value, ConditionMode.GREATER_THEN, "flowTaskType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("flow_task_type", value, ConditionMode.GREATER_EQUAL, "flowTaskType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskTypeLessThan(java.lang.Integer value) {
          addCriterion("flow_task_type", value, ConditionMode.LESS_THEN, "flowTaskType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("flow_task_type", value, ConditionMode.LESS_EQUAL, "flowTaskType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("flow_task_type", value1, value2, ConditionMode.BETWEEN, "flowTaskType", "java.lang.Integer", "Float");
    	  return this;
      }

      public BpmsServiceTaskSQL andFlowTaskTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("flow_task_type", value1, value2, ConditionMode.NOT_BETWEEN, "flowTaskType", "java.lang.Integer", "Float");
          return this;
      }
        
      public BpmsServiceTaskSQL andFlowTaskTypeIn(List<java.lang.Integer> values) {
          addCriterion("flow_task_type", values, ConditionMode.IN, "flowTaskType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowTaskTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("flow_task_type", values, ConditionMode.NOT_IN, "flowTaskType", "java.lang.Integer", "Float");
          return this;
      }
	public BpmsServiceTaskSQL andFlowStatusIsNull() {
		isnull("flow_status");
		return this;
	}
	
	public BpmsServiceTaskSQL andFlowStatusIsNotNull() {
		notNull("flow_status");
		return this;
	}
	
	public BpmsServiceTaskSQL andFlowStatusIsEmpty() {
		empty("flow_status");
		return this;
	}

	public BpmsServiceTaskSQL andFlowStatusIsNotEmpty() {
		notEmpty("flow_status");
		return this;
	}
      public BpmsServiceTaskSQL andFlowStatusEqualTo(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.EQUAL, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.NOT_EQUAL, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowStatusGreaterThan(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.GREATER_THEN, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.GREATER_EQUAL, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowStatusLessThan(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.LESS_THEN, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("flow_status", value, ConditionMode.LESS_EQUAL, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("flow_status", value1, value2, ConditionMode.BETWEEN, "flowStatus", "java.lang.Integer", "Float");
    	  return this;
      }

      public BpmsServiceTaskSQL andFlowStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("flow_status", value1, value2, ConditionMode.NOT_BETWEEN, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }
        
      public BpmsServiceTaskSQL andFlowStatusIn(List<java.lang.Integer> values) {
          addCriterion("flow_status", values, ConditionMode.IN, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceTaskSQL andFlowStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("flow_status", values, ConditionMode.NOT_IN, "flowStatus", "java.lang.Integer", "Float");
          return this;
      }
	public BpmsServiceTaskSQL andApproverIsNull() {
		isnull("approver");
		return this;
	}
	
	public BpmsServiceTaskSQL andApproverIsNotNull() {
		notNull("approver");
		return this;
	}
	
	public BpmsServiceTaskSQL andApproverIsEmpty() {
		empty("approver");
		return this;
	}

	public BpmsServiceTaskSQL andApproverIsNotEmpty() {
		notEmpty("approver");
		return this;
	}
       public BpmsServiceTaskSQL andApproverLike(java.lang.String value) {
    	   addCriterion("approver", value, ConditionMode.FUZZY, "approver", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceTaskSQL andApproverNotLike(java.lang.String value) {
          addCriterion("approver", value, ConditionMode.NOT_FUZZY, "approver", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceTaskSQL andApproverEqualTo(java.lang.String value) {
          addCriterion("approver", value, ConditionMode.EQUAL, "approver", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andApproverNotEqualTo(java.lang.String value) {
          addCriterion("approver", value, ConditionMode.NOT_EQUAL, "approver", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andApproverGreaterThan(java.lang.String value) {
          addCriterion("approver", value, ConditionMode.GREATER_THEN, "approver", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andApproverGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("approver", value, ConditionMode.GREATER_EQUAL, "approver", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andApproverLessThan(java.lang.String value) {
          addCriterion("approver", value, ConditionMode.LESS_THEN, "approver", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andApproverLessThanOrEqualTo(java.lang.String value) {
          addCriterion("approver", value, ConditionMode.LESS_EQUAL, "approver", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andApproverBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("approver", value1, value2, ConditionMode.BETWEEN, "approver", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceTaskSQL andApproverNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("approver", value1, value2, ConditionMode.NOT_BETWEEN, "approver", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceTaskSQL andApproverIn(List<java.lang.String> values) {
          addCriterion("approver", values, ConditionMode.IN, "approver", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andApproverNotIn(List<java.lang.String> values) {
          addCriterion("approver", values, ConditionMode.NOT_IN, "approver", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceTaskSQL andAssigneeIsNull() {
		isnull("assignee");
		return this;
	}
	
	public BpmsServiceTaskSQL andAssigneeIsNotNull() {
		notNull("assignee");
		return this;
	}
	
	public BpmsServiceTaskSQL andAssigneeIsEmpty() {
		empty("assignee");
		return this;
	}

	public BpmsServiceTaskSQL andAssigneeIsNotEmpty() {
		notEmpty("assignee");
		return this;
	}
       public BpmsServiceTaskSQL andAssigneeLike(java.lang.String value) {
    	   addCriterion("assignee", value, ConditionMode.FUZZY, "assignee", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceTaskSQL andAssigneeNotLike(java.lang.String value) {
          addCriterion("assignee", value, ConditionMode.NOT_FUZZY, "assignee", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceTaskSQL andAssigneeEqualTo(java.lang.String value) {
          addCriterion("assignee", value, ConditionMode.EQUAL, "assignee", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andAssigneeNotEqualTo(java.lang.String value) {
          addCriterion("assignee", value, ConditionMode.NOT_EQUAL, "assignee", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andAssigneeGreaterThan(java.lang.String value) {
          addCriterion("assignee", value, ConditionMode.GREATER_THEN, "assignee", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andAssigneeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("assignee", value, ConditionMode.GREATER_EQUAL, "assignee", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andAssigneeLessThan(java.lang.String value) {
          addCriterion("assignee", value, ConditionMode.LESS_THEN, "assignee", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andAssigneeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("assignee", value, ConditionMode.LESS_EQUAL, "assignee", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andAssigneeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("assignee", value1, value2, ConditionMode.BETWEEN, "assignee", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceTaskSQL andAssigneeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("assignee", value1, value2, ConditionMode.NOT_BETWEEN, "assignee", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceTaskSQL andAssigneeIn(List<java.lang.String> values) {
          addCriterion("assignee", values, ConditionMode.IN, "assignee", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andAssigneeNotIn(List<java.lang.String> values) {
          addCriterion("assignee", values, ConditionMode.NOT_IN, "assignee", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceTaskSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public BpmsServiceTaskSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public BpmsServiceTaskSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public BpmsServiceTaskSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public BpmsServiceTaskSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceTaskSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceTaskSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceTaskSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceTaskSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceTaskSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceTaskSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceTaskSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceTaskSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceTaskSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceTaskSQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public BpmsServiceTaskSQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public BpmsServiceTaskSQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public BpmsServiceTaskSQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public BpmsServiceTaskSQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceTaskSQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceTaskSQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceTaskSQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceTaskSQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceTaskSQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceTaskSQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceTaskSQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceTaskSQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceTaskSQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceTaskSQL andDelFlagIsNull() {
		isnull("del_flag");
		return this;
	}
	
	public BpmsServiceTaskSQL andDelFlagIsNotNull() {
		notNull("del_flag");
		return this;
	}
	
	public BpmsServiceTaskSQL andDelFlagIsEmpty() {
		empty("del_flag");
		return this;
	}

	public BpmsServiceTaskSQL andDelFlagIsNotEmpty() {
		notEmpty("del_flag");
		return this;
	}
       public BpmsServiceTaskSQL andDelFlagLike(java.lang.String value) {
    	   addCriterion("del_flag", value, ConditionMode.FUZZY, "delFlag", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceTaskSQL andDelFlagNotLike(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_FUZZY, "delFlag", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceTaskSQL andDelFlagEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andDelFlagNotEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andDelFlagGreaterThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andDelFlagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andDelFlagLessThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andDelFlagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andDelFlagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("del_flag", value1, value2, ConditionMode.BETWEEN, "delFlag", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceTaskSQL andDelFlagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("del_flag", value1, value2, ConditionMode.NOT_BETWEEN, "delFlag", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceTaskSQL andDelFlagIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.IN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andDelFlagNotIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.NOT_IN, "delFlag", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceTaskSQL andUserCodeIsNull() {
		isnull("user_code");
		return this;
	}
	
	public BpmsServiceTaskSQL andUserCodeIsNotNull() {
		notNull("user_code");
		return this;
	}
	
	public BpmsServiceTaskSQL andUserCodeIsEmpty() {
		empty("user_code");
		return this;
	}

	public BpmsServiceTaskSQL andUserCodeIsNotEmpty() {
		notEmpty("user_code");
		return this;
	}
       public BpmsServiceTaskSQL andUserCodeLike(java.lang.String value) {
    	   addCriterion("user_code", value, ConditionMode.FUZZY, "userCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceTaskSQL andUserCodeNotLike(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_FUZZY, "userCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceTaskSQL andUserCodeEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andUserCodeNotEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andUserCodeGreaterThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andUserCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andUserCodeLessThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andUserCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andUserCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_code", value1, value2, ConditionMode.BETWEEN, "userCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceTaskSQL andUserCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_code", value1, value2, ConditionMode.NOT_BETWEEN, "userCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceTaskSQL andUserCodeIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.IN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceTaskSQL andUserCodeNotIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.NOT_IN, "userCode", "java.lang.String", "String");
          return this;
      }
}