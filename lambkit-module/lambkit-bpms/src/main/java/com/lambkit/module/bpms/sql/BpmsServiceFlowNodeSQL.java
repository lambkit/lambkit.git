/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceFlowNodeSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static BpmsServiceFlowNodeSQL of() {
		return new BpmsServiceFlowNodeSQL();
	}
	
	public static BpmsServiceFlowNodeSQL by(Column column) {
		BpmsServiceFlowNodeSQL that = new BpmsServiceFlowNodeSQL();
		that.add(column);
        return that;
    }

    public static BpmsServiceFlowNodeSQL by(String name, Object value) {
        return (BpmsServiceFlowNodeSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("bpms_service_flow_node", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public BpmsServiceFlowNodeSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public BpmsServiceFlowNodeSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceFlowNodeSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public BpmsServiceFlowNodeSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public BpmsServiceFlowNodeSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public BpmsServiceFlowNodeSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public BpmsServiceFlowNodeSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public BpmsServiceFlowNodeSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public BpmsServiceFlowNodeSQL andFlowNodeIdIsNull() {
		isnull("flow_node_id");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andFlowNodeIdIsNotNull() {
		notNull("flow_node_id");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andFlowNodeIdIsEmpty() {
		empty("flow_node_id");
		return this;
	}

	public BpmsServiceFlowNodeSQL andFlowNodeIdIsNotEmpty() {
		notEmpty("flow_node_id");
		return this;
	}
      public BpmsServiceFlowNodeSQL andFlowNodeIdEqualTo(java.lang.Long value) {
          addCriterion("flow_node_id", value, ConditionMode.EQUAL, "flowNodeId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeIdNotEqualTo(java.lang.Long value) {
          addCriterion("flow_node_id", value, ConditionMode.NOT_EQUAL, "flowNodeId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeIdGreaterThan(java.lang.Long value) {
          addCriterion("flow_node_id", value, ConditionMode.GREATER_THEN, "flowNodeId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("flow_node_id", value, ConditionMode.GREATER_EQUAL, "flowNodeId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeIdLessThan(java.lang.Long value) {
          addCriterion("flow_node_id", value, ConditionMode.LESS_THEN, "flowNodeId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("flow_node_id", value, ConditionMode.LESS_EQUAL, "flowNodeId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("flow_node_id", value1, value2, ConditionMode.BETWEEN, "flowNodeId", "java.lang.Long", "Float");
    	  return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("flow_node_id", value1, value2, ConditionMode.NOT_BETWEEN, "flowNodeId", "java.lang.Long", "Float");
          return this;
      }
        
      public BpmsServiceFlowNodeSQL andFlowNodeIdIn(List<java.lang.Long> values) {
          addCriterion("flow_node_id", values, ConditionMode.IN, "flowNodeId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeIdNotIn(List<java.lang.Long> values) {
          addCriterion("flow_node_id", values, ConditionMode.NOT_IN, "flowNodeId", "java.lang.Long", "Float");
          return this;
      }
	public BpmsServiceFlowNodeSQL andFlowCodeIsNull() {
		isnull("flow_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andFlowCodeIsNotNull() {
		notNull("flow_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andFlowCodeIsEmpty() {
		empty("flow_code");
		return this;
	}

	public BpmsServiceFlowNodeSQL andFlowCodeIsNotEmpty() {
		notEmpty("flow_code");
		return this;
	}
       public BpmsServiceFlowNodeSQL andFlowCodeLike(java.lang.String value) {
    	   addCriterion("flow_code", value, ConditionMode.FUZZY, "flowCode", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceFlowNodeSQL andFlowCodeNotLike(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.NOT_FUZZY, "flowCode", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceFlowNodeSQL andFlowCodeEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowCodeNotEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.NOT_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowCodeGreaterThan(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.GREATER_THEN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.GREATER_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowCodeLessThan(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.LESS_THEN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_code", value, ConditionMode.LESS_EQUAL, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flow_code", value1, value2, ConditionMode.BETWEEN, "flowCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSQL andFlowCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flow_code", value1, value2, ConditionMode.NOT_BETWEEN, "flowCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSQL andFlowCodeIn(List<java.lang.String> values) {
          addCriterion("flow_code", values, ConditionMode.IN, "flowCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowCodeNotIn(List<java.lang.String> values) {
          addCriterion("flow_code", values, ConditionMode.NOT_IN, "flowCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeSQL andNodeCodeIsNull() {
		isnull("node_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andNodeCodeIsNotNull() {
		notNull("node_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andNodeCodeIsEmpty() {
		empty("node_code");
		return this;
	}

	public BpmsServiceFlowNodeSQL andNodeCodeIsNotEmpty() {
		notEmpty("node_code");
		return this;
	}
       public BpmsServiceFlowNodeSQL andNodeCodeLike(java.lang.String value) {
    	   addCriterion("node_code", value, ConditionMode.FUZZY, "nodeCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowNodeSQL andNodeCodeNotLike(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.NOT_FUZZY, "nodeCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowNodeSQL andNodeCodeEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andNodeCodeNotEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.NOT_EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andNodeCodeGreaterThan(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.GREATER_THEN, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andNodeCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.GREATER_EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andNodeCodeLessThan(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.LESS_THEN, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andNodeCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.LESS_EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andNodeCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("node_code", value1, value2, ConditionMode.BETWEEN, "nodeCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSQL andNodeCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("node_code", value1, value2, ConditionMode.NOT_BETWEEN, "nodeCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSQL andNodeCodeIn(List<java.lang.String> values) {
          addCriterion("node_code", values, ConditionMode.IN, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andNodeCodeNotIn(List<java.lang.String> values) {
          addCriterion("node_code", values, ConditionMode.NOT_IN, "nodeCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeSQL andFlowNodeCodeIsNull() {
		isnull("flow_node_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andFlowNodeCodeIsNotNull() {
		notNull("flow_node_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andFlowNodeCodeIsEmpty() {
		empty("flow_node_code");
		return this;
	}

	public BpmsServiceFlowNodeSQL andFlowNodeCodeIsNotEmpty() {
		notEmpty("flow_node_code");
		return this;
	}
       public BpmsServiceFlowNodeSQL andFlowNodeCodeLike(java.lang.String value) {
    	   addCriterion("flow_node_code", value, ConditionMode.FUZZY, "flowNodeCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeCodeNotLike(java.lang.String value) {
          addCriterion("flow_node_code", value, ConditionMode.NOT_FUZZY, "flowNodeCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowNodeSQL andFlowNodeCodeEqualTo(java.lang.String value) {
          addCriterion("flow_node_code", value, ConditionMode.EQUAL, "flowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeCodeNotEqualTo(java.lang.String value) {
          addCriterion("flow_node_code", value, ConditionMode.NOT_EQUAL, "flowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeCodeGreaterThan(java.lang.String value) {
          addCriterion("flow_node_code", value, ConditionMode.GREATER_THEN, "flowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_node_code", value, ConditionMode.GREATER_EQUAL, "flowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeCodeLessThan(java.lang.String value) {
          addCriterion("flow_node_code", value, ConditionMode.LESS_THEN, "flowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_node_code", value, ConditionMode.LESS_EQUAL, "flowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flow_node_code", value1, value2, ConditionMode.BETWEEN, "flowNodeCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flow_node_code", value1, value2, ConditionMode.NOT_BETWEEN, "flowNodeCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSQL andFlowNodeCodeIn(List<java.lang.String> values) {
          addCriterion("flow_node_code", values, ConditionMode.IN, "flowNodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeCodeNotIn(List<java.lang.String> values) {
          addCriterion("flow_node_code", values, ConditionMode.NOT_IN, "flowNodeCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeSQL andFlowNodeTypeIsNull() {
		isnull("flow_node_type");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andFlowNodeTypeIsNotNull() {
		notNull("flow_node_type");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andFlowNodeTypeIsEmpty() {
		empty("flow_node_type");
		return this;
	}

	public BpmsServiceFlowNodeSQL andFlowNodeTypeIsNotEmpty() {
		notEmpty("flow_node_type");
		return this;
	}
      public BpmsServiceFlowNodeSQL andFlowNodeTypeEqualTo(java.lang.Integer value) {
          addCriterion("flow_node_type", value, ConditionMode.EQUAL, "flowNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("flow_node_type", value, ConditionMode.NOT_EQUAL, "flowNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeTypeGreaterThan(java.lang.Integer value) {
          addCriterion("flow_node_type", value, ConditionMode.GREATER_THEN, "flowNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("flow_node_type", value, ConditionMode.GREATER_EQUAL, "flowNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeTypeLessThan(java.lang.Integer value) {
          addCriterion("flow_node_type", value, ConditionMode.LESS_THEN, "flowNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("flow_node_type", value, ConditionMode.LESS_EQUAL, "flowNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("flow_node_type", value1, value2, ConditionMode.BETWEEN, "flowNodeType", "java.lang.Integer", "Float");
    	  return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("flow_node_type", value1, value2, ConditionMode.NOT_BETWEEN, "flowNodeType", "java.lang.Integer", "Float");
          return this;
      }
        
      public BpmsServiceFlowNodeSQL andFlowNodeTypeIn(List<java.lang.Integer> values) {
          addCriterion("flow_node_type", values, ConditionMode.IN, "flowNodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("flow_node_type", values, ConditionMode.NOT_IN, "flowNodeType", "java.lang.Integer", "Float");
          return this;
      }
	public BpmsServiceFlowNodeSQL andFlowNodeNameIsNull() {
		isnull("flow_node_name");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andFlowNodeNameIsNotNull() {
		notNull("flow_node_name");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andFlowNodeNameIsEmpty() {
		empty("flow_node_name");
		return this;
	}

	public BpmsServiceFlowNodeSQL andFlowNodeNameIsNotEmpty() {
		notEmpty("flow_node_name");
		return this;
	}
       public BpmsServiceFlowNodeSQL andFlowNodeNameLike(java.lang.String value) {
    	   addCriterion("flow_node_name", value, ConditionMode.FUZZY, "flowNodeName", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeNameNotLike(java.lang.String value) {
          addCriterion("flow_node_name", value, ConditionMode.NOT_FUZZY, "flowNodeName", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceFlowNodeSQL andFlowNodeNameEqualTo(java.lang.String value) {
          addCriterion("flow_node_name", value, ConditionMode.EQUAL, "flowNodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeNameNotEqualTo(java.lang.String value) {
          addCriterion("flow_node_name", value, ConditionMode.NOT_EQUAL, "flowNodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeNameGreaterThan(java.lang.String value) {
          addCriterion("flow_node_name", value, ConditionMode.GREATER_THEN, "flowNodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_node_name", value, ConditionMode.GREATER_EQUAL, "flowNodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeNameLessThan(java.lang.String value) {
          addCriterion("flow_node_name", value, ConditionMode.LESS_THEN, "flowNodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flow_node_name", value, ConditionMode.LESS_EQUAL, "flowNodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flow_node_name", value1, value2, ConditionMode.BETWEEN, "flowNodeName", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flow_node_name", value1, value2, ConditionMode.NOT_BETWEEN, "flowNodeName", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSQL andFlowNodeNameIn(List<java.lang.String> values) {
          addCriterion("flow_node_name", values, ConditionMode.IN, "flowNodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andFlowNodeNameNotIn(List<java.lang.String> values) {
          addCriterion("flow_node_name", values, ConditionMode.NOT_IN, "flowNodeName", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public BpmsServiceFlowNodeSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public BpmsServiceFlowNodeSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceFlowNodeSQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public BpmsServiceFlowNodeSQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public BpmsServiceFlowNodeSQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceFlowNodeSQL andDelFlagIsNull() {
		isnull("del_flag");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andDelFlagIsNotNull() {
		notNull("del_flag");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andDelFlagIsEmpty() {
		empty("del_flag");
		return this;
	}

	public BpmsServiceFlowNodeSQL andDelFlagIsNotEmpty() {
		notEmpty("del_flag");
		return this;
	}
       public BpmsServiceFlowNodeSQL andDelFlagLike(java.lang.String value) {
    	   addCriterion("del_flag", value, ConditionMode.FUZZY, "delFlag", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowNodeSQL andDelFlagNotLike(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_FUZZY, "delFlag", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowNodeSQL andDelFlagEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andDelFlagNotEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andDelFlagGreaterThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andDelFlagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andDelFlagLessThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andDelFlagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andDelFlagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("del_flag", value1, value2, ConditionMode.BETWEEN, "delFlag", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSQL andDelFlagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("del_flag", value1, value2, ConditionMode.NOT_BETWEEN, "delFlag", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSQL andDelFlagIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.IN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andDelFlagNotIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.NOT_IN, "delFlag", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceFlowNodeSQL andUserCodeIsNull() {
		isnull("user_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andUserCodeIsNotNull() {
		notNull("user_code");
		return this;
	}
	
	public BpmsServiceFlowNodeSQL andUserCodeIsEmpty() {
		empty("user_code");
		return this;
	}

	public BpmsServiceFlowNodeSQL andUserCodeIsNotEmpty() {
		notEmpty("user_code");
		return this;
	}
       public BpmsServiceFlowNodeSQL andUserCodeLike(java.lang.String value) {
    	   addCriterion("user_code", value, ConditionMode.FUZZY, "userCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceFlowNodeSQL andUserCodeNotLike(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_FUZZY, "userCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceFlowNodeSQL andUserCodeEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andUserCodeNotEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andUserCodeGreaterThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andUserCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andUserCodeLessThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andUserCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andUserCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_code", value1, value2, ConditionMode.BETWEEN, "userCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceFlowNodeSQL andUserCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_code", value1, value2, ConditionMode.NOT_BETWEEN, "userCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceFlowNodeSQL andUserCodeIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.IN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceFlowNodeSQL andUserCodeNotIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.NOT_IN, "userCode", "java.lang.String", "String");
          return this;
      }
}