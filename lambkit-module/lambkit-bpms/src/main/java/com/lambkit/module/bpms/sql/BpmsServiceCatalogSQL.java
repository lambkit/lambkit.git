/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceCatalogSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static BpmsServiceCatalogSQL of() {
		return new BpmsServiceCatalogSQL();
	}
	
	public static BpmsServiceCatalogSQL by(Column column) {
		BpmsServiceCatalogSQL that = new BpmsServiceCatalogSQL();
		that.add(column);
        return that;
    }

    public static BpmsServiceCatalogSQL by(String name, Object value) {
        return (BpmsServiceCatalogSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("bpms_service_catalog", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceCatalogSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceCatalogSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public BpmsServiceCatalogSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public BpmsServiceCatalogSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceCatalogSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceCatalogSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceCatalogSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceCatalogSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public BpmsServiceCatalogSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public BpmsServiceCatalogSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public BpmsServiceCatalogSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public BpmsServiceCatalogSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public BpmsServiceCatalogSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public BpmsServiceCatalogSQL andCatalogIdIsNull() {
		isnull("catalog_id");
		return this;
	}
	
	public BpmsServiceCatalogSQL andCatalogIdIsNotNull() {
		notNull("catalog_id");
		return this;
	}
	
	public BpmsServiceCatalogSQL andCatalogIdIsEmpty() {
		empty("catalog_id");
		return this;
	}

	public BpmsServiceCatalogSQL andCatalogIdIsNotEmpty() {
		notEmpty("catalog_id");
		return this;
	}
      public BpmsServiceCatalogSQL andCatalogIdEqualTo(java.lang.Long value) {
          addCriterion("catalog_id", value, ConditionMode.EQUAL, "catalogId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogIdNotEqualTo(java.lang.Long value) {
          addCriterion("catalog_id", value, ConditionMode.NOT_EQUAL, "catalogId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogIdGreaterThan(java.lang.Long value) {
          addCriterion("catalog_id", value, ConditionMode.GREATER_THEN, "catalogId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("catalog_id", value, ConditionMode.GREATER_EQUAL, "catalogId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogIdLessThan(java.lang.Long value) {
          addCriterion("catalog_id", value, ConditionMode.LESS_THEN, "catalogId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("catalog_id", value, ConditionMode.LESS_EQUAL, "catalogId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("catalog_id", value1, value2, ConditionMode.BETWEEN, "catalogId", "java.lang.Long", "Float");
    	  return this;
      }

      public BpmsServiceCatalogSQL andCatalogIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("catalog_id", value1, value2, ConditionMode.NOT_BETWEEN, "catalogId", "java.lang.Long", "Float");
          return this;
      }
        
      public BpmsServiceCatalogSQL andCatalogIdIn(List<java.lang.Long> values) {
          addCriterion("catalog_id", values, ConditionMode.IN, "catalogId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogIdNotIn(List<java.lang.Long> values) {
          addCriterion("catalog_id", values, ConditionMode.NOT_IN, "catalogId", "java.lang.Long", "Float");
          return this;
      }
	public BpmsServiceCatalogSQL andCatalogCodeIsNull() {
		isnull("catalog_code");
		return this;
	}
	
	public BpmsServiceCatalogSQL andCatalogCodeIsNotNull() {
		notNull("catalog_code");
		return this;
	}
	
	public BpmsServiceCatalogSQL andCatalogCodeIsEmpty() {
		empty("catalog_code");
		return this;
	}

	public BpmsServiceCatalogSQL andCatalogCodeIsNotEmpty() {
		notEmpty("catalog_code");
		return this;
	}
       public BpmsServiceCatalogSQL andCatalogCodeLike(java.lang.String value) {
    	   addCriterion("catalog_code", value, ConditionMode.FUZZY, "catalogCode", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceCatalogSQL andCatalogCodeNotLike(java.lang.String value) {
          addCriterion("catalog_code", value, ConditionMode.NOT_FUZZY, "catalogCode", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceCatalogSQL andCatalogCodeEqualTo(java.lang.String value) {
          addCriterion("catalog_code", value, ConditionMode.EQUAL, "catalogCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogCodeNotEqualTo(java.lang.String value) {
          addCriterion("catalog_code", value, ConditionMode.NOT_EQUAL, "catalogCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogCodeGreaterThan(java.lang.String value) {
          addCriterion("catalog_code", value, ConditionMode.GREATER_THEN, "catalogCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("catalog_code", value, ConditionMode.GREATER_EQUAL, "catalogCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogCodeLessThan(java.lang.String value) {
          addCriterion("catalog_code", value, ConditionMode.LESS_THEN, "catalogCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("catalog_code", value, ConditionMode.LESS_EQUAL, "catalogCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("catalog_code", value1, value2, ConditionMode.BETWEEN, "catalogCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceCatalogSQL andCatalogCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("catalog_code", value1, value2, ConditionMode.NOT_BETWEEN, "catalogCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceCatalogSQL andCatalogCodeIn(List<java.lang.String> values) {
          addCriterion("catalog_code", values, ConditionMode.IN, "catalogCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogCodeNotIn(List<java.lang.String> values) {
          addCriterion("catalog_code", values, ConditionMode.NOT_IN, "catalogCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceCatalogSQL andCatalogNameIsNull() {
		isnull("catalog_name");
		return this;
	}
	
	public BpmsServiceCatalogSQL andCatalogNameIsNotNull() {
		notNull("catalog_name");
		return this;
	}
	
	public BpmsServiceCatalogSQL andCatalogNameIsEmpty() {
		empty("catalog_name");
		return this;
	}

	public BpmsServiceCatalogSQL andCatalogNameIsNotEmpty() {
		notEmpty("catalog_name");
		return this;
	}
       public BpmsServiceCatalogSQL andCatalogNameLike(java.lang.String value) {
    	   addCriterion("catalog_name", value, ConditionMode.FUZZY, "catalogName", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceCatalogSQL andCatalogNameNotLike(java.lang.String value) {
          addCriterion("catalog_name", value, ConditionMode.NOT_FUZZY, "catalogName", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceCatalogSQL andCatalogNameEqualTo(java.lang.String value) {
          addCriterion("catalog_name", value, ConditionMode.EQUAL, "catalogName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogNameNotEqualTo(java.lang.String value) {
          addCriterion("catalog_name", value, ConditionMode.NOT_EQUAL, "catalogName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogNameGreaterThan(java.lang.String value) {
          addCriterion("catalog_name", value, ConditionMode.GREATER_THEN, "catalogName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("catalog_name", value, ConditionMode.GREATER_EQUAL, "catalogName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogNameLessThan(java.lang.String value) {
          addCriterion("catalog_name", value, ConditionMode.LESS_THEN, "catalogName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("catalog_name", value, ConditionMode.LESS_EQUAL, "catalogName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("catalog_name", value1, value2, ConditionMode.BETWEEN, "catalogName", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceCatalogSQL andCatalogNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("catalog_name", value1, value2, ConditionMode.NOT_BETWEEN, "catalogName", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceCatalogSQL andCatalogNameIn(List<java.lang.String> values) {
          addCriterion("catalog_name", values, ConditionMode.IN, "catalogName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCatalogNameNotIn(List<java.lang.String> values) {
          addCriterion("catalog_name", values, ConditionMode.NOT_IN, "catalogName", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceCatalogSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public BpmsServiceCatalogSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public BpmsServiceCatalogSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public BpmsServiceCatalogSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public BpmsServiceCatalogSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceCatalogSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceCatalogSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceCatalogSQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public BpmsServiceCatalogSQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public BpmsServiceCatalogSQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public BpmsServiceCatalogSQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public BpmsServiceCatalogSQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceCatalogSQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceCatalogSQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceCatalogSQL andDelFlagIsNull() {
		isnull("del_flag");
		return this;
	}
	
	public BpmsServiceCatalogSQL andDelFlagIsNotNull() {
		notNull("del_flag");
		return this;
	}
	
	public BpmsServiceCatalogSQL andDelFlagIsEmpty() {
		empty("del_flag");
		return this;
	}

	public BpmsServiceCatalogSQL andDelFlagIsNotEmpty() {
		notEmpty("del_flag");
		return this;
	}
       public BpmsServiceCatalogSQL andDelFlagLike(java.lang.String value) {
    	   addCriterion("del_flag", value, ConditionMode.FUZZY, "delFlag", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceCatalogSQL andDelFlagNotLike(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_FUZZY, "delFlag", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceCatalogSQL andDelFlagEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andDelFlagNotEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andDelFlagGreaterThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andDelFlagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andDelFlagLessThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andDelFlagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andDelFlagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("del_flag", value1, value2, ConditionMode.BETWEEN, "delFlag", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceCatalogSQL andDelFlagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("del_flag", value1, value2, ConditionMode.NOT_BETWEEN, "delFlag", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceCatalogSQL andDelFlagIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.IN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andDelFlagNotIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.NOT_IN, "delFlag", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceCatalogSQL andUserCodeIsNull() {
		isnull("user_code");
		return this;
	}
	
	public BpmsServiceCatalogSQL andUserCodeIsNotNull() {
		notNull("user_code");
		return this;
	}
	
	public BpmsServiceCatalogSQL andUserCodeIsEmpty() {
		empty("user_code");
		return this;
	}

	public BpmsServiceCatalogSQL andUserCodeIsNotEmpty() {
		notEmpty("user_code");
		return this;
	}
       public BpmsServiceCatalogSQL andUserCodeLike(java.lang.String value) {
    	   addCriterion("user_code", value, ConditionMode.FUZZY, "userCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceCatalogSQL andUserCodeNotLike(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_FUZZY, "userCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceCatalogSQL andUserCodeEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andUserCodeNotEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andUserCodeGreaterThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andUserCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andUserCodeLessThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andUserCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andUserCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_code", value1, value2, ConditionMode.BETWEEN, "userCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceCatalogSQL andUserCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_code", value1, value2, ConditionMode.NOT_BETWEEN, "userCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceCatalogSQL andUserCodeIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.IN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceCatalogSQL andUserCodeNotIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.NOT_IN, "userCode", "java.lang.String", "String");
          return this;
      }
}