/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsRuleScriptSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static BpmsRuleScriptSQL of() {
		return new BpmsRuleScriptSQL();
	}
	
	public static BpmsRuleScriptSQL by(Column column) {
		BpmsRuleScriptSQL that = new BpmsRuleScriptSQL();
		that.add(column);
        return that;
    }

    public static BpmsRuleScriptSQL by(String name, Object value) {
        return (BpmsRuleScriptSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("bpms_rule_script", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsRuleScriptSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsRuleScriptSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public BpmsRuleScriptSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public BpmsRuleScriptSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsRuleScriptSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsRuleScriptSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsRuleScriptSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsRuleScriptSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public BpmsRuleScriptSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public BpmsRuleScriptSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public BpmsRuleScriptSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public BpmsRuleScriptSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public BpmsRuleScriptSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public BpmsRuleScriptSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public BpmsRuleScriptSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public BpmsRuleScriptSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public BpmsRuleScriptSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public BpmsRuleScriptSQL andIdEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andIdNotEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andIdGreaterThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andIdLessThan(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Long", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Long", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Long", "Float");
    	  return this;
      }

      public BpmsRuleScriptSQL andIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Long", "Float");
          return this;
      }
        
      public BpmsRuleScriptSQL andIdIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Long", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andIdNotIn(List<java.lang.Long> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Long", "Float");
          return this;
      }
	public BpmsRuleScriptSQL andApplicationNameIsNull() {
		isnull("application_name");
		return this;
	}
	
	public BpmsRuleScriptSQL andApplicationNameIsNotNull() {
		notNull("application_name");
		return this;
	}
	
	public BpmsRuleScriptSQL andApplicationNameIsEmpty() {
		empty("application_name");
		return this;
	}

	public BpmsRuleScriptSQL andApplicationNameIsNotEmpty() {
		notEmpty("application_name");
		return this;
	}
       public BpmsRuleScriptSQL andApplicationNameLike(java.lang.String value) {
    	   addCriterion("application_name", value, ConditionMode.FUZZY, "applicationName", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsRuleScriptSQL andApplicationNameNotLike(java.lang.String value) {
          addCriterion("application_name", value, ConditionMode.NOT_FUZZY, "applicationName", "java.lang.String", "Float");
          return this;
      }
      public BpmsRuleScriptSQL andApplicationNameEqualTo(java.lang.String value) {
          addCriterion("application_name", value, ConditionMode.EQUAL, "applicationName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andApplicationNameNotEqualTo(java.lang.String value) {
          addCriterion("application_name", value, ConditionMode.NOT_EQUAL, "applicationName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andApplicationNameGreaterThan(java.lang.String value) {
          addCriterion("application_name", value, ConditionMode.GREATER_THEN, "applicationName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andApplicationNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("application_name", value, ConditionMode.GREATER_EQUAL, "applicationName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andApplicationNameLessThan(java.lang.String value) {
          addCriterion("application_name", value, ConditionMode.LESS_THEN, "applicationName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andApplicationNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("application_name", value, ConditionMode.LESS_EQUAL, "applicationName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andApplicationNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("application_name", value1, value2, ConditionMode.BETWEEN, "applicationName", "java.lang.String", "String");
    	  return this;
      }

      public BpmsRuleScriptSQL andApplicationNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("application_name", value1, value2, ConditionMode.NOT_BETWEEN, "applicationName", "java.lang.String", "String");
          return this;
      }
        
      public BpmsRuleScriptSQL andApplicationNameIn(List<java.lang.String> values) {
          addCriterion("application_name", values, ConditionMode.IN, "applicationName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andApplicationNameNotIn(List<java.lang.String> values) {
          addCriterion("application_name", values, ConditionMode.NOT_IN, "applicationName", "java.lang.String", "String");
          return this;
      }
	public BpmsRuleScriptSQL andScriptIdIsNull() {
		isnull("script_id");
		return this;
	}
	
	public BpmsRuleScriptSQL andScriptIdIsNotNull() {
		notNull("script_id");
		return this;
	}
	
	public BpmsRuleScriptSQL andScriptIdIsEmpty() {
		empty("script_id");
		return this;
	}

	public BpmsRuleScriptSQL andScriptIdIsNotEmpty() {
		notEmpty("script_id");
		return this;
	}
       public BpmsRuleScriptSQL andScriptIdLike(java.lang.String value) {
    	   addCriterion("script_id", value, ConditionMode.FUZZY, "scriptId", "java.lang.String", "String");
    	   return this;
      }

      public BpmsRuleScriptSQL andScriptIdNotLike(java.lang.String value) {
          addCriterion("script_id", value, ConditionMode.NOT_FUZZY, "scriptId", "java.lang.String", "String");
          return this;
      }
      public BpmsRuleScriptSQL andScriptIdEqualTo(java.lang.String value) {
          addCriterion("script_id", value, ConditionMode.EQUAL, "scriptId", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptIdNotEqualTo(java.lang.String value) {
          addCriterion("script_id", value, ConditionMode.NOT_EQUAL, "scriptId", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptIdGreaterThan(java.lang.String value) {
          addCriterion("script_id", value, ConditionMode.GREATER_THEN, "scriptId", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptIdGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("script_id", value, ConditionMode.GREATER_EQUAL, "scriptId", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptIdLessThan(java.lang.String value) {
          addCriterion("script_id", value, ConditionMode.LESS_THEN, "scriptId", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptIdLessThanOrEqualTo(java.lang.String value) {
          addCriterion("script_id", value, ConditionMode.LESS_EQUAL, "scriptId", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptIdBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("script_id", value1, value2, ConditionMode.BETWEEN, "scriptId", "java.lang.String", "String");
    	  return this;
      }

      public BpmsRuleScriptSQL andScriptIdNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("script_id", value1, value2, ConditionMode.NOT_BETWEEN, "scriptId", "java.lang.String", "String");
          return this;
      }
        
      public BpmsRuleScriptSQL andScriptIdIn(List<java.lang.String> values) {
          addCriterion("script_id", values, ConditionMode.IN, "scriptId", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptIdNotIn(List<java.lang.String> values) {
          addCriterion("script_id", values, ConditionMode.NOT_IN, "scriptId", "java.lang.String", "String");
          return this;
      }
	public BpmsRuleScriptSQL andScriptNameIsNull() {
		isnull("script_name");
		return this;
	}
	
	public BpmsRuleScriptSQL andScriptNameIsNotNull() {
		notNull("script_name");
		return this;
	}
	
	public BpmsRuleScriptSQL andScriptNameIsEmpty() {
		empty("script_name");
		return this;
	}

	public BpmsRuleScriptSQL andScriptNameIsNotEmpty() {
		notEmpty("script_name");
		return this;
	}
       public BpmsRuleScriptSQL andScriptNameLike(java.lang.String value) {
    	   addCriterion("script_name", value, ConditionMode.FUZZY, "scriptName", "java.lang.String", "String");
    	   return this;
      }

      public BpmsRuleScriptSQL andScriptNameNotLike(java.lang.String value) {
          addCriterion("script_name", value, ConditionMode.NOT_FUZZY, "scriptName", "java.lang.String", "String");
          return this;
      }
      public BpmsRuleScriptSQL andScriptNameEqualTo(java.lang.String value) {
          addCriterion("script_name", value, ConditionMode.EQUAL, "scriptName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptNameNotEqualTo(java.lang.String value) {
          addCriterion("script_name", value, ConditionMode.NOT_EQUAL, "scriptName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptNameGreaterThan(java.lang.String value) {
          addCriterion("script_name", value, ConditionMode.GREATER_THEN, "scriptName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("script_name", value, ConditionMode.GREATER_EQUAL, "scriptName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptNameLessThan(java.lang.String value) {
          addCriterion("script_name", value, ConditionMode.LESS_THEN, "scriptName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("script_name", value, ConditionMode.LESS_EQUAL, "scriptName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("script_name", value1, value2, ConditionMode.BETWEEN, "scriptName", "java.lang.String", "String");
    	  return this;
      }

      public BpmsRuleScriptSQL andScriptNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("script_name", value1, value2, ConditionMode.NOT_BETWEEN, "scriptName", "java.lang.String", "String");
          return this;
      }
        
      public BpmsRuleScriptSQL andScriptNameIn(List<java.lang.String> values) {
          addCriterion("script_name", values, ConditionMode.IN, "scriptName", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptNameNotIn(List<java.lang.String> values) {
          addCriterion("script_name", values, ConditionMode.NOT_IN, "scriptName", "java.lang.String", "String");
          return this;
      }
	public BpmsRuleScriptSQL andScriptDataIsNull() {
		isnull("script_data");
		return this;
	}
	
	public BpmsRuleScriptSQL andScriptDataIsNotNull() {
		notNull("script_data");
		return this;
	}
	
	public BpmsRuleScriptSQL andScriptDataIsEmpty() {
		empty("script_data");
		return this;
	}

	public BpmsRuleScriptSQL andScriptDataIsNotEmpty() {
		notEmpty("script_data");
		return this;
	}
       public BpmsRuleScriptSQL andScriptDataLike(java.lang.String value) {
    	   addCriterion("script_data", value, ConditionMode.FUZZY, "scriptData", "java.lang.String", "String");
    	   return this;
      }

      public BpmsRuleScriptSQL andScriptDataNotLike(java.lang.String value) {
          addCriterion("script_data", value, ConditionMode.NOT_FUZZY, "scriptData", "java.lang.String", "String");
          return this;
      }
      public BpmsRuleScriptSQL andScriptDataEqualTo(java.lang.String value) {
          addCriterion("script_data", value, ConditionMode.EQUAL, "scriptData", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptDataNotEqualTo(java.lang.String value) {
          addCriterion("script_data", value, ConditionMode.NOT_EQUAL, "scriptData", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptDataGreaterThan(java.lang.String value) {
          addCriterion("script_data", value, ConditionMode.GREATER_THEN, "scriptData", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptDataGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("script_data", value, ConditionMode.GREATER_EQUAL, "scriptData", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptDataLessThan(java.lang.String value) {
          addCriterion("script_data", value, ConditionMode.LESS_THEN, "scriptData", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptDataLessThanOrEqualTo(java.lang.String value) {
          addCriterion("script_data", value, ConditionMode.LESS_EQUAL, "scriptData", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptDataBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("script_data", value1, value2, ConditionMode.BETWEEN, "scriptData", "java.lang.String", "String");
    	  return this;
      }

      public BpmsRuleScriptSQL andScriptDataNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("script_data", value1, value2, ConditionMode.NOT_BETWEEN, "scriptData", "java.lang.String", "String");
          return this;
      }
        
      public BpmsRuleScriptSQL andScriptDataIn(List<java.lang.String> values) {
          addCriterion("script_data", values, ConditionMode.IN, "scriptData", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptDataNotIn(List<java.lang.String> values) {
          addCriterion("script_data", values, ConditionMode.NOT_IN, "scriptData", "java.lang.String", "String");
          return this;
      }
	public BpmsRuleScriptSQL andScriptTypeIsNull() {
		isnull("script_type");
		return this;
	}
	
	public BpmsRuleScriptSQL andScriptTypeIsNotNull() {
		notNull("script_type");
		return this;
	}
	
	public BpmsRuleScriptSQL andScriptTypeIsEmpty() {
		empty("script_type");
		return this;
	}

	public BpmsRuleScriptSQL andScriptTypeIsNotEmpty() {
		notEmpty("script_type");
		return this;
	}
       public BpmsRuleScriptSQL andScriptTypeLike(java.lang.String value) {
    	   addCriterion("script_type", value, ConditionMode.FUZZY, "scriptType", "java.lang.String", "String");
    	   return this;
      }

      public BpmsRuleScriptSQL andScriptTypeNotLike(java.lang.String value) {
          addCriterion("script_type", value, ConditionMode.NOT_FUZZY, "scriptType", "java.lang.String", "String");
          return this;
      }
      public BpmsRuleScriptSQL andScriptTypeEqualTo(java.lang.String value) {
          addCriterion("script_type", value, ConditionMode.EQUAL, "scriptType", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptTypeNotEqualTo(java.lang.String value) {
          addCriterion("script_type", value, ConditionMode.NOT_EQUAL, "scriptType", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptTypeGreaterThan(java.lang.String value) {
          addCriterion("script_type", value, ConditionMode.GREATER_THEN, "scriptType", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("script_type", value, ConditionMode.GREATER_EQUAL, "scriptType", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptTypeLessThan(java.lang.String value) {
          addCriterion("script_type", value, ConditionMode.LESS_THEN, "scriptType", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("script_type", value, ConditionMode.LESS_EQUAL, "scriptType", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("script_type", value1, value2, ConditionMode.BETWEEN, "scriptType", "java.lang.String", "String");
    	  return this;
      }

      public BpmsRuleScriptSQL andScriptTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("script_type", value1, value2, ConditionMode.NOT_BETWEEN, "scriptType", "java.lang.String", "String");
          return this;
      }
        
      public BpmsRuleScriptSQL andScriptTypeIn(List<java.lang.String> values) {
          addCriterion("script_type", values, ConditionMode.IN, "scriptType", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptTypeNotIn(List<java.lang.String> values) {
          addCriterion("script_type", values, ConditionMode.NOT_IN, "scriptType", "java.lang.String", "String");
          return this;
      }
	public BpmsRuleScriptSQL andScriptLanguageIsNull() {
		isnull("script_language");
		return this;
	}
	
	public BpmsRuleScriptSQL andScriptLanguageIsNotNull() {
		notNull("script_language");
		return this;
	}
	
	public BpmsRuleScriptSQL andScriptLanguageIsEmpty() {
		empty("script_language");
		return this;
	}

	public BpmsRuleScriptSQL andScriptLanguageIsNotEmpty() {
		notEmpty("script_language");
		return this;
	}
       public BpmsRuleScriptSQL andScriptLanguageLike(java.lang.String value) {
    	   addCriterion("script_language", value, ConditionMode.FUZZY, "scriptLanguage", "java.lang.String", "String");
    	   return this;
      }

      public BpmsRuleScriptSQL andScriptLanguageNotLike(java.lang.String value) {
          addCriterion("script_language", value, ConditionMode.NOT_FUZZY, "scriptLanguage", "java.lang.String", "String");
          return this;
      }
      public BpmsRuleScriptSQL andScriptLanguageEqualTo(java.lang.String value) {
          addCriterion("script_language", value, ConditionMode.EQUAL, "scriptLanguage", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptLanguageNotEqualTo(java.lang.String value) {
          addCriterion("script_language", value, ConditionMode.NOT_EQUAL, "scriptLanguage", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptLanguageGreaterThan(java.lang.String value) {
          addCriterion("script_language", value, ConditionMode.GREATER_THEN, "scriptLanguage", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptLanguageGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("script_language", value, ConditionMode.GREATER_EQUAL, "scriptLanguage", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptLanguageLessThan(java.lang.String value) {
          addCriterion("script_language", value, ConditionMode.LESS_THEN, "scriptLanguage", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptLanguageLessThanOrEqualTo(java.lang.String value) {
          addCriterion("script_language", value, ConditionMode.LESS_EQUAL, "scriptLanguage", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptLanguageBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("script_language", value1, value2, ConditionMode.BETWEEN, "scriptLanguage", "java.lang.String", "String");
    	  return this;
      }

      public BpmsRuleScriptSQL andScriptLanguageNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("script_language", value1, value2, ConditionMode.NOT_BETWEEN, "scriptLanguage", "java.lang.String", "String");
          return this;
      }
        
      public BpmsRuleScriptSQL andScriptLanguageIn(List<java.lang.String> values) {
          addCriterion("script_language", values, ConditionMode.IN, "scriptLanguage", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andScriptLanguageNotIn(List<java.lang.String> values) {
          addCriterion("script_language", values, ConditionMode.NOT_IN, "scriptLanguage", "java.lang.String", "String");
          return this;
      }
	public BpmsRuleScriptSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public BpmsRuleScriptSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public BpmsRuleScriptSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public BpmsRuleScriptSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public BpmsRuleScriptSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public BpmsRuleScriptSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public BpmsRuleScriptSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public BpmsRuleScriptSQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public BpmsRuleScriptSQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public BpmsRuleScriptSQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public BpmsRuleScriptSQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public BpmsRuleScriptSQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsRuleScriptSQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsRuleScriptSQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsRuleScriptSQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsRuleScriptSQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsRuleScriptSQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsRuleScriptSQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsRuleScriptSQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsRuleScriptSQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsRuleScriptSQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
	public BpmsRuleScriptSQL andUpdaterIsNull() {
		isnull("updater");
		return this;
	}
	
	public BpmsRuleScriptSQL andUpdaterIsNotNull() {
		notNull("updater");
		return this;
	}
	
	public BpmsRuleScriptSQL andUpdaterIsEmpty() {
		empty("updater");
		return this;
	}

	public BpmsRuleScriptSQL andUpdaterIsNotEmpty() {
		notEmpty("updater");
		return this;
	}
       public BpmsRuleScriptSQL andUpdaterLike(java.lang.String value) {
    	   addCriterion("updater", value, ConditionMode.FUZZY, "updater", "java.lang.String", "String");
    	   return this;
      }

      public BpmsRuleScriptSQL andUpdaterNotLike(java.lang.String value) {
          addCriterion("updater", value, ConditionMode.NOT_FUZZY, "updater", "java.lang.String", "String");
          return this;
      }
      public BpmsRuleScriptSQL andUpdaterEqualTo(java.lang.String value) {
          addCriterion("updater", value, ConditionMode.EQUAL, "updater", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andUpdaterNotEqualTo(java.lang.String value) {
          addCriterion("updater", value, ConditionMode.NOT_EQUAL, "updater", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andUpdaterGreaterThan(java.lang.String value) {
          addCriterion("updater", value, ConditionMode.GREATER_THEN, "updater", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andUpdaterGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("updater", value, ConditionMode.GREATER_EQUAL, "updater", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andUpdaterLessThan(java.lang.String value) {
          addCriterion("updater", value, ConditionMode.LESS_THEN, "updater", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andUpdaterLessThanOrEqualTo(java.lang.String value) {
          addCriterion("updater", value, ConditionMode.LESS_EQUAL, "updater", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andUpdaterBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("updater", value1, value2, ConditionMode.BETWEEN, "updater", "java.lang.String", "String");
    	  return this;
      }

      public BpmsRuleScriptSQL andUpdaterNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("updater", value1, value2, ConditionMode.NOT_BETWEEN, "updater", "java.lang.String", "String");
          return this;
      }
        
      public BpmsRuleScriptSQL andUpdaterIn(List<java.lang.String> values) {
          addCriterion("updater", values, ConditionMode.IN, "updater", "java.lang.String", "String");
          return this;
      }

      public BpmsRuleScriptSQL andUpdaterNotIn(List<java.lang.String> values) {
          addCriterion("updater", values, ConditionMode.NOT_IN, "updater", "java.lang.String", "String");
          return this;
      }
	public BpmsRuleScriptSQL andVersionIsNull() {
		isnull("version");
		return this;
	}
	
	public BpmsRuleScriptSQL andVersionIsNotNull() {
		notNull("version");
		return this;
	}
	
	public BpmsRuleScriptSQL andVersionIsEmpty() {
		empty("version");
		return this;
	}

	public BpmsRuleScriptSQL andVersionIsNotEmpty() {
		notEmpty("version");
		return this;
	}
      public BpmsRuleScriptSQL andVersionEqualTo(java.lang.Integer value) {
          addCriterion("version", value, ConditionMode.EQUAL, "version", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andVersionNotEqualTo(java.lang.Integer value) {
          addCriterion("version", value, ConditionMode.NOT_EQUAL, "version", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andVersionGreaterThan(java.lang.Integer value) {
          addCriterion("version", value, ConditionMode.GREATER_THEN, "version", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andVersionGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("version", value, ConditionMode.GREATER_EQUAL, "version", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andVersionLessThan(java.lang.Integer value) {
          addCriterion("version", value, ConditionMode.LESS_THEN, "version", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andVersionLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("version", value, ConditionMode.LESS_EQUAL, "version", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andVersionBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("version", value1, value2, ConditionMode.BETWEEN, "version", "java.lang.Integer", "Float");
    	  return this;
      }

      public BpmsRuleScriptSQL andVersionNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("version", value1, value2, ConditionMode.NOT_BETWEEN, "version", "java.lang.Integer", "Float");
          return this;
      }
        
      public BpmsRuleScriptSQL andVersionIn(List<java.lang.Integer> values) {
          addCriterion("version", values, ConditionMode.IN, "version", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsRuleScriptSQL andVersionNotIn(List<java.lang.Integer> values) {
          addCriterion("version", values, ConditionMode.NOT_IN, "version", "java.lang.Integer", "Float");
          return this;
      }
}