/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsRuleScript extends RowModel<BpmsRuleScript> {
	public BpmsRuleScript() {
		setTableName("bpms_rule_script");
		setPrimaryKey("id");
	}
	public java.lang.Long getId() {
		return this.get("id");
	}
	public void setId(java.lang.Long id) {
		this.set("id", id);
	}
	public java.lang.String getApplicationName() {
		return this.get("application_name");
	}
	public void setApplicationName(java.lang.String applicationName) {
		this.set("application_name", applicationName);
	}
	public java.lang.String getScriptId() {
		return this.get("script_id");
	}
	public void setScriptId(java.lang.String scriptId) {
		this.set("script_id", scriptId);
	}
	public java.lang.String getScriptName() {
		return this.get("script_name");
	}
	public void setScriptName(java.lang.String scriptName) {
		this.set("script_name", scriptName);
	}
	public java.lang.String getScriptData() {
		return this.get("script_data");
	}
	public void setScriptData(java.lang.String scriptData) {
		this.set("script_data", scriptData);
	}
	public java.lang.String getScriptType() {
		return this.get("script_type");
	}
	public void setScriptType(java.lang.String scriptType) {
		this.set("script_type", scriptType);
	}
	public java.lang.String getScriptLanguage() {
		return this.get("script_language");
	}
	public void setScriptLanguage(java.lang.String scriptLanguage) {
		this.set("script_language", scriptLanguage);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.util.Date getUpdateTime() {
		return this.get("update_time");
	}
	public void setUpdateTime(java.util.Date updateTime) {
		this.set("update_time", updateTime);
	}
	public java.lang.String getUpdater() {
		return this.get("updater");
	}
	public void setUpdater(java.lang.String updater) {
		this.set("updater", updater);
	}
	public java.lang.Integer getVersion() {
		return this.get("version");
	}
	public void setVersion(java.lang.Integer version) {
		this.set("version", version);
	}
}
