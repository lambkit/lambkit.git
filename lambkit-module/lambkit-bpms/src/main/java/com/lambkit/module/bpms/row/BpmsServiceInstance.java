/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceInstance extends RowModel<BpmsServiceInstance> {
	public BpmsServiceInstance() {
		setTableName("bpms_service_instance");
		setPrimaryKey("instance_id");
	}
	public java.lang.Long getInstanceId() {
		return this.get("instance_id");
	}
	public void setInstanceId(java.lang.Long instanceId) {
		this.set("instance_id", instanceId);
	}
	public java.lang.String getFlowCode() {
		return this.get("flow_code");
	}
	public void setFlowCode(java.lang.String flowCode) {
		this.set("flow_code", flowCode);
	}
	public java.lang.String getInstanceCode() {
		return this.get("instance_code");
	}
	public void setInstanceCode(java.lang.String instanceCode) {
		this.set("instance_code", instanceCode);
	}
	public java.lang.String getBusinessId() {
		return this.get("business_id");
	}
	public void setBusinessId(java.lang.String businessId) {
		this.set("business_id", businessId);
	}
	public java.lang.Integer getFlowStatus() {
		return this.get("flow_status");
	}
	public void setFlowStatus(java.lang.Integer flowStatus) {
		this.set("flow_status", flowStatus);
	}
	public java.lang.String getCreateUser() {
		return this.get("create_user");
	}
	public void setCreateUser(java.lang.String createUser) {
		this.set("create_user", createUser);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
	public java.util.Date getUpdateTime() {
		return this.get("update_time");
	}
	public void setUpdateTime(java.util.Date updateTime) {
		this.set("update_time", updateTime);
	}
	public java.lang.String getDelFlag() {
		return this.get("del_flag");
	}
	public void setDelFlag(java.lang.String delFlag) {
		this.set("del_flag", delFlag);
	}
	public java.lang.String getUserCode() {
		return this.get("user_code");
	}
	public void setUserCode(java.lang.String userCode) {
		this.set("user_code", userCode);
	}
}
