/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.bpms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class BpmsServiceNodeSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static BpmsServiceNodeSQL of() {
		return new BpmsServiceNodeSQL();
	}
	
	public static BpmsServiceNodeSQL by(Column column) {
		BpmsServiceNodeSQL that = new BpmsServiceNodeSQL();
		that.add(column);
        return that;
    }

    public static BpmsServiceNodeSQL by(String name, Object value) {
        return (BpmsServiceNodeSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("bpms_service_node", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceNodeSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceNodeSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public BpmsServiceNodeSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public BpmsServiceNodeSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceNodeSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceNodeSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceNodeSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public BpmsServiceNodeSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public BpmsServiceNodeSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public BpmsServiceNodeSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public BpmsServiceNodeSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public BpmsServiceNodeSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public BpmsServiceNodeSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public BpmsServiceNodeSQL andNodeIdIsNull() {
		isnull("node_id");
		return this;
	}
	
	public BpmsServiceNodeSQL andNodeIdIsNotNull() {
		notNull("node_id");
		return this;
	}
	
	public BpmsServiceNodeSQL andNodeIdIsEmpty() {
		empty("node_id");
		return this;
	}

	public BpmsServiceNodeSQL andNodeIdIsNotEmpty() {
		notEmpty("node_id");
		return this;
	}
      public BpmsServiceNodeSQL andNodeIdEqualTo(java.lang.Long value) {
          addCriterion("node_id", value, ConditionMode.EQUAL, "nodeId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeIdNotEqualTo(java.lang.Long value) {
          addCriterion("node_id", value, ConditionMode.NOT_EQUAL, "nodeId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeIdGreaterThan(java.lang.Long value) {
          addCriterion("node_id", value, ConditionMode.GREATER_THEN, "nodeId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("node_id", value, ConditionMode.GREATER_EQUAL, "nodeId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeIdLessThan(java.lang.Long value) {
          addCriterion("node_id", value, ConditionMode.LESS_THEN, "nodeId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("node_id", value, ConditionMode.LESS_EQUAL, "nodeId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("node_id", value1, value2, ConditionMode.BETWEEN, "nodeId", "java.lang.Long", "Float");
    	  return this;
      }

      public BpmsServiceNodeSQL andNodeIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("node_id", value1, value2, ConditionMode.NOT_BETWEEN, "nodeId", "java.lang.Long", "Float");
          return this;
      }
        
      public BpmsServiceNodeSQL andNodeIdIn(List<java.lang.Long> values) {
          addCriterion("node_id", values, ConditionMode.IN, "nodeId", "java.lang.Long", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeIdNotIn(List<java.lang.Long> values) {
          addCriterion("node_id", values, ConditionMode.NOT_IN, "nodeId", "java.lang.Long", "Float");
          return this;
      }
	public BpmsServiceNodeSQL andNodeCodeIsNull() {
		isnull("node_code");
		return this;
	}
	
	public BpmsServiceNodeSQL andNodeCodeIsNotNull() {
		notNull("node_code");
		return this;
	}
	
	public BpmsServiceNodeSQL andNodeCodeIsEmpty() {
		empty("node_code");
		return this;
	}

	public BpmsServiceNodeSQL andNodeCodeIsNotEmpty() {
		notEmpty("node_code");
		return this;
	}
       public BpmsServiceNodeSQL andNodeCodeLike(java.lang.String value) {
    	   addCriterion("node_code", value, ConditionMode.FUZZY, "nodeCode", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceNodeSQL andNodeCodeNotLike(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.NOT_FUZZY, "nodeCode", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceNodeSQL andNodeCodeEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeCodeNotEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.NOT_EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeCodeGreaterThan(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.GREATER_THEN, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.GREATER_EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeCodeLessThan(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.LESS_THEN, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("node_code", value, ConditionMode.LESS_EQUAL, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("node_code", value1, value2, ConditionMode.BETWEEN, "nodeCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceNodeSQL andNodeCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("node_code", value1, value2, ConditionMode.NOT_BETWEEN, "nodeCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceNodeSQL andNodeCodeIn(List<java.lang.String> values) {
          addCriterion("node_code", values, ConditionMode.IN, "nodeCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeCodeNotIn(List<java.lang.String> values) {
          addCriterion("node_code", values, ConditionMode.NOT_IN, "nodeCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceNodeSQL andNodeNameIsNull() {
		isnull("node_name");
		return this;
	}
	
	public BpmsServiceNodeSQL andNodeNameIsNotNull() {
		notNull("node_name");
		return this;
	}
	
	public BpmsServiceNodeSQL andNodeNameIsEmpty() {
		empty("node_name");
		return this;
	}

	public BpmsServiceNodeSQL andNodeNameIsNotEmpty() {
		notEmpty("node_name");
		return this;
	}
       public BpmsServiceNodeSQL andNodeNameLike(java.lang.String value) {
    	   addCriterion("node_name", value, ConditionMode.FUZZY, "nodeName", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceNodeSQL andNodeNameNotLike(java.lang.String value) {
          addCriterion("node_name", value, ConditionMode.NOT_FUZZY, "nodeName", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceNodeSQL andNodeNameEqualTo(java.lang.String value) {
          addCriterion("node_name", value, ConditionMode.EQUAL, "nodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeNameNotEqualTo(java.lang.String value) {
          addCriterion("node_name", value, ConditionMode.NOT_EQUAL, "nodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeNameGreaterThan(java.lang.String value) {
          addCriterion("node_name", value, ConditionMode.GREATER_THEN, "nodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("node_name", value, ConditionMode.GREATER_EQUAL, "nodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeNameLessThan(java.lang.String value) {
          addCriterion("node_name", value, ConditionMode.LESS_THEN, "nodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("node_name", value, ConditionMode.LESS_EQUAL, "nodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("node_name", value1, value2, ConditionMode.BETWEEN, "nodeName", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceNodeSQL andNodeNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("node_name", value1, value2, ConditionMode.NOT_BETWEEN, "nodeName", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceNodeSQL andNodeNameIn(List<java.lang.String> values) {
          addCriterion("node_name", values, ConditionMode.IN, "nodeName", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeNameNotIn(List<java.lang.String> values) {
          addCriterion("node_name", values, ConditionMode.NOT_IN, "nodeName", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceNodeSQL andNodeTypeIsNull() {
		isnull("node_type");
		return this;
	}
	
	public BpmsServiceNodeSQL andNodeTypeIsNotNull() {
		notNull("node_type");
		return this;
	}
	
	public BpmsServiceNodeSQL andNodeTypeIsEmpty() {
		empty("node_type");
		return this;
	}

	public BpmsServiceNodeSQL andNodeTypeIsNotEmpty() {
		notEmpty("node_type");
		return this;
	}
      public BpmsServiceNodeSQL andNodeTypeEqualTo(java.lang.Integer value) {
          addCriterion("node_type", value, ConditionMode.EQUAL, "nodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeTypeNotEqualTo(java.lang.Integer value) {
          addCriterion("node_type", value, ConditionMode.NOT_EQUAL, "nodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeTypeGreaterThan(java.lang.Integer value) {
          addCriterion("node_type", value, ConditionMode.GREATER_THEN, "nodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeTypeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("node_type", value, ConditionMode.GREATER_EQUAL, "nodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeTypeLessThan(java.lang.Integer value) {
          addCriterion("node_type", value, ConditionMode.LESS_THEN, "nodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeTypeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("node_type", value, ConditionMode.LESS_EQUAL, "nodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeTypeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("node_type", value1, value2, ConditionMode.BETWEEN, "nodeType", "java.lang.Integer", "Float");
    	  return this;
      }

      public BpmsServiceNodeSQL andNodeTypeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("node_type", value1, value2, ConditionMode.NOT_BETWEEN, "nodeType", "java.lang.Integer", "Float");
          return this;
      }
        
      public BpmsServiceNodeSQL andNodeTypeIn(List<java.lang.Integer> values) {
          addCriterion("node_type", values, ConditionMode.IN, "nodeType", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeTypeNotIn(List<java.lang.Integer> values) {
          addCriterion("node_type", values, ConditionMode.NOT_IN, "nodeType", "java.lang.Integer", "Float");
          return this;
      }
	public BpmsServiceNodeSQL andPermissionFlagIsNull() {
		isnull("permission_flag");
		return this;
	}
	
	public BpmsServiceNodeSQL andPermissionFlagIsNotNull() {
		notNull("permission_flag");
		return this;
	}
	
	public BpmsServiceNodeSQL andPermissionFlagIsEmpty() {
		empty("permission_flag");
		return this;
	}

	public BpmsServiceNodeSQL andPermissionFlagIsNotEmpty() {
		notEmpty("permission_flag");
		return this;
	}
       public BpmsServiceNodeSQL andPermissionFlagLike(java.lang.String value) {
    	   addCriterion("permission_flag", value, ConditionMode.FUZZY, "permissionFlag", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceNodeSQL andPermissionFlagNotLike(java.lang.String value) {
          addCriterion("permission_flag", value, ConditionMode.NOT_FUZZY, "permissionFlag", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceNodeSQL andPermissionFlagEqualTo(java.lang.String value) {
          addCriterion("permission_flag", value, ConditionMode.EQUAL, "permissionFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andPermissionFlagNotEqualTo(java.lang.String value) {
          addCriterion("permission_flag", value, ConditionMode.NOT_EQUAL, "permissionFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andPermissionFlagGreaterThan(java.lang.String value) {
          addCriterion("permission_flag", value, ConditionMode.GREATER_THEN, "permissionFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andPermissionFlagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("permission_flag", value, ConditionMode.GREATER_EQUAL, "permissionFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andPermissionFlagLessThan(java.lang.String value) {
          addCriterion("permission_flag", value, ConditionMode.LESS_THEN, "permissionFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andPermissionFlagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("permission_flag", value, ConditionMode.LESS_EQUAL, "permissionFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andPermissionFlagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("permission_flag", value1, value2, ConditionMode.BETWEEN, "permissionFlag", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceNodeSQL andPermissionFlagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("permission_flag", value1, value2, ConditionMode.NOT_BETWEEN, "permissionFlag", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceNodeSQL andPermissionFlagIn(List<java.lang.String> values) {
          addCriterion("permission_flag", values, ConditionMode.IN, "permissionFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andPermissionFlagNotIn(List<java.lang.String> values) {
          addCriterion("permission_flag", values, ConditionMode.NOT_IN, "permissionFlag", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceNodeSQL andNodeStatusIsNull() {
		isnull("node_status");
		return this;
	}
	
	public BpmsServiceNodeSQL andNodeStatusIsNotNull() {
		notNull("node_status");
		return this;
	}
	
	public BpmsServiceNodeSQL andNodeStatusIsEmpty() {
		empty("node_status");
		return this;
	}

	public BpmsServiceNodeSQL andNodeStatusIsNotEmpty() {
		notEmpty("node_status");
		return this;
	}
      public BpmsServiceNodeSQL andNodeStatusEqualTo(java.lang.Integer value) {
          addCriterion("node_status", value, ConditionMode.EQUAL, "nodeStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("node_status", value, ConditionMode.NOT_EQUAL, "nodeStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeStatusGreaterThan(java.lang.Integer value) {
          addCriterion("node_status", value, ConditionMode.GREATER_THEN, "nodeStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("node_status", value, ConditionMode.GREATER_EQUAL, "nodeStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeStatusLessThan(java.lang.Integer value) {
          addCriterion("node_status", value, ConditionMode.LESS_THEN, "nodeStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("node_status", value, ConditionMode.LESS_EQUAL, "nodeStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("node_status", value1, value2, ConditionMode.BETWEEN, "nodeStatus", "java.lang.Integer", "Float");
    	  return this;
      }

      public BpmsServiceNodeSQL andNodeStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("node_status", value1, value2, ConditionMode.NOT_BETWEEN, "nodeStatus", "java.lang.Integer", "Float");
          return this;
      }
        
      public BpmsServiceNodeSQL andNodeStatusIn(List<java.lang.Integer> values) {
          addCriterion("node_status", values, ConditionMode.IN, "nodeStatus", "java.lang.Integer", "Float");
          return this;
      }

      public BpmsServiceNodeSQL andNodeStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("node_status", values, ConditionMode.NOT_IN, "nodeStatus", "java.lang.Integer", "Float");
          return this;
      }
	public BpmsServiceNodeSQL andCreateUserIsNull() {
		isnull("create_user");
		return this;
	}
	
	public BpmsServiceNodeSQL andCreateUserIsNotNull() {
		notNull("create_user");
		return this;
	}
	
	public BpmsServiceNodeSQL andCreateUserIsEmpty() {
		empty("create_user");
		return this;
	}

	public BpmsServiceNodeSQL andCreateUserIsNotEmpty() {
		notEmpty("create_user");
		return this;
	}
       public BpmsServiceNodeSQL andCreateUserLike(java.lang.String value) {
    	   addCriterion("create_user", value, ConditionMode.FUZZY, "createUser", "java.lang.String", "Float");
    	   return this;
      }

      public BpmsServiceNodeSQL andCreateUserNotLike(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.NOT_FUZZY, "createUser", "java.lang.String", "Float");
          return this;
      }
      public BpmsServiceNodeSQL andCreateUserEqualTo(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.EQUAL, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andCreateUserNotEqualTo(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.NOT_EQUAL, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andCreateUserGreaterThan(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.GREATER_THEN, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andCreateUserGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.GREATER_EQUAL, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andCreateUserLessThan(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.LESS_THEN, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andCreateUserLessThanOrEqualTo(java.lang.String value) {
          addCriterion("create_user", value, ConditionMode.LESS_EQUAL, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andCreateUserBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("create_user", value1, value2, ConditionMode.BETWEEN, "createUser", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceNodeSQL andCreateUserNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("create_user", value1, value2, ConditionMode.NOT_BETWEEN, "createUser", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceNodeSQL andCreateUserIn(List<java.lang.String> values) {
          addCriterion("create_user", values, ConditionMode.IN, "createUser", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andCreateUserNotIn(List<java.lang.String> values) {
          addCriterion("create_user", values, ConditionMode.NOT_IN, "createUser", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceNodeSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public BpmsServiceNodeSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public BpmsServiceNodeSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public BpmsServiceNodeSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public BpmsServiceNodeSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceNodeSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceNodeSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceNodeSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceNodeSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceNodeSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceNodeSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceNodeSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceNodeSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceNodeSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceNodeSQL andUpdateTimeIsNull() {
		isnull("update_time");
		return this;
	}
	
	public BpmsServiceNodeSQL andUpdateTimeIsNotNull() {
		notNull("update_time");
		return this;
	}
	
	public BpmsServiceNodeSQL andUpdateTimeIsEmpty() {
		empty("update_time");
		return this;
	}

	public BpmsServiceNodeSQL andUpdateTimeIsNotEmpty() {
		notEmpty("update_time");
		return this;
	}
      public BpmsServiceNodeSQL andUpdateTimeEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceNodeSQL andUpdateTimeNotEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.NOT_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceNodeSQL andUpdateTimeGreaterThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceNodeSQL andUpdateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.GREATER_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceNodeSQL andUpdateTimeLessThan(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_THEN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceNodeSQL andUpdateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("update_time", value, ConditionMode.LESS_EQUAL, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceNodeSQL andUpdateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("update_time", value1, value2, ConditionMode.BETWEEN, "updateTime", "java.util.Date", "String");
    	  return this;
      }

      public BpmsServiceNodeSQL andUpdateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("update_time", value1, value2, ConditionMode.NOT_BETWEEN, "updateTime", "java.util.Date", "String");
          return this;
      }
        
      public BpmsServiceNodeSQL andUpdateTimeIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.IN, "updateTime", "java.util.Date", "String");
          return this;
      }

      public BpmsServiceNodeSQL andUpdateTimeNotIn(List<java.util.Date> values) {
          addCriterion("update_time", values, ConditionMode.NOT_IN, "updateTime", "java.util.Date", "String");
          return this;
      }
	public BpmsServiceNodeSQL andDelFlagIsNull() {
		isnull("del_flag");
		return this;
	}
	
	public BpmsServiceNodeSQL andDelFlagIsNotNull() {
		notNull("del_flag");
		return this;
	}
	
	public BpmsServiceNodeSQL andDelFlagIsEmpty() {
		empty("del_flag");
		return this;
	}

	public BpmsServiceNodeSQL andDelFlagIsNotEmpty() {
		notEmpty("del_flag");
		return this;
	}
       public BpmsServiceNodeSQL andDelFlagLike(java.lang.String value) {
    	   addCriterion("del_flag", value, ConditionMode.FUZZY, "delFlag", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceNodeSQL andDelFlagNotLike(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_FUZZY, "delFlag", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceNodeSQL andDelFlagEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andDelFlagNotEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.NOT_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andDelFlagGreaterThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andDelFlagGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.GREATER_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andDelFlagLessThan(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_THEN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andDelFlagLessThanOrEqualTo(java.lang.String value) {
          addCriterion("del_flag", value, ConditionMode.LESS_EQUAL, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andDelFlagBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("del_flag", value1, value2, ConditionMode.BETWEEN, "delFlag", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceNodeSQL andDelFlagNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("del_flag", value1, value2, ConditionMode.NOT_BETWEEN, "delFlag", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceNodeSQL andDelFlagIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.IN, "delFlag", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andDelFlagNotIn(List<java.lang.String> values) {
          addCriterion("del_flag", values, ConditionMode.NOT_IN, "delFlag", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceNodeSQL andUserCodeIsNull() {
		isnull("user_code");
		return this;
	}
	
	public BpmsServiceNodeSQL andUserCodeIsNotNull() {
		notNull("user_code");
		return this;
	}
	
	public BpmsServiceNodeSQL andUserCodeIsEmpty() {
		empty("user_code");
		return this;
	}

	public BpmsServiceNodeSQL andUserCodeIsNotEmpty() {
		notEmpty("user_code");
		return this;
	}
       public BpmsServiceNodeSQL andUserCodeLike(java.lang.String value) {
    	   addCriterion("user_code", value, ConditionMode.FUZZY, "userCode", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceNodeSQL andUserCodeNotLike(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_FUZZY, "userCode", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceNodeSQL andUserCodeEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andUserCodeNotEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.NOT_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andUserCodeGreaterThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andUserCodeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.GREATER_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andUserCodeLessThan(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_THEN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andUserCodeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("user_code", value, ConditionMode.LESS_EQUAL, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andUserCodeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("user_code", value1, value2, ConditionMode.BETWEEN, "userCode", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceNodeSQL andUserCodeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("user_code", value1, value2, ConditionMode.NOT_BETWEEN, "userCode", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceNodeSQL andUserCodeIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.IN, "userCode", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andUserCodeNotIn(List<java.lang.String> values) {
          addCriterion("user_code", values, ConditionMode.NOT_IN, "userCode", "java.lang.String", "String");
          return this;
      }
	public BpmsServiceNodeSQL andNodeServiceIsNull() {
		isnull("node_service");
		return this;
	}
	
	public BpmsServiceNodeSQL andNodeServiceIsNotNull() {
		notNull("node_service");
		return this;
	}
	
	public BpmsServiceNodeSQL andNodeServiceIsEmpty() {
		empty("node_service");
		return this;
	}

	public BpmsServiceNodeSQL andNodeServiceIsNotEmpty() {
		notEmpty("node_service");
		return this;
	}
       public BpmsServiceNodeSQL andNodeServiceLike(java.lang.String value) {
    	   addCriterion("node_service", value, ConditionMode.FUZZY, "nodeService", "java.lang.String", "String");
    	   return this;
      }

      public BpmsServiceNodeSQL andNodeServiceNotLike(java.lang.String value) {
          addCriterion("node_service", value, ConditionMode.NOT_FUZZY, "nodeService", "java.lang.String", "String");
          return this;
      }
      public BpmsServiceNodeSQL andNodeServiceEqualTo(java.lang.String value) {
          addCriterion("node_service", value, ConditionMode.EQUAL, "nodeService", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeServiceNotEqualTo(java.lang.String value) {
          addCriterion("node_service", value, ConditionMode.NOT_EQUAL, "nodeService", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeServiceGreaterThan(java.lang.String value) {
          addCriterion("node_service", value, ConditionMode.GREATER_THEN, "nodeService", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeServiceGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("node_service", value, ConditionMode.GREATER_EQUAL, "nodeService", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeServiceLessThan(java.lang.String value) {
          addCriterion("node_service", value, ConditionMode.LESS_THEN, "nodeService", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeServiceLessThanOrEqualTo(java.lang.String value) {
          addCriterion("node_service", value, ConditionMode.LESS_EQUAL, "nodeService", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeServiceBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("node_service", value1, value2, ConditionMode.BETWEEN, "nodeService", "java.lang.String", "String");
    	  return this;
      }

      public BpmsServiceNodeSQL andNodeServiceNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("node_service", value1, value2, ConditionMode.NOT_BETWEEN, "nodeService", "java.lang.String", "String");
          return this;
      }
        
      public BpmsServiceNodeSQL andNodeServiceIn(List<java.lang.String> values) {
          addCriterion("node_service", values, ConditionMode.IN, "nodeService", "java.lang.String", "String");
          return this;
      }

      public BpmsServiceNodeSQL andNodeServiceNotIn(List<java.lang.String> values) {
          addCriterion("node_service", values, ConditionMode.NOT_IN, "nodeService", "java.lang.String", "String");
          return this;
      }
}