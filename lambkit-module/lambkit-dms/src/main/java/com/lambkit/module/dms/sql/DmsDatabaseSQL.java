/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsDatabaseSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static DmsDatabaseSQL of() {
		return new DmsDatabaseSQL();
	}
	
	public static DmsDatabaseSQL by(Column column) {
		DmsDatabaseSQL that = new DmsDatabaseSQL();
		that.add(column);
        return that;
    }

    public static DmsDatabaseSQL by(String name, Object value) {
        return (DmsDatabaseSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("dms_database", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public DmsDatabaseSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public DmsDatabaseSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public DmsDatabaseSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public DmsDatabaseSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsDatabaseSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsDatabaseSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsDatabaseSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsDatabaseSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public DmsDatabaseSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public DmsDatabaseSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public DmsDatabaseSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public DmsDatabaseSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public DmsDatabaseSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public DmsDatabaseSQL andDbidIsNull() {
		isnull("dbid");
		return this;
	}
	
	public DmsDatabaseSQL andDbidIsNotNull() {
		notNull("dbid");
		return this;
	}
	
	public DmsDatabaseSQL andDbidIsEmpty() {
		empty("dbid");
		return this;
	}

	public DmsDatabaseSQL andDbidIsNotEmpty() {
		notEmpty("dbid");
		return this;
	}
      public DmsDatabaseSQL andDbidEqualTo(java.lang.Integer value) {
          addCriterion("dbid", value, ConditionMode.EQUAL, "dbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andDbidNotEqualTo(java.lang.Integer value) {
          addCriterion("dbid", value, ConditionMode.NOT_EQUAL, "dbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andDbidGreaterThan(java.lang.Integer value) {
          addCriterion("dbid", value, ConditionMode.GREATER_THEN, "dbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andDbidGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("dbid", value, ConditionMode.GREATER_EQUAL, "dbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andDbidLessThan(java.lang.Integer value) {
          addCriterion("dbid", value, ConditionMode.LESS_THEN, "dbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andDbidLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("dbid", value, ConditionMode.LESS_EQUAL, "dbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andDbidBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("dbid", value1, value2, ConditionMode.BETWEEN, "dbid", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsDatabaseSQL andDbidNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("dbid", value1, value2, ConditionMode.NOT_BETWEEN, "dbid", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsDatabaseSQL andDbidIn(List<java.lang.Integer> values) {
          addCriterion("dbid", values, ConditionMode.IN, "dbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andDbidNotIn(List<java.lang.Integer> values) {
          addCriterion("dbid", values, ConditionMode.NOT_IN, "dbid", "java.lang.Integer", "Float");
          return this;
      }
	public DmsDatabaseSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public DmsDatabaseSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public DmsDatabaseSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public DmsDatabaseSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public DmsDatabaseSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public DmsDatabaseSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public DmsDatabaseSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public DmsDatabaseSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public DmsDatabaseSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public DmsDatabaseSQL andTypeIsNull() {
		isnull("type");
		return this;
	}
	
	public DmsDatabaseSQL andTypeIsNotNull() {
		notNull("type");
		return this;
	}
	
	public DmsDatabaseSQL andTypeIsEmpty() {
		empty("type");
		return this;
	}

	public DmsDatabaseSQL andTypeIsNotEmpty() {
		notEmpty("type");
		return this;
	}
       public DmsDatabaseSQL andTypeLike(java.lang.String value) {
    	   addCriterion("type", value, ConditionMode.FUZZY, "type", "java.lang.String", "String");
    	   return this;
      }

      public DmsDatabaseSQL andTypeNotLike(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_FUZZY, "type", "java.lang.String", "String");
          return this;
      }
      public DmsDatabaseSQL andTypeEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andTypeNotEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.NOT_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andTypeGreaterThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.GREATER_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andTypeLessThan(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_THEN, "type", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("type", value, ConditionMode.LESS_EQUAL, "type", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("type", value1, value2, ConditionMode.BETWEEN, "type", "java.lang.String", "String");
    	  return this;
      }

      public DmsDatabaseSQL andTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("type", value1, value2, ConditionMode.NOT_BETWEEN, "type", "java.lang.String", "String");
          return this;
      }
        
      public DmsDatabaseSQL andTypeIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.IN, "type", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andTypeNotIn(List<java.lang.String> values) {
          addCriterion("type", values, ConditionMode.NOT_IN, "type", "java.lang.String", "String");
          return this;
      }
	public DmsDatabaseSQL andVersionIsNull() {
		isnull("version");
		return this;
	}
	
	public DmsDatabaseSQL andVersionIsNotNull() {
		notNull("version");
		return this;
	}
	
	public DmsDatabaseSQL andVersionIsEmpty() {
		empty("version");
		return this;
	}

	public DmsDatabaseSQL andVersionIsNotEmpty() {
		notEmpty("version");
		return this;
	}
       public DmsDatabaseSQL andVersionLike(java.lang.String value) {
    	   addCriterion("version", value, ConditionMode.FUZZY, "version", "java.lang.String", "String");
    	   return this;
      }

      public DmsDatabaseSQL andVersionNotLike(java.lang.String value) {
          addCriterion("version", value, ConditionMode.NOT_FUZZY, "version", "java.lang.String", "String");
          return this;
      }
      public DmsDatabaseSQL andVersionEqualTo(java.lang.String value) {
          addCriterion("version", value, ConditionMode.EQUAL, "version", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andVersionNotEqualTo(java.lang.String value) {
          addCriterion("version", value, ConditionMode.NOT_EQUAL, "version", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andVersionGreaterThan(java.lang.String value) {
          addCriterion("version", value, ConditionMode.GREATER_THEN, "version", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andVersionGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("version", value, ConditionMode.GREATER_EQUAL, "version", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andVersionLessThan(java.lang.String value) {
          addCriterion("version", value, ConditionMode.LESS_THEN, "version", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andVersionLessThanOrEqualTo(java.lang.String value) {
          addCriterion("version", value, ConditionMode.LESS_EQUAL, "version", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andVersionBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("version", value1, value2, ConditionMode.BETWEEN, "version", "java.lang.String", "String");
    	  return this;
      }

      public DmsDatabaseSQL andVersionNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("version", value1, value2, ConditionMode.NOT_BETWEEN, "version", "java.lang.String", "String");
          return this;
      }
        
      public DmsDatabaseSQL andVersionIn(List<java.lang.String> values) {
          addCriterion("version", values, ConditionMode.IN, "version", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andVersionNotIn(List<java.lang.String> values) {
          addCriterion("version", values, ConditionMode.NOT_IN, "version", "java.lang.String", "String");
          return this;
      }
	public DmsDatabaseSQL andDriverClassIsNull() {
		isnull("driver_class");
		return this;
	}
	
	public DmsDatabaseSQL andDriverClassIsNotNull() {
		notNull("driver_class");
		return this;
	}
	
	public DmsDatabaseSQL andDriverClassIsEmpty() {
		empty("driver_class");
		return this;
	}

	public DmsDatabaseSQL andDriverClassIsNotEmpty() {
		notEmpty("driver_class");
		return this;
	}
       public DmsDatabaseSQL andDriverClassLike(java.lang.String value) {
    	   addCriterion("driver_class", value, ConditionMode.FUZZY, "driverClass", "java.lang.String", "String");
    	   return this;
      }

      public DmsDatabaseSQL andDriverClassNotLike(java.lang.String value) {
          addCriterion("driver_class", value, ConditionMode.NOT_FUZZY, "driverClass", "java.lang.String", "String");
          return this;
      }
      public DmsDatabaseSQL andDriverClassEqualTo(java.lang.String value) {
          addCriterion("driver_class", value, ConditionMode.EQUAL, "driverClass", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDriverClassNotEqualTo(java.lang.String value) {
          addCriterion("driver_class", value, ConditionMode.NOT_EQUAL, "driverClass", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDriverClassGreaterThan(java.lang.String value) {
          addCriterion("driver_class", value, ConditionMode.GREATER_THEN, "driverClass", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDriverClassGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("driver_class", value, ConditionMode.GREATER_EQUAL, "driverClass", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDriverClassLessThan(java.lang.String value) {
          addCriterion("driver_class", value, ConditionMode.LESS_THEN, "driverClass", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDriverClassLessThanOrEqualTo(java.lang.String value) {
          addCriterion("driver_class", value, ConditionMode.LESS_EQUAL, "driverClass", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDriverClassBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("driver_class", value1, value2, ConditionMode.BETWEEN, "driverClass", "java.lang.String", "String");
    	  return this;
      }

      public DmsDatabaseSQL andDriverClassNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("driver_class", value1, value2, ConditionMode.NOT_BETWEEN, "driverClass", "java.lang.String", "String");
          return this;
      }
        
      public DmsDatabaseSQL andDriverClassIn(List<java.lang.String> values) {
          addCriterion("driver_class", values, ConditionMode.IN, "driverClass", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDriverClassNotIn(List<java.lang.String> values) {
          addCriterion("driver_class", values, ConditionMode.NOT_IN, "driverClass", "java.lang.String", "String");
          return this;
      }
	public DmsDatabaseSQL andUrlIsNull() {
		isnull("url");
		return this;
	}
	
	public DmsDatabaseSQL andUrlIsNotNull() {
		notNull("url");
		return this;
	}
	
	public DmsDatabaseSQL andUrlIsEmpty() {
		empty("url");
		return this;
	}

	public DmsDatabaseSQL andUrlIsNotEmpty() {
		notEmpty("url");
		return this;
	}
       public DmsDatabaseSQL andUrlLike(java.lang.String value) {
    	   addCriterion("url", value, ConditionMode.FUZZY, "url", "java.lang.String", "String");
    	   return this;
      }

      public DmsDatabaseSQL andUrlNotLike(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_FUZZY, "url", "java.lang.String", "String");
          return this;
      }
      public DmsDatabaseSQL andUrlEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andUrlNotEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.NOT_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andUrlGreaterThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andUrlGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.GREATER_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andUrlLessThan(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_THEN, "url", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andUrlLessThanOrEqualTo(java.lang.String value) {
          addCriterion("url", value, ConditionMode.LESS_EQUAL, "url", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andUrlBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("url", value1, value2, ConditionMode.BETWEEN, "url", "java.lang.String", "String");
    	  return this;
      }

      public DmsDatabaseSQL andUrlNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("url", value1, value2, ConditionMode.NOT_BETWEEN, "url", "java.lang.String", "String");
          return this;
      }
        
      public DmsDatabaseSQL andUrlIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.IN, "url", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andUrlNotIn(List<java.lang.String> values) {
          addCriterion("url", values, ConditionMode.NOT_IN, "url", "java.lang.String", "String");
          return this;
      }
	public DmsDatabaseSQL andUsernameIsNull() {
		isnull("username");
		return this;
	}
	
	public DmsDatabaseSQL andUsernameIsNotNull() {
		notNull("username");
		return this;
	}
	
	public DmsDatabaseSQL andUsernameIsEmpty() {
		empty("username");
		return this;
	}

	public DmsDatabaseSQL andUsernameIsNotEmpty() {
		notEmpty("username");
		return this;
	}
       public DmsDatabaseSQL andUsernameLike(java.lang.String value) {
    	   addCriterion("username", value, ConditionMode.FUZZY, "username", "java.lang.String", "String");
    	   return this;
      }

      public DmsDatabaseSQL andUsernameNotLike(java.lang.String value) {
          addCriterion("username", value, ConditionMode.NOT_FUZZY, "username", "java.lang.String", "String");
          return this;
      }
      public DmsDatabaseSQL andUsernameEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andUsernameNotEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.NOT_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andUsernameGreaterThan(java.lang.String value) {
          addCriterion("username", value, ConditionMode.GREATER_THEN, "username", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andUsernameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.GREATER_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andUsernameLessThan(java.lang.String value) {
          addCriterion("username", value, ConditionMode.LESS_THEN, "username", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andUsernameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("username", value, ConditionMode.LESS_EQUAL, "username", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andUsernameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("username", value1, value2, ConditionMode.BETWEEN, "username", "java.lang.String", "String");
    	  return this;
      }

      public DmsDatabaseSQL andUsernameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("username", value1, value2, ConditionMode.NOT_BETWEEN, "username", "java.lang.String", "String");
          return this;
      }
        
      public DmsDatabaseSQL andUsernameIn(List<java.lang.String> values) {
          addCriterion("username", values, ConditionMode.IN, "username", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andUsernameNotIn(List<java.lang.String> values) {
          addCriterion("username", values, ConditionMode.NOT_IN, "username", "java.lang.String", "String");
          return this;
      }
	public DmsDatabaseSQL andPasswordIsNull() {
		isnull("password");
		return this;
	}
	
	public DmsDatabaseSQL andPasswordIsNotNull() {
		notNull("password");
		return this;
	}
	
	public DmsDatabaseSQL andPasswordIsEmpty() {
		empty("password");
		return this;
	}

	public DmsDatabaseSQL andPasswordIsNotEmpty() {
		notEmpty("password");
		return this;
	}
       public DmsDatabaseSQL andPasswordLike(java.lang.String value) {
    	   addCriterion("password", value, ConditionMode.FUZZY, "password", "java.lang.String", "String");
    	   return this;
      }

      public DmsDatabaseSQL andPasswordNotLike(java.lang.String value) {
          addCriterion("password", value, ConditionMode.NOT_FUZZY, "password", "java.lang.String", "String");
          return this;
      }
      public DmsDatabaseSQL andPasswordEqualTo(java.lang.String value) {
          addCriterion("password", value, ConditionMode.EQUAL, "password", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andPasswordNotEqualTo(java.lang.String value) {
          addCriterion("password", value, ConditionMode.NOT_EQUAL, "password", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andPasswordGreaterThan(java.lang.String value) {
          addCriterion("password", value, ConditionMode.GREATER_THEN, "password", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andPasswordGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("password", value, ConditionMode.GREATER_EQUAL, "password", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andPasswordLessThan(java.lang.String value) {
          addCriterion("password", value, ConditionMode.LESS_THEN, "password", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andPasswordLessThanOrEqualTo(java.lang.String value) {
          addCriterion("password", value, ConditionMode.LESS_EQUAL, "password", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andPasswordBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("password", value1, value2, ConditionMode.BETWEEN, "password", "java.lang.String", "String");
    	  return this;
      }

      public DmsDatabaseSQL andPasswordNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("password", value1, value2, ConditionMode.NOT_BETWEEN, "password", "java.lang.String", "String");
          return this;
      }
        
      public DmsDatabaseSQL andPasswordIn(List<java.lang.String> values) {
          addCriterion("password", values, ConditionMode.IN, "password", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andPasswordNotIn(List<java.lang.String> values) {
          addCriterion("password", values, ConditionMode.NOT_IN, "password", "java.lang.String", "String");
          return this;
      }
	public DmsDatabaseSQL andInitSizeIsNull() {
		isnull("init_size");
		return this;
	}
	
	public DmsDatabaseSQL andInitSizeIsNotNull() {
		notNull("init_size");
		return this;
	}
	
	public DmsDatabaseSQL andInitSizeIsEmpty() {
		empty("init_size");
		return this;
	}

	public DmsDatabaseSQL andInitSizeIsNotEmpty() {
		notEmpty("init_size");
		return this;
	}
      public DmsDatabaseSQL andInitSizeEqualTo(java.lang.Integer value) {
          addCriterion("init_size", value, ConditionMode.EQUAL, "initSize", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andInitSizeNotEqualTo(java.lang.Integer value) {
          addCriterion("init_size", value, ConditionMode.NOT_EQUAL, "initSize", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andInitSizeGreaterThan(java.lang.Integer value) {
          addCriterion("init_size", value, ConditionMode.GREATER_THEN, "initSize", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andInitSizeGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("init_size", value, ConditionMode.GREATER_EQUAL, "initSize", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andInitSizeLessThan(java.lang.Integer value) {
          addCriterion("init_size", value, ConditionMode.LESS_THEN, "initSize", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andInitSizeLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("init_size", value, ConditionMode.LESS_EQUAL, "initSize", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andInitSizeBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("init_size", value1, value2, ConditionMode.BETWEEN, "initSize", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsDatabaseSQL andInitSizeNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("init_size", value1, value2, ConditionMode.NOT_BETWEEN, "initSize", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsDatabaseSQL andInitSizeIn(List<java.lang.Integer> values) {
          addCriterion("init_size", values, ConditionMode.IN, "initSize", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andInitSizeNotIn(List<java.lang.Integer> values) {
          addCriterion("init_size", values, ConditionMode.NOT_IN, "initSize", "java.lang.Integer", "Float");
          return this;
      }
	public DmsDatabaseSQL andMinIdleIsNull() {
		isnull("min_idle");
		return this;
	}
	
	public DmsDatabaseSQL andMinIdleIsNotNull() {
		notNull("min_idle");
		return this;
	}
	
	public DmsDatabaseSQL andMinIdleIsEmpty() {
		empty("min_idle");
		return this;
	}

	public DmsDatabaseSQL andMinIdleIsNotEmpty() {
		notEmpty("min_idle");
		return this;
	}
      public DmsDatabaseSQL andMinIdleEqualTo(java.lang.Integer value) {
          addCriterion("min_idle", value, ConditionMode.EQUAL, "minIdle", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMinIdleNotEqualTo(java.lang.Integer value) {
          addCriterion("min_idle", value, ConditionMode.NOT_EQUAL, "minIdle", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMinIdleGreaterThan(java.lang.Integer value) {
          addCriterion("min_idle", value, ConditionMode.GREATER_THEN, "minIdle", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMinIdleGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("min_idle", value, ConditionMode.GREATER_EQUAL, "minIdle", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMinIdleLessThan(java.lang.Integer value) {
          addCriterion("min_idle", value, ConditionMode.LESS_THEN, "minIdle", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMinIdleLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("min_idle", value, ConditionMode.LESS_EQUAL, "minIdle", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMinIdleBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("min_idle", value1, value2, ConditionMode.BETWEEN, "minIdle", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsDatabaseSQL andMinIdleNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("min_idle", value1, value2, ConditionMode.NOT_BETWEEN, "minIdle", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsDatabaseSQL andMinIdleIn(List<java.lang.Integer> values) {
          addCriterion("min_idle", values, ConditionMode.IN, "minIdle", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMinIdleNotIn(List<java.lang.Integer> values) {
          addCriterion("min_idle", values, ConditionMode.NOT_IN, "minIdle", "java.lang.Integer", "Float");
          return this;
      }
	public DmsDatabaseSQL andMaxActiveIsNull() {
		isnull("max_active");
		return this;
	}
	
	public DmsDatabaseSQL andMaxActiveIsNotNull() {
		notNull("max_active");
		return this;
	}
	
	public DmsDatabaseSQL andMaxActiveIsEmpty() {
		empty("max_active");
		return this;
	}

	public DmsDatabaseSQL andMaxActiveIsNotEmpty() {
		notEmpty("max_active");
		return this;
	}
      public DmsDatabaseSQL andMaxActiveEqualTo(java.lang.Integer value) {
          addCriterion("max_active", value, ConditionMode.EQUAL, "maxActive", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMaxActiveNotEqualTo(java.lang.Integer value) {
          addCriterion("max_active", value, ConditionMode.NOT_EQUAL, "maxActive", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMaxActiveGreaterThan(java.lang.Integer value) {
          addCriterion("max_active", value, ConditionMode.GREATER_THEN, "maxActive", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMaxActiveGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("max_active", value, ConditionMode.GREATER_EQUAL, "maxActive", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMaxActiveLessThan(java.lang.Integer value) {
          addCriterion("max_active", value, ConditionMode.LESS_THEN, "maxActive", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMaxActiveLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("max_active", value, ConditionMode.LESS_EQUAL, "maxActive", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMaxActiveBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("max_active", value1, value2, ConditionMode.BETWEEN, "maxActive", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsDatabaseSQL andMaxActiveNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("max_active", value1, value2, ConditionMode.NOT_BETWEEN, "maxActive", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsDatabaseSQL andMaxActiveIn(List<java.lang.Integer> values) {
          addCriterion("max_active", values, ConditionMode.IN, "maxActive", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMaxActiveNotIn(List<java.lang.Integer> values) {
          addCriterion("max_active", values, ConditionMode.NOT_IN, "maxActive", "java.lang.Integer", "Float");
          return this;
      }
	public DmsDatabaseSQL andMaxWaitIsNull() {
		isnull("max_wait");
		return this;
	}
	
	public DmsDatabaseSQL andMaxWaitIsNotNull() {
		notNull("max_wait");
		return this;
	}
	
	public DmsDatabaseSQL andMaxWaitIsEmpty() {
		empty("max_wait");
		return this;
	}

	public DmsDatabaseSQL andMaxWaitIsNotEmpty() {
		notEmpty("max_wait");
		return this;
	}
      public DmsDatabaseSQL andMaxWaitEqualTo(java.lang.Integer value) {
          addCriterion("max_wait", value, ConditionMode.EQUAL, "maxWait", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMaxWaitNotEqualTo(java.lang.Integer value) {
          addCriterion("max_wait", value, ConditionMode.NOT_EQUAL, "maxWait", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMaxWaitGreaterThan(java.lang.Integer value) {
          addCriterion("max_wait", value, ConditionMode.GREATER_THEN, "maxWait", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMaxWaitGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("max_wait", value, ConditionMode.GREATER_EQUAL, "maxWait", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMaxWaitLessThan(java.lang.Integer value) {
          addCriterion("max_wait", value, ConditionMode.LESS_THEN, "maxWait", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMaxWaitLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("max_wait", value, ConditionMode.LESS_EQUAL, "maxWait", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMaxWaitBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("max_wait", value1, value2, ConditionMode.BETWEEN, "maxWait", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsDatabaseSQL andMaxWaitNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("max_wait", value1, value2, ConditionMode.NOT_BETWEEN, "maxWait", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsDatabaseSQL andMaxWaitIn(List<java.lang.Integer> values) {
          addCriterion("max_wait", values, ConditionMode.IN, "maxWait", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andMaxWaitNotIn(List<java.lang.Integer> values) {
          addCriterion("max_wait", values, ConditionMode.NOT_IN, "maxWait", "java.lang.Integer", "Float");
          return this;
      }
	public DmsDatabaseSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public DmsDatabaseSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public DmsDatabaseSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public DmsDatabaseSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public DmsDatabaseSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsDatabaseSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsDatabaseSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsDatabaseSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public DmsDatabaseSQL andCreateUserIsNull() {
		isnull("create_user");
		return this;
	}
	
	public DmsDatabaseSQL andCreateUserIsNotNull() {
		notNull("create_user");
		return this;
	}
	
	public DmsDatabaseSQL andCreateUserIsEmpty() {
		empty("create_user");
		return this;
	}

	public DmsDatabaseSQL andCreateUserIsNotEmpty() {
		notEmpty("create_user");
		return this;
	}
      public DmsDatabaseSQL andCreateUserEqualTo(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.EQUAL, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public DmsDatabaseSQL andCreateUserNotEqualTo(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.NOT_EQUAL, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public DmsDatabaseSQL andCreateUserGreaterThan(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.GREATER_THEN, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public DmsDatabaseSQL andCreateUserGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.GREATER_EQUAL, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public DmsDatabaseSQL andCreateUserLessThan(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.LESS_THEN, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public DmsDatabaseSQL andCreateUserLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("create_user", value, ConditionMode.LESS_EQUAL, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public DmsDatabaseSQL andCreateUserBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("create_user", value1, value2, ConditionMode.BETWEEN, "createUser", "java.lang.Long", "Float");
    	  return this;
      }

      public DmsDatabaseSQL andCreateUserNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("create_user", value1, value2, ConditionMode.NOT_BETWEEN, "createUser", "java.lang.Long", "Float");
          return this;
      }
        
      public DmsDatabaseSQL andCreateUserIn(List<java.lang.Long> values) {
          addCriterion("create_user", values, ConditionMode.IN, "createUser", "java.lang.Long", "Float");
          return this;
      }

      public DmsDatabaseSQL andCreateUserNotIn(List<java.lang.Long> values) {
          addCriterion("create_user", values, ConditionMode.NOT_IN, "createUser", "java.lang.Long", "Float");
          return this;
      }
	public DmsDatabaseSQL andUpdateUserIsNull() {
		isnull("update_user");
		return this;
	}
	
	public DmsDatabaseSQL andUpdateUserIsNotNull() {
		notNull("update_user");
		return this;
	}
	
	public DmsDatabaseSQL andUpdateUserIsEmpty() {
		empty("update_user");
		return this;
	}

	public DmsDatabaseSQL andUpdateUserIsNotEmpty() {
		notEmpty("update_user");
		return this;
	}
      public DmsDatabaseSQL andUpdateUserEqualTo(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.EQUAL, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public DmsDatabaseSQL andUpdateUserNotEqualTo(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.NOT_EQUAL, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public DmsDatabaseSQL andUpdateUserGreaterThan(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.GREATER_THEN, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public DmsDatabaseSQL andUpdateUserGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.GREATER_EQUAL, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public DmsDatabaseSQL andUpdateUserLessThan(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.LESS_THEN, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public DmsDatabaseSQL andUpdateUserLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("update_user", value, ConditionMode.LESS_EQUAL, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public DmsDatabaseSQL andUpdateUserBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("update_user", value1, value2, ConditionMode.BETWEEN, "updateUser", "java.lang.Long", "Float");
    	  return this;
      }

      public DmsDatabaseSQL andUpdateUserNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("update_user", value1, value2, ConditionMode.NOT_BETWEEN, "updateUser", "java.lang.Long", "Float");
          return this;
      }
        
      public DmsDatabaseSQL andUpdateUserIn(List<java.lang.Long> values) {
          addCriterion("update_user", values, ConditionMode.IN, "updateUser", "java.lang.Long", "Float");
          return this;
      }

      public DmsDatabaseSQL andUpdateUserNotIn(List<java.lang.Long> values) {
          addCriterion("update_user", values, ConditionMode.NOT_IN, "updateUser", "java.lang.Long", "Float");
          return this;
      }
	public DmsDatabaseSQL andCreateTimeIsNull() {
		isnull("create_time");
		return this;
	}
	
	public DmsDatabaseSQL andCreateTimeIsNotNull() {
		notNull("create_time");
		return this;
	}
	
	public DmsDatabaseSQL andCreateTimeIsEmpty() {
		empty("create_time");
		return this;
	}

	public DmsDatabaseSQL andCreateTimeIsNotEmpty() {
		notEmpty("create_time");
		return this;
	}
      public DmsDatabaseSQL andCreateTimeEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public DmsDatabaseSQL andCreateTimeNotEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.NOT_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public DmsDatabaseSQL andCreateTimeGreaterThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public DmsDatabaseSQL andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.GREATER_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public DmsDatabaseSQL andCreateTimeLessThan(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_THEN, "createTime", "java.util.Date", "String");
          return this;
      }

      public DmsDatabaseSQL andCreateTimeLessThanOrEqualTo(java.util.Date value) {
          addCriterion("create_time", value, ConditionMode.LESS_EQUAL, "createTime", "java.util.Date", "String");
          return this;
      }

      public DmsDatabaseSQL andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
    	  addCriterion("create_time", value1, value2, ConditionMode.BETWEEN, "createTime", "java.util.Date", "String");
    	  return this;
      }

      public DmsDatabaseSQL andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
          addCriterion("create_time", value1, value2, ConditionMode.NOT_BETWEEN, "createTime", "java.util.Date", "String");
          return this;
      }
        
      public DmsDatabaseSQL andCreateTimeIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.IN, "createTime", "java.util.Date", "String");
          return this;
      }

      public DmsDatabaseSQL andCreateTimeNotIn(List<java.util.Date> values) {
          addCriterion("create_time", values, ConditionMode.NOT_IN, "createTime", "java.util.Date", "String");
          return this;
      }
	public DmsDatabaseSQL andRemarksIsNull() {
		isnull("remarks");
		return this;
	}
	
	public DmsDatabaseSQL andRemarksIsNotNull() {
		notNull("remarks");
		return this;
	}
	
	public DmsDatabaseSQL andRemarksIsEmpty() {
		empty("remarks");
		return this;
	}

	public DmsDatabaseSQL andRemarksIsNotEmpty() {
		notEmpty("remarks");
		return this;
	}
       public DmsDatabaseSQL andRemarksLike(java.lang.String value) {
    	   addCriterion("remarks", value, ConditionMode.FUZZY, "remarks", "java.lang.String", "String");
    	   return this;
      }

      public DmsDatabaseSQL andRemarksNotLike(java.lang.String value) {
          addCriterion("remarks", value, ConditionMode.NOT_FUZZY, "remarks", "java.lang.String", "String");
          return this;
      }
      public DmsDatabaseSQL andRemarksEqualTo(java.lang.String value) {
          addCriterion("remarks", value, ConditionMode.EQUAL, "remarks", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andRemarksNotEqualTo(java.lang.String value) {
          addCriterion("remarks", value, ConditionMode.NOT_EQUAL, "remarks", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andRemarksGreaterThan(java.lang.String value) {
          addCriterion("remarks", value, ConditionMode.GREATER_THEN, "remarks", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andRemarksGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("remarks", value, ConditionMode.GREATER_EQUAL, "remarks", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andRemarksLessThan(java.lang.String value) {
          addCriterion("remarks", value, ConditionMode.LESS_THEN, "remarks", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andRemarksLessThanOrEqualTo(java.lang.String value) {
          addCriterion("remarks", value, ConditionMode.LESS_EQUAL, "remarks", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andRemarksBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("remarks", value1, value2, ConditionMode.BETWEEN, "remarks", "java.lang.String", "String");
    	  return this;
      }

      public DmsDatabaseSQL andRemarksNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("remarks", value1, value2, ConditionMode.NOT_BETWEEN, "remarks", "java.lang.String", "String");
          return this;
      }
        
      public DmsDatabaseSQL andRemarksIn(List<java.lang.String> values) {
          addCriterion("remarks", values, ConditionMode.IN, "remarks", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andRemarksNotIn(List<java.lang.String> values) {
          addCriterion("remarks", values, ConditionMode.NOT_IN, "remarks", "java.lang.String", "String");
          return this;
      }
	public DmsDatabaseSQL andDbSchemaIsNull() {
		isnull("db_schema");
		return this;
	}
	
	public DmsDatabaseSQL andDbSchemaIsNotNull() {
		notNull("db_schema");
		return this;
	}
	
	public DmsDatabaseSQL andDbSchemaIsEmpty() {
		empty("db_schema");
		return this;
	}

	public DmsDatabaseSQL andDbSchemaIsNotEmpty() {
		notEmpty("db_schema");
		return this;
	}
       public DmsDatabaseSQL andDbSchemaLike(java.lang.String value) {
    	   addCriterion("db_schema", value, ConditionMode.FUZZY, "dbSchema", "java.lang.String", "String");
    	   return this;
      }

      public DmsDatabaseSQL andDbSchemaNotLike(java.lang.String value) {
          addCriterion("db_schema", value, ConditionMode.NOT_FUZZY, "dbSchema", "java.lang.String", "String");
          return this;
      }
      public DmsDatabaseSQL andDbSchemaEqualTo(java.lang.String value) {
          addCriterion("db_schema", value, ConditionMode.EQUAL, "dbSchema", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbSchemaNotEqualTo(java.lang.String value) {
          addCriterion("db_schema", value, ConditionMode.NOT_EQUAL, "dbSchema", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbSchemaGreaterThan(java.lang.String value) {
          addCriterion("db_schema", value, ConditionMode.GREATER_THEN, "dbSchema", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbSchemaGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("db_schema", value, ConditionMode.GREATER_EQUAL, "dbSchema", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbSchemaLessThan(java.lang.String value) {
          addCriterion("db_schema", value, ConditionMode.LESS_THEN, "dbSchema", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbSchemaLessThanOrEqualTo(java.lang.String value) {
          addCriterion("db_schema", value, ConditionMode.LESS_EQUAL, "dbSchema", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbSchemaBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("db_schema", value1, value2, ConditionMode.BETWEEN, "dbSchema", "java.lang.String", "String");
    	  return this;
      }

      public DmsDatabaseSQL andDbSchemaNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("db_schema", value1, value2, ConditionMode.NOT_BETWEEN, "dbSchema", "java.lang.String", "String");
          return this;
      }
        
      public DmsDatabaseSQL andDbSchemaIn(List<java.lang.String> values) {
          addCriterion("db_schema", values, ConditionMode.IN, "dbSchema", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbSchemaNotIn(List<java.lang.String> values) {
          addCriterion("db_schema", values, ConditionMode.NOT_IN, "dbSchema", "java.lang.String", "String");
          return this;
      }
	public DmsDatabaseSQL andDbNameIsNull() {
		isnull("db_name");
		return this;
	}
	
	public DmsDatabaseSQL andDbNameIsNotNull() {
		notNull("db_name");
		return this;
	}
	
	public DmsDatabaseSQL andDbNameIsEmpty() {
		empty("db_name");
		return this;
	}

	public DmsDatabaseSQL andDbNameIsNotEmpty() {
		notEmpty("db_name");
		return this;
	}
       public DmsDatabaseSQL andDbNameLike(java.lang.String value) {
    	   addCriterion("db_name", value, ConditionMode.FUZZY, "dbName", "java.lang.String", "String");
    	   return this;
      }

      public DmsDatabaseSQL andDbNameNotLike(java.lang.String value) {
          addCriterion("db_name", value, ConditionMode.NOT_FUZZY, "dbName", "java.lang.String", "String");
          return this;
      }
      public DmsDatabaseSQL andDbNameEqualTo(java.lang.String value) {
          addCriterion("db_name", value, ConditionMode.EQUAL, "dbName", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbNameNotEqualTo(java.lang.String value) {
          addCriterion("db_name", value, ConditionMode.NOT_EQUAL, "dbName", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbNameGreaterThan(java.lang.String value) {
          addCriterion("db_name", value, ConditionMode.GREATER_THEN, "dbName", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("db_name", value, ConditionMode.GREATER_EQUAL, "dbName", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbNameLessThan(java.lang.String value) {
          addCriterion("db_name", value, ConditionMode.LESS_THEN, "dbName", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("db_name", value, ConditionMode.LESS_EQUAL, "dbName", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("db_name", value1, value2, ConditionMode.BETWEEN, "dbName", "java.lang.String", "String");
    	  return this;
      }

      public DmsDatabaseSQL andDbNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("db_name", value1, value2, ConditionMode.NOT_BETWEEN, "dbName", "java.lang.String", "String");
          return this;
      }
        
      public DmsDatabaseSQL andDbNameIn(List<java.lang.String> values) {
          addCriterion("db_name", values, ConditionMode.IN, "dbName", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbNameNotIn(List<java.lang.String> values) {
          addCriterion("db_name", values, ConditionMode.NOT_IN, "dbName", "java.lang.String", "String");
          return this;
      }
	public DmsDatabaseSQL andDbFactoryIsNull() {
		isnull("db_factory");
		return this;
	}
	
	public DmsDatabaseSQL andDbFactoryIsNotNull() {
		notNull("db_factory");
		return this;
	}
	
	public DmsDatabaseSQL andDbFactoryIsEmpty() {
		empty("db_factory");
		return this;
	}

	public DmsDatabaseSQL andDbFactoryIsNotEmpty() {
		notEmpty("db_factory");
		return this;
	}
       public DmsDatabaseSQL andDbFactoryLike(java.lang.String value) {
    	   addCriterion("db_factory", value, ConditionMode.FUZZY, "dbFactory", "java.lang.String", "String");
    	   return this;
      }

      public DmsDatabaseSQL andDbFactoryNotLike(java.lang.String value) {
          addCriterion("db_factory", value, ConditionMode.NOT_FUZZY, "dbFactory", "java.lang.String", "String");
          return this;
      }
      public DmsDatabaseSQL andDbFactoryEqualTo(java.lang.String value) {
          addCriterion("db_factory", value, ConditionMode.EQUAL, "dbFactory", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbFactoryNotEqualTo(java.lang.String value) {
          addCriterion("db_factory", value, ConditionMode.NOT_EQUAL, "dbFactory", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbFactoryGreaterThan(java.lang.String value) {
          addCriterion("db_factory", value, ConditionMode.GREATER_THEN, "dbFactory", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbFactoryGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("db_factory", value, ConditionMode.GREATER_EQUAL, "dbFactory", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbFactoryLessThan(java.lang.String value) {
          addCriterion("db_factory", value, ConditionMode.LESS_THEN, "dbFactory", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbFactoryLessThanOrEqualTo(java.lang.String value) {
          addCriterion("db_factory", value, ConditionMode.LESS_EQUAL, "dbFactory", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbFactoryBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("db_factory", value1, value2, ConditionMode.BETWEEN, "dbFactory", "java.lang.String", "String");
    	  return this;
      }

      public DmsDatabaseSQL andDbFactoryNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("db_factory", value1, value2, ConditionMode.NOT_BETWEEN, "dbFactory", "java.lang.String", "String");
          return this;
      }
        
      public DmsDatabaseSQL andDbFactoryIn(List<java.lang.String> values) {
          addCriterion("db_factory", values, ConditionMode.IN, "dbFactory", "java.lang.String", "String");
          return this;
      }

      public DmsDatabaseSQL andDbFactoryNotIn(List<java.lang.String> values) {
          addCriterion("db_factory", values, ConditionMode.NOT_IN, "dbFactory", "java.lang.String", "String");
          return this;
      }
}