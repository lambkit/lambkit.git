/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.service;

import cn.hutool.core.util.StrUtil;
import com.lambkit.core.Lambkit;
import com.lambkit.db.*;
import com.lambkit.db.enjoy.EnjoySQL;
import com.lambkit.db.sql.Example;
import com.lambkit.module.dms.DmsConfig;
import com.lambkit.module.dms.row.DmsSqlTemplate;
import com.lambkit.module.dms.sql.DmsSqlTemplateSQL;

import java.util.List;
import java.util.Map;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsSqlTemplateService implements IDaoService<DmsSqlTemplate> {
	private EnjoySQL enjoySQL = new EnjoySQL("sqltemplateEngine");

	public IRowDao<DmsSqlTemplate> dao() {
		String dbPoolName = Lambkit.config(DmsConfig.class).getDbPoolName();
		return DbPool.use(dbPoolName).dao(DmsSqlTemplate.class);
	}

	public IRowDao<DmsSqlTemplate> dao(String dbPoolName) {
		return DbPool.use(dbPoolName).dao(DmsSqlTemplate.class);
	}

	public DmsSqlTemplate getTemplate(String key) {
		if(StrUtil.isBlank(key)) {
			return null;
		}
		if(key.indexOf(".") > 0) {
			String name =  key.substring(0, key.lastIndexOf("."));
			String catalog = key.substring(key.lastIndexOf(".")+1);
			Example example = DmsSqlTemplateSQL.of().andNameEqualTo(name).andCatalogEqualTo(catalog).example();
			example.setCount(1);
			return dao().findFirst(example);
		} else {
			Example example = DmsSqlTemplateSQL.of().andNameEqualTo(key).example();
			example.setCount(1);
			return dao().findFirst(example);
		}
	}

	public Sql getSql(DmsSqlTemplate sqlTemplate, Map<String, Object> data) {
		return enjoySQL.getSqlPara(sqlTemplate.getContent(), data);
	}

	public Sql getSql(DmsSqlTemplate sqlTemplate, Object... data) {
		return enjoySQL.getSqlPara(sqlTemplate.getContent(), data);
	}

	//////////////////////////////////////////////////////////////////

	public List<RowData> find(DmsSqlTemplate sqlTemplate, String db_pool_name, Map<String, Object> data) {
		if(sqlTemplate==null) {
			return null;
		}
		String dbname = StrUtil.isBlank(db_pool_name) ? sqlTemplate.getDbname() : db_pool_name;
		dbname = StrUtil.isBlank(dbname) ? DbPool.DEFAULT_NAME : dbname;
		return DbPool.use(dbname).find(getSql(sqlTemplate, data));
	}

	public List<RowData> find(DmsSqlTemplate sqlTemplate, String db_pool_name, Object... data) {
		if(sqlTemplate==null) {
			return null;
		}
		String dbname = StrUtil.isBlank(db_pool_name) ? sqlTemplate.getDbname() : db_pool_name;
		dbname = StrUtil.isBlank(dbname) ? DbPool.DEFAULT_NAME : dbname;
		return DbPool.use(dbname).find(getSql(sqlTemplate, data));
	}

	public RowData findFirst(DmsSqlTemplate sqlTemplate, String db_pool_name, Map<String, Object> data) {
		if(sqlTemplate==null) {
			return null;
		}
		String dbname = StrUtil.isBlank(db_pool_name) ? sqlTemplate.getDbname() : db_pool_name;
		dbname = StrUtil.isBlank(dbname) ? DbPool.DEFAULT_NAME : dbname;
		return DbPool.use(dbname).findFirst(getSql(sqlTemplate, data));
	}

	public RowData findFirst(DmsSqlTemplate sqlTemplate, String db_pool_name, Object... data) {
		if(sqlTemplate==null) {
			return null;
		}
		String dbname = StrUtil.isBlank(db_pool_name) ? sqlTemplate.getDbname() : db_pool_name;
		dbname = StrUtil.isBlank(dbname) ? DbPool.DEFAULT_NAME : dbname;
		return DbPool.use(dbname).findFirst(getSql(sqlTemplate, data));
	}

	public PageData<RowData> paginate(DmsSqlTemplate sqlTemplate, String db_pool_name, Integer page_number, Integer page_size, Map<String, Object> data) {
		if(sqlTemplate==null) {
			return null;
		}
		String dbname = StrUtil.isBlank(db_pool_name) ? sqlTemplate.getDbname() : db_pool_name;
		dbname = StrUtil.isBlank(dbname) ? DbPool.DEFAULT_NAME : dbname;
		return DbPool.use(dbname).paginate(page_number, page_size, getSql(sqlTemplate, data));
	}

	public PageData<RowData> paginate(DmsSqlTemplate sqlTemplate, String db_pool_name, Integer page_number, Integer page_size, Object... data) {
		if(sqlTemplate==null) {
			return null;
		}
		String dbname = StrUtil.isBlank(db_pool_name) ? sqlTemplate.getDbname() : db_pool_name;
		dbname = StrUtil.isBlank(dbname) ? DbPool.DEFAULT_NAME : dbname;
		return DbPool.use(dbname).paginate(page_number, page_size, getSql(sqlTemplate, data));
	}

	//////////////////////////////////////////////////////////////////

	public List<RowData> find(String tamplate_name, String db_pool_name, Map<String, Object> data) {
		return find(getTemplate(tamplate_name), db_pool_name, data);
	}

	public List<RowData> find(String tamplate_name, String db_pool_name, Object... data) {
		return find(getTemplate(tamplate_name), db_pool_name, data);
	}

	public RowData findFirst(String tamplate_name, String db_pool_name, Map<String, Object> data) {
		return findFirst(getTemplate(tamplate_name), db_pool_name, data);
	}

	public RowData findFirst(String tamplate_name, String db_pool_name, Object... data) {
		return findFirst(getTemplate(tamplate_name), db_pool_name, data);
	}

	public PageData<RowData> paginate(String tamplate_name, String db_pool_name, Integer page_number, Integer page_size, Map<String, Object> data) {
		return paginate(getTemplate(tamplate_name), db_pool_name, page_number, page_size, data);
	}

	public PageData<RowData> paginate(String tamplate_name, String db_pool_name, Integer page_number, Integer page_size, Object... data) {
		return paginate(getTemplate(tamplate_name), db_pool_name, page_number, page_size, data);
	}

	//////////////////////////////////////////////////////////////////

	public <T extends RowModel<T>> List<T> find(String tamplate_name, String db_pool_name, Class<T> clazz, Map<String, Object> data) {
		DmsSqlTemplate sqlTemplate = getTemplate(tamplate_name);
		if(sqlTemplate==null) {
			return null;
		}
		String dbname = StrUtil.isBlank(db_pool_name) ? sqlTemplate.getDbname() : db_pool_name;
		dbname = StrUtil.isBlank(dbname) ? DbPool.DEFAULT_NAME : dbname;
		return DbPool.use(dbname).dao(clazz).find(getSql(sqlTemplate, data));
	}

	public <T extends RowModel<T>> List<T> find(String tamplate_name, String db_pool_name, Class<T> clazz, Object... data) {
		DmsSqlTemplate sqlTemplate = getTemplate(tamplate_name);
		if(sqlTemplate==null) {
			return null;
		}
		String dbname = StrUtil.isBlank(db_pool_name) ? sqlTemplate.getDbname() : db_pool_name;
		dbname = StrUtil.isBlank(dbname) ? DbPool.DEFAULT_NAME : dbname;
		return DbPool.use(dbname).dao(clazz).find(getSql(sqlTemplate, data));
	}

	public <T extends RowModel<T>> T findFirst(String tamplate_name, String db_pool_name, Class<T> clazz, Map<String, Object> data) {
		DmsSqlTemplate sqlTemplate = getTemplate(tamplate_name);
		if(sqlTemplate==null) {
			return null;
		}
		String dbname = StrUtil.isBlank(db_pool_name) ? sqlTemplate.getDbname() : db_pool_name;
		dbname = StrUtil.isBlank(dbname) ? DbPool.DEFAULT_NAME : dbname;
		return (T) DbPool.use(dbname).dao(clazz).findFirst(getSql(sqlTemplate, data));
	}

	public <T extends RowModel<T>> T findFirst(String tamplate_name, String db_pool_name, Class<T> clazz, Object... data) {
		DmsSqlTemplate sqlTemplate = getTemplate(tamplate_name);
		if(sqlTemplate==null) {
			return null;
		}
		String dbname = StrUtil.isBlank(db_pool_name) ? sqlTemplate.getDbname() : db_pool_name;
		dbname = StrUtil.isBlank(dbname) ? DbPool.DEFAULT_NAME : dbname;
		return (T) DbPool.use(dbname).dao(clazz).findFirst(getSql(sqlTemplate, data));
	}

	public <T extends RowModel<T>> PageData<T> paginate(String tamplate_name, String db_pool_name, Class<T> clazz, Integer page_number, Integer page_size, Map<String, Object> data) {
		DmsSqlTemplate sqlTemplate = getTemplate(tamplate_name);
		if(sqlTemplate==null) {
			return null;
		}
		String dbname = StrUtil.isBlank(db_pool_name) ? sqlTemplate.getDbname() : db_pool_name;
		dbname = StrUtil.isBlank(dbname) ? DbPool.DEFAULT_NAME : dbname;
		return DbPool.use(dbname).dao(clazz).paginate(page_number, page_size, getSql(sqlTemplate, data));
	}

	public <T extends RowModel<T>> PageData<T> paginate(String tamplate_name, String db_pool_name, Class<T> clazz, Integer page_number, Integer page_size, Object... data) {
		DmsSqlTemplate sqlTemplate = getTemplate(tamplate_name);
		if(sqlTemplate==null) {
			return null;
		}
		String dbname = StrUtil.isBlank(db_pool_name) ? sqlTemplate.getDbname() : db_pool_name;
		dbname = StrUtil.isBlank(dbname) ? DbPool.DEFAULT_NAME : dbname;
		return DbPool.use(dbname).dao(clazz).paginate(page_number, page_size, getSql(sqlTemplate, data));
	}
}
