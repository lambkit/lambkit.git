/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsSqlTemplateSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static DmsSqlTemplateSQL of() {
		return new DmsSqlTemplateSQL();
	}
	
	public static DmsSqlTemplateSQL by(Column column) {
		DmsSqlTemplateSQL that = new DmsSqlTemplateSQL();
		that.add(column);
        return that;
    }

    public static DmsSqlTemplateSQL by(String name, Object value) {
        return (DmsSqlTemplateSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("dms_sql_template", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public DmsSqlTemplateSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public DmsSqlTemplateSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public DmsSqlTemplateSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public DmsSqlTemplateSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsSqlTemplateSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsSqlTemplateSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsSqlTemplateSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsSqlTemplateSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public DmsSqlTemplateSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public DmsSqlTemplateSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public DmsSqlTemplateSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public DmsSqlTemplateSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public DmsSqlTemplateSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public DmsSqlTemplateSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public DmsSqlTemplateSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public DmsSqlTemplateSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public DmsSqlTemplateSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public DmsSqlTemplateSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "String");
    	   return this;
      }

      public DmsSqlTemplateSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "String");
          return this;
      }
      public DmsSqlTemplateSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public DmsSqlTemplateSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public DmsSqlTemplateSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public DmsSqlTemplateSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public DmsSqlTemplateSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public DmsSqlTemplateSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public DmsSqlTemplateSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public DmsSqlTemplateSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "String");
    	   return this;
      }

      public DmsSqlTemplateSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "String");
          return this;
      }
      public DmsSqlTemplateSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public DmsSqlTemplateSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public DmsSqlTemplateSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public DmsSqlTemplateSQL andContentIsNull() {
		isnull("content");
		return this;
	}
	
	public DmsSqlTemplateSQL andContentIsNotNull() {
		notNull("content");
		return this;
	}
	
	public DmsSqlTemplateSQL andContentIsEmpty() {
		empty("content");
		return this;
	}

	public DmsSqlTemplateSQL andContentIsNotEmpty() {
		notEmpty("content");
		return this;
	}
       public DmsSqlTemplateSQL andContentLike(java.lang.String value) {
    	   addCriterion("content", value, ConditionMode.FUZZY, "content", "java.lang.String", "String");
    	   return this;
      }

      public DmsSqlTemplateSQL andContentNotLike(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_FUZZY, "content", "java.lang.String", "String");
          return this;
      }
      public DmsSqlTemplateSQL andContentEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andContentNotEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.NOT_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andContentGreaterThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andContentGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.GREATER_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andContentLessThan(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_THEN, "content", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andContentLessThanOrEqualTo(java.lang.String value) {
          addCriterion("content", value, ConditionMode.LESS_EQUAL, "content", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andContentBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("content", value1, value2, ConditionMode.BETWEEN, "content", "java.lang.String", "String");
    	  return this;
      }

      public DmsSqlTemplateSQL andContentNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("content", value1, value2, ConditionMode.NOT_BETWEEN, "content", "java.lang.String", "String");
          return this;
      }
        
      public DmsSqlTemplateSQL andContentIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.IN, "content", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andContentNotIn(List<java.lang.String> values) {
          addCriterion("content", values, ConditionMode.NOT_IN, "content", "java.lang.String", "String");
          return this;
      }
	public DmsSqlTemplateSQL andUseridIsNull() {
		isnull("userid");
		return this;
	}
	
	public DmsSqlTemplateSQL andUseridIsNotNull() {
		notNull("userid");
		return this;
	}
	
	public DmsSqlTemplateSQL andUseridIsEmpty() {
		empty("userid");
		return this;
	}

	public DmsSqlTemplateSQL andUseridIsNotEmpty() {
		notEmpty("userid");
		return this;
	}
      public DmsSqlTemplateSQL andUseridEqualTo(java.lang.Integer value) {
          addCriterion("userid", value, ConditionMode.EQUAL, "userid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andUseridNotEqualTo(java.lang.Integer value) {
          addCriterion("userid", value, ConditionMode.NOT_EQUAL, "userid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andUseridGreaterThan(java.lang.Integer value) {
          addCriterion("userid", value, ConditionMode.GREATER_THEN, "userid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andUseridGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("userid", value, ConditionMode.GREATER_EQUAL, "userid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andUseridLessThan(java.lang.Integer value) {
          addCriterion("userid", value, ConditionMode.LESS_THEN, "userid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andUseridLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("userid", value, ConditionMode.LESS_EQUAL, "userid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andUseridBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("userid", value1, value2, ConditionMode.BETWEEN, "userid", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsSqlTemplateSQL andUseridNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("userid", value1, value2, ConditionMode.NOT_BETWEEN, "userid", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsSqlTemplateSQL andUseridIn(List<java.lang.Integer> values) {
          addCriterion("userid", values, ConditionMode.IN, "userid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andUseridNotIn(List<java.lang.Integer> values) {
          addCriterion("userid", values, ConditionMode.NOT_IN, "userid", "java.lang.Integer", "Float");
          return this;
      }
	public DmsSqlTemplateSQL andAppidIsNull() {
		isnull("appid");
		return this;
	}
	
	public DmsSqlTemplateSQL andAppidIsNotNull() {
		notNull("appid");
		return this;
	}
	
	public DmsSqlTemplateSQL andAppidIsEmpty() {
		empty("appid");
		return this;
	}

	public DmsSqlTemplateSQL andAppidIsNotEmpty() {
		notEmpty("appid");
		return this;
	}
      public DmsSqlTemplateSQL andAppidEqualTo(java.lang.Integer value) {
          addCriterion("appid", value, ConditionMode.EQUAL, "appid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andAppidNotEqualTo(java.lang.Integer value) {
          addCriterion("appid", value, ConditionMode.NOT_EQUAL, "appid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andAppidGreaterThan(java.lang.Integer value) {
          addCriterion("appid", value, ConditionMode.GREATER_THEN, "appid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andAppidGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("appid", value, ConditionMode.GREATER_EQUAL, "appid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andAppidLessThan(java.lang.Integer value) {
          addCriterion("appid", value, ConditionMode.LESS_THEN, "appid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andAppidLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("appid", value, ConditionMode.LESS_EQUAL, "appid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andAppidBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("appid", value1, value2, ConditionMode.BETWEEN, "appid", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsSqlTemplateSQL andAppidNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("appid", value1, value2, ConditionMode.NOT_BETWEEN, "appid", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsSqlTemplateSQL andAppidIn(List<java.lang.Integer> values) {
          addCriterion("appid", values, ConditionMode.IN, "appid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andAppidNotIn(List<java.lang.Integer> values) {
          addCriterion("appid", values, ConditionMode.NOT_IN, "appid", "java.lang.Integer", "Float");
          return this;
      }
	public DmsSqlTemplateSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public DmsSqlTemplateSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public DmsSqlTemplateSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public DmsSqlTemplateSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public DmsSqlTemplateSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsSqlTemplateSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsSqlTemplateSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsSqlTemplateSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public DmsSqlTemplateSQL andRuletypeIsNull() {
		isnull("ruletype");
		return this;
	}
	
	public DmsSqlTemplateSQL andRuletypeIsNotNull() {
		notNull("ruletype");
		return this;
	}
	
	public DmsSqlTemplateSQL andRuletypeIsEmpty() {
		empty("ruletype");
		return this;
	}

	public DmsSqlTemplateSQL andRuletypeIsNotEmpty() {
		notEmpty("ruletype");
		return this;
	}
       public DmsSqlTemplateSQL andRuletypeLike(java.lang.String value) {
    	   addCriterion("ruletype", value, ConditionMode.FUZZY, "ruletype", "java.lang.String", "Float");
    	   return this;
      }

      public DmsSqlTemplateSQL andRuletypeNotLike(java.lang.String value) {
          addCriterion("ruletype", value, ConditionMode.NOT_FUZZY, "ruletype", "java.lang.String", "Float");
          return this;
      }
      public DmsSqlTemplateSQL andRuletypeEqualTo(java.lang.String value) {
          addCriterion("ruletype", value, ConditionMode.EQUAL, "ruletype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andRuletypeNotEqualTo(java.lang.String value) {
          addCriterion("ruletype", value, ConditionMode.NOT_EQUAL, "ruletype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andRuletypeGreaterThan(java.lang.String value) {
          addCriterion("ruletype", value, ConditionMode.GREATER_THEN, "ruletype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andRuletypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ruletype", value, ConditionMode.GREATER_EQUAL, "ruletype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andRuletypeLessThan(java.lang.String value) {
          addCriterion("ruletype", value, ConditionMode.LESS_THEN, "ruletype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andRuletypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ruletype", value, ConditionMode.LESS_EQUAL, "ruletype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andRuletypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ruletype", value1, value2, ConditionMode.BETWEEN, "ruletype", "java.lang.String", "String");
    	  return this;
      }

      public DmsSqlTemplateSQL andRuletypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ruletype", value1, value2, ConditionMode.NOT_BETWEEN, "ruletype", "java.lang.String", "String");
          return this;
      }
        
      public DmsSqlTemplateSQL andRuletypeIn(List<java.lang.String> values) {
          addCriterion("ruletype", values, ConditionMode.IN, "ruletype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andRuletypeNotIn(List<java.lang.String> values) {
          addCriterion("ruletype", values, ConditionMode.NOT_IN, "ruletype", "java.lang.String", "String");
          return this;
      }
	public DmsSqlTemplateSQL andSqltypeIsNull() {
		isnull("sqltype");
		return this;
	}
	
	public DmsSqlTemplateSQL andSqltypeIsNotNull() {
		notNull("sqltype");
		return this;
	}
	
	public DmsSqlTemplateSQL andSqltypeIsEmpty() {
		empty("sqltype");
		return this;
	}

	public DmsSqlTemplateSQL andSqltypeIsNotEmpty() {
		notEmpty("sqltype");
		return this;
	}
       public DmsSqlTemplateSQL andSqltypeLike(java.lang.String value) {
    	   addCriterion("sqltype", value, ConditionMode.FUZZY, "sqltype", "java.lang.String", "String");
    	   return this;
      }

      public DmsSqlTemplateSQL andSqltypeNotLike(java.lang.String value) {
          addCriterion("sqltype", value, ConditionMode.NOT_FUZZY, "sqltype", "java.lang.String", "String");
          return this;
      }
      public DmsSqlTemplateSQL andSqltypeEqualTo(java.lang.String value) {
          addCriterion("sqltype", value, ConditionMode.EQUAL, "sqltype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andSqltypeNotEqualTo(java.lang.String value) {
          addCriterion("sqltype", value, ConditionMode.NOT_EQUAL, "sqltype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andSqltypeGreaterThan(java.lang.String value) {
          addCriterion("sqltype", value, ConditionMode.GREATER_THEN, "sqltype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andSqltypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("sqltype", value, ConditionMode.GREATER_EQUAL, "sqltype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andSqltypeLessThan(java.lang.String value) {
          addCriterion("sqltype", value, ConditionMode.LESS_THEN, "sqltype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andSqltypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("sqltype", value, ConditionMode.LESS_EQUAL, "sqltype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andSqltypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("sqltype", value1, value2, ConditionMode.BETWEEN, "sqltype", "java.lang.String", "String");
    	  return this;
      }

      public DmsSqlTemplateSQL andSqltypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("sqltype", value1, value2, ConditionMode.NOT_BETWEEN, "sqltype", "java.lang.String", "String");
          return this;
      }
        
      public DmsSqlTemplateSQL andSqltypeIn(List<java.lang.String> values) {
          addCriterion("sqltype", values, ConditionMode.IN, "sqltype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andSqltypeNotIn(List<java.lang.String> values) {
          addCriterion("sqltype", values, ConditionMode.NOT_IN, "sqltype", "java.lang.String", "String");
          return this;
      }
	public DmsSqlTemplateSQL andCatalogIsNull() {
		isnull("catalog");
		return this;
	}
	
	public DmsSqlTemplateSQL andCatalogIsNotNull() {
		notNull("catalog");
		return this;
	}
	
	public DmsSqlTemplateSQL andCatalogIsEmpty() {
		empty("catalog");
		return this;
	}

	public DmsSqlTemplateSQL andCatalogIsNotEmpty() {
		notEmpty("catalog");
		return this;
	}
       public DmsSqlTemplateSQL andCatalogLike(java.lang.String value) {
    	   addCriterion("catalog", value, ConditionMode.FUZZY, "catalog", "java.lang.String", "String");
    	   return this;
      }

      public DmsSqlTemplateSQL andCatalogNotLike(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.NOT_FUZZY, "catalog", "java.lang.String", "String");
          return this;
      }
      public DmsSqlTemplateSQL andCatalogEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andCatalogNotEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.NOT_EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andCatalogGreaterThan(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.GREATER_THEN, "catalog", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andCatalogGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.GREATER_EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andCatalogLessThan(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.LESS_THEN, "catalog", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andCatalogLessThanOrEqualTo(java.lang.String value) {
          addCriterion("catalog", value, ConditionMode.LESS_EQUAL, "catalog", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andCatalogBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("catalog", value1, value2, ConditionMode.BETWEEN, "catalog", "java.lang.String", "String");
    	  return this;
      }

      public DmsSqlTemplateSQL andCatalogNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("catalog", value1, value2, ConditionMode.NOT_BETWEEN, "catalog", "java.lang.String", "String");
          return this;
      }
        
      public DmsSqlTemplateSQL andCatalogIn(List<java.lang.String> values) {
          addCriterion("catalog", values, ConditionMode.IN, "catalog", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andCatalogNotIn(List<java.lang.String> values) {
          addCriterion("catalog", values, ConditionMode.NOT_IN, "catalog", "java.lang.String", "String");
          return this;
      }
	public DmsSqlTemplateSQL andDbtypeIsNull() {
		isnull("dbtype");
		return this;
	}
	
	public DmsSqlTemplateSQL andDbtypeIsNotNull() {
		notNull("dbtype");
		return this;
	}
	
	public DmsSqlTemplateSQL andDbtypeIsEmpty() {
		empty("dbtype");
		return this;
	}

	public DmsSqlTemplateSQL andDbtypeIsNotEmpty() {
		notEmpty("dbtype");
		return this;
	}
       public DmsSqlTemplateSQL andDbtypeLike(java.lang.String value) {
    	   addCriterion("dbtype", value, ConditionMode.FUZZY, "dbtype", "java.lang.String", "String");
    	   return this;
      }

      public DmsSqlTemplateSQL andDbtypeNotLike(java.lang.String value) {
          addCriterion("dbtype", value, ConditionMode.NOT_FUZZY, "dbtype", "java.lang.String", "String");
          return this;
      }
      public DmsSqlTemplateSQL andDbtypeEqualTo(java.lang.String value) {
          addCriterion("dbtype", value, ConditionMode.EQUAL, "dbtype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andDbtypeNotEqualTo(java.lang.String value) {
          addCriterion("dbtype", value, ConditionMode.NOT_EQUAL, "dbtype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andDbtypeGreaterThan(java.lang.String value) {
          addCriterion("dbtype", value, ConditionMode.GREATER_THEN, "dbtype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andDbtypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("dbtype", value, ConditionMode.GREATER_EQUAL, "dbtype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andDbtypeLessThan(java.lang.String value) {
          addCriterion("dbtype", value, ConditionMode.LESS_THEN, "dbtype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andDbtypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("dbtype", value, ConditionMode.LESS_EQUAL, "dbtype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andDbtypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("dbtype", value1, value2, ConditionMode.BETWEEN, "dbtype", "java.lang.String", "String");
    	  return this;
      }

      public DmsSqlTemplateSQL andDbtypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("dbtype", value1, value2, ConditionMode.NOT_BETWEEN, "dbtype", "java.lang.String", "String");
          return this;
      }
        
      public DmsSqlTemplateSQL andDbtypeIn(List<java.lang.String> values) {
          addCriterion("dbtype", values, ConditionMode.IN, "dbtype", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andDbtypeNotIn(List<java.lang.String> values) {
          addCriterion("dbtype", values, ConditionMode.NOT_IN, "dbtype", "java.lang.String", "String");
          return this;
      }
	public DmsSqlTemplateSQL andDbnameIsNull() {
		isnull("dbname");
		return this;
	}
	
	public DmsSqlTemplateSQL andDbnameIsNotNull() {
		notNull("dbname");
		return this;
	}
	
	public DmsSqlTemplateSQL andDbnameIsEmpty() {
		empty("dbname");
		return this;
	}

	public DmsSqlTemplateSQL andDbnameIsNotEmpty() {
		notEmpty("dbname");
		return this;
	}
       public DmsSqlTemplateSQL andDbnameLike(java.lang.String value) {
    	   addCriterion("dbname", value, ConditionMode.FUZZY, "dbname", "java.lang.String", "String");
    	   return this;
      }

      public DmsSqlTemplateSQL andDbnameNotLike(java.lang.String value) {
          addCriterion("dbname", value, ConditionMode.NOT_FUZZY, "dbname", "java.lang.String", "String");
          return this;
      }
      public DmsSqlTemplateSQL andDbnameEqualTo(java.lang.String value) {
          addCriterion("dbname", value, ConditionMode.EQUAL, "dbname", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andDbnameNotEqualTo(java.lang.String value) {
          addCriterion("dbname", value, ConditionMode.NOT_EQUAL, "dbname", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andDbnameGreaterThan(java.lang.String value) {
          addCriterion("dbname", value, ConditionMode.GREATER_THEN, "dbname", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andDbnameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("dbname", value, ConditionMode.GREATER_EQUAL, "dbname", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andDbnameLessThan(java.lang.String value) {
          addCriterion("dbname", value, ConditionMode.LESS_THEN, "dbname", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andDbnameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("dbname", value, ConditionMode.LESS_EQUAL, "dbname", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andDbnameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("dbname", value1, value2, ConditionMode.BETWEEN, "dbname", "java.lang.String", "String");
    	  return this;
      }

      public DmsSqlTemplateSQL andDbnameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("dbname", value1, value2, ConditionMode.NOT_BETWEEN, "dbname", "java.lang.String", "String");
          return this;
      }
        
      public DmsSqlTemplateSQL andDbnameIn(List<java.lang.String> values) {
          addCriterion("dbname", values, ConditionMode.IN, "dbname", "java.lang.String", "String");
          return this;
      }

      public DmsSqlTemplateSQL andDbnameNotIn(List<java.lang.String> values) {
          addCriterion("dbname", values, ConditionMode.NOT_IN, "dbname", "java.lang.String", "String");
          return this;
      }
}