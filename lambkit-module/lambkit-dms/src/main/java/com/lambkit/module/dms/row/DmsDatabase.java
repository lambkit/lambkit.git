/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsDatabase extends RowModel<DmsDatabase> {
	public DmsDatabase() {
		setTableName("dms_database");
		setPrimaryKey("dbid");
	}
	public java.lang.Integer getDbid() {
		return this.get("dbid");
	}
	public void setDbid(java.lang.Integer dbid) {
		this.set("dbid", dbid);
	}
	public java.lang.String getName() {
		return this.get("name");
	}
	public void setName(java.lang.String name) {
		this.set("name", name);
	}
	public java.lang.String getType() {
		return this.get("type");
	}
	public void setType(java.lang.String type) {
		this.set("type", type);
	}
	public java.lang.String getVersion() {
		return this.get("version");
	}
	public void setVersion(java.lang.String version) {
		this.set("version", version);
	}
	public java.lang.String getDriverClass() {
		return this.get("driver_class");
	}
	public void setDriverClass(java.lang.String driverClass) {
		this.set("driver_class", driverClass);
	}
	public java.lang.String getUrl() {
		return this.get("url");
	}
	public void setUrl(java.lang.String url) {
		this.set("url", url);
	}
	public java.lang.String getUsername() {
		return this.get("username");
	}
	public void setUsername(java.lang.String username) {
		this.set("username", username);
	}
	public java.lang.String getPassword() {
		return this.get("password");
	}
	public void setPassword(java.lang.String password) {
		this.set("password", password);
	}
	public java.lang.Integer getInitSize() {
		return this.get("init_size");
	}
	public void setInitSize(java.lang.Integer initSize) {
		this.set("init_size", initSize);
	}
	public java.lang.Integer getMinIdle() {
		return this.get("min_idle");
	}
	public void setMinIdle(java.lang.Integer minIdle) {
		this.set("min_idle", minIdle);
	}
	public java.lang.Integer getMaxActive() {
		return this.get("max_active");
	}
	public void setMaxActive(java.lang.Integer maxActive) {
		this.set("max_active", maxActive);
	}
	public java.lang.Integer getMaxWait() {
		return this.get("max_wait");
	}
	public void setMaxWait(java.lang.Integer maxWait) {
		this.set("max_wait", maxWait);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.lang.Long getCreateUser() {
		return this.get("create_user");
	}
	public void setCreateUser(java.lang.Long createUser) {
		this.set("create_user", createUser);
	}
	public java.lang.Long getUpdateUser() {
		return this.get("update_user");
	}
	public void setUpdateUser(java.lang.Long updateUser) {
		this.set("update_user", updateUser);
	}
	public java.util.Date getCreateTime() {
		return this.get("create_time");
	}
	public void setCreateTime(java.util.Date createTime) {
		this.set("create_time", createTime);
	}
	public java.lang.String getRemarks() {
		return this.get("remarks");
	}
	public void setRemarks(java.lang.String remarks) {
		this.set("remarks", remarks);
	}
	public java.lang.String getDbSchema() {
		return this.get("db_schema");
	}
	public void setDbSchema(java.lang.String dbSchema) {
		this.set("db_schema", dbSchema);
	}
	public java.lang.String getDbName() {
		return this.get("db_name");
	}
	public void setDbName(java.lang.String dbName) {
		this.set("db_name", dbName);
	}
	public java.lang.String getDbFactory() {
		return this.get("db_factory");
	}
	public void setDbFactory(java.lang.String dbFactory) {
		this.set("db_factory", dbFactory);
	}
}
