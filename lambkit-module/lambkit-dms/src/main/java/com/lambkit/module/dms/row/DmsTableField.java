/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsTableField extends RowModel<DmsTableField> {
	public DmsTableField() {
		setTableName("dms_table_field");
		setPrimaryKey("fldid");
	}
	public java.lang.Integer getFldid() {
		return this.get("fldid");
	}
	public void setFldid(java.lang.Integer fldid) {
		this.set("fldid", fldid);
	}
	public java.lang.Integer getTbid() {
		return this.get("tbid");
	}
	public void setTbid(java.lang.Integer tbid) {
		this.set("tbid", tbid);
	}
	public java.lang.String getName() {
		return this.get("name");
	}
	public void setName(java.lang.String name) {
		this.set("name", name);
	}
	public java.lang.String getTitle() {
		return this.get("title");
	}
	public void setTitle(java.lang.String title) {
		this.set("title", title);
	}
	public java.lang.String getDatatype() {
		return this.get("datatype");
	}
	public void setDatatype(java.lang.String datatype) {
		this.set("datatype", datatype);
	}
	public java.lang.String getIskey() {
		return this.get("iskey");
	}
	public void setIskey(java.lang.String iskey) {
		this.set("iskey", iskey);
	}
	public java.lang.String getIsunsigned() {
		return this.get("isunsigned");
	}
	public void setIsunsigned(java.lang.String isunsigned) {
		this.set("isunsigned", isunsigned);
	}
	public java.lang.String getIsnullable() {
		return this.get("isnullable");
	}
	public void setIsnullable(java.lang.String isnullable) {
		this.set("isnullable", isnullable);
	}
	public java.lang.String getIsai() {
		return this.get("isai");
	}
	public void setIsai(java.lang.String isai) {
		this.set("isai", isai);
	}
	public java.lang.String getFlddefault() {
		return this.get("flddefault");
	}
	public void setFlddefault(java.lang.String flddefault) {
		this.set("flddefault", flddefault);
	}
	public java.lang.String getDescript() {
		return this.get("descript");
	}
	public void setDescript(java.lang.String descript) {
		this.set("descript", descript);
	}
	public java.lang.String getIsfk() {
		return this.get("isfk");
	}
	public void setIsfk(java.lang.String isfk) {
		this.set("isfk", isfk);
	}
	public java.lang.String getFktbname() {
		return this.get("fktbname");
	}
	public void setFktbname(java.lang.String fktbname) {
		this.set("fktbname", fktbname);
	}
	public java.lang.String getFktbkey() {
		return this.get("fktbkey");
	}
	public void setFktbkey(java.lang.String fktbkey) {
		this.set("fktbkey", fktbkey);
	}
	public java.lang.String getFldlinkfk() {
		return this.get("fldlinkfk");
	}
	public void setFldlinkfk(java.lang.String fldlinkfk) {
		this.set("fldlinkfk", fldlinkfk);
	}
	public java.lang.Integer getOrders() {
		return this.get("orders");
	}
	public void setOrders(java.lang.Integer orders) {
		this.set("orders", orders);
	}
	public java.lang.String getAlias() {
		return this.get("alias");
	}
	public void setAlias(java.lang.String alias) {
		this.set("alias", alias);
	}
	public java.lang.String getJtype() {
		return this.get("jtype");
	}
	public void setJtype(java.lang.String jtype) {
		this.set("jtype", jtype);
	}
	public java.lang.String getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.String status) {
		this.set("status", status);
	}
	public java.lang.String getIsview() {
		return this.get("isview");
	}
	public void setIsview(java.lang.String isview) {
		this.set("isview", isview);
	}
	public java.lang.String getIsedit() {
		return this.get("isedit");
	}
	public void setIsedit(java.lang.String isedit) {
		this.set("isedit", isedit);
	}
	public java.lang.String getIsselect() {
		return this.get("isselect");
	}
	public void setIsselect(java.lang.String isselect) {
		this.set("isselect", isselect);
	}
	public java.lang.String getIsmustfld() {
		return this.get("ismustfld");
	}
	public void setIsmustfld(java.lang.String ismustfld) {
		this.set("ismustfld", ismustfld);
	}
	public java.lang.String getFormType() {
		return this.get("form_type");
	}
	public void setFormType(java.lang.String formType) {
		this.set("form_type", formType);
	}
	public java.lang.String getDictKey() {
		return this.get("dict_key");
	}
	public void setDictKey(java.lang.String dictKey) {
		this.set("dict_key", dictKey);
	}
	public java.lang.String getRelationshipType() {
		return this.get("relationship_type");
	}
	public void setRelationshipType(java.lang.String relationshipType) {
		this.set("relationship_type", relationshipType);
	}
}
