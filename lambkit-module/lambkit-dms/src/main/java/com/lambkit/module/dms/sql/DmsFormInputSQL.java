/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsFormInputSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static DmsFormInputSQL of() {
		return new DmsFormInputSQL();
	}
	
	public static DmsFormInputSQL by(Column column) {
		DmsFormInputSQL that = new DmsFormInputSQL();
		that.add(column);
        return that;
    }

    public static DmsFormInputSQL by(String name, Object value) {
        return (DmsFormInputSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("dms_form_input", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public DmsFormInputSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public DmsFormInputSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public DmsFormInputSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public DmsFormInputSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsFormInputSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsFormInputSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsFormInputSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsFormInputSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public DmsFormInputSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public DmsFormInputSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public DmsFormInputSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public DmsFormInputSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public DmsFormInputSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public DmsFormInputSQL andFormInputIdIsNull() {
		isnull("form_input_id");
		return this;
	}
	
	public DmsFormInputSQL andFormInputIdIsNotNull() {
		notNull("form_input_id");
		return this;
	}
	
	public DmsFormInputSQL andFormInputIdIsEmpty() {
		empty("form_input_id");
		return this;
	}

	public DmsFormInputSQL andFormInputIdIsNotEmpty() {
		notEmpty("form_input_id");
		return this;
	}
      public DmsFormInputSQL andFormInputIdEqualTo(java.lang.Long value) {
          addCriterion("form_input_id", value, ConditionMode.EQUAL, "formInputId", "java.lang.Long", "Float");
          return this;
      }

      public DmsFormInputSQL andFormInputIdNotEqualTo(java.lang.Long value) {
          addCriterion("form_input_id", value, ConditionMode.NOT_EQUAL, "formInputId", "java.lang.Long", "Float");
          return this;
      }

      public DmsFormInputSQL andFormInputIdGreaterThan(java.lang.Long value) {
          addCriterion("form_input_id", value, ConditionMode.GREATER_THEN, "formInputId", "java.lang.Long", "Float");
          return this;
      }

      public DmsFormInputSQL andFormInputIdGreaterThanOrEqualTo(java.lang.Long value) {
          addCriterion("form_input_id", value, ConditionMode.GREATER_EQUAL, "formInputId", "java.lang.Long", "Float");
          return this;
      }

      public DmsFormInputSQL andFormInputIdLessThan(java.lang.Long value) {
          addCriterion("form_input_id", value, ConditionMode.LESS_THEN, "formInputId", "java.lang.Long", "Float");
          return this;
      }

      public DmsFormInputSQL andFormInputIdLessThanOrEqualTo(java.lang.Long value) {
          addCriterion("form_input_id", value, ConditionMode.LESS_EQUAL, "formInputId", "java.lang.Long", "Float");
          return this;
      }

      public DmsFormInputSQL andFormInputIdBetween(java.lang.Long value1, java.lang.Long value2) {
    	  addCriterion("form_input_id", value1, value2, ConditionMode.BETWEEN, "formInputId", "java.lang.Long", "Float");
    	  return this;
      }

      public DmsFormInputSQL andFormInputIdNotBetween(java.lang.Long value1, java.lang.Long value2) {
          addCriterion("form_input_id", value1, value2, ConditionMode.NOT_BETWEEN, "formInputId", "java.lang.Long", "Float");
          return this;
      }
        
      public DmsFormInputSQL andFormInputIdIn(List<java.lang.Long> values) {
          addCriterion("form_input_id", values, ConditionMode.IN, "formInputId", "java.lang.Long", "Float");
          return this;
      }

      public DmsFormInputSQL andFormInputIdNotIn(List<java.lang.Long> values) {
          addCriterion("form_input_id", values, ConditionMode.NOT_IN, "formInputId", "java.lang.Long", "Float");
          return this;
      }
	public DmsFormInputSQL andInputTypeIsNull() {
		isnull("input_type");
		return this;
	}
	
	public DmsFormInputSQL andInputTypeIsNotNull() {
		notNull("input_type");
		return this;
	}
	
	public DmsFormInputSQL andInputTypeIsEmpty() {
		empty("input_type");
		return this;
	}

	public DmsFormInputSQL andInputTypeIsNotEmpty() {
		notEmpty("input_type");
		return this;
	}
       public DmsFormInputSQL andInputTypeLike(java.lang.String value) {
    	   addCriterion("input_type", value, ConditionMode.FUZZY, "inputType", "java.lang.String", "Float");
    	   return this;
      }

      public DmsFormInputSQL andInputTypeNotLike(java.lang.String value) {
          addCriterion("input_type", value, ConditionMode.NOT_FUZZY, "inputType", "java.lang.String", "Float");
          return this;
      }
      public DmsFormInputSQL andInputTypeEqualTo(java.lang.String value) {
          addCriterion("input_type", value, ConditionMode.EQUAL, "inputType", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andInputTypeNotEqualTo(java.lang.String value) {
          addCriterion("input_type", value, ConditionMode.NOT_EQUAL, "inputType", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andInputTypeGreaterThan(java.lang.String value) {
          addCriterion("input_type", value, ConditionMode.GREATER_THEN, "inputType", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andInputTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("input_type", value, ConditionMode.GREATER_EQUAL, "inputType", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andInputTypeLessThan(java.lang.String value) {
          addCriterion("input_type", value, ConditionMode.LESS_THEN, "inputType", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andInputTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("input_type", value, ConditionMode.LESS_EQUAL, "inputType", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andInputTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("input_type", value1, value2, ConditionMode.BETWEEN, "inputType", "java.lang.String", "String");
    	  return this;
      }

      public DmsFormInputSQL andInputTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("input_type", value1, value2, ConditionMode.NOT_BETWEEN, "inputType", "java.lang.String", "String");
          return this;
      }
        
      public DmsFormInputSQL andInputTypeIn(List<java.lang.String> values) {
          addCriterion("input_type", values, ConditionMode.IN, "inputType", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andInputTypeNotIn(List<java.lang.String> values) {
          addCriterion("input_type", values, ConditionMode.NOT_IN, "inputType", "java.lang.String", "String");
          return this;
      }
	public DmsFormInputSQL andLabelIsNull() {
		isnull("label");
		return this;
	}
	
	public DmsFormInputSQL andLabelIsNotNull() {
		notNull("label");
		return this;
	}
	
	public DmsFormInputSQL andLabelIsEmpty() {
		empty("label");
		return this;
	}

	public DmsFormInputSQL andLabelIsNotEmpty() {
		notEmpty("label");
		return this;
	}
       public DmsFormInputSQL andLabelLike(java.lang.String value) {
    	   addCriterion("label", value, ConditionMode.FUZZY, "label", "java.lang.String", "String");
    	   return this;
      }

      public DmsFormInputSQL andLabelNotLike(java.lang.String value) {
          addCriterion("label", value, ConditionMode.NOT_FUZZY, "label", "java.lang.String", "String");
          return this;
      }
      public DmsFormInputSQL andLabelEqualTo(java.lang.String value) {
          addCriterion("label", value, ConditionMode.EQUAL, "label", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andLabelNotEqualTo(java.lang.String value) {
          addCriterion("label", value, ConditionMode.NOT_EQUAL, "label", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andLabelGreaterThan(java.lang.String value) {
          addCriterion("label", value, ConditionMode.GREATER_THEN, "label", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andLabelGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("label", value, ConditionMode.GREATER_EQUAL, "label", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andLabelLessThan(java.lang.String value) {
          addCriterion("label", value, ConditionMode.LESS_THEN, "label", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andLabelLessThanOrEqualTo(java.lang.String value) {
          addCriterion("label", value, ConditionMode.LESS_EQUAL, "label", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andLabelBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("label", value1, value2, ConditionMode.BETWEEN, "label", "java.lang.String", "String");
    	  return this;
      }

      public DmsFormInputSQL andLabelNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("label", value1, value2, ConditionMode.NOT_BETWEEN, "label", "java.lang.String", "String");
          return this;
      }
        
      public DmsFormInputSQL andLabelIn(List<java.lang.String> values) {
          addCriterion("label", values, ConditionMode.IN, "label", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andLabelNotIn(List<java.lang.String> values) {
          addCriterion("label", values, ConditionMode.NOT_IN, "label", "java.lang.String", "String");
          return this;
      }
	public DmsFormInputSQL andRequiredIsNull() {
		isnull("required");
		return this;
	}
	
	public DmsFormInputSQL andRequiredIsNotNull() {
		notNull("required");
		return this;
	}
	
	public DmsFormInputSQL andRequiredIsEmpty() {
		empty("required");
		return this;
	}

	public DmsFormInputSQL andRequiredIsNotEmpty() {
		notEmpty("required");
		return this;
	}
      public DmsFormInputSQL andRequiredEqualTo(java.lang.Integer value) {
          addCriterion("required", value, ConditionMode.EQUAL, "required", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andRequiredNotEqualTo(java.lang.Integer value) {
          addCriterion("required", value, ConditionMode.NOT_EQUAL, "required", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andRequiredGreaterThan(java.lang.Integer value) {
          addCriterion("required", value, ConditionMode.GREATER_THEN, "required", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andRequiredGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("required", value, ConditionMode.GREATER_EQUAL, "required", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andRequiredLessThan(java.lang.Integer value) {
          addCriterion("required", value, ConditionMode.LESS_THEN, "required", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andRequiredLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("required", value, ConditionMode.LESS_EQUAL, "required", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andRequiredBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("required", value1, value2, ConditionMode.BETWEEN, "required", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormInputSQL andRequiredNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("required", value1, value2, ConditionMode.NOT_BETWEEN, "required", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormInputSQL andRequiredIn(List<java.lang.Integer> values) {
          addCriterion("required", values, ConditionMode.IN, "required", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andRequiredNotIn(List<java.lang.Integer> values) {
          addCriterion("required", values, ConditionMode.NOT_IN, "required", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormInputSQL andHintIsNull() {
		isnull("hint");
		return this;
	}
	
	public DmsFormInputSQL andHintIsNotNull() {
		notNull("hint");
		return this;
	}
	
	public DmsFormInputSQL andHintIsEmpty() {
		empty("hint");
		return this;
	}

	public DmsFormInputSQL andHintIsNotEmpty() {
		notEmpty("hint");
		return this;
	}
       public DmsFormInputSQL andHintLike(java.lang.String value) {
    	   addCriterion("hint", value, ConditionMode.FUZZY, "hint", "java.lang.String", "Float");
    	   return this;
      }

      public DmsFormInputSQL andHintNotLike(java.lang.String value) {
          addCriterion("hint", value, ConditionMode.NOT_FUZZY, "hint", "java.lang.String", "Float");
          return this;
      }
      public DmsFormInputSQL andHintEqualTo(java.lang.String value) {
          addCriterion("hint", value, ConditionMode.EQUAL, "hint", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andHintNotEqualTo(java.lang.String value) {
          addCriterion("hint", value, ConditionMode.NOT_EQUAL, "hint", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andHintGreaterThan(java.lang.String value) {
          addCriterion("hint", value, ConditionMode.GREATER_THEN, "hint", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andHintGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("hint", value, ConditionMode.GREATER_EQUAL, "hint", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andHintLessThan(java.lang.String value) {
          addCriterion("hint", value, ConditionMode.LESS_THEN, "hint", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andHintLessThanOrEqualTo(java.lang.String value) {
          addCriterion("hint", value, ConditionMode.LESS_EQUAL, "hint", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andHintBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("hint", value1, value2, ConditionMode.BETWEEN, "hint", "java.lang.String", "String");
    	  return this;
      }

      public DmsFormInputSQL andHintNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("hint", value1, value2, ConditionMode.NOT_BETWEEN, "hint", "java.lang.String", "String");
          return this;
      }
        
      public DmsFormInputSQL andHintIn(List<java.lang.String> values) {
          addCriterion("hint", values, ConditionMode.IN, "hint", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andHintNotIn(List<java.lang.String> values) {
          addCriterion("hint", values, ConditionMode.NOT_IN, "hint", "java.lang.String", "String");
          return this;
      }
	public DmsFormInputSQL andPlaceholderIsNull() {
		isnull("placeholder");
		return this;
	}
	
	public DmsFormInputSQL andPlaceholderIsNotNull() {
		notNull("placeholder");
		return this;
	}
	
	public DmsFormInputSQL andPlaceholderIsEmpty() {
		empty("placeholder");
		return this;
	}

	public DmsFormInputSQL andPlaceholderIsNotEmpty() {
		notEmpty("placeholder");
		return this;
	}
       public DmsFormInputSQL andPlaceholderLike(java.lang.String value) {
    	   addCriterion("placeholder", value, ConditionMode.FUZZY, "placeholder", "java.lang.String", "String");
    	   return this;
      }

      public DmsFormInputSQL andPlaceholderNotLike(java.lang.String value) {
          addCriterion("placeholder", value, ConditionMode.NOT_FUZZY, "placeholder", "java.lang.String", "String");
          return this;
      }
      public DmsFormInputSQL andPlaceholderEqualTo(java.lang.String value) {
          addCriterion("placeholder", value, ConditionMode.EQUAL, "placeholder", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andPlaceholderNotEqualTo(java.lang.String value) {
          addCriterion("placeholder", value, ConditionMode.NOT_EQUAL, "placeholder", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andPlaceholderGreaterThan(java.lang.String value) {
          addCriterion("placeholder", value, ConditionMode.GREATER_THEN, "placeholder", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andPlaceholderGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("placeholder", value, ConditionMode.GREATER_EQUAL, "placeholder", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andPlaceholderLessThan(java.lang.String value) {
          addCriterion("placeholder", value, ConditionMode.LESS_THEN, "placeholder", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andPlaceholderLessThanOrEqualTo(java.lang.String value) {
          addCriterion("placeholder", value, ConditionMode.LESS_EQUAL, "placeholder", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andPlaceholderBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("placeholder", value1, value2, ConditionMode.BETWEEN, "placeholder", "java.lang.String", "String");
    	  return this;
      }

      public DmsFormInputSQL andPlaceholderNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("placeholder", value1, value2, ConditionMode.NOT_BETWEEN, "placeholder", "java.lang.String", "String");
          return this;
      }
        
      public DmsFormInputSQL andPlaceholderIn(List<java.lang.String> values) {
          addCriterion("placeholder", values, ConditionMode.IN, "placeholder", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andPlaceholderNotIn(List<java.lang.String> values) {
          addCriterion("placeholder", values, ConditionMode.NOT_IN, "placeholder", "java.lang.String", "String");
          return this;
      }
	public DmsFormInputSQL andMinLengthIsNull() {
		isnull("min_length");
		return this;
	}
	
	public DmsFormInputSQL andMinLengthIsNotNull() {
		notNull("min_length");
		return this;
	}
	
	public DmsFormInputSQL andMinLengthIsEmpty() {
		empty("min_length");
		return this;
	}

	public DmsFormInputSQL andMinLengthIsNotEmpty() {
		notEmpty("min_length");
		return this;
	}
      public DmsFormInputSQL andMinLengthEqualTo(java.lang.Integer value) {
          addCriterion("min_length", value, ConditionMode.EQUAL, "minLength", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andMinLengthNotEqualTo(java.lang.Integer value) {
          addCriterion("min_length", value, ConditionMode.NOT_EQUAL, "minLength", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andMinLengthGreaterThan(java.lang.Integer value) {
          addCriterion("min_length", value, ConditionMode.GREATER_THEN, "minLength", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andMinLengthGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("min_length", value, ConditionMode.GREATER_EQUAL, "minLength", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andMinLengthLessThan(java.lang.Integer value) {
          addCriterion("min_length", value, ConditionMode.LESS_THEN, "minLength", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andMinLengthLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("min_length", value, ConditionMode.LESS_EQUAL, "minLength", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andMinLengthBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("min_length", value1, value2, ConditionMode.BETWEEN, "minLength", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormInputSQL andMinLengthNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("min_length", value1, value2, ConditionMode.NOT_BETWEEN, "minLength", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormInputSQL andMinLengthIn(List<java.lang.Integer> values) {
          addCriterion("min_length", values, ConditionMode.IN, "minLength", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andMinLengthNotIn(List<java.lang.Integer> values) {
          addCriterion("min_length", values, ConditionMode.NOT_IN, "minLength", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormInputSQL andMaxLengthIsNull() {
		isnull("max_length");
		return this;
	}
	
	public DmsFormInputSQL andMaxLengthIsNotNull() {
		notNull("max_length");
		return this;
	}
	
	public DmsFormInputSQL andMaxLengthIsEmpty() {
		empty("max_length");
		return this;
	}

	public DmsFormInputSQL andMaxLengthIsNotEmpty() {
		notEmpty("max_length");
		return this;
	}
      public DmsFormInputSQL andMaxLengthEqualTo(java.lang.Integer value) {
          addCriterion("max_length", value, ConditionMode.EQUAL, "maxLength", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andMaxLengthNotEqualTo(java.lang.Integer value) {
          addCriterion("max_length", value, ConditionMode.NOT_EQUAL, "maxLength", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andMaxLengthGreaterThan(java.lang.Integer value) {
          addCriterion("max_length", value, ConditionMode.GREATER_THEN, "maxLength", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andMaxLengthGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("max_length", value, ConditionMode.GREATER_EQUAL, "maxLength", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andMaxLengthLessThan(java.lang.Integer value) {
          addCriterion("max_length", value, ConditionMode.LESS_THEN, "maxLength", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andMaxLengthLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("max_length", value, ConditionMode.LESS_EQUAL, "maxLength", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andMaxLengthBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("max_length", value1, value2, ConditionMode.BETWEEN, "maxLength", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormInputSQL andMaxLengthNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("max_length", value1, value2, ConditionMode.NOT_BETWEEN, "maxLength", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormInputSQL andMaxLengthIn(List<java.lang.Integer> values) {
          addCriterion("max_length", values, ConditionMode.IN, "maxLength", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andMaxLengthNotIn(List<java.lang.Integer> values) {
          addCriterion("max_length", values, ConditionMode.NOT_IN, "maxLength", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormInputSQL andDisabledIsNull() {
		isnull("disabled");
		return this;
	}
	
	public DmsFormInputSQL andDisabledIsNotNull() {
		notNull("disabled");
		return this;
	}
	
	public DmsFormInputSQL andDisabledIsEmpty() {
		empty("disabled");
		return this;
	}

	public DmsFormInputSQL andDisabledIsNotEmpty() {
		notEmpty("disabled");
		return this;
	}
      public DmsFormInputSQL andDisabledEqualTo(java.lang.Integer value) {
          addCriterion("disabled", value, ConditionMode.EQUAL, "disabled", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andDisabledNotEqualTo(java.lang.Integer value) {
          addCriterion("disabled", value, ConditionMode.NOT_EQUAL, "disabled", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andDisabledGreaterThan(java.lang.Integer value) {
          addCriterion("disabled", value, ConditionMode.GREATER_THEN, "disabled", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andDisabledGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("disabled", value, ConditionMode.GREATER_EQUAL, "disabled", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andDisabledLessThan(java.lang.Integer value) {
          addCriterion("disabled", value, ConditionMode.LESS_THEN, "disabled", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andDisabledLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("disabled", value, ConditionMode.LESS_EQUAL, "disabled", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andDisabledBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("disabled", value1, value2, ConditionMode.BETWEEN, "disabled", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormInputSQL andDisabledNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("disabled", value1, value2, ConditionMode.NOT_BETWEEN, "disabled", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormInputSQL andDisabledIn(List<java.lang.Integer> values) {
          addCriterion("disabled", values, ConditionMode.IN, "disabled", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andDisabledNotIn(List<java.lang.Integer> values) {
          addCriterion("disabled", values, ConditionMode.NOT_IN, "disabled", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormInputSQL andReadonlyIsNull() {
		isnull("readonly");
		return this;
	}
	
	public DmsFormInputSQL andReadonlyIsNotNull() {
		notNull("readonly");
		return this;
	}
	
	public DmsFormInputSQL andReadonlyIsEmpty() {
		empty("readonly");
		return this;
	}

	public DmsFormInputSQL andReadonlyIsNotEmpty() {
		notEmpty("readonly");
		return this;
	}
      public DmsFormInputSQL andReadonlyEqualTo(java.lang.Integer value) {
          addCriterion("readonly", value, ConditionMode.EQUAL, "readonly", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andReadonlyNotEqualTo(java.lang.Integer value) {
          addCriterion("readonly", value, ConditionMode.NOT_EQUAL, "readonly", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andReadonlyGreaterThan(java.lang.Integer value) {
          addCriterion("readonly", value, ConditionMode.GREATER_THEN, "readonly", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andReadonlyGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("readonly", value, ConditionMode.GREATER_EQUAL, "readonly", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andReadonlyLessThan(java.lang.Integer value) {
          addCriterion("readonly", value, ConditionMode.LESS_THEN, "readonly", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andReadonlyLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("readonly", value, ConditionMode.LESS_EQUAL, "readonly", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andReadonlyBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("readonly", value1, value2, ConditionMode.BETWEEN, "readonly", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormInputSQL andReadonlyNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("readonly", value1, value2, ConditionMode.NOT_BETWEEN, "readonly", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormInputSQL andReadonlyIn(List<java.lang.Integer> values) {
          addCriterion("readonly", values, ConditionMode.IN, "readonly", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andReadonlyNotIn(List<java.lang.Integer> values) {
          addCriterion("readonly", values, ConditionMode.NOT_IN, "readonly", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormInputSQL andTextColIsNull() {
		isnull("text_col");
		return this;
	}
	
	public DmsFormInputSQL andTextColIsNotNull() {
		notNull("text_col");
		return this;
	}
	
	public DmsFormInputSQL andTextColIsEmpty() {
		empty("text_col");
		return this;
	}

	public DmsFormInputSQL andTextColIsNotEmpty() {
		notEmpty("text_col");
		return this;
	}
      public DmsFormInputSQL andTextColEqualTo(java.lang.Integer value) {
          addCriterion("text_col", value, ConditionMode.EQUAL, "textCol", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andTextColNotEqualTo(java.lang.Integer value) {
          addCriterion("text_col", value, ConditionMode.NOT_EQUAL, "textCol", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andTextColGreaterThan(java.lang.Integer value) {
          addCriterion("text_col", value, ConditionMode.GREATER_THEN, "textCol", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andTextColGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("text_col", value, ConditionMode.GREATER_EQUAL, "textCol", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andTextColLessThan(java.lang.Integer value) {
          addCriterion("text_col", value, ConditionMode.LESS_THEN, "textCol", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andTextColLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("text_col", value, ConditionMode.LESS_EQUAL, "textCol", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andTextColBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("text_col", value1, value2, ConditionMode.BETWEEN, "textCol", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormInputSQL andTextColNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("text_col", value1, value2, ConditionMode.NOT_BETWEEN, "textCol", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormInputSQL andTextColIn(List<java.lang.Integer> values) {
          addCriterion("text_col", values, ConditionMode.IN, "textCol", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andTextColNotIn(List<java.lang.Integer> values) {
          addCriterion("text_col", values, ConditionMode.NOT_IN, "textCol", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormInputSQL andTextRowIsNull() {
		isnull("text_row");
		return this;
	}
	
	public DmsFormInputSQL andTextRowIsNotNull() {
		notNull("text_row");
		return this;
	}
	
	public DmsFormInputSQL andTextRowIsEmpty() {
		empty("text_row");
		return this;
	}

	public DmsFormInputSQL andTextRowIsNotEmpty() {
		notEmpty("text_row");
		return this;
	}
      public DmsFormInputSQL andTextRowEqualTo(java.lang.Integer value) {
          addCriterion("text_row", value, ConditionMode.EQUAL, "textRow", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andTextRowNotEqualTo(java.lang.Integer value) {
          addCriterion("text_row", value, ConditionMode.NOT_EQUAL, "textRow", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andTextRowGreaterThan(java.lang.Integer value) {
          addCriterion("text_row", value, ConditionMode.GREATER_THEN, "textRow", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andTextRowGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("text_row", value, ConditionMode.GREATER_EQUAL, "textRow", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andTextRowLessThan(java.lang.Integer value) {
          addCriterion("text_row", value, ConditionMode.LESS_THEN, "textRow", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andTextRowLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("text_row", value, ConditionMode.LESS_EQUAL, "textRow", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andTextRowBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("text_row", value1, value2, ConditionMode.BETWEEN, "textRow", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormInputSQL andTextRowNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("text_row", value1, value2, ConditionMode.NOT_BETWEEN, "textRow", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormInputSQL andTextRowIn(List<java.lang.Integer> values) {
          addCriterion("text_row", values, ConditionMode.IN, "textRow", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andTextRowNotIn(List<java.lang.Integer> values) {
          addCriterion("text_row", values, ConditionMode.NOT_IN, "textRow", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormInputSQL andFldidIsNull() {
		isnull("fldid");
		return this;
	}
	
	public DmsFormInputSQL andFldidIsNotNull() {
		notNull("fldid");
		return this;
	}
	
	public DmsFormInputSQL andFldidIsEmpty() {
		empty("fldid");
		return this;
	}

	public DmsFormInputSQL andFldidIsNotEmpty() {
		notEmpty("fldid");
		return this;
	}
      public DmsFormInputSQL andFldidEqualTo(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.EQUAL, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andFldidNotEqualTo(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.NOT_EQUAL, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andFldidGreaterThan(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.GREATER_THEN, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andFldidGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.GREATER_EQUAL, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andFldidLessThan(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.LESS_THEN, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andFldidLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.LESS_EQUAL, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andFldidBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("fldid", value1, value2, ConditionMode.BETWEEN, "fldid", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormInputSQL andFldidNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("fldid", value1, value2, ConditionMode.NOT_BETWEEN, "fldid", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormInputSQL andFldidIn(List<java.lang.Integer> values) {
          addCriterion("fldid", values, ConditionMode.IN, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andFldidNotIn(List<java.lang.Integer> values) {
          addCriterion("fldid", values, ConditionMode.NOT_IN, "fldid", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormInputSQL andFldnameIsNull() {
		isnull("fldname");
		return this;
	}
	
	public DmsFormInputSQL andFldnameIsNotNull() {
		notNull("fldname");
		return this;
	}
	
	public DmsFormInputSQL andFldnameIsEmpty() {
		empty("fldname");
		return this;
	}

	public DmsFormInputSQL andFldnameIsNotEmpty() {
		notEmpty("fldname");
		return this;
	}
       public DmsFormInputSQL andFldnameLike(java.lang.String value) {
    	   addCriterion("fldname", value, ConditionMode.FUZZY, "fldname", "java.lang.String", "Float");
    	   return this;
      }

      public DmsFormInputSQL andFldnameNotLike(java.lang.String value) {
          addCriterion("fldname", value, ConditionMode.NOT_FUZZY, "fldname", "java.lang.String", "Float");
          return this;
      }
      public DmsFormInputSQL andFldnameEqualTo(java.lang.String value) {
          addCriterion("fldname", value, ConditionMode.EQUAL, "fldname", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andFldnameNotEqualTo(java.lang.String value) {
          addCriterion("fldname", value, ConditionMode.NOT_EQUAL, "fldname", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andFldnameGreaterThan(java.lang.String value) {
          addCriterion("fldname", value, ConditionMode.GREATER_THEN, "fldname", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andFldnameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("fldname", value, ConditionMode.GREATER_EQUAL, "fldname", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andFldnameLessThan(java.lang.String value) {
          addCriterion("fldname", value, ConditionMode.LESS_THEN, "fldname", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andFldnameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("fldname", value, ConditionMode.LESS_EQUAL, "fldname", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andFldnameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("fldname", value1, value2, ConditionMode.BETWEEN, "fldname", "java.lang.String", "String");
    	  return this;
      }

      public DmsFormInputSQL andFldnameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("fldname", value1, value2, ConditionMode.NOT_BETWEEN, "fldname", "java.lang.String", "String");
          return this;
      }
        
      public DmsFormInputSQL andFldnameIn(List<java.lang.String> values) {
          addCriterion("fldname", values, ConditionMode.IN, "fldname", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andFldnameNotIn(List<java.lang.String> values) {
          addCriterion("fldname", values, ConditionMode.NOT_IN, "fldname", "java.lang.String", "String");
          return this;
      }
	public DmsFormInputSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public DmsFormInputSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public DmsFormInputSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public DmsFormInputSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
      public DmsFormInputSQL andStatusEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andStatusNotEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andStatusGreaterThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andStatusGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andStatusLessThan(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andStatusLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andStatusBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormInputSQL andStatusNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormInputSQL andStatusIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andStatusNotIn(List<java.lang.Integer> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormInputSQL andDefalutValueIsNull() {
		isnull("defalut_value");
		return this;
	}
	
	public DmsFormInputSQL andDefalutValueIsNotNull() {
		notNull("defalut_value");
		return this;
	}
	
	public DmsFormInputSQL andDefalutValueIsEmpty() {
		empty("defalut_value");
		return this;
	}

	public DmsFormInputSQL andDefalutValueIsNotEmpty() {
		notEmpty("defalut_value");
		return this;
	}
       public DmsFormInputSQL andDefalutValueLike(java.lang.String value) {
    	   addCriterion("defalut_value", value, ConditionMode.FUZZY, "defalutValue", "java.lang.String", "Float");
    	   return this;
      }

      public DmsFormInputSQL andDefalutValueNotLike(java.lang.String value) {
          addCriterion("defalut_value", value, ConditionMode.NOT_FUZZY, "defalutValue", "java.lang.String", "Float");
          return this;
      }
      public DmsFormInputSQL andDefalutValueEqualTo(java.lang.String value) {
          addCriterion("defalut_value", value, ConditionMode.EQUAL, "defalutValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andDefalutValueNotEqualTo(java.lang.String value) {
          addCriterion("defalut_value", value, ConditionMode.NOT_EQUAL, "defalutValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andDefalutValueGreaterThan(java.lang.String value) {
          addCriterion("defalut_value", value, ConditionMode.GREATER_THEN, "defalutValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andDefalutValueGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("defalut_value", value, ConditionMode.GREATER_EQUAL, "defalutValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andDefalutValueLessThan(java.lang.String value) {
          addCriterion("defalut_value", value, ConditionMode.LESS_THEN, "defalutValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andDefalutValueLessThanOrEqualTo(java.lang.String value) {
          addCriterion("defalut_value", value, ConditionMode.LESS_EQUAL, "defalutValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andDefalutValueBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("defalut_value", value1, value2, ConditionMode.BETWEEN, "defalutValue", "java.lang.String", "String");
    	  return this;
      }

      public DmsFormInputSQL andDefalutValueNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("defalut_value", value1, value2, ConditionMode.NOT_BETWEEN, "defalutValue", "java.lang.String", "String");
          return this;
      }
        
      public DmsFormInputSQL andDefalutValueIn(List<java.lang.String> values) {
          addCriterion("defalut_value", values, ConditionMode.IN, "defalutValue", "java.lang.String", "String");
          return this;
      }

      public DmsFormInputSQL andDefalutValueNotIn(List<java.lang.String> values) {
          addCriterion("defalut_value", values, ConditionMode.NOT_IN, "defalutValue", "java.lang.String", "String");
          return this;
      }
	public DmsFormInputSQL andPrecisionIsNull() {
		isnull("precision");
		return this;
	}
	
	public DmsFormInputSQL andPrecisionIsNotNull() {
		notNull("precision");
		return this;
	}
	
	public DmsFormInputSQL andPrecisionIsEmpty() {
		empty("precision");
		return this;
	}

	public DmsFormInputSQL andPrecisionIsNotEmpty() {
		notEmpty("precision");
		return this;
	}
      public DmsFormInputSQL andPrecisionEqualTo(java.lang.Integer value) {
          addCriterion("precision", value, ConditionMode.EQUAL, "precision", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andPrecisionNotEqualTo(java.lang.Integer value) {
          addCriterion("precision", value, ConditionMode.NOT_EQUAL, "precision", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andPrecisionGreaterThan(java.lang.Integer value) {
          addCriterion("precision", value, ConditionMode.GREATER_THEN, "precision", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andPrecisionGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("precision", value, ConditionMode.GREATER_EQUAL, "precision", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andPrecisionLessThan(java.lang.Integer value) {
          addCriterion("precision", value, ConditionMode.LESS_THEN, "precision", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andPrecisionLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("precision", value, ConditionMode.LESS_EQUAL, "precision", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andPrecisionBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("precision", value1, value2, ConditionMode.BETWEEN, "precision", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsFormInputSQL andPrecisionNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("precision", value1, value2, ConditionMode.NOT_BETWEEN, "precision", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsFormInputSQL andPrecisionIn(List<java.lang.Integer> values) {
          addCriterion("precision", values, ConditionMode.IN, "precision", "java.lang.Integer", "Float");
          return this;
      }

      public DmsFormInputSQL andPrecisionNotIn(List<java.lang.Integer> values) {
          addCriterion("precision", values, ConditionMode.NOT_IN, "precision", "java.lang.Integer", "Float");
          return this;
      }
	public DmsFormInputSQL andMinValueIsNull() {
		isnull("min_value");
		return this;
	}
	
	public DmsFormInputSQL andMinValueIsNotNull() {
		notNull("min_value");
		return this;
	}
	
	public DmsFormInputSQL andMinValueIsEmpty() {
		empty("min_value");
		return this;
	}

	public DmsFormInputSQL andMinValueIsNotEmpty() {
		notEmpty("min_value");
		return this;
	}
      public DmsFormInputSQL andMinValueEqualTo(java.lang.Double value) {
          addCriterion("min_value", value, ConditionMode.EQUAL, "minValue", "java.lang.Double", "Float");
          return this;
      }

      public DmsFormInputSQL andMinValueNotEqualTo(java.lang.Double value) {
          addCriterion("min_value", value, ConditionMode.NOT_EQUAL, "minValue", "java.lang.Double", "Float");
          return this;
      }

      public DmsFormInputSQL andMinValueGreaterThan(java.lang.Double value) {
          addCriterion("min_value", value, ConditionMode.GREATER_THEN, "minValue", "java.lang.Double", "Float");
          return this;
      }

      public DmsFormInputSQL andMinValueGreaterThanOrEqualTo(java.lang.Double value) {
          addCriterion("min_value", value, ConditionMode.GREATER_EQUAL, "minValue", "java.lang.Double", "Float");
          return this;
      }

      public DmsFormInputSQL andMinValueLessThan(java.lang.Double value) {
          addCriterion("min_value", value, ConditionMode.LESS_THEN, "minValue", "java.lang.Double", "Float");
          return this;
      }

      public DmsFormInputSQL andMinValueLessThanOrEqualTo(java.lang.Double value) {
          addCriterion("min_value", value, ConditionMode.LESS_EQUAL, "minValue", "java.lang.Double", "Float");
          return this;
      }

      public DmsFormInputSQL andMinValueBetween(java.lang.Double value1, java.lang.Double value2) {
    	  addCriterion("min_value", value1, value2, ConditionMode.BETWEEN, "minValue", "java.lang.Double", "Float");
    	  return this;
      }

      public DmsFormInputSQL andMinValueNotBetween(java.lang.Double value1, java.lang.Double value2) {
          addCriterion("min_value", value1, value2, ConditionMode.NOT_BETWEEN, "minValue", "java.lang.Double", "Float");
          return this;
      }
        
      public DmsFormInputSQL andMinValueIn(List<java.lang.Double> values) {
          addCriterion("min_value", values, ConditionMode.IN, "minValue", "java.lang.Double", "Float");
          return this;
      }

      public DmsFormInputSQL andMinValueNotIn(List<java.lang.Double> values) {
          addCriterion("min_value", values, ConditionMode.NOT_IN, "minValue", "java.lang.Double", "Float");
          return this;
      }
	public DmsFormInputSQL andMaxValueIsNull() {
		isnull("max_value");
		return this;
	}
	
	public DmsFormInputSQL andMaxValueIsNotNull() {
		notNull("max_value");
		return this;
	}
	
	public DmsFormInputSQL andMaxValueIsEmpty() {
		empty("max_value");
		return this;
	}

	public DmsFormInputSQL andMaxValueIsNotEmpty() {
		notEmpty("max_value");
		return this;
	}
      public DmsFormInputSQL andMaxValueEqualTo(java.lang.Double value) {
          addCriterion("max_value", value, ConditionMode.EQUAL, "maxValue", "java.lang.Double", "Float");
          return this;
      }

      public DmsFormInputSQL andMaxValueNotEqualTo(java.lang.Double value) {
          addCriterion("max_value", value, ConditionMode.NOT_EQUAL, "maxValue", "java.lang.Double", "Float");
          return this;
      }

      public DmsFormInputSQL andMaxValueGreaterThan(java.lang.Double value) {
          addCriterion("max_value", value, ConditionMode.GREATER_THEN, "maxValue", "java.lang.Double", "Float");
          return this;
      }

      public DmsFormInputSQL andMaxValueGreaterThanOrEqualTo(java.lang.Double value) {
          addCriterion("max_value", value, ConditionMode.GREATER_EQUAL, "maxValue", "java.lang.Double", "Float");
          return this;
      }

      public DmsFormInputSQL andMaxValueLessThan(java.lang.Double value) {
          addCriterion("max_value", value, ConditionMode.LESS_THEN, "maxValue", "java.lang.Double", "Float");
          return this;
      }

      public DmsFormInputSQL andMaxValueLessThanOrEqualTo(java.lang.Double value) {
          addCriterion("max_value", value, ConditionMode.LESS_EQUAL, "maxValue", "java.lang.Double", "Float");
          return this;
      }

      public DmsFormInputSQL andMaxValueBetween(java.lang.Double value1, java.lang.Double value2) {
    	  addCriterion("max_value", value1, value2, ConditionMode.BETWEEN, "maxValue", "java.lang.Double", "Float");
    	  return this;
      }

      public DmsFormInputSQL andMaxValueNotBetween(java.lang.Double value1, java.lang.Double value2) {
          addCriterion("max_value", value1, value2, ConditionMode.NOT_BETWEEN, "maxValue", "java.lang.Double", "Float");
          return this;
      }
        
      public DmsFormInputSQL andMaxValueIn(List<java.lang.Double> values) {
          addCriterion("max_value", values, ConditionMode.IN, "maxValue", "java.lang.Double", "Float");
          return this;
      }

      public DmsFormInputSQL andMaxValueNotIn(List<java.lang.Double> values) {
          addCriterion("max_value", values, ConditionMode.NOT_IN, "maxValue", "java.lang.Double", "Float");
          return this;
      }
	public DmsFormInputSQL andStepValueIsNull() {
		isnull("step_value");
		return this;
	}
	
	public DmsFormInputSQL andStepValueIsNotNull() {
		notNull("step_value");
		return this;
	}
	
	public DmsFormInputSQL andStepValueIsEmpty() {
		empty("step_value");
		return this;
	}

	public DmsFormInputSQL andStepValueIsNotEmpty() {
		notEmpty("step_value");
		return this;
	}
      public DmsFormInputSQL andStepValueEqualTo(java.lang.Float value) {
          addCriterion("step_value", value, ConditionMode.EQUAL, "stepValue", "java.lang.Float", "Float");
          return this;
      }

      public DmsFormInputSQL andStepValueNotEqualTo(java.lang.Float value) {
          addCriterion("step_value", value, ConditionMode.NOT_EQUAL, "stepValue", "java.lang.Float", "Float");
          return this;
      }

      public DmsFormInputSQL andStepValueGreaterThan(java.lang.Float value) {
          addCriterion("step_value", value, ConditionMode.GREATER_THEN, "stepValue", "java.lang.Float", "Float");
          return this;
      }

      public DmsFormInputSQL andStepValueGreaterThanOrEqualTo(java.lang.Float value) {
          addCriterion("step_value", value, ConditionMode.GREATER_EQUAL, "stepValue", "java.lang.Float", "Float");
          return this;
      }

      public DmsFormInputSQL andStepValueLessThan(java.lang.Float value) {
          addCriterion("step_value", value, ConditionMode.LESS_THEN, "stepValue", "java.lang.Float", "Float");
          return this;
      }

      public DmsFormInputSQL andStepValueLessThanOrEqualTo(java.lang.Float value) {
          addCriterion("step_value", value, ConditionMode.LESS_EQUAL, "stepValue", "java.lang.Float", "Float");
          return this;
      }

      public DmsFormInputSQL andStepValueBetween(java.lang.Float value1, java.lang.Float value2) {
    	  addCriterion("step_value", value1, value2, ConditionMode.BETWEEN, "stepValue", "java.lang.Float", "Float");
    	  return this;
      }

      public DmsFormInputSQL andStepValueNotBetween(java.lang.Float value1, java.lang.Float value2) {
          addCriterion("step_value", value1, value2, ConditionMode.NOT_BETWEEN, "stepValue", "java.lang.Float", "Float");
          return this;
      }
        
      public DmsFormInputSQL andStepValueIn(List<java.lang.Float> values) {
          addCriterion("step_value", values, ConditionMode.IN, "stepValue", "java.lang.Float", "Float");
          return this;
      }

      public DmsFormInputSQL andStepValueNotIn(List<java.lang.Float> values) {
          addCriterion("step_value", values, ConditionMode.NOT_IN, "stepValue", "java.lang.Float", "Float");
          return this;
      }
}