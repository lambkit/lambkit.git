/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsTable extends RowModel<DmsTable> {
	public DmsTable() {
		setTableName("dms_table");
		setPrimaryKey("tbid");
	}
	public java.lang.Integer getTbid() {
		return this.get("tbid");
	}
	public void setTbid(java.lang.Integer tbid) {
		this.set("tbid", tbid);
	}
	public java.lang.String getName() {
		return this.get("name");
	}
	public void setName(java.lang.String name) {
		this.set("name", name);
	}
	public java.lang.String getTitle() {
		return this.get("title");
	}
	public void setTitle(java.lang.String title) {
		this.set("title", title);
	}
	public java.lang.String getAlias() {
		return this.get("alias");
	}
	public void setAlias(java.lang.String alias) {
		this.set("alias", alias);
	}
	public java.lang.Integer getOrders() {
		return this.get("orders");
	}
	public void setOrders(java.lang.Integer orders) {
		this.set("orders", orders);
	}
	public java.util.Date getCtime() {
		return this.get("ctime");
	}
	public void setCtime(java.util.Date ctime) {
		this.set("ctime", ctime);
	}
	public java.lang.String getCatalog() {
		return this.get("catalog");
	}
	public void setCatalog(java.lang.String catalog) {
		this.set("catalog", catalog);
	}
	public java.lang.String getTag() {
		return this.get("tag");
	}
	public void setTag(java.lang.String tag) {
		this.set("tag", tag);
	}
	public java.lang.String getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.String status) {
		this.set("status", status);
	}
	public java.lang.String getGuid() {
		return this.get("guid");
	}
	public void setGuid(java.lang.String guid) {
		this.set("guid", guid);
	}
}
