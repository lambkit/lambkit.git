/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.row;

import com.lambkit.db.RowModel;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsFormSelect extends RowModel<DmsFormSelect> {
	public DmsFormSelect() {
		setTableName("dms_form_select");
		setPrimaryKey("form_select_id");
	}
	public java.lang.Long getFormSelectId() {
		return this.get("form_select_id");
	}
	public void setFormSelectId(java.lang.Long formSelectId) {
		this.set("form_select_id", formSelectId);
	}
	public java.lang.String getSelectType() {
		return this.get("select_type");
	}
	public void setSelectType(java.lang.String selectType) {
		this.set("select_type", selectType);
	}
	public java.lang.String getLabel() {
		return this.get("label");
	}
	public void setLabel(java.lang.String label) {
		this.set("label", label);
	}
	public java.lang.Integer getRequired() {
		return this.get("required");
	}
	public void setRequired(java.lang.Integer required) {
		this.set("required", required);
	}
	public java.lang.String getHint() {
		return this.get("hint");
	}
	public void setHint(java.lang.String hint) {
		this.set("hint", hint);
	}
	public java.lang.String getPlaceholder() {
		return this.get("placeholder");
	}
	public void setPlaceholder(java.lang.String placeholder) {
		this.set("placeholder", placeholder);
	}
	public java.lang.Integer getDisabled() {
		return this.get("disabled");
	}
	public void setDisabled(java.lang.Integer disabled) {
		this.set("disabled", disabled);
	}
	public java.lang.Integer getReadonly() {
		return this.get("readonly");
	}
	public void setReadonly(java.lang.Integer readonly) {
		this.set("readonly", readonly);
	}
	public java.lang.Integer getFldid() {
		return this.get("fldid");
	}
	public void setFldid(java.lang.Integer fldid) {
		this.set("fldid", fldid);
	}
	public java.lang.String getFldname() {
		return this.get("fldname");
	}
	public void setFldname(java.lang.String fldname) {
		this.set("fldname", fldname);
	}
	public java.lang.Integer getStatus() {
		return this.get("status");
	}
	public void setStatus(java.lang.Integer status) {
		this.set("status", status);
	}
	public java.lang.String getDefalutValue() {
		return this.get("defalut_value");
	}
	public void setDefalutValue(java.lang.String defalutValue) {
		this.set("defalut_value", defalutValue);
	}
	public java.lang.String getDictValueKey() {
		return this.get("dict_value_key");
	}
	public void setDictValueKey(java.lang.String dictValueKey) {
		this.set("dict_value_key", dictValueKey);
	}
	public java.lang.String getSwitchOnValue() {
		return this.get("switch_on_value");
	}
	public void setSwitchOnValue(java.lang.String switchOnValue) {
		this.set("switch_on_value", switchOnValue);
	}
	public java.lang.String getSwitchOffValue() {
		return this.get("switch_off_value");
	}
	public void setSwitchOffValue(java.lang.String switchOffValue) {
		this.set("switch_off_value", switchOffValue);
	}
	public java.lang.Integer getMultiMinNum() {
		return this.get("multi_min_num");
	}
	public void setMultiMinNum(java.lang.Integer multiMinNum) {
		this.set("multi_min_num", multiMinNum);
	}
	public java.lang.Integer getMultiMaxNum() {
		return this.get("multi_max_num");
	}
	public void setMultiMaxNum(java.lang.Integer multiMaxNum) {
		this.set("multi_max_num", multiMaxNum);
	}
}
