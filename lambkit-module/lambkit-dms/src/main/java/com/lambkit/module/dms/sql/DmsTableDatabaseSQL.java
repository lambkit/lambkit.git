/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsTableDatabaseSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static DmsTableDatabaseSQL of() {
		return new DmsTableDatabaseSQL();
	}
	
	public static DmsTableDatabaseSQL by(Column column) {
		DmsTableDatabaseSQL that = new DmsTableDatabaseSQL();
		that.add(column);
        return that;
    }

    public static DmsTableDatabaseSQL by(String name, Object value) {
        return (DmsTableDatabaseSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("dms_table_database", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableDatabaseSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableDatabaseSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public DmsTableDatabaseSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public DmsTableDatabaseSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableDatabaseSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableDatabaseSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableDatabaseSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableDatabaseSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public DmsTableDatabaseSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public DmsTableDatabaseSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public DmsTableDatabaseSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public DmsTableDatabaseSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public DmsTableDatabaseSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public DmsTableDatabaseSQL andIdIsNull() {
		isnull("id");
		return this;
	}
	
	public DmsTableDatabaseSQL andIdIsNotNull() {
		notNull("id");
		return this;
	}
	
	public DmsTableDatabaseSQL andIdIsEmpty() {
		empty("id");
		return this;
	}

	public DmsTableDatabaseSQL andIdIsNotEmpty() {
		notEmpty("id");
		return this;
	}
      public DmsTableDatabaseSQL andIdEqualTo(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.EQUAL, "id", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andIdNotEqualTo(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.NOT_EQUAL, "id", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andIdGreaterThan(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.GREATER_THEN, "id", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andIdGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.GREATER_EQUAL, "id", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andIdLessThan(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.LESS_THEN, "id", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andIdLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("id", value, ConditionMode.LESS_EQUAL, "id", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andIdBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("id", value1, value2, ConditionMode.BETWEEN, "id", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsTableDatabaseSQL andIdNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("id", value1, value2, ConditionMode.NOT_BETWEEN, "id", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsTableDatabaseSQL andIdIn(List<java.lang.Integer> values) {
          addCriterion("id", values, ConditionMode.IN, "id", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andIdNotIn(List<java.lang.Integer> values) {
          addCriterion("id", values, ConditionMode.NOT_IN, "id", "java.lang.Integer", "Float");
          return this;
      }
	public DmsTableDatabaseSQL andDbidIsNull() {
		isnull("dbid");
		return this;
	}
	
	public DmsTableDatabaseSQL andDbidIsNotNull() {
		notNull("dbid");
		return this;
	}
	
	public DmsTableDatabaseSQL andDbidIsEmpty() {
		empty("dbid");
		return this;
	}

	public DmsTableDatabaseSQL andDbidIsNotEmpty() {
		notEmpty("dbid");
		return this;
	}
      public DmsTableDatabaseSQL andDbidEqualTo(java.lang.Integer value) {
          addCriterion("dbid", value, ConditionMode.EQUAL, "dbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andDbidNotEqualTo(java.lang.Integer value) {
          addCriterion("dbid", value, ConditionMode.NOT_EQUAL, "dbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andDbidGreaterThan(java.lang.Integer value) {
          addCriterion("dbid", value, ConditionMode.GREATER_THEN, "dbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andDbidGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("dbid", value, ConditionMode.GREATER_EQUAL, "dbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andDbidLessThan(java.lang.Integer value) {
          addCriterion("dbid", value, ConditionMode.LESS_THEN, "dbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andDbidLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("dbid", value, ConditionMode.LESS_EQUAL, "dbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andDbidBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("dbid", value1, value2, ConditionMode.BETWEEN, "dbid", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsTableDatabaseSQL andDbidNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("dbid", value1, value2, ConditionMode.NOT_BETWEEN, "dbid", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsTableDatabaseSQL andDbidIn(List<java.lang.Integer> values) {
          addCriterion("dbid", values, ConditionMode.IN, "dbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andDbidNotIn(List<java.lang.Integer> values) {
          addCriterion("dbid", values, ConditionMode.NOT_IN, "dbid", "java.lang.Integer", "Float");
          return this;
      }
	public DmsTableDatabaseSQL andTbidIsNull() {
		isnull("tbid");
		return this;
	}
	
	public DmsTableDatabaseSQL andTbidIsNotNull() {
		notNull("tbid");
		return this;
	}
	
	public DmsTableDatabaseSQL andTbidIsEmpty() {
		empty("tbid");
		return this;
	}

	public DmsTableDatabaseSQL andTbidIsNotEmpty() {
		notEmpty("tbid");
		return this;
	}
      public DmsTableDatabaseSQL andTbidEqualTo(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.EQUAL, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andTbidNotEqualTo(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.NOT_EQUAL, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andTbidGreaterThan(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.GREATER_THEN, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andTbidGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.GREATER_EQUAL, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andTbidLessThan(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.LESS_THEN, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andTbidLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.LESS_EQUAL, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andTbidBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("tbid", value1, value2, ConditionMode.BETWEEN, "tbid", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsTableDatabaseSQL andTbidNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("tbid", value1, value2, ConditionMode.NOT_BETWEEN, "tbid", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsTableDatabaseSQL andTbidIn(List<java.lang.Integer> values) {
          addCriterion("tbid", values, ConditionMode.IN, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableDatabaseSQL andTbidNotIn(List<java.lang.Integer> values) {
          addCriterion("tbid", values, ConditionMode.NOT_IN, "tbid", "java.lang.Integer", "Float");
          return this;
      }
}