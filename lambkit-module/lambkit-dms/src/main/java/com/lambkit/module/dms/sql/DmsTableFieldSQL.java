/**
 * Copyright (c) 2015-2017, Henry Yang 杨勇 (gismail@foxmail.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lambkit.module.dms.sql;

import com.lambkit.db.sql.ConditionMode;
import com.lambkit.db.sql.Column;
import com.lambkit.db.sql.Columns;
import com.lambkit.db.sql.Example;

import java.io.Serializable;
import java.util.List;

/**
 * @author yangyong 
 * @website: www.lambkit.com
 * @email: gismail@foxmail.com
 * @date ${date}
 * @version 1.0
 * @since 1.0
 */

public class DmsTableFieldSQL extends Columns implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static DmsTableFieldSQL of() {
		return new DmsTableFieldSQL();
	}
	
	public static DmsTableFieldSQL by(Column column) {
		DmsTableFieldSQL that = new DmsTableFieldSQL();
		that.add(column);
        return that;
    }

    public static DmsTableFieldSQL by(String name, Object value) {
        return (DmsTableFieldSQL) create().eq(name, value);
    }
    
    public Example example() {
    	return Example.create("dms_table_field", this);
    }
    
    /**
     * equals
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableFieldSQL eq(String name, Object value) {
    	super.eq(name, value);
        return this;
    }

    /**
     * not equals !=
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableFieldSQL ne(String name, Object value) {
    	super.ne(name, value);
        return this;
    }


    /**
     * like
     *
     * @param name
     * @param value
     * @return
     */

    public DmsTableFieldSQL like(String name, Object value) {
    	super.like(name, value);
        return this;
    }
    
    public DmsTableFieldSQL notLike(String name, Object value) {
    	super.notLike(name, value);
        return this;
    }

    /**
     * 大于 great than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableFieldSQL gt(String name, Object value) {
    	super.gt(name, value);
        return this;
    }

    /**
     * 大于等于 great or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableFieldSQL ge(String name, Object value) {
    	super.ge(name, value);
        return this;
    }

    /**
     * 小于 less than
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableFieldSQL lt(String name, Object value) {
    	super.lt(name, value);
        return this;
    }

    /**
     * 小于等于 less or equal
     *
     * @param name
     * @param value
     * @return
     */
    public DmsTableFieldSQL le(String name, Object value) {
    	super.le(name, value);
        return this;
    }
    
    public DmsTableFieldSQL isnull(String name) {
    	super.isnull(name);
        return this;
    } 

    public DmsTableFieldSQL notNull(String name) {
    	super.notNull(name);
        return this;
    } 
    
    public DmsTableFieldSQL empty(String name) {
    	super.empty(name);
        return this;
    } 
    
    public DmsTableFieldSQL notEmpty(String name) {
    	super.notEmpty(name);
        return this;
    } 
    
    public DmsTableFieldSQL add(Column column) {
    	super.add(column);
    	return this;
    }
    
    /**************************/
	
	public void addCriterion(String name, Object value, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value == null) {
			 throw new RuntimeException("Value for " + property + " cannot be null");
		 }
		 add(Column.create(name, value, logic, typeHandler, valueType));
	}
   
	public void addCriterion(String name, Object value1, Object value2, ConditionMode logic, String property, String typeHandler, String valueType) {
		 if (value1 == null || value2 == null) {
			 throw new RuntimeException("Between values for " + property + " cannot be null");
		 }
		 add(Column.create(name, value1, value2, logic, typeHandler, valueType));
	}
		 
	public DmsTableFieldSQL andFldidIsNull() {
		isnull("fldid");
		return this;
	}
	
	public DmsTableFieldSQL andFldidIsNotNull() {
		notNull("fldid");
		return this;
	}
	
	public DmsTableFieldSQL andFldidIsEmpty() {
		empty("fldid");
		return this;
	}

	public DmsTableFieldSQL andFldidIsNotEmpty() {
		notEmpty("fldid");
		return this;
	}
      public DmsTableFieldSQL andFldidEqualTo(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.EQUAL, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andFldidNotEqualTo(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.NOT_EQUAL, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andFldidGreaterThan(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.GREATER_THEN, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andFldidGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.GREATER_EQUAL, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andFldidLessThan(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.LESS_THEN, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andFldidLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("fldid", value, ConditionMode.LESS_EQUAL, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andFldidBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("fldid", value1, value2, ConditionMode.BETWEEN, "fldid", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsTableFieldSQL andFldidNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("fldid", value1, value2, ConditionMode.NOT_BETWEEN, "fldid", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsTableFieldSQL andFldidIn(List<java.lang.Integer> values) {
          addCriterion("fldid", values, ConditionMode.IN, "fldid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andFldidNotIn(List<java.lang.Integer> values) {
          addCriterion("fldid", values, ConditionMode.NOT_IN, "fldid", "java.lang.Integer", "Float");
          return this;
      }
	public DmsTableFieldSQL andTbidIsNull() {
		isnull("tbid");
		return this;
	}
	
	public DmsTableFieldSQL andTbidIsNotNull() {
		notNull("tbid");
		return this;
	}
	
	public DmsTableFieldSQL andTbidIsEmpty() {
		empty("tbid");
		return this;
	}

	public DmsTableFieldSQL andTbidIsNotEmpty() {
		notEmpty("tbid");
		return this;
	}
      public DmsTableFieldSQL andTbidEqualTo(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.EQUAL, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andTbidNotEqualTo(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.NOT_EQUAL, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andTbidGreaterThan(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.GREATER_THEN, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andTbidGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.GREATER_EQUAL, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andTbidLessThan(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.LESS_THEN, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andTbidLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("tbid", value, ConditionMode.LESS_EQUAL, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andTbidBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("tbid", value1, value2, ConditionMode.BETWEEN, "tbid", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsTableFieldSQL andTbidNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("tbid", value1, value2, ConditionMode.NOT_BETWEEN, "tbid", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsTableFieldSQL andTbidIn(List<java.lang.Integer> values) {
          addCriterion("tbid", values, ConditionMode.IN, "tbid", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andTbidNotIn(List<java.lang.Integer> values) {
          addCriterion("tbid", values, ConditionMode.NOT_IN, "tbid", "java.lang.Integer", "Float");
          return this;
      }
	public DmsTableFieldSQL andNameIsNull() {
		isnull("name");
		return this;
	}
	
	public DmsTableFieldSQL andNameIsNotNull() {
		notNull("name");
		return this;
	}
	
	public DmsTableFieldSQL andNameIsEmpty() {
		empty("name");
		return this;
	}

	public DmsTableFieldSQL andNameIsNotEmpty() {
		notEmpty("name");
		return this;
	}
       public DmsTableFieldSQL andNameLike(java.lang.String value) {
    	   addCriterion("name", value, ConditionMode.FUZZY, "name", "java.lang.String", "Float");
    	   return this;
      }

      public DmsTableFieldSQL andNameNotLike(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_FUZZY, "name", "java.lang.String", "Float");
          return this;
      }
      public DmsTableFieldSQL andNameEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andNameNotEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.NOT_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andNameGreaterThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andNameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.GREATER_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andNameLessThan(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_THEN, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andNameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("name", value, ConditionMode.LESS_EQUAL, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andNameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("name", value1, value2, ConditionMode.BETWEEN, "name", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andNameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("name", value1, value2, ConditionMode.NOT_BETWEEN, "name", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andNameIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.IN, "name", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andNameNotIn(List<java.lang.String> values) {
          addCriterion("name", values, ConditionMode.NOT_IN, "name", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andTitleIsNull() {
		isnull("title");
		return this;
	}
	
	public DmsTableFieldSQL andTitleIsNotNull() {
		notNull("title");
		return this;
	}
	
	public DmsTableFieldSQL andTitleIsEmpty() {
		empty("title");
		return this;
	}

	public DmsTableFieldSQL andTitleIsNotEmpty() {
		notEmpty("title");
		return this;
	}
       public DmsTableFieldSQL andTitleLike(java.lang.String value) {
    	   addCriterion("title", value, ConditionMode.FUZZY, "title", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andTitleNotLike(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_FUZZY, "title", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andTitleEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andTitleNotEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.NOT_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andTitleGreaterThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andTitleGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.GREATER_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andTitleLessThan(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_THEN, "title", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andTitleLessThanOrEqualTo(java.lang.String value) {
          addCriterion("title", value, ConditionMode.LESS_EQUAL, "title", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andTitleBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("title", value1, value2, ConditionMode.BETWEEN, "title", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andTitleNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("title", value1, value2, ConditionMode.NOT_BETWEEN, "title", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andTitleIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.IN, "title", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andTitleNotIn(List<java.lang.String> values) {
          addCriterion("title", values, ConditionMode.NOT_IN, "title", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andDatatypeIsNull() {
		isnull("datatype");
		return this;
	}
	
	public DmsTableFieldSQL andDatatypeIsNotNull() {
		notNull("datatype");
		return this;
	}
	
	public DmsTableFieldSQL andDatatypeIsEmpty() {
		empty("datatype");
		return this;
	}

	public DmsTableFieldSQL andDatatypeIsNotEmpty() {
		notEmpty("datatype");
		return this;
	}
       public DmsTableFieldSQL andDatatypeLike(java.lang.String value) {
    	   addCriterion("datatype", value, ConditionMode.FUZZY, "datatype", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andDatatypeNotLike(java.lang.String value) {
          addCriterion("datatype", value, ConditionMode.NOT_FUZZY, "datatype", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andDatatypeEqualTo(java.lang.String value) {
          addCriterion("datatype", value, ConditionMode.EQUAL, "datatype", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDatatypeNotEqualTo(java.lang.String value) {
          addCriterion("datatype", value, ConditionMode.NOT_EQUAL, "datatype", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDatatypeGreaterThan(java.lang.String value) {
          addCriterion("datatype", value, ConditionMode.GREATER_THEN, "datatype", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDatatypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("datatype", value, ConditionMode.GREATER_EQUAL, "datatype", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDatatypeLessThan(java.lang.String value) {
          addCriterion("datatype", value, ConditionMode.LESS_THEN, "datatype", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDatatypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("datatype", value, ConditionMode.LESS_EQUAL, "datatype", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDatatypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("datatype", value1, value2, ConditionMode.BETWEEN, "datatype", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andDatatypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("datatype", value1, value2, ConditionMode.NOT_BETWEEN, "datatype", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andDatatypeIn(List<java.lang.String> values) {
          addCriterion("datatype", values, ConditionMode.IN, "datatype", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDatatypeNotIn(List<java.lang.String> values) {
          addCriterion("datatype", values, ConditionMode.NOT_IN, "datatype", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andIskeyIsNull() {
		isnull("iskey");
		return this;
	}
	
	public DmsTableFieldSQL andIskeyIsNotNull() {
		notNull("iskey");
		return this;
	}
	
	public DmsTableFieldSQL andIskeyIsEmpty() {
		empty("iskey");
		return this;
	}

	public DmsTableFieldSQL andIskeyIsNotEmpty() {
		notEmpty("iskey");
		return this;
	}
       public DmsTableFieldSQL andIskeyLike(java.lang.String value) {
    	   addCriterion("iskey", value, ConditionMode.FUZZY, "iskey", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andIskeyNotLike(java.lang.String value) {
          addCriterion("iskey", value, ConditionMode.NOT_FUZZY, "iskey", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andIskeyEqualTo(java.lang.String value) {
          addCriterion("iskey", value, ConditionMode.EQUAL, "iskey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIskeyNotEqualTo(java.lang.String value) {
          addCriterion("iskey", value, ConditionMode.NOT_EQUAL, "iskey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIskeyGreaterThan(java.lang.String value) {
          addCriterion("iskey", value, ConditionMode.GREATER_THEN, "iskey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIskeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("iskey", value, ConditionMode.GREATER_EQUAL, "iskey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIskeyLessThan(java.lang.String value) {
          addCriterion("iskey", value, ConditionMode.LESS_THEN, "iskey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIskeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("iskey", value, ConditionMode.LESS_EQUAL, "iskey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIskeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("iskey", value1, value2, ConditionMode.BETWEEN, "iskey", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andIskeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("iskey", value1, value2, ConditionMode.NOT_BETWEEN, "iskey", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andIskeyIn(List<java.lang.String> values) {
          addCriterion("iskey", values, ConditionMode.IN, "iskey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIskeyNotIn(List<java.lang.String> values) {
          addCriterion("iskey", values, ConditionMode.NOT_IN, "iskey", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andIsunsignedIsNull() {
		isnull("isunsigned");
		return this;
	}
	
	public DmsTableFieldSQL andIsunsignedIsNotNull() {
		notNull("isunsigned");
		return this;
	}
	
	public DmsTableFieldSQL andIsunsignedIsEmpty() {
		empty("isunsigned");
		return this;
	}

	public DmsTableFieldSQL andIsunsignedIsNotEmpty() {
		notEmpty("isunsigned");
		return this;
	}
       public DmsTableFieldSQL andIsunsignedLike(java.lang.String value) {
    	   addCriterion("isunsigned", value, ConditionMode.FUZZY, "isunsigned", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andIsunsignedNotLike(java.lang.String value) {
          addCriterion("isunsigned", value, ConditionMode.NOT_FUZZY, "isunsigned", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andIsunsignedEqualTo(java.lang.String value) {
          addCriterion("isunsigned", value, ConditionMode.EQUAL, "isunsigned", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsunsignedNotEqualTo(java.lang.String value) {
          addCriterion("isunsigned", value, ConditionMode.NOT_EQUAL, "isunsigned", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsunsignedGreaterThan(java.lang.String value) {
          addCriterion("isunsigned", value, ConditionMode.GREATER_THEN, "isunsigned", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsunsignedGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("isunsigned", value, ConditionMode.GREATER_EQUAL, "isunsigned", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsunsignedLessThan(java.lang.String value) {
          addCriterion("isunsigned", value, ConditionMode.LESS_THEN, "isunsigned", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsunsignedLessThanOrEqualTo(java.lang.String value) {
          addCriterion("isunsigned", value, ConditionMode.LESS_EQUAL, "isunsigned", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsunsignedBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("isunsigned", value1, value2, ConditionMode.BETWEEN, "isunsigned", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andIsunsignedNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("isunsigned", value1, value2, ConditionMode.NOT_BETWEEN, "isunsigned", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andIsunsignedIn(List<java.lang.String> values) {
          addCriterion("isunsigned", values, ConditionMode.IN, "isunsigned", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsunsignedNotIn(List<java.lang.String> values) {
          addCriterion("isunsigned", values, ConditionMode.NOT_IN, "isunsigned", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andIsnullableIsNull() {
		isnull("isnullable");
		return this;
	}
	
	public DmsTableFieldSQL andIsnullableIsNotNull() {
		notNull("isnullable");
		return this;
	}
	
	public DmsTableFieldSQL andIsnullableIsEmpty() {
		empty("isnullable");
		return this;
	}

	public DmsTableFieldSQL andIsnullableIsNotEmpty() {
		notEmpty("isnullable");
		return this;
	}
       public DmsTableFieldSQL andIsnullableLike(java.lang.String value) {
    	   addCriterion("isnullable", value, ConditionMode.FUZZY, "isnullable", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andIsnullableNotLike(java.lang.String value) {
          addCriterion("isnullable", value, ConditionMode.NOT_FUZZY, "isnullable", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andIsnullableEqualTo(java.lang.String value) {
          addCriterion("isnullable", value, ConditionMode.EQUAL, "isnullable", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsnullableNotEqualTo(java.lang.String value) {
          addCriterion("isnullable", value, ConditionMode.NOT_EQUAL, "isnullable", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsnullableGreaterThan(java.lang.String value) {
          addCriterion("isnullable", value, ConditionMode.GREATER_THEN, "isnullable", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsnullableGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("isnullable", value, ConditionMode.GREATER_EQUAL, "isnullable", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsnullableLessThan(java.lang.String value) {
          addCriterion("isnullable", value, ConditionMode.LESS_THEN, "isnullable", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsnullableLessThanOrEqualTo(java.lang.String value) {
          addCriterion("isnullable", value, ConditionMode.LESS_EQUAL, "isnullable", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsnullableBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("isnullable", value1, value2, ConditionMode.BETWEEN, "isnullable", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andIsnullableNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("isnullable", value1, value2, ConditionMode.NOT_BETWEEN, "isnullable", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andIsnullableIn(List<java.lang.String> values) {
          addCriterion("isnullable", values, ConditionMode.IN, "isnullable", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsnullableNotIn(List<java.lang.String> values) {
          addCriterion("isnullable", values, ConditionMode.NOT_IN, "isnullable", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andIsaiIsNull() {
		isnull("isai");
		return this;
	}
	
	public DmsTableFieldSQL andIsaiIsNotNull() {
		notNull("isai");
		return this;
	}
	
	public DmsTableFieldSQL andIsaiIsEmpty() {
		empty("isai");
		return this;
	}

	public DmsTableFieldSQL andIsaiIsNotEmpty() {
		notEmpty("isai");
		return this;
	}
       public DmsTableFieldSQL andIsaiLike(java.lang.String value) {
    	   addCriterion("isai", value, ConditionMode.FUZZY, "isai", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andIsaiNotLike(java.lang.String value) {
          addCriterion("isai", value, ConditionMode.NOT_FUZZY, "isai", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andIsaiEqualTo(java.lang.String value) {
          addCriterion("isai", value, ConditionMode.EQUAL, "isai", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsaiNotEqualTo(java.lang.String value) {
          addCriterion("isai", value, ConditionMode.NOT_EQUAL, "isai", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsaiGreaterThan(java.lang.String value) {
          addCriterion("isai", value, ConditionMode.GREATER_THEN, "isai", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsaiGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("isai", value, ConditionMode.GREATER_EQUAL, "isai", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsaiLessThan(java.lang.String value) {
          addCriterion("isai", value, ConditionMode.LESS_THEN, "isai", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsaiLessThanOrEqualTo(java.lang.String value) {
          addCriterion("isai", value, ConditionMode.LESS_EQUAL, "isai", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsaiBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("isai", value1, value2, ConditionMode.BETWEEN, "isai", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andIsaiNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("isai", value1, value2, ConditionMode.NOT_BETWEEN, "isai", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andIsaiIn(List<java.lang.String> values) {
          addCriterion("isai", values, ConditionMode.IN, "isai", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsaiNotIn(List<java.lang.String> values) {
          addCriterion("isai", values, ConditionMode.NOT_IN, "isai", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andFlddefaultIsNull() {
		isnull("flddefault");
		return this;
	}
	
	public DmsTableFieldSQL andFlddefaultIsNotNull() {
		notNull("flddefault");
		return this;
	}
	
	public DmsTableFieldSQL andFlddefaultIsEmpty() {
		empty("flddefault");
		return this;
	}

	public DmsTableFieldSQL andFlddefaultIsNotEmpty() {
		notEmpty("flddefault");
		return this;
	}
       public DmsTableFieldSQL andFlddefaultLike(java.lang.String value) {
    	   addCriterion("flddefault", value, ConditionMode.FUZZY, "flddefault", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andFlddefaultNotLike(java.lang.String value) {
          addCriterion("flddefault", value, ConditionMode.NOT_FUZZY, "flddefault", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andFlddefaultEqualTo(java.lang.String value) {
          addCriterion("flddefault", value, ConditionMode.EQUAL, "flddefault", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFlddefaultNotEqualTo(java.lang.String value) {
          addCriterion("flddefault", value, ConditionMode.NOT_EQUAL, "flddefault", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFlddefaultGreaterThan(java.lang.String value) {
          addCriterion("flddefault", value, ConditionMode.GREATER_THEN, "flddefault", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFlddefaultGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("flddefault", value, ConditionMode.GREATER_EQUAL, "flddefault", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFlddefaultLessThan(java.lang.String value) {
          addCriterion("flddefault", value, ConditionMode.LESS_THEN, "flddefault", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFlddefaultLessThanOrEqualTo(java.lang.String value) {
          addCriterion("flddefault", value, ConditionMode.LESS_EQUAL, "flddefault", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFlddefaultBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("flddefault", value1, value2, ConditionMode.BETWEEN, "flddefault", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andFlddefaultNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("flddefault", value1, value2, ConditionMode.NOT_BETWEEN, "flddefault", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andFlddefaultIn(List<java.lang.String> values) {
          addCriterion("flddefault", values, ConditionMode.IN, "flddefault", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFlddefaultNotIn(List<java.lang.String> values) {
          addCriterion("flddefault", values, ConditionMode.NOT_IN, "flddefault", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andDescriptIsNull() {
		isnull("descript");
		return this;
	}
	
	public DmsTableFieldSQL andDescriptIsNotNull() {
		notNull("descript");
		return this;
	}
	
	public DmsTableFieldSQL andDescriptIsEmpty() {
		empty("descript");
		return this;
	}

	public DmsTableFieldSQL andDescriptIsNotEmpty() {
		notEmpty("descript");
		return this;
	}
       public DmsTableFieldSQL andDescriptLike(java.lang.String value) {
    	   addCriterion("descript", value, ConditionMode.FUZZY, "descript", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andDescriptNotLike(java.lang.String value) {
          addCriterion("descript", value, ConditionMode.NOT_FUZZY, "descript", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andDescriptEqualTo(java.lang.String value) {
          addCriterion("descript", value, ConditionMode.EQUAL, "descript", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDescriptNotEqualTo(java.lang.String value) {
          addCriterion("descript", value, ConditionMode.NOT_EQUAL, "descript", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDescriptGreaterThan(java.lang.String value) {
          addCriterion("descript", value, ConditionMode.GREATER_THEN, "descript", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDescriptGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("descript", value, ConditionMode.GREATER_EQUAL, "descript", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDescriptLessThan(java.lang.String value) {
          addCriterion("descript", value, ConditionMode.LESS_THEN, "descript", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDescriptLessThanOrEqualTo(java.lang.String value) {
          addCriterion("descript", value, ConditionMode.LESS_EQUAL, "descript", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDescriptBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("descript", value1, value2, ConditionMode.BETWEEN, "descript", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andDescriptNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("descript", value1, value2, ConditionMode.NOT_BETWEEN, "descript", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andDescriptIn(List<java.lang.String> values) {
          addCriterion("descript", values, ConditionMode.IN, "descript", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDescriptNotIn(List<java.lang.String> values) {
          addCriterion("descript", values, ConditionMode.NOT_IN, "descript", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andIsfkIsNull() {
		isnull("isfk");
		return this;
	}
	
	public DmsTableFieldSQL andIsfkIsNotNull() {
		notNull("isfk");
		return this;
	}
	
	public DmsTableFieldSQL andIsfkIsEmpty() {
		empty("isfk");
		return this;
	}

	public DmsTableFieldSQL andIsfkIsNotEmpty() {
		notEmpty("isfk");
		return this;
	}
       public DmsTableFieldSQL andIsfkLike(java.lang.String value) {
    	   addCriterion("isfk", value, ConditionMode.FUZZY, "isfk", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andIsfkNotLike(java.lang.String value) {
          addCriterion("isfk", value, ConditionMode.NOT_FUZZY, "isfk", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andIsfkEqualTo(java.lang.String value) {
          addCriterion("isfk", value, ConditionMode.EQUAL, "isfk", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsfkNotEqualTo(java.lang.String value) {
          addCriterion("isfk", value, ConditionMode.NOT_EQUAL, "isfk", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsfkGreaterThan(java.lang.String value) {
          addCriterion("isfk", value, ConditionMode.GREATER_THEN, "isfk", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsfkGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("isfk", value, ConditionMode.GREATER_EQUAL, "isfk", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsfkLessThan(java.lang.String value) {
          addCriterion("isfk", value, ConditionMode.LESS_THEN, "isfk", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsfkLessThanOrEqualTo(java.lang.String value) {
          addCriterion("isfk", value, ConditionMode.LESS_EQUAL, "isfk", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsfkBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("isfk", value1, value2, ConditionMode.BETWEEN, "isfk", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andIsfkNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("isfk", value1, value2, ConditionMode.NOT_BETWEEN, "isfk", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andIsfkIn(List<java.lang.String> values) {
          addCriterion("isfk", values, ConditionMode.IN, "isfk", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsfkNotIn(List<java.lang.String> values) {
          addCriterion("isfk", values, ConditionMode.NOT_IN, "isfk", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andFktbnameIsNull() {
		isnull("fktbname");
		return this;
	}
	
	public DmsTableFieldSQL andFktbnameIsNotNull() {
		notNull("fktbname");
		return this;
	}
	
	public DmsTableFieldSQL andFktbnameIsEmpty() {
		empty("fktbname");
		return this;
	}

	public DmsTableFieldSQL andFktbnameIsNotEmpty() {
		notEmpty("fktbname");
		return this;
	}
       public DmsTableFieldSQL andFktbnameLike(java.lang.String value) {
    	   addCriterion("fktbname", value, ConditionMode.FUZZY, "fktbname", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andFktbnameNotLike(java.lang.String value) {
          addCriterion("fktbname", value, ConditionMode.NOT_FUZZY, "fktbname", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andFktbnameEqualTo(java.lang.String value) {
          addCriterion("fktbname", value, ConditionMode.EQUAL, "fktbname", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFktbnameNotEqualTo(java.lang.String value) {
          addCriterion("fktbname", value, ConditionMode.NOT_EQUAL, "fktbname", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFktbnameGreaterThan(java.lang.String value) {
          addCriterion("fktbname", value, ConditionMode.GREATER_THEN, "fktbname", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFktbnameGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("fktbname", value, ConditionMode.GREATER_EQUAL, "fktbname", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFktbnameLessThan(java.lang.String value) {
          addCriterion("fktbname", value, ConditionMode.LESS_THEN, "fktbname", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFktbnameLessThanOrEqualTo(java.lang.String value) {
          addCriterion("fktbname", value, ConditionMode.LESS_EQUAL, "fktbname", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFktbnameBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("fktbname", value1, value2, ConditionMode.BETWEEN, "fktbname", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andFktbnameNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("fktbname", value1, value2, ConditionMode.NOT_BETWEEN, "fktbname", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andFktbnameIn(List<java.lang.String> values) {
          addCriterion("fktbname", values, ConditionMode.IN, "fktbname", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFktbnameNotIn(List<java.lang.String> values) {
          addCriterion("fktbname", values, ConditionMode.NOT_IN, "fktbname", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andFktbkeyIsNull() {
		isnull("fktbkey");
		return this;
	}
	
	public DmsTableFieldSQL andFktbkeyIsNotNull() {
		notNull("fktbkey");
		return this;
	}
	
	public DmsTableFieldSQL andFktbkeyIsEmpty() {
		empty("fktbkey");
		return this;
	}

	public DmsTableFieldSQL andFktbkeyIsNotEmpty() {
		notEmpty("fktbkey");
		return this;
	}
       public DmsTableFieldSQL andFktbkeyLike(java.lang.String value) {
    	   addCriterion("fktbkey", value, ConditionMode.FUZZY, "fktbkey", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andFktbkeyNotLike(java.lang.String value) {
          addCriterion("fktbkey", value, ConditionMode.NOT_FUZZY, "fktbkey", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andFktbkeyEqualTo(java.lang.String value) {
          addCriterion("fktbkey", value, ConditionMode.EQUAL, "fktbkey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFktbkeyNotEqualTo(java.lang.String value) {
          addCriterion("fktbkey", value, ConditionMode.NOT_EQUAL, "fktbkey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFktbkeyGreaterThan(java.lang.String value) {
          addCriterion("fktbkey", value, ConditionMode.GREATER_THEN, "fktbkey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFktbkeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("fktbkey", value, ConditionMode.GREATER_EQUAL, "fktbkey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFktbkeyLessThan(java.lang.String value) {
          addCriterion("fktbkey", value, ConditionMode.LESS_THEN, "fktbkey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFktbkeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("fktbkey", value, ConditionMode.LESS_EQUAL, "fktbkey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFktbkeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("fktbkey", value1, value2, ConditionMode.BETWEEN, "fktbkey", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andFktbkeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("fktbkey", value1, value2, ConditionMode.NOT_BETWEEN, "fktbkey", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andFktbkeyIn(List<java.lang.String> values) {
          addCriterion("fktbkey", values, ConditionMode.IN, "fktbkey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFktbkeyNotIn(List<java.lang.String> values) {
          addCriterion("fktbkey", values, ConditionMode.NOT_IN, "fktbkey", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andFldlinkfkIsNull() {
		isnull("fldlinkfk");
		return this;
	}
	
	public DmsTableFieldSQL andFldlinkfkIsNotNull() {
		notNull("fldlinkfk");
		return this;
	}
	
	public DmsTableFieldSQL andFldlinkfkIsEmpty() {
		empty("fldlinkfk");
		return this;
	}

	public DmsTableFieldSQL andFldlinkfkIsNotEmpty() {
		notEmpty("fldlinkfk");
		return this;
	}
       public DmsTableFieldSQL andFldlinkfkLike(java.lang.String value) {
    	   addCriterion("fldlinkfk", value, ConditionMode.FUZZY, "fldlinkfk", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andFldlinkfkNotLike(java.lang.String value) {
          addCriterion("fldlinkfk", value, ConditionMode.NOT_FUZZY, "fldlinkfk", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andFldlinkfkEqualTo(java.lang.String value) {
          addCriterion("fldlinkfk", value, ConditionMode.EQUAL, "fldlinkfk", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFldlinkfkNotEqualTo(java.lang.String value) {
          addCriterion("fldlinkfk", value, ConditionMode.NOT_EQUAL, "fldlinkfk", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFldlinkfkGreaterThan(java.lang.String value) {
          addCriterion("fldlinkfk", value, ConditionMode.GREATER_THEN, "fldlinkfk", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFldlinkfkGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("fldlinkfk", value, ConditionMode.GREATER_EQUAL, "fldlinkfk", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFldlinkfkLessThan(java.lang.String value) {
          addCriterion("fldlinkfk", value, ConditionMode.LESS_THEN, "fldlinkfk", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFldlinkfkLessThanOrEqualTo(java.lang.String value) {
          addCriterion("fldlinkfk", value, ConditionMode.LESS_EQUAL, "fldlinkfk", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFldlinkfkBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("fldlinkfk", value1, value2, ConditionMode.BETWEEN, "fldlinkfk", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andFldlinkfkNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("fldlinkfk", value1, value2, ConditionMode.NOT_BETWEEN, "fldlinkfk", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andFldlinkfkIn(List<java.lang.String> values) {
          addCriterion("fldlinkfk", values, ConditionMode.IN, "fldlinkfk", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFldlinkfkNotIn(List<java.lang.String> values) {
          addCriterion("fldlinkfk", values, ConditionMode.NOT_IN, "fldlinkfk", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andOrdersIsNull() {
		isnull("orders");
		return this;
	}
	
	public DmsTableFieldSQL andOrdersIsNotNull() {
		notNull("orders");
		return this;
	}
	
	public DmsTableFieldSQL andOrdersIsEmpty() {
		empty("orders");
		return this;
	}

	public DmsTableFieldSQL andOrdersIsNotEmpty() {
		notEmpty("orders");
		return this;
	}
      public DmsTableFieldSQL andOrdersEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andOrdersNotEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.NOT_EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andOrdersGreaterThan(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.GREATER_THEN, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andOrdersGreaterThanOrEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.GREATER_EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andOrdersLessThan(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.LESS_THEN, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andOrdersLessThanOrEqualTo(java.lang.Integer value) {
          addCriterion("orders", value, ConditionMode.LESS_EQUAL, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andOrdersBetween(java.lang.Integer value1, java.lang.Integer value2) {
    	  addCriterion("orders", value1, value2, ConditionMode.BETWEEN, "orders", "java.lang.Integer", "Float");
    	  return this;
      }

      public DmsTableFieldSQL andOrdersNotBetween(java.lang.Integer value1, java.lang.Integer value2) {
          addCriterion("orders", value1, value2, ConditionMode.NOT_BETWEEN, "orders", "java.lang.Integer", "Float");
          return this;
      }
        
      public DmsTableFieldSQL andOrdersIn(List<java.lang.Integer> values) {
          addCriterion("orders", values, ConditionMode.IN, "orders", "java.lang.Integer", "Float");
          return this;
      }

      public DmsTableFieldSQL andOrdersNotIn(List<java.lang.Integer> values) {
          addCriterion("orders", values, ConditionMode.NOT_IN, "orders", "java.lang.Integer", "Float");
          return this;
      }
	public DmsTableFieldSQL andAliasIsNull() {
		isnull("alias");
		return this;
	}
	
	public DmsTableFieldSQL andAliasIsNotNull() {
		notNull("alias");
		return this;
	}
	
	public DmsTableFieldSQL andAliasIsEmpty() {
		empty("alias");
		return this;
	}

	public DmsTableFieldSQL andAliasIsNotEmpty() {
		notEmpty("alias");
		return this;
	}
       public DmsTableFieldSQL andAliasLike(java.lang.String value) {
    	   addCriterion("alias", value, ConditionMode.FUZZY, "alias", "java.lang.String", "Float");
    	   return this;
      }

      public DmsTableFieldSQL andAliasNotLike(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_FUZZY, "alias", "java.lang.String", "Float");
          return this;
      }
      public DmsTableFieldSQL andAliasEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andAliasNotEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.NOT_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andAliasGreaterThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andAliasGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.GREATER_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andAliasLessThan(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_THEN, "alias", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andAliasLessThanOrEqualTo(java.lang.String value) {
          addCriterion("alias", value, ConditionMode.LESS_EQUAL, "alias", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andAliasBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("alias", value1, value2, ConditionMode.BETWEEN, "alias", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andAliasNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("alias", value1, value2, ConditionMode.NOT_BETWEEN, "alias", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andAliasIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.IN, "alias", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andAliasNotIn(List<java.lang.String> values) {
          addCriterion("alias", values, ConditionMode.NOT_IN, "alias", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andJtypeIsNull() {
		isnull("jtype");
		return this;
	}
	
	public DmsTableFieldSQL andJtypeIsNotNull() {
		notNull("jtype");
		return this;
	}
	
	public DmsTableFieldSQL andJtypeIsEmpty() {
		empty("jtype");
		return this;
	}

	public DmsTableFieldSQL andJtypeIsNotEmpty() {
		notEmpty("jtype");
		return this;
	}
       public DmsTableFieldSQL andJtypeLike(java.lang.String value) {
    	   addCriterion("jtype", value, ConditionMode.FUZZY, "jtype", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andJtypeNotLike(java.lang.String value) {
          addCriterion("jtype", value, ConditionMode.NOT_FUZZY, "jtype", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andJtypeEqualTo(java.lang.String value) {
          addCriterion("jtype", value, ConditionMode.EQUAL, "jtype", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andJtypeNotEqualTo(java.lang.String value) {
          addCriterion("jtype", value, ConditionMode.NOT_EQUAL, "jtype", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andJtypeGreaterThan(java.lang.String value) {
          addCriterion("jtype", value, ConditionMode.GREATER_THEN, "jtype", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andJtypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("jtype", value, ConditionMode.GREATER_EQUAL, "jtype", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andJtypeLessThan(java.lang.String value) {
          addCriterion("jtype", value, ConditionMode.LESS_THEN, "jtype", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andJtypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("jtype", value, ConditionMode.LESS_EQUAL, "jtype", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andJtypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("jtype", value1, value2, ConditionMode.BETWEEN, "jtype", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andJtypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("jtype", value1, value2, ConditionMode.NOT_BETWEEN, "jtype", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andJtypeIn(List<java.lang.String> values) {
          addCriterion("jtype", values, ConditionMode.IN, "jtype", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andJtypeNotIn(List<java.lang.String> values) {
          addCriterion("jtype", values, ConditionMode.NOT_IN, "jtype", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andStatusIsNull() {
		isnull("status");
		return this;
	}
	
	public DmsTableFieldSQL andStatusIsNotNull() {
		notNull("status");
		return this;
	}
	
	public DmsTableFieldSQL andStatusIsEmpty() {
		empty("status");
		return this;
	}

	public DmsTableFieldSQL andStatusIsNotEmpty() {
		notEmpty("status");
		return this;
	}
       public DmsTableFieldSQL andStatusLike(java.lang.String value) {
    	   addCriterion("status", value, ConditionMode.FUZZY, "status", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andStatusNotLike(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_FUZZY, "status", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andStatusEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andStatusNotEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.NOT_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andStatusGreaterThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andStatusGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.GREATER_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andStatusLessThan(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_THEN, "status", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andStatusLessThanOrEqualTo(java.lang.String value) {
          addCriterion("status", value, ConditionMode.LESS_EQUAL, "status", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andStatusBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("status", value1, value2, ConditionMode.BETWEEN, "status", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andStatusNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("status", value1, value2, ConditionMode.NOT_BETWEEN, "status", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andStatusIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.IN, "status", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andStatusNotIn(List<java.lang.String> values) {
          addCriterion("status", values, ConditionMode.NOT_IN, "status", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andIsviewIsNull() {
		isnull("isview");
		return this;
	}
	
	public DmsTableFieldSQL andIsviewIsNotNull() {
		notNull("isview");
		return this;
	}
	
	public DmsTableFieldSQL andIsviewIsEmpty() {
		empty("isview");
		return this;
	}

	public DmsTableFieldSQL andIsviewIsNotEmpty() {
		notEmpty("isview");
		return this;
	}
       public DmsTableFieldSQL andIsviewLike(java.lang.String value) {
    	   addCriterion("isview", value, ConditionMode.FUZZY, "isview", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andIsviewNotLike(java.lang.String value) {
          addCriterion("isview", value, ConditionMode.NOT_FUZZY, "isview", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andIsviewEqualTo(java.lang.String value) {
          addCriterion("isview", value, ConditionMode.EQUAL, "isview", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsviewNotEqualTo(java.lang.String value) {
          addCriterion("isview", value, ConditionMode.NOT_EQUAL, "isview", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsviewGreaterThan(java.lang.String value) {
          addCriterion("isview", value, ConditionMode.GREATER_THEN, "isview", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsviewGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("isview", value, ConditionMode.GREATER_EQUAL, "isview", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsviewLessThan(java.lang.String value) {
          addCriterion("isview", value, ConditionMode.LESS_THEN, "isview", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsviewLessThanOrEqualTo(java.lang.String value) {
          addCriterion("isview", value, ConditionMode.LESS_EQUAL, "isview", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsviewBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("isview", value1, value2, ConditionMode.BETWEEN, "isview", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andIsviewNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("isview", value1, value2, ConditionMode.NOT_BETWEEN, "isview", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andIsviewIn(List<java.lang.String> values) {
          addCriterion("isview", values, ConditionMode.IN, "isview", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsviewNotIn(List<java.lang.String> values) {
          addCriterion("isview", values, ConditionMode.NOT_IN, "isview", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andIseditIsNull() {
		isnull("isedit");
		return this;
	}
	
	public DmsTableFieldSQL andIseditIsNotNull() {
		notNull("isedit");
		return this;
	}
	
	public DmsTableFieldSQL andIseditIsEmpty() {
		empty("isedit");
		return this;
	}

	public DmsTableFieldSQL andIseditIsNotEmpty() {
		notEmpty("isedit");
		return this;
	}
       public DmsTableFieldSQL andIseditLike(java.lang.String value) {
    	   addCriterion("isedit", value, ConditionMode.FUZZY, "isedit", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andIseditNotLike(java.lang.String value) {
          addCriterion("isedit", value, ConditionMode.NOT_FUZZY, "isedit", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andIseditEqualTo(java.lang.String value) {
          addCriterion("isedit", value, ConditionMode.EQUAL, "isedit", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIseditNotEqualTo(java.lang.String value) {
          addCriterion("isedit", value, ConditionMode.NOT_EQUAL, "isedit", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIseditGreaterThan(java.lang.String value) {
          addCriterion("isedit", value, ConditionMode.GREATER_THEN, "isedit", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIseditGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("isedit", value, ConditionMode.GREATER_EQUAL, "isedit", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIseditLessThan(java.lang.String value) {
          addCriterion("isedit", value, ConditionMode.LESS_THEN, "isedit", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIseditLessThanOrEqualTo(java.lang.String value) {
          addCriterion("isedit", value, ConditionMode.LESS_EQUAL, "isedit", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIseditBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("isedit", value1, value2, ConditionMode.BETWEEN, "isedit", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andIseditNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("isedit", value1, value2, ConditionMode.NOT_BETWEEN, "isedit", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andIseditIn(List<java.lang.String> values) {
          addCriterion("isedit", values, ConditionMode.IN, "isedit", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIseditNotIn(List<java.lang.String> values) {
          addCriterion("isedit", values, ConditionMode.NOT_IN, "isedit", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andIsselectIsNull() {
		isnull("isselect");
		return this;
	}
	
	public DmsTableFieldSQL andIsselectIsNotNull() {
		notNull("isselect");
		return this;
	}
	
	public DmsTableFieldSQL andIsselectIsEmpty() {
		empty("isselect");
		return this;
	}

	public DmsTableFieldSQL andIsselectIsNotEmpty() {
		notEmpty("isselect");
		return this;
	}
       public DmsTableFieldSQL andIsselectLike(java.lang.String value) {
    	   addCriterion("isselect", value, ConditionMode.FUZZY, "isselect", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andIsselectNotLike(java.lang.String value) {
          addCriterion("isselect", value, ConditionMode.NOT_FUZZY, "isselect", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andIsselectEqualTo(java.lang.String value) {
          addCriterion("isselect", value, ConditionMode.EQUAL, "isselect", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsselectNotEqualTo(java.lang.String value) {
          addCriterion("isselect", value, ConditionMode.NOT_EQUAL, "isselect", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsselectGreaterThan(java.lang.String value) {
          addCriterion("isselect", value, ConditionMode.GREATER_THEN, "isselect", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsselectGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("isselect", value, ConditionMode.GREATER_EQUAL, "isselect", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsselectLessThan(java.lang.String value) {
          addCriterion("isselect", value, ConditionMode.LESS_THEN, "isselect", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsselectLessThanOrEqualTo(java.lang.String value) {
          addCriterion("isselect", value, ConditionMode.LESS_EQUAL, "isselect", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsselectBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("isselect", value1, value2, ConditionMode.BETWEEN, "isselect", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andIsselectNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("isselect", value1, value2, ConditionMode.NOT_BETWEEN, "isselect", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andIsselectIn(List<java.lang.String> values) {
          addCriterion("isselect", values, ConditionMode.IN, "isselect", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsselectNotIn(List<java.lang.String> values) {
          addCriterion("isselect", values, ConditionMode.NOT_IN, "isselect", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andIsmustfldIsNull() {
		isnull("ismustfld");
		return this;
	}
	
	public DmsTableFieldSQL andIsmustfldIsNotNull() {
		notNull("ismustfld");
		return this;
	}
	
	public DmsTableFieldSQL andIsmustfldIsEmpty() {
		empty("ismustfld");
		return this;
	}

	public DmsTableFieldSQL andIsmustfldIsNotEmpty() {
		notEmpty("ismustfld");
		return this;
	}
       public DmsTableFieldSQL andIsmustfldLike(java.lang.String value) {
    	   addCriterion("ismustfld", value, ConditionMode.FUZZY, "ismustfld", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andIsmustfldNotLike(java.lang.String value) {
          addCriterion("ismustfld", value, ConditionMode.NOT_FUZZY, "ismustfld", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andIsmustfldEqualTo(java.lang.String value) {
          addCriterion("ismustfld", value, ConditionMode.EQUAL, "ismustfld", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsmustfldNotEqualTo(java.lang.String value) {
          addCriterion("ismustfld", value, ConditionMode.NOT_EQUAL, "ismustfld", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsmustfldGreaterThan(java.lang.String value) {
          addCriterion("ismustfld", value, ConditionMode.GREATER_THEN, "ismustfld", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsmustfldGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("ismustfld", value, ConditionMode.GREATER_EQUAL, "ismustfld", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsmustfldLessThan(java.lang.String value) {
          addCriterion("ismustfld", value, ConditionMode.LESS_THEN, "ismustfld", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsmustfldLessThanOrEqualTo(java.lang.String value) {
          addCriterion("ismustfld", value, ConditionMode.LESS_EQUAL, "ismustfld", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsmustfldBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("ismustfld", value1, value2, ConditionMode.BETWEEN, "ismustfld", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andIsmustfldNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("ismustfld", value1, value2, ConditionMode.NOT_BETWEEN, "ismustfld", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andIsmustfldIn(List<java.lang.String> values) {
          addCriterion("ismustfld", values, ConditionMode.IN, "ismustfld", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andIsmustfldNotIn(List<java.lang.String> values) {
          addCriterion("ismustfld", values, ConditionMode.NOT_IN, "ismustfld", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andFormTypeIsNull() {
		isnull("form_type");
		return this;
	}
	
	public DmsTableFieldSQL andFormTypeIsNotNull() {
		notNull("form_type");
		return this;
	}
	
	public DmsTableFieldSQL andFormTypeIsEmpty() {
		empty("form_type");
		return this;
	}

	public DmsTableFieldSQL andFormTypeIsNotEmpty() {
		notEmpty("form_type");
		return this;
	}
       public DmsTableFieldSQL andFormTypeLike(java.lang.String value) {
    	   addCriterion("form_type", value, ConditionMode.FUZZY, "formType", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andFormTypeNotLike(java.lang.String value) {
          addCriterion("form_type", value, ConditionMode.NOT_FUZZY, "formType", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andFormTypeEqualTo(java.lang.String value) {
          addCriterion("form_type", value, ConditionMode.EQUAL, "formType", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFormTypeNotEqualTo(java.lang.String value) {
          addCriterion("form_type", value, ConditionMode.NOT_EQUAL, "formType", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFormTypeGreaterThan(java.lang.String value) {
          addCriterion("form_type", value, ConditionMode.GREATER_THEN, "formType", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFormTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("form_type", value, ConditionMode.GREATER_EQUAL, "formType", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFormTypeLessThan(java.lang.String value) {
          addCriterion("form_type", value, ConditionMode.LESS_THEN, "formType", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFormTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("form_type", value, ConditionMode.LESS_EQUAL, "formType", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFormTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("form_type", value1, value2, ConditionMode.BETWEEN, "formType", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andFormTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("form_type", value1, value2, ConditionMode.NOT_BETWEEN, "formType", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andFormTypeIn(List<java.lang.String> values) {
          addCriterion("form_type", values, ConditionMode.IN, "formType", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andFormTypeNotIn(List<java.lang.String> values) {
          addCriterion("form_type", values, ConditionMode.NOT_IN, "formType", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andDictKeyIsNull() {
		isnull("dict_key");
		return this;
	}
	
	public DmsTableFieldSQL andDictKeyIsNotNull() {
		notNull("dict_key");
		return this;
	}
	
	public DmsTableFieldSQL andDictKeyIsEmpty() {
		empty("dict_key");
		return this;
	}

	public DmsTableFieldSQL andDictKeyIsNotEmpty() {
		notEmpty("dict_key");
		return this;
	}
       public DmsTableFieldSQL andDictKeyLike(java.lang.String value) {
    	   addCriterion("dict_key", value, ConditionMode.FUZZY, "dictKey", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andDictKeyNotLike(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.NOT_FUZZY, "dictKey", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andDictKeyEqualTo(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.EQUAL, "dictKey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDictKeyNotEqualTo(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.NOT_EQUAL, "dictKey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDictKeyGreaterThan(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.GREATER_THEN, "dictKey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDictKeyGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.GREATER_EQUAL, "dictKey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDictKeyLessThan(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.LESS_THEN, "dictKey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDictKeyLessThanOrEqualTo(java.lang.String value) {
          addCriterion("dict_key", value, ConditionMode.LESS_EQUAL, "dictKey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDictKeyBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("dict_key", value1, value2, ConditionMode.BETWEEN, "dictKey", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andDictKeyNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("dict_key", value1, value2, ConditionMode.NOT_BETWEEN, "dictKey", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andDictKeyIn(List<java.lang.String> values) {
          addCriterion("dict_key", values, ConditionMode.IN, "dictKey", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andDictKeyNotIn(List<java.lang.String> values) {
          addCriterion("dict_key", values, ConditionMode.NOT_IN, "dictKey", "java.lang.String", "String");
          return this;
      }
	public DmsTableFieldSQL andRelationshipTypeIsNull() {
		isnull("relationship_type");
		return this;
	}
	
	public DmsTableFieldSQL andRelationshipTypeIsNotNull() {
		notNull("relationship_type");
		return this;
	}
	
	public DmsTableFieldSQL andRelationshipTypeIsEmpty() {
		empty("relationship_type");
		return this;
	}

	public DmsTableFieldSQL andRelationshipTypeIsNotEmpty() {
		notEmpty("relationship_type");
		return this;
	}
       public DmsTableFieldSQL andRelationshipTypeLike(java.lang.String value) {
    	   addCriterion("relationship_type", value, ConditionMode.FUZZY, "relationshipType", "java.lang.String", "String");
    	   return this;
      }

      public DmsTableFieldSQL andRelationshipTypeNotLike(java.lang.String value) {
          addCriterion("relationship_type", value, ConditionMode.NOT_FUZZY, "relationshipType", "java.lang.String", "String");
          return this;
      }
      public DmsTableFieldSQL andRelationshipTypeEqualTo(java.lang.String value) {
          addCriterion("relationship_type", value, ConditionMode.EQUAL, "relationshipType", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andRelationshipTypeNotEqualTo(java.lang.String value) {
          addCriterion("relationship_type", value, ConditionMode.NOT_EQUAL, "relationshipType", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andRelationshipTypeGreaterThan(java.lang.String value) {
          addCriterion("relationship_type", value, ConditionMode.GREATER_THEN, "relationshipType", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andRelationshipTypeGreaterThanOrEqualTo(java.lang.String value) {
          addCriterion("relationship_type", value, ConditionMode.GREATER_EQUAL, "relationshipType", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andRelationshipTypeLessThan(java.lang.String value) {
          addCriterion("relationship_type", value, ConditionMode.LESS_THEN, "relationshipType", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andRelationshipTypeLessThanOrEqualTo(java.lang.String value) {
          addCriterion("relationship_type", value, ConditionMode.LESS_EQUAL, "relationshipType", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andRelationshipTypeBetween(java.lang.String value1, java.lang.String value2) {
    	  addCriterion("relationship_type", value1, value2, ConditionMode.BETWEEN, "relationshipType", "java.lang.String", "String");
    	  return this;
      }

      public DmsTableFieldSQL andRelationshipTypeNotBetween(java.lang.String value1, java.lang.String value2) {
          addCriterion("relationship_type", value1, value2, ConditionMode.NOT_BETWEEN, "relationshipType", "java.lang.String", "String");
          return this;
      }
        
      public DmsTableFieldSQL andRelationshipTypeIn(List<java.lang.String> values) {
          addCriterion("relationship_type", values, ConditionMode.IN, "relationshipType", "java.lang.String", "String");
          return this;
      }

      public DmsTableFieldSQL andRelationshipTypeNotIn(List<java.lang.String> values) {
          addCriterion("relationship_type", values, ConditionMode.NOT_IN, "relationshipType", "java.lang.String", "String");
          return this;
      }
}